package org.ws4d.java.security;

public class CDCTrustManagers implements TrustManagers {

	com.sun.net.ssl.TrustManager[]	trustManagers	= null;

	public CDCTrustManagers(com.sun.net.ssl.TrustManager[] trustManagers) {
		super();
		this.trustManagers = trustManagers;
	}

	public com.sun.net.ssl.TrustManager[] getTrustManagers() {
		return trustManagers;
	}

	public void setTrustManagers(com.sun.net.ssl.TrustManager[] trustManagers) {
		this.trustManagers = trustManagers;
	}

}
