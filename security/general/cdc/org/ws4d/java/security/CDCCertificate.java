package org.ws4d.java.security;

public class CDCCertificate implements Certificate {

	java.security.cert.Certificate	cert	= null;

	public CDCCertificate(java.security.cert.Certificate cert) {
		super();
		this.cert = cert;
	}

	public java.security.cert.Certificate getCertificate() {
		return cert;
	}

	public Object getCertificateAsObject() {
		return cert;
	}

	public void setCertificate(java.security.cert.Certificate cert) {
		this.cert = cert;
	}

}
