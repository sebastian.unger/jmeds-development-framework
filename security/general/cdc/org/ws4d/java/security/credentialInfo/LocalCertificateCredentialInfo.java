package org.ws4d.java.security.credentialInfo;

import java.security.cert.X509Certificate;
import java.util.Arrays;

import org.ws4d.java.security.CDCKeyManagers;
import org.ws4d.java.security.CDCKeyStore;
import org.ws4d.java.security.CDCPrivateKey;
import org.ws4d.java.security.CDCTrustManagers;
import org.ws4d.java.security.KeyManagers;
import org.ws4d.java.security.KeyStore;
import org.ws4d.java.security.PrivateKey;
import org.ws4d.java.security.TrustManagers;
import org.ws4d.java.security.keymanagement.ForcedAliasKeyManager;

import com.sun.net.ssl.X509KeyManager;

public class LocalCertificateCredentialInfo {

	private final String			aliasForPrivateKey;

	private final CDCKeyManagers	keyManagers;

	private final CDCTrustManagers	trustManagers;

	private final CDCPrivateKey		privateKey;

	private final CDCKeyStore		keyStore;

	private final CDCKeyStore		trustStore;

	private final int				hashCode;

	public LocalCertificateCredentialInfo(String alias, com.sun.net.ssl.KeyManager[] keyManagers, com.sun.net.ssl.TrustManager[] trustManagers, java.security.PrivateKey privateKey, java.security.KeyStore keyStore, java.security.KeyStore trustStore) {
		super();

		if (keyManagers != null) {
			com.sun.net.ssl.KeyManager[] kms = new com.sun.net.ssl.KeyManager[keyManagers.length];
			System.arraycopy(keyManagers, 0, kms, 0, keyManagers.length);
			if (alias != null) {
				for (int i = 0; i < kms.length; i++) {
					if (keyManagers[i] instanceof X509KeyManager) {
						kms[i] = new ForcedAliasKeyManager((X509KeyManager) keyManagers[i], alias);
					}
				}
			}
			this.keyManagers = new CDCKeyManagers(kms);
		} else {
			this.keyManagers = new CDCKeyManagers(new com.sun.net.ssl.KeyManager[0]);
		}

		this.aliasForPrivateKey = alias;
		this.trustManagers = new CDCTrustManagers(trustManagers);
		this.privateKey = new CDCPrivateKey(privateKey);
		this.keyStore = new CDCKeyStore(keyStore);
		this.trustStore = new CDCKeyStore(trustStore);

		hashCode = calculateHashCode();
	}

	public RemoteCertificateCredentialInfo createRemoteCertificateCredentialInfo() {
		com.sun.net.ssl.KeyManager[] kms = keyManagers.getKeyManagers();
		Object[] certs = new Object[kms.length];
		int j = 0;
		for (int i = 0; i < kms.length; i++) {
			if (kms[i] instanceof X509KeyManager) {
				X509KeyManager x509KeyManager = (X509KeyManager) kms[i];
				X509Certificate[] tmpCerts = x509KeyManager.getCertificateChain(aliasForPrivateKey);
				if (tmpCerts != null && tmpCerts.length > 0) {
					certs[j++] = tmpCerts[0];
				}
			}
		}

		if (++j < certs.length) {
			Object[] resultCerts = new Object[j];
			System.arraycopy(certs, 0, resultCerts, 0, j);
			return new RemoteCertificateCredentialInfo(resultCerts);
		}

		return new RemoteCertificateCredentialInfo(certs);
	}

	public String getAlias() {
		return aliasForPrivateKey;
	}

	public KeyManagers getKeyManagers() {
		return keyManagers;
	}

	public TrustManagers getTrustManagers() {
		return trustManagers;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public KeyStore getKeyStore() {
		return keyStore;
	}

	public KeyStore getTrustStore() {
		return trustStore;
	}

	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		LocalCertificateCredentialInfo other = (LocalCertificateCredentialInfo) obj;
		if (!Arrays.equals(keyManagers.getKeyManagers(), other.keyManagers.getKeyManagers())) return false;
		if (!Arrays.equals(trustManagers.getTrustManagers(), other.trustManagers.getTrustManagers())) return false;
		if (privateKey == null) {
			if (other.privateKey != null) return false;
		} else if (!privateKey.equals(other.privateKey)) return false;
		if (trustStore == null) {
			if (other.trustStore != null) return false;
		} else if (!trustStore.equals(other.trustStore)) return false;
		return true;
	}

	public int hashCode() {
		return hashCode;
	}

	private int calculateHashCode() {
		final int prime = 31;
		int hash = 1;
		hash = prime * hash + hashCode(keyManagers.getKeyManagers());
		hash = prime * hash + hashCode(trustManagers.getTrustManagers());
		hash = prime * hash + ((privateKey == null) ? 0 : privateKey.hashCode());
		return prime * hash + ((trustStore == null) ? 0 : trustStore.hashCode());
	}

	private static int hashCode(Object[] array) {
		int prime = 31;
		if (array == null) return 0;
		int result = 1;
		for (int index = 0; index < array.length; index++) {
			result = prime * result + (array[index] == null ? 0 : array[index].hashCode());
		}
		return result;
	}
}
