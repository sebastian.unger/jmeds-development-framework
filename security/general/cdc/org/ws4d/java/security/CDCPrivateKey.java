package org.ws4d.java.security;

public class CDCPrivateKey implements PrivateKey {

	java.security.PrivateKey	pk	= null;

	public CDCPrivateKey(java.security.PrivateKey pk) {
		super();
		this.pk = pk;
	}

	public java.security.PrivateKey getPrivateKey() {
		return pk;
	}

	public Object getPrivateKeyAsObject() {
		return pk;
	}

	public void setPrivateKey(java.security.PrivateKey pk) {
		this.pk = pk;
	}
}
