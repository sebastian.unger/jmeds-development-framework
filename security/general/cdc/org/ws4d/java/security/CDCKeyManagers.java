package org.ws4d.java.security;

public class CDCKeyManagers implements KeyManagers {

	com.sun.net.ssl.KeyManager[]	keyManagers	= null;

	public CDCKeyManagers(com.sun.net.ssl.KeyManager[] keyManagers) {
		super();
		this.keyManagers = keyManagers;

	}

	public com.sun.net.ssl.KeyManager[] getKeyManagers() {
		return keyManagers;
	}

	public void setKeyManagers(com.sun.net.ssl.KeyManager[] keyManagers) {
		this.keyManagers = keyManagers;
	}

}
