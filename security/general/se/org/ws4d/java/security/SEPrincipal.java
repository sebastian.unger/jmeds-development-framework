package org.ws4d.java.security;

import java.security.Principal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.ws4d.java.security.util.CertificateAndKeyUtil;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class SEPrincipal {

	private HashMap<String, String>	principalFields	= new HashMap<String, String>();

	private String					name			= null;

	public SEPrincipal(Principal principal) {
		this(principal.getName());
	}

	public SEPrincipal(String principalString) {
		principalFields = getPrincipalMapFromString(principalString);
	}

	public SEPrincipal(HashMap<String, String> principals) {
		this.principalFields = principals;
	}

	public String getCN() {
		String returnValue = principalFields.get(CertificateAndKeyUtil.CERT_INFO_CN);
		return returnValue != null ? returnValue : "";
	}

	public String getC() {
		String returnValue = principalFields.get(CertificateAndKeyUtil.CERT_INFO_C);
		return returnValue != null ? returnValue : "";
	}

	public String getO() {
		String returnValue = principalFields.get(CertificateAndKeyUtil.CERT_INFO_O);
		return returnValue != null ? returnValue : "";
	}

	public String getOU() {
		String returnValue = principalFields.get(CertificateAndKeyUtil.CERT_INFO_OU);
		return returnValue != null ? returnValue : "";
	}

	public String getL() {
		String returnValue = principalFields.get(CertificateAndKeyUtil.CERT_INFO_L);
		return returnValue != null ? returnValue : "";
	}

	public String getST() {
		String returnValue = principalFields.get(CertificateAndKeyUtil.CERT_INFO_ST);
		return returnValue != null ? returnValue : "";
	}

	public static HashMap<String, String> getPrincipalMapFromString(String principalString) {
		HashMap<String, String> principalMap = new HashMap<String, String>();

		String[] principalElements = principalString.split(",");
		for (int i = 0; i < principalElements.length; i++) {
			String element = principalElements[i];
			int equalSignPos = element.indexOf('=');
			principalMap.put(element.substring(0, equalSignPos + 1).trim(), (equalSignPos == element.length()) ? null : element.substring(equalSignPos).trim());
		}

		return principalMap;
	}

	public String getName() {
		if (name == null) {
			SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
			Iterator<Map.Entry<String, String>> ei = principalFields.entrySet().iterator();
			while (ei.hasNext()) {
				Map.Entry<String, String> entry = ei.next();
				String key = entry.getKey();

				if (key != null && !key.equals("")) {
					if (sb.length() != 0) {
						sb.append(", ");
					}
					sb.append(key);
					sb.append('=');
					sb.append(entry.getValue());
				}
			}
			name = sb.toString();
		}

		return name;
	}
}
