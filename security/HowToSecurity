How to secure a Device and/or a Service JMEDS 2.0
JMEDS security capabilities

The JMED Stack offers certain security capabilities as specified in the DPWS 1.1
and WSD 1.1 (since release 2.0beta4). The stack supports the following features:
 
    * Signing and validating of discovery messages using SHA1 digesting and
      SHA1withRSA signing
    * SSL/TLS based connection encryption for metadata-exchange; no control
      traffic is encrypted. 

To use these the following credentials are required: the device needs a private
key and a corresponding public key/certificate in its keystore. The client only
needs the certificate of the device to trust in its truststore.
Follow the steps to set up a secure device and its client:

   1. Open the terminal type the command 'keytool -genkey -alias [the
      urn:uuid:XXX/XAddress of the device] -sigalg SHA1withRSA -keyalg RSA' You
      may add the -keystore [filepath] argument to use a (new) keystore. It is
      created automatically if not existent. 

   2. Answer the questions on the screen. There must be no password for the key
      itself. 

   3. Export the generated public key to a [file].cer file: "keytool -export
      -alias [the urn:uuid:XXX/XAddress of the device] -file [file].cer" 

   4. Import the certificate into java's truststore: "keytool -import -alias
      [the urn:uuid:XXX/XAddress of the device] -file [file].cer -keystore
      [truststore filepath]" 

The only difference between generating these credentials for a device and for a
service is, that you use the XAddress (e.g. 139.2.52.168:9999/wordseventer) of
the service instead of the urn:uuid.

Last steps to get secure devices/services: Client: Use the following properties:

[Security]
PropertiesHandler=org.ws4d.java.configuration.SecurityProperties

TrustStoreFile=[path to the truststore]
TrustStorePswd=[password for the truststore]

Device/Service: Use the following properties (you may point the stack to the
.properties file with the path in the first program argument):

[Security]
PropertiesHandler=org.ws4d.java.configuration.SecurityProperties

KeyStoreFile=[path to the keystore]
KeyStorePswd=[password for the keystore]

Now you add a HTTPSBinding to the device. Call HTTPSBinding.setAlias(String)
with the devices urn:uuid as parameter, if you generated the certificate with 
the EPR as alias. This last call is not required when working with a service of
when using the XAddress as alias, because in these cases HTTPSBinding is able to
 extract these informations itself. Finally call setSecure{Device | Service}(). 