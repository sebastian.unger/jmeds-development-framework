package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.security.CDCKeyManagers;
import org.ws4d.java.security.CDCTrustManagers;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.credentialInfo.LocalCertificateCredentialInfo;
import org.ws4d.java.security.credentialInfo.RemoteCertificateCredentialInfo;
import org.ws4d.java.util.Log;

import com.sun.net.ssl.SSLContext;

public class CDCSecureSocket extends CDCSocket {

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param host host name.
	 * @param port port number.
	 * @param alias of the certificate to use.
	 * @throws IOException
	 */
	public CDCSecureSocket(IPAddress host, int port, LocalCertificateCredentialInfo credentialInfo) throws IOException {
		try {
			SSLContext context = SSLContext.getInstance(SecurePlatformSocketFactory.SSL_CONTEXT_METHOD_TLS);
			context.init(((CDCKeyManagers) credentialInfo.getKeyManagers()).getKeyManagers(), ((CDCTrustManagers) credentialInfo.getTrustManagers()).getTrustManagers(), null);

			socket = context.getSocketFactory().createSocket(host.getAddressWithoutNicId(), port);
		} catch (NoSuchAlgorithmException e) {
			if (Log.isError()) {
				Log.printStackTrace(e);
			}
		} catch (Exception e) {
			if (Log.isError()) {
				Log.printStackTrace(e);
			}
		}

		this.port = socket.getLocalPort();
	}

	public CDCSecureSocket(java.net.Socket socket, IPAddress address) {
		super(socket, address);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.tcp.SESocket#getRemoteCredentialInfo
	 * ()
	 */
	public CredentialInfo getRemoteCredentialInfo() {
		if (socket instanceof SSLSocket) {
			SSLSocket sslSocket = (SSLSocket) socket;
			try {
				return new CredentialInfo(new RemoteCertificateCredentialInfo(sslSocket.getSession().getPeerCertificateChain()));
			} catch (SSLPeerUnverifiedException e) {
				if (Log.isDebug()) {
					Log.debug("CDCSecureSocket: No peer certificate available: " + e.getMessage());
				}
				return null;
			}
		}
		return null;
	}
}
