package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.net.InetAddress;

import org.ws4d.java.communication.protocol.http.HTTPSBinding;
import org.ws4d.java.security.CDCKeyManagers;
import org.ws4d.java.security.CDCTrustManagers;
import org.ws4d.java.security.credentialInfo.LocalCertificateCredentialInfo;

import com.sun.net.ssl.SSLContext;

public class CDCSecureServerSocket extends CDCServerSocket {

	public CDCSecureServerSocket(HTTPSBinding sBinding) throws IOException {
		ipAddress = sBinding.getHostIPAddress();

		try {
			InetAddress adr = InetAddress.getByName(ipAddress.getAddress());
			int port = sBinding.getPort();

			// cldc fix -> xyz.class is not available under cldc
			LocalCertificateCredentialInfo lcci = new LocalCertificateCredentialInfo(null, null, null, null, null, null);
			Class _class = lcci.getClass();
			lcci = null;

			lcci = (LocalCertificateCredentialInfo) sBinding.getCredentialInfo().getCredential(_class);
			if (lcci == null) {
				throw new IllegalArgumentException("CredentialInfo does not contain LocalCertificateCredentialInfo.");
			}

			SSLContext context = SSLContext.getInstance(SecurePlatformSocketFactory.SSL_CONTEXT_METHOD_TLS);
			context.init(((CDCKeyManagers) lcci.getKeyManagers()).getKeyManagers(), ((CDCTrustManagers) lcci.getTrustManagers()).getTrustManagers(), null);

			serverSocket = context.getServerSocketFactory().createServerSocket(port, 0, adr);
			if (port == 0) {
				sBinding.setPort(serverSocket.getLocalPort());
			}
		} catch (IOException e) {
			throw new IOException(e.getMessage() + " For " + sBinding.getHostAddress() + " at port " + sBinding.getPort());
		} catch (Exception e) {
			throw new IOException("Exception: " + e.getMessage() + " For " + sBinding.getHostAddress() + " at port " + sBinding.getPort());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#accept()
	 */
	public Socket accept() throws IOException {
		return new CDCSecureSocket(serverSocket.accept(), getIPAddress());
	}

}
