package org.ws4d.java.compactsecurity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.ws4d.java.communication.ConnectionInfo;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.constants.WSSecurityConstants;
import org.ws4d.java.constants.general.DPWSConstantsHelper;
import org.ws4d.java.incubation.arne.compactsecurity.KeyStoreWrapper;
import org.ws4d.java.io.xml.Ws4dXmlSerializer;
import org.ws4d.java.security.XMLSignatureManager;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class CompactSecurity {

	public static void buildSecurityHeader(ConnectionInfo ci, Ws4dXmlSerializer serializer, DPWSConstantsHelper helper, OutputStream out){



		if (serializer.getOutput() != null) {


			//			---------- new Code to substitute rbaos ------------------------------

			ByteArrayOutputStream os = (ByteArrayOutputStream) serializer.getOutput();
			byte[] sourceBytes = os.toByteArray();


			//			System.out.println("sourceBytes " + new String(sourceBytes));

			//			---------- new Code to substitute rbaos ------------------------------


			String signature = null;
			if (XMLSignatureManager.useShortSignedInfo) {
				signature = getSignature(new byte[][] { serializer.getSourceBytesAsOnePart(sourceBytes) }, null);
			} else {
				signature = getSignature(serializer.getSourceBytesParts(sourceBytes), serializer.getSignatureIds());
			}

			try {
				String ref = "";
				boolean first = true;
				for (Iterator it = serializer.getSignatureIds().iterator(); it.hasNext();) {
					String tmpId = (String) it.next();
					if (first) {
						ref = tmpId;
						first = false;
					} else {
						ref += " " + tmpId;
					}
				}


				try {
					serializer.endDocument();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (org.ws4d.java.xmlpull.v1.IllegalStateException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


				//				build the <Security>-field

				SimpleStringBuilder securityTag = Toolkit.getInstance().createSimpleStringBuilder();
				//				securityTag.append("<").append(WSDConstants.WSD_NAMESPACE_PREFIX).append(":").append(WSSecurityConstants.COMPACT_SECURITY_NAME).append(">");
				securityTag.append("<").append("wsa").append(":").append(WSSecurityConstants.COMPACT_SECURITY_NAME).append(">");


				//				build the <Enc>-field

				//					securityTag.append("<").append(WSDConstants.WSD_NAMESPACE_PREFIX).append(":" + "Enc").append(" ");
				securityTag.append("<").append("wsa").append(":" + "Enc").append(" ");

				securityTag.append(WSSecurityConstants.COMPACT_ATTR_KEYID_NAME + "=\"").append("123345678").append("\" ");
				securityTag.append(WSSecurityConstants.COMPACT_ATTR_REFS_NAME).append("=\"").append("\" ");
				securityTag.append(WSSecurityConstants.COMPACT_ATTR_SCHEME_NAME).append("=\"").append(helper.getWSDNamespace()).append("/aes").append("\"").append(">");
				//					securityTag.append("<").append("/").append(WSDConstants.WSD_NAMESPACE_PREFIX).append(":" + "Enc").append(">");
				securityTag.append("<").append("/").append("wsa").append(":" + "Enc").append(">");


				//				build the <Sig>-field

				//					securityTag.append("<").append(WSDConstants.WSD_NAMESPACE_PREFIX).append(":" + WSSecurityConstants.COMPACT_SIG_NAME).append(" ");
				securityTag.append("<").append("wsa").append(":" + WSSecurityConstants.COMPACT_SIG_NAME).append(" ");

				securityTag.append(WSSecurityConstants.COMPACT_ATTR_SCHEME_NAME).append("=\"").append(helper.getWSDNamespace()).append(XMLSignatureManager.NAMESPACE_RSA_EXTENSION).append("\" ");
				securityTag.append(WSSecurityConstants.COMPACT_ATTR_REFS_NAME).append("=\"").append(ref).append("\" ");
				securityTag.append(WSSecurityConstants.COMPACT_ATTR_SIG_NAME).append("=\"" + signature).append("\">");
				//					securityTag.append("</").append(WSDConstants.WSD_NAMESPACE_PREFIX + ":").append(WSSecurityConstants.COMPACT_ATTR_SIG_NAME).append(">");
				securityTag.append("</").append("wsa" + ":").append(WSSecurityConstants.COMPACT_ATTR_SIG_NAME).append(">");


				//				securityTag.append("</").append(WSDConstants.WSD_NAMESPACE_PREFIX).append(":").append(WSSecurityConstants.COMPACT_SECURITY_NAME).append(">");
				securityTag.append("</").append("wsa").append(":").append(WSSecurityConstants.COMPACT_SECURITY_NAME).append(">");


				// 				Change source bytes -> add security block into header

				byte[] securityTagBytes = securityTag.toString().getBytes();
				int preSecLength = serializer.getHeaderEndPosition() + 1;

				//				System.out.println("securityTagBytes = " + new String(securityTagBytes));

				//				paste the security-field into the header
				//				to achieve this we have to do some byte-array manipulation

				byte[] copysourceBytes = sourceBytes;
				sourceBytes = new byte[copysourceBytes.length + securityTagBytes.length];

				System.arraycopy(copysourceBytes, 0, sourceBytes, 0, copysourceBytes.length);
				System.arraycopy(sourceBytes, preSecLength, sourceBytes, preSecLength + securityTagBytes.length, copysourceBytes.length - (preSecLength));
				System.arraycopy(securityTagBytes, 0, sourceBytes, preSecLength, securityTagBytes.length);


				//				sourceBytes contains the whole SOAP-message
				//				System.out.println("sourceBytes = " + new String(sourceBytes));


				//				give the serializer the new SOAP-message
				//				serializer.getOutput().flush();

				//				serializer.resoreOriginalOutputStream();

				for(int i = 0; i < sourceBytes.length; i++)
					out.write(sourceBytes[i]);
				//					serializer.getOutput().write(sourceBytes[i]);

				//				serializer.getOutput().flush();
				out.flush();



			} catch (IllegalArgumentException e) {
				Log.printStackTrace(e);
			} catch (IllegalStateException e) {
				Log.printStackTrace(e);
			} catch (Exception e) {
				Log.printStackTrace(e);
			}
		}

	}

	static String getSignature(byte[][] byteParts, ArrayList refs) {


		//		System.err.println("Message to be signed: " + new String(byteParts[0]));

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");

			byte[] hash = md.digest(byteParts[0]);

			SecretKeySpec newkey = new SecretKeySpec(KeyStoreWrapper.getWrapper().getkeybyID(null), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, newkey,  new IvParameterSpec(new byte[16]));

			byte[] mac = cipher.doFinal(hash);

			return Base64Util.encodeBytes(mac);

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
