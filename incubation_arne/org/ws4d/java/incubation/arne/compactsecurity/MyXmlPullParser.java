package org.ws4d.java.incubation.arne.compactsecurity;


import java.io.ByteArrayInputStream;

import org.ws4d.java.xmlpull.v1.XmlPullParser;
import org.ws4d.java.xmlpull.v1.XmlPullParserException;
/*
 * -*- c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * //------100-columns-wide------>|
 */
// for license please see accompanying LICENSE.txt file (available also at
// http://www.xmlpull.org/)
import org.ws4d.java.xmlpull.v1.XmlPullParserFactory;

/**
 * XML Pull Parser is an interface that defines parsing functionality provided
 * in <a href="http://www.xmlpull.org/">XMLPULL V1 API</a> (visit this website
 * to learn more about API and its implementations).
 * <p>
 * There are following different kinds of parser depending on which features are set:
 * <ul>
 * <li><b>non-validating</b> parser as defined in XML 1.0 spec when FEATURE_PROCESS_DOCDECL is set to true
 * <li><b>validating parser</b> as defined in XML 1.0 spec when FEATURE_VALIDATION is true (and that implies that FEATURE_PROCESS_DOCDECL is true)
 * <li>when FEATURE_PROCESS_DOCDECL is false (this is default and if different value is required necessary must be changed before parsing is started) then parser behaves like XML 1.0 compliant non-validating parser under condition that <em>no DOCDECL is present</em> in XML documents (internal entities can still be defined with defineEntityReplacementText()). This mode of operation is intended <b>for operation in constrained environments</b> such as J2ME.
 * </ul>
 * <p>
 * There are two key methods: next() and nextToken(). While next() provides access to high level parsing events, nextToken() allows access to lower level tokens.
 * <p>
 * The current event state of the parser can be determined by calling the <a href="#getEventType()">getEventType()</a> method. Initially, the parser is in the <a href="#START_DOCUMENT">START_DOCUMENT</a> state.
 * <p>
 * The method <a href="#next()">next()</a> advances the parser to the next event. The int value returned from next determines the current parser state and is identical to the value returned from following calls to getEventType ().
 * <p>
 * Th following event types are seen by next()
 * <dl>
 * <dt><a href="#START_TAG">START_TAG</a>
 * <dd>An XML start tag was read.
 * <dt><a href="#TEXT">TEXT</a>
 * <dd>Text content was read; the text content can be retreived using the getText() method. (when in validating mode next() will not report ignorable whitespaces, use nextToken() instead)
 * <dt><a href="#END_TAG">END_TAG</a>
 * <dd>An end tag was read
 * <dt><a href="#END_DOCUMENT">END_DOCUMENT</a>
 * <dd>No more events are available
 * </dl>
 * <p>
 * after first next() or nextToken() (or any other next*() method) has been called, the user application can obtain XML version, standalone and encoding from XML declaration in the following ways:
 * <ul>
 * <li><b>version</b>: getProperty(&quot;<a href="http://xmlpull.org/v1/doc/properties.html#xmldecl-version" >http://xmlpull.org/v1/doc/properties.html#xmldecl-version</a>&quot;) returns String ("1.0") or null if XMLDecl has not been read or if property is not supported
 * <li><b>standalone</b>: getProperty(&quot;<a href="http://xmlpull.org/v1/doc/features.html#xmldecl-standalone" >http://xmlpull.org/v1/doc/features.html#xmldecl-standalone</a>&quot;) returns Boolean: null if there was no standalone declaration or if property is not supported otherwise returns Boolean(true) if standalone="yes" and Boolean(false) when standalone="no"
 * <li><b>encoding</b>: obtained from getInputEncoding() null if stream had unknown encoding (not set in setInputStream) and it was not declared in XMLDecl
 * </ul>
 * A minimal example for using this API may look as follows:
 * 
 * <pre>
 * import java.io.IOException;
 * import java.io.StringReader;
 * 
 * import org.xmlpull.v1.XmlPullParser;
 * import org.xmlpull.v1.<a href="XmlPullParserException.html">XmlPullParserException.html</a>;
 * import org.xmlpull.v1.<a href="XmlPullParserFactory.html">XmlPullParserFactory</a>;
 * 
 * public class SimpleXmlPullApp
 * {
 * 
 *     public static void main (String args[])
 *         throws XmlPullParserException, IOException
 *     {
 *         XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
 *         factory.setNamespaceAware(true);
 *         XmlPullParser xpp = factory.newPullParser();
 * 
 *         xpp.<a href="#setInput">setInput</a>( new StringReader ( "&lt;foo>Hello World!&lt;/foo>" ) );
 *         int eventType = xpp.getEventType();
 *         while (eventType != XmlPullParser.END_DOCUMENT) {
 *          if(eventType == XmlPullParser.START_DOCUMENT) {
 *              System.out.println("Start document");
 *          } else if(eventType == XmlPullParser.END_DOCUMENT) {
 *              System.out.println("End document");
 *          } else if(eventType == XmlPullParser.START_TAG) {
 *              System.out.println("Start tag "+xpp.<a href="#getName()">getName()</a>);
 *          } else if(eventType == XmlPullParser.END_TAG) {
 *              System.out.println("End tag "+xpp.getName());
 *          } else if(eventType == XmlPullParser.TEXT) {
 *              System.out.println("Text "+xpp.<a href="#getText()">getText()</a>);
 *          }
 *          eventType = xpp.next();
 *         }
 *     }
 * }
 * </pre>
 * <p>
 * The above example will generate the following output:
 * 
 * <pre>
 * Start document
 * Start tag foo
 * Text Hello World!
 * End tag foo
 * </pre>
 * <p>
 * For more details on API usage, please refer to the quick Introduction available at <a href="http://www.xmlpull.org">http://www.xmlpull.org</a>
 * 
 * @see XmlPullParserFactory
 * @see #defineEntityReplacementText
 * @see #getName
 * @see #getNamespace
 * @see #getText
 * @see #next
 * @see #nextToken
 * @see #setInput
 * @see #FEATURE_PROCESS_DOCDECL
 * @see #FEATURE_VALIDATION
 * @see #START_DOCUMENT
 * @see #START_TAG
 * @see #TEXT
 * @see #END_TAG
 * @see #END_DOCUMENT
 * @author <a
 *         href="http://www-ai.cs.uni-dortmund.de/PERSONAL/haustein.html">Stefan
 *         Haustein</a>
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander
 *         Slominski</a>
 */
public interface MyXmlPullParser extends XmlPullParser{


	void mysetInput(ByteArrayInputStream byteArrayInputStream,
			String inputEncoding) throws XmlPullParserException;

	// /**
	// * Skip sub tree on which the parser is currently positioned on.
	// * <br>NOTE: parser must be on START_TAG and when function returns
	// * parser will be positioned on matching END_TAG
	// *
	// * This method is typically optimized by parser but the its logic should
	// follow this:
	// * <code>
	// * require(XmlPullParser.START_TAG, null, null);
	// * int level = 1;
	// * while(level > 0) {
	// * int eventType = next();
	// * if(eventType == XmlPullParser.END_TAG) {
	// * --level;
	// * } else if(eventType == XmlPullParser.START_TAG) {
	// * ++level;
	// * }
	// * }
	// * </code>
	// */
	// public void skipSubTree() throws XmlPullParserException, IOException;

}
