package org.ws4d.java.incubation.arne.compactsecurity;

public class SecurityControl {
	
	//this class is used for controlling the CompactSecurity-standard
	//set both values to false by default
	
	public static boolean useSignature 			=	false;
	public static boolean useBodyencryption		=	false;
	
//	public void setControlvariables(boolean useSignaturesExt, boolean useBodyencryptionExt){
//		this.useSignature = useSignaturesExt;
//		this.useBodyencryption = useBodyencryptionExt;
//	}
	
}
