package org.ws4d.java.incubation.arne.compactsecurity;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.io.xml.ElementHandler;
import org.ws4d.java.io.xml.ElementHandlerRegistry;
import org.ws4d.java.io.xml.Ws4dXmlSerializer;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.StringUtil;
import org.ws4d.java.util.WS4DIllegalStateException;

public class MyWs4dXmlSerializer extends MyMXSerializer implements Ws4dXmlSerializer {

	private ArrayList					signaturePositions				= null;

	private ArrayList					ids								= null;

	private int							headerEndPos					= -1;

	private int							currentPos						= 0;

	private OutputStream				stream							= null;

	private WrappedOutputStreamWriter	wosw							= null;

	private boolean						signMessage						= false;

	private boolean						corrected						= false;

	private byte						lessThan						= (byte) '<';

	private int							localGenericQNamePrefixCounter	= 0;


	public void resoreOriginalOutputStream(){

		this.buffer = this.out;
		this.out = this.originalOutputstream;

	}

	public void safeOriginalOutputStream(){
		this.originalOutputstream = this.out;
		this.out = new StringWriter();
	}


	@Override
	public void setStartPosition(String id) {
		signaturePositions.add(new int[] { wosw.currentIndex, -1 });
		ids.add(currentPos, id);
	}

	@Override
	public void setStopPosition() {
		((int[]) signaturePositions.get(currentPos++))[1] = wosw.currentIndex - 1;
	}

	@Override
	public void setHeaderEndPosition() {
		headerEndPos = wosw.currentIndex - 1;
	}

	@Override
	public int getHeaderEndPosition() {
		return headerEndPos;
	}

	@Override
	public byte[] getSourceBytesAsOnePart(byte[] sourceBytes) {
		correctStartPositions(sourceBytes);

		int size = 0;
		for (int i = 0; i < signaturePositions.size(); i++) {
			int[] pos = (int[]) signaturePositions.get(i);
			size += (pos[1] - pos[0]) + 1;
		}

		byte[] sourceBytesOnePart = new byte[size];

		int currentPos = 0;
		for (int j = 0; j < signaturePositions.size(); j++) {
			int[] pos = (int[]) signaturePositions.get(j);
			int len = (pos[1] - pos[0]) + 1;
			System.arraycopy(sourceBytes, pos[0], sourceBytesOnePart, currentPos, len);
			currentPos += len;
		}

		return sourceBytesOnePart;
	}

	@Override
	public byte[][] getSourceBytesParts(byte[] sourceBytes) {

		correctStartPositions(sourceBytes);

		byte[][] sourceBytesParts = new byte[signaturePositions.size()][];
		for (int j = 0; j < signaturePositions.size(); j++) {
			int[] pos = (int[]) signaturePositions.get(j);
			sourceBytesParts[j] = new byte[(pos[1] - pos[0]) + 1];
			System.arraycopy(sourceBytes, pos[0], sourceBytesParts[j], 0, sourceBytesParts[j].length);
		}

		return sourceBytesParts;
	}

	private void correctStartPositions(byte[] sourceBytes) {
		if (!corrected) {
			for (int i = 0; i < signaturePositions.size(); i++) {
				int[] positions = (int[]) signaturePositions.get(i);
				while (sourceBytes[positions[0]] != lessThan) {
					positions[0]++;
					if (positions[0] >= positions[1]) {
						throw new IllegalArgumentException("DefaultWs4dXmlSerializer: No start tag found between in part " + i + ".");
					}
				}
			}
			corrected = true;
		}
	}

	@Override
	public ArrayList getSignatureIds() {
		return ids;
	}

	@Override
	public boolean isSignMessage() {
		return signMessage;
	}

	@Override
	public void resetSignaturePositions() {
		signaturePositions = null;
		stream = null;
		wosw = null;
		currentPos = 0;
		signMessage = false;
		ids = null;
		corrected = false;
	}

	@Override
	public void setOutput(OutputStream os, String encoding, boolean signMessage) throws IOException {
		if (this.signMessage) {
			throw new WS4DIllegalStateException("This DefaultWs4dXmlSerializer object was used in signMessage mode before. A call to resetSignaturePositions() is required before reuse.");
		}

		if (signMessage) {
			wosw = new WrappedOutputStreamWriter(os, encoding);
			super.setOutput(wosw);
			this.stream = os;
			this.signaturePositions = new ArrayList(10);
			this.ids = new ArrayList(10);
			this.signMessage = true;
		} else {
			super.setOutput(os, encoding);
		}
	}

	@Override
	public OutputStream getOutput() {
		return stream;
	}

	public MyWs4dXmlSerializer() {
		super();
		//		System.out.println("Redirection of \"DefaultWs4dXmlSerializer\" to \"MyWs4dXmlSerializer\" successful");
	}

	/**
	 * Write a block of XML directly to the underlying stream, especially
	 * without escaping any special chars.
	 * 
	 * @param text the XML block to write
	 * @throws IOException
	 */
	@Override
	public void plainText(String text) throws IOException {
		getWriter().write(text);
	}

	/**
	 * @param qname the fully qualified name of the elements to expect within <code>list</code>
	 * @param elements the list of elements to serialize; all are expected to be
	 *            of the same type; note that this list can be empty or have
	 *            just one element
	 * @throws IOException
	 */
	@Override
	public void unknownElements(QName qname, List elements) throws IOException {
		ElementHandler handler = ElementHandlerRegistry.getRegistry().getElementHandler(qname);
		if (handler != null) {
			for (Iterator at = elements.iterator(); at.hasNext();) {
				handler.serializeElement(this, qname, at.next());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.io.xml.XmlSerializer#getType()
	 */
	@Override
	public int getType() {
		return Ws4dXmlSerializer.TYPE_XML_SERIALIZER;
	}

	public class WrappedOutputStreamWriter extends OutputStreamWriter {

		int	currentIndex	= 0;

		public WrappedOutputStreamWriter(OutputStream out, String encoding) throws UnsupportedEncodingException {
			super(out, encoding);

		}

		@Override
		public void write(int c) throws IOException {
			super.write(c);
			currentIndex++;
		}

		@Override
		public void write(char[] cbuf, int off, int len) throws IOException {
			super.write(cbuf, off, len);
			currentIndex += len;
		}

		@Override
		public void write(String str, int off, int len) throws IOException {
			super.write(str, off, len);
			currentIndex += len;
		}
	}

	@Override
	public void setPrefix(String prefix, String namespace) throws IOException {
		if (StringUtil.isEmpty(prefix)) {
			prefix = getNewLocalGenericPrefix();
		}
		super.setPrefix(prefix, namespace);
	}

	@Override
	public void resetPrefixCounter() {
		localGenericQNamePrefixCounter = 0;
	}

	private String getNewLocalGenericPrefix() {
		localGenericQNamePrefixCounter++;
		return XMLConstants.XMLNS_DEFAULT_PREFIX + localGenericQNamePrefixCounter;
	}
}

