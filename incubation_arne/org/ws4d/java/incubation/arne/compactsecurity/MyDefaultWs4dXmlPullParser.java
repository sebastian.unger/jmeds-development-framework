package org.ws4d.java.incubation.arne.compactsecurity;

import java.io.IOException;

import org.ws4d.java.io.xml.Ws4dXmlPullParser;
import org.ws4d.java.io.xml.Ws4dXmlPullParserListener;
import org.ws4d.java.xmlpull.v1.XmlPullParserException;

public class MyDefaultWs4dXmlPullParser extends MyMXParser implements Ws4dXmlPullParser{

	private Ws4dXmlPullParserListener	listener;

	@Override
	protected void reset() {
		listener = null;
		super.reset();
	}

	@Override
	public void setListener(Ws4dXmlPullParserListener listener) {
		if (listener == null) {
			return;
		}
		if (this.listener != null) {
			throw new RuntimeException("listener already set");
		}
		this.listener = listener;
	}

	@Override
	public void removeListener(Ws4dXmlPullParserListener listener) {
		if (listener == this.listener) {
			this.listener = listener;
		}
	}

	@Override
	public void removeListener() {
		listener = null;
	}

	@Override
	protected int nextImpl() throws XmlPullParserException, IOException {
		int result = super.nextImpl();
		if (listener != null) {
			listener.notify(posStart, posEnd - 1);
		}
		return result;
	}
}
