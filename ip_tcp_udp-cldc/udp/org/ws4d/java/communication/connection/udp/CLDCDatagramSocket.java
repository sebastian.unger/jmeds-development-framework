/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.udp;

import java.io.IOException;
import java.util.Random;

import javax.microedition.io.Connector;
import javax.microedition.io.DatagramConnection;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.filter.AddressFilter;
import org.ws4d.java.configuration.IPProperties;
import org.ws4d.java.constants.Specialchars;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.ObjectPool;
import org.ws4d.java.util.ObjectPool.InstanceCreator;

/**
 * DatagramSocket wrapper for CLDC.
 */
public class CLDCDatagramSocket implements DatagramSocket {

	public static final String	CLDC_DATAGRAM_PREFIX		= "datagram://";

	public static final String	CLDC_IP_DETECTION_ADDRESS	= "239.255.255.250:3702";

	public static final int		UDP_PORT_RETRIES			= 10;

	private ObjectPool			datagrams;

	private DatagramConnection	connection					= null;

	private NetworkInterface	iface						= null;

	private IPAddress			host						= null;

	private int					port						= -1;
	
	private boolean				isMulticast		            = false;

	private AddressFilter		filter						= null;

	public CLDCDatagramSocket(IPAddress host, int port, NetworkInterface iface, AddressFilter filter) throws IOException {
		String adr = null;

		if (iface == null || host == null) {
			adr = CLDC_DATAGRAM_PREFIX;
		} else {
			adr = CLDC_DATAGRAM_PREFIX + host.getAddress();
			if (host.isMulticastAddress()) {
				isMulticast = true;
			}
		}
		int portRetries = 0;
		while (true) {
			try {
				if (port == 0) {
					port = getRandomPortNumber();
				}
				if (port != -1) {
					adr += ":" + port;
				}
				connection = (DatagramConnection) Connector.open(adr);
				datagrams = new ObjectPool(new InstanceCreator() {

					public Object createInstance() {
						try {
							return connection.newDatagram(IPProperties.getInstance().getMaxDatagramSize());
						} catch (IOException e) {
							return null;
						}
					}

				}, 5);
				break;
			} catch (IOException e) {
				if (portRetries++ == UDP_PORT_RETRIES) {
					throw e;
				}
			}
		}
		this.iface = iface;
		this.host = host;
		this.port = port;
		this.filter = filter;

		if (iface != null) {
			DatagramSocketFactory.registerMulticastSource(this.iface, this.host, this.port);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.udp.DatagramSocket#close()
	 */
	public void close() throws IOException {
		if (iface != null) {
			DatagramSocketFactory.unregisterMulticastSource(iface, host, port);
		}
		
		connection.close();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.udp.DatagramSocket#receive()
	 */
	public Datagram receive() throws IOException {
		javax.microedition.io.Datagram dgram = (javax.microedition.io.Datagram) datagrams.acquire();
		connection.receive(dgram);
		Datagram result = new Datagram(this, dgram.getData(), dgram.getLength());
		String remoteAddressStr = dgram.getAddress();
		int port =0;
		if (remoteAddressStr.startsWith(CLDC_DATAGRAM_PREFIX)) {
			remoteAddressStr = remoteAddressStr.substring(CLDC_DATAGRAM_PREFIX.length());
		}
		int k = remoteAddressStr.indexOf(Specialchars.COL);
		if (k > -1) {
			port = Integer.parseInt(remoteAddressStr.substring(k + 1));
			remoteAddressStr = remoteAddressStr.substring(0, k);
		}

		Long[] keyForIPAddress = IPAddress.getKeyForIPAddress(remoteAddressStr);
		
		if (isMulticast && DatagramSocketFactory.hasMulticastSource(iface, keyForIPAddress, port)) {
			 if (Log.isDebug()) {
				 Log.debug("Deny own message from " + remoteAddressStr + ":" + port, Log.DEBUG_LAYER_COMMUNICATION);
			 }
			 return null;
		}
		
		if (filter != null) {
			if (!filter.isAllowedByFilter(keyForIPAddress)) {
//				if (Log.isDebug()) {
//					Log.debug("Deny " + remoteAddressStr + " by filter", Log.DEBUG_LAYER_COMMUNICATION);
//				}
				return null;
			}
		}

		result.setSocketAddress(this.host);
		result.setSocketPort(this.port);
		result.setAddress(IPAddress.createRemoteIPAddress(remoteAddressStr, keyForIPAddress));
		result.setPort(port);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#send(org.ws4d
	 * .java.communication.connection.udp.Datagram)
	 */
	public void send(Datagram datagram) throws IOException {
		String dagramAdr = CLDC_DATAGRAM_PREFIX + datagram.getIPAddress() + ":" + datagram.getPort();
		javax.microedition.io.Datagram dgram = connection.newDatagram(datagram.getData(), datagram.getLength(), dagramAdr);

		connection.send(dgram);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#release(org
	 * .ws4d.java.communication.connection.udp.Datagram)
	 */
	public void release(Datagram datagram) {
		// return to datagram pool
		datagrams.release(datagram);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if (host != null) {
			if (port != -1) {
				return "host: " + host + ", port: " + port;
			}
			return "host: " + host;
		} else {
			return "host: unknown (cldc)";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#getSocketAddress
	 * ()
	 */
	public IPAddress getSocketAddress() {
		return host;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#getSocketPort()
	 */
	public int getSocketPort() {
		return port;
	}

	public NetworkInterface getIface() {
		return iface;
	}

	/**
	 * Generates a random port number between 1025 and 65535.
	 * 
	 * @return a random number to try as port number for the server.
	 */
	public static int getRandomPortNumber() {
		Random r = new Random(System.currentTimeMillis());

		return (r.nextInt() & 0xefff) + 1025;
	}

}
