/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.udp;

import java.io.IOException;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.filter.AddressFilter;
import org.ws4d.java.configuration.CommunicationProperties;
import org.ws4d.java.constants.FrameworkConstants;
import org.ws4d.java.util.Log;

/**
 * Creates server and client sockets.
 */
public class PlatformDatagramSocketFactory extends DatagramSocketFactory {

	/**
	 * Creates a <code>DatagramSocket</code>.<br />
	 * 
	 * @param host the host address.
	 * @param port port
	 * @return the ServerSocket.
	 * @throws IOException
	 */
	public DatagramSocket createDatagramServerSocket(IPAddress localAddress, int localPort, NetworkInterface iface, AddressFilter filter) throws IOException {
		if (CommunicationProperties.useNativeRouter) {
			return createTcpToNativeUdpBridge(localAddress, localPort, iface, filter, false);
		} else
			return new CLDCDatagramSocket(localAddress, localPort, iface, filter);
	}

	/**
	 * Creates a <code>DatagramSocket</code>.<br />
	 * 
	 * @param host the host address.
	 * @param port port
	 * @return the ServerSocket.
	 * @throws IOException
	 */
	public DatagramSocket createDatagramServerSocket(int localPort, AddressFilter filter) throws IOException {
		return new CLDCDatagramSocket(null, localPort, null, filter);
	}

	private DatagramSocket createTcpToNativeUdpBridge(IPAddress host, int port, NetworkInterface iface, AddressFilter filter, boolean listen) throws IOException {
		CLDCNativeMulticastRouter ret = null;
		try {
			ret = new CLDCNativeMulticastRouter(host, port, IPAddress.createRemoteIPAddress(CommunicationProperties.routerIp), CommunicationProperties.routerPort, iface, filter, listen);
		} catch (IOException e) {
			Log.error("Check TcpToNativeUdpBridge!");
			throw e;
		}
		return ret;
	}

	public String getJavaVersion() {
		return FrameworkConstants.JAVA_VERSION_CLDC;
	}
}
