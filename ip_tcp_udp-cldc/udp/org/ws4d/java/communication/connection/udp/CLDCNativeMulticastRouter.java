/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.udp;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.connection.tcp.Socket;
import org.ws4d.java.communication.connection.tcp.SocketFactory;
import org.ws4d.java.communication.filter.AddressFilter;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.util.Toolkit;

public class CLDCNativeMulticastRouter implements DatagramSocket {

	public static final String	CLDC_DATAGRAM_PREFIX	= "datagram://";

	private InputStream			socketInputStream;

	private Socket				routerSocket;

	private DatagramSocket		dsocket;

	private UdpOverTcpListener	udpOTcp;

	IPAddress					host;

	int							port;

	public static final int		IPv4					= 15;

	private int					ip_policy;

	public CLDCNativeMulticastRouter(IPAddress localhost, int localport, IPAddress host, int port, NetworkInterface iface, AddressFilter filter, boolean listen) throws IOException {
		this.host = localhost;
		this.port = localport;

		ip_policy = IPv4;

		routerSocket = SocketFactory.getInstance().createSocket(host, port, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		socketInputStream = Toolkit.getInstance().buffer(routerSocket.getInputStream());
		dsocket = new CLDCDatagramSocket(localhost, localport, iface, filter);
		udpOTcp = new UdpOverTcpListener(socketInputStream, ip_policy);
	}

	public void close() throws IOException {
		routerSocket.close();
	}

	public Datagram receive() throws IOException {
		Datagram result = udpOTcp.receiveDatagram();
		IPAddress address = result.getIPAddress();

		String adr = address.getAddress();

		// required?
		if (adr.startsWith(CLDC_DATAGRAM_PREFIX)) {
			adr = adr.substring(CLDC_DATAGRAM_PREFIX.length());
		}

		result.setAddress(address);

		return result;
	}

	public void send(Datagram datagram) throws IOException {
		dsocket.send(datagram);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#release(org
	 * .ws4d.java.communication.connection.udp.Datagram)
	 */
	public void release(Datagram datagram) {
		// TODO Auto-generated method stub
	}

	int getIpPolicy() {
		return ip_policy;
	}

	void setIpPolicy(int ipp) {
		ip_policy = ipp;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#getSocketAddress
	 * ()
	 */
	public IPAddress getSocketAddress() {
		return host;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#getSocketPort()
	 */
	public int getSocketPort() {
		return port;
	}

	public NetworkInterface getIface() {
		// TODO Auto-generated method stub
		return null;
	}
}
