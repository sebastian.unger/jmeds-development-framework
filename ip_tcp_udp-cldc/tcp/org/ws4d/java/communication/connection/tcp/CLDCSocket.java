/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/

package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.security.CredentialInfo;

/**
 * This class implements a connection for the CLDC Platform.
 */
public final class CLDCSocket implements Socket {

	public static final String	CLDC_SOCKET_PREFIX	= "socket://";

	private String				remoteAddress		= null;

	private int					remotePort			= -1;

	private IPAddress			localAddress		= null;

	private int					localPort			= -1;

	private InputStream			in					= null;

	private OutputStream		out					= null;

	/** The underlying CLDC Connection object. */
	private StreamConnection	streamConnection;

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param address the IP address for this connection.
	 * @param port the port for this connection.
	 * @throws IOException
	 */
	public CLDCSocket(IPAddress address, int port) throws IOException {
		String adr = address.getAddress();
		if (adr.length() > 0 && adr.charAt(0) == '[') {
			adr = (adr.indexOf("%") != -1) ? adr.substring(0, adr.lastIndexOf('%')) + ']' : adr;
		} else {
			adr = (adr.indexOf("%") != -1) ? adr.substring(0, adr.lastIndexOf('%')) : adr;
		}

		String con = CLDC_SOCKET_PREFIX + adr + ":" + port;
		streamConnection = (StreamConnection) Connector.open(con);

		this.remoteAddress = address.getAddress();
		this.remotePort = port;
	}

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param streamConnection the CLDC connection.
	 * @param address the IP address for this connection.
	 * @param port the port for this connection.
	 * @throws IOException
	 */
	public CLDCSocket(StreamConnection stream, IPAddress localAddress, int localPort) throws IOException {
		streamConnection = stream;
		this.localAddress = localAddress;
		this.localPort = localPort;
	}

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param streamConnection the CLDC connection.
	 * @param host the host to connect to.
	 * @throws IOException
	 */
	public CLDCSocket(StreamConnection stream) throws IOException {
		streamConnection = stream;
	}

	/**
	 * Opens an <code>InputStream</code> on the StreamConnection.
	 * 
	 * @return an InputStream.
	 */
	public InputStream getInputStream() throws IOException {
		if (streamConnection == null) {
			throw new IOException("No open connection. Cannot open input stream");
		}
		if (in == null) {
			final InputStream sIn = streamConnection.openInputStream();
			in = new InputStream() {

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#read()
				 */
				public int read() throws IOException {
					return sIn.read();
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#read(byte[], int, int)
				 */
				public int read(byte[] b, int off, int len) throws IOException {
					return sIn.read(b, off, len);
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#available()
				 */
				public int available() throws IOException {
					return sIn.available();
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#mark(int)
				 */
				public void mark(int readlimit) {
					sIn.mark(readlimit);
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#markSupported()
				 */
				public boolean markSupported() {
					return sIn.markSupported();
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#reset()
				 */
				public void reset() throws IOException {
					sIn.reset();
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#skip(long)
				 */
				public long skip(long n) throws IOException {
					return sIn.skip(n);
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.InputStream#close()
				 */
				public void close() throws IOException {
					sIn.close();
					in = null;
					if (out != null) {
						out.close();
						out = null;
					}
					if (streamConnection != null) {
						streamConnection.close();
						streamConnection = null;
					}
				}

			};
		}

		return in;
	}

	/**
	 * Opens an <code>OutputStream</code> on the StreamConnection.
	 * 
	 * @return an OutputStream.
	 */
	public OutputStream getOutputStream() throws IOException {
		if (streamConnection == null) {
			throw new IOException("No open connection. Cannot open output stream");
		}
		if (out == null) {
			final OutputStream sOut = streamConnection.openOutputStream();
			out = new OutputStream() {

				/*
				 * (non-Javadoc)
				 * @see java.io.OutputStream#write(int)
				 */
				public void write(int b) throws IOException {
					sOut.write(b);
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.OutputStream#write(byte[], int, int)
				 */
				public void write(byte[] b, int off, int len) throws IOException {
					sOut.write(b, off, len);
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.OutputStream#flush()
				 */
				public void flush() throws IOException {
					sOut.flush();
				}

				/*
				 * (non-Javadoc)
				 * @see java.io.OutputStream#close()
				 */
				public void close() throws IOException {
					sOut.close();
					out = null;
					if (in != null) {
						in.close();
						in = null;
					}
					if (streamConnection != null) {
						streamConnection.close();
						streamConnection = null;
					}
				}

			};
		}
		return out;
	}

	/**
	 * Closes the connection.
	 */
	public void close() throws IOException {
		if (streamConnection == null) {
			return;
		}
		if (in != null) {
			in.close();
		}
		if (out != null) {
			out.close();
		}
		if (streamConnection != null) {
			streamConnection.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemoteAddress()
	 */
	public String getRemoteAddress() {
		return remoteAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemotePort()
	 */
	public int getRemotePort() {
		return remotePort;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalAddress()
	 */
	public IPAddress getLocalAddress() {
		return localAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalPort()
	 */
	public int getLocalPort() {
		return localPort;
	}

	public CredentialInfo getRemoteCredentialInfo() {
		return null;
	}

}
