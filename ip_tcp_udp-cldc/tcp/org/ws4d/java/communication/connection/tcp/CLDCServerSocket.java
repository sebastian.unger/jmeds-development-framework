/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/

package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.util.Random;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnectionNotifier;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.protocol.http.HTTPBinding;

/**
 * This class encapsulates a CLDC listening socket.
 */
public final class CLDCServerSocket implements ServerSocket {

	/**
	 * The number of tries of finding a random port to listen on before
	 * resigning.
	 */
	public static final int				TCP_PORT_RETRIES	= 10;

	private StreamConnectionNotifier	server				= null;

	private IPAddress					address				= null;

	private int							port				= -1;

	/**
	 * Default constructor.
	 */
	public CLDCServerSocket(HTTPBinding binding) throws IOException {
		this.address = binding.getHostIPAddress();
		this.port = binding.getPort();

		int portRetries = 0;
		while (true) {
			try {
				if (port == 0) {
					port = getRandomPortNumber();
				}
				server = (StreamConnectionNotifier) Connector.open(CLDCSocket.CLDC_SOCKET_PREFIX + ":" + port, Connector.READ_WRITE);
				break;
			} catch (IOException e) {
				if (portRetries++ == TCP_PORT_RETRIES) {
					throw e;
				}
			}
		}
		this.port = binding.getPort();
	}

	/**
	 * The method waits until a new client connects and returns an Socket
	 * object.
	 * 
	 * @return a Connection to the client.
	 */
	public Socket accept() throws IOException {
		return new CLDCSocket(server.acceptAndOpen(), address, port);
	}

	/**
	 * Closes the underlying server socket.
	 */
	public void close() throws IOException {
		server.close();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.tcp.ServerSocket#getIPAddress()
	 */
	public IPAddress getIPAddress() {
		return address;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getPort()
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Generates a random port number between 1025 and 65535.
	 * 
	 * @return a random number to try as port number for the server.
	 */
	public static int getRandomPortNumber() {
		Random r = new Random(System.currentTimeMillis());

		return (r.nextInt() & 0xefff) + 1025;
	}
}
