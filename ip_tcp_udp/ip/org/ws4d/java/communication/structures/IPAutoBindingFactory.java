package org.ws4d.java.communication.structures;

import org.ws4d.java.communication.AutoBindingFactory;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.util.Log;

public class IPAutoBindingFactory implements AutoBindingFactory {

	public static final boolean	HTTP_AUTOBINDING				= false;

	public static final boolean	DISCOVERY_UNICAST_AUTOBINDING	= true;

	private String				comManId;

	public IPAutoBindingFactory(String comManId) {
		if (comManId == null || comManId.equals("")) {
			throw new IllegalArgumentException("CommunicationManagerId not set");
		}
		this.comManId = comManId;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createDiscoveryMulticastAutobinding()
	 */
	public DiscoveryAutoBinding createDiscoveryMulticastAutobinding() {
		return new IPDiscoveryAutoBinding(comManId);

	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createDiscoveryMulticastAutobinding(java.lang.String[],
	 * java.lang.String[], boolean)
	 */
	public DiscoveryAutoBinding createDiscoveryMulticastAutobinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible) {
		return new IPDiscoveryAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryMulticastAutobinding
	 * (org.ws4d.java.security.CredentialInfo)
	 */
	public DiscoveryAutoBinding createSecureDiscoveryMulticastAutobinding(CredentialInfo credentialInfo) {
		IPDiscoveryAutoBinding dab = new IPDiscoveryAutoBinding(comManId);
		dab.setCredentialInfo(credentialInfo);
		return dab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryMulticastAutobinding(java.lang.String[],
	 * java.lang.String[], boolean, org.ws4d.java.security.CredentialInfo)
	 */
	public DiscoveryAutoBinding createSecureDiscoveryMulticastAutobinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, CredentialInfo credentialInfo) {
		IPDiscoveryAutoBinding dab = new IPDiscoveryAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible);
		dab.setCredentialInfo(credentialInfo);
		return dab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createDiscoveryMulticastAutobindingForCommunicationAutoBinding
	 * (org.ws4d.java.communication.structures.CommunicationAutoBinding)
	 */
	public DiscoveryAutoBinding createDiscoveryMulticastAutobindingForCommunicationAutoBinding(CommunicationAutoBinding cab) {
		IPCommunicationAutoBinding ipCab;
		try {
			ipCab = (IPCommunicationAutoBinding) cab;
		} catch (ClassCastException e) {
			if (Log.isWarn()) {
				Log.warn("Could't create DiscoveryAutoBinding from CommunicationAutoBinding, because CommunicationAutoBinding technology is wrong");
			}
			return null;
		}
		IPDiscoveryAutoBinding ipDab = new IPDiscoveryAutoBinding(ipCab.getCommunicationManagerId(), ipCab.getInterfaceNames(), ipCab.getAddressFamilies(), ipCab.isSuppressLoopbackIfPossible());
		ipDab.setCredentialInfo(ipCab.getCredentialInfo());
		return ipDab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createDiscoveryUnicastAutobinding()
	 */
	public CommunicationAutoBinding createDiscoveryUnicastAutobinding() {
		return new IPCommunicationAutoBinding(comManId, true, null, 0, DISCOVERY_UNICAST_AUTOBINDING);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createDiscoveryUnicastAutobinding(java.lang.String[], java.lang.String[],
	 * boolean, boolean, int)
	 */
	public CommunicationAutoBinding createDiscoveryUnicastAutobinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, int port) {
		return new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, null, port, DISCOVERY_UNICAST_AUTOBINDING);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryUnicastAutobinding
	 * (org.ws4d.java.security.CredentialInfo)
	 */
	public CommunicationAutoBinding createSecureDiscoveryUnicastAutobinding(CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, true, null, 0, DISCOVERY_UNICAST_AUTOBINDING);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryUnicastAutobinding(java.lang.String[],
	 * java.lang.String[], boolean, boolean, int,
	 * org.ws4d.java.security.CredentialInfo)
	 */
	public CommunicationAutoBinding createSecureDiscoveryUnicastAutobinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, int port, CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, null, port, DISCOVERY_UNICAST_AUTOBINDING);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.AutoBindingFactory#createCommunicationAutobinding
	 * (java.lang.String, int)
	 */
	public CommunicationAutoBinding createCommunicationAutobinding(boolean suppressLoopbackIfPossible, String path, int port) {
		return new IPCommunicationAutoBinding(comManId, suppressLoopbackIfPossible, path, port, HTTP_AUTOBINDING);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.AutoBindingFactory#createCommunicationAutobinding
	 * (java.lang.String[], java.lang.String[], boolean, boolean,
	 * java.lang.String, int)
	 */
	public CommunicationAutoBinding createCommunicationAutobinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, String path, int port) {
		return new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, path, port, HTTP_AUTOBINDING);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createCommunicationSecureAutobinding(java.lang.String, int,
	 * org.ws4d.java.security.CredentialInfo)
	 */
	public CommunicationAutoBinding createCommunicationSecureAutobinding(boolean suppressLoopbackIfPossible, String path, int port, CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, suppressLoopbackIfPossible, path, port, HTTP_AUTOBINDING);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createCommunicationSecureAutobinding(java.lang.String[],
	 * java.lang.String[], boolean, boolean, java.lang.String, int,
	 * org.ws4d.java.security.CredentialInfo)
	 */
	public CommunicationAutoBinding createCommunicationSecureAutobinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, String path, int port, CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, path, port, HTTP_AUTOBINDING);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.AutoBindingFactory#
	 * createCommunicationAutoBindingForDiscoveryAutoBinding
	 * (org.ws4d.java.communication.structures.DiscoveryAutoBinding)
	 */
	public CommunicationAutoBinding createCommunicationAutoBindingForDiscoveryAutoBinding(DiscoveryAutoBinding dab) {
		IPDiscoveryAutoBinding ipDab;
		try {
			ipDab = (IPDiscoveryAutoBinding) dab;
		} catch (ClassCastException e) {
			if (Log.isWarn()) {
				Log.warn("Could't create CommunicationAutoBinding from DiscoveryAutoBinding, because DiscoveryAutoBinding technology is wrong");
			}
			return null;
		}

		IPCommunicationAutoBinding ipCab = new IPCommunicationAutoBinding(ipDab.getCommunicationManagerId(), ipDab.getInterfaceNames(), ipDab.getAddressFamilies(), ipDab.isSuppressLoopbackIfPossible(), ipDab.suppressMulticastDisabledInterfaces, null, 0, HTTP_AUTOBINDING);
		ipCab.setCredentialInfo(ipDab.getCredentialInfo());
		return ipCab;
	}
}
