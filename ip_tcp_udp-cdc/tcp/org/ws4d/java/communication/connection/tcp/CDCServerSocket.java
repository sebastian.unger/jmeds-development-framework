/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.net.InetAddress;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.protocol.http.HTTPBinding;

/**
 * This class encapsulates an SE listening socket.
 */
public class CDCServerSocket implements ServerSocket {

	protected IPAddress			ipAddress		= null;

	java.net.ServerSocket		serverSocket	= null;

	/**
	 * The number of attempts to find a random port before giving up
	 */
	protected static final int	PORT_RETRIES	= 3;

	public CDCServerSocket(HTTPBinding binding) throws IOException {
		ipAddress = binding.getHostIPAddress();
		try {
			InetAddress adr = InetAddress.getByName(ipAddress.getAddress());
			int port = binding.getPort();
			serverSocket = new java.net.ServerSocket(port, 0, adr);
			if (port == 0) {
				binding.setPort(serverSocket.getLocalPort());
			}
		} catch (Exception e) {
			throw new IOException(e.getMessage() + " for " + binding);
		}
	}

	protected CDCServerSocket() {}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#accept()
	 */
	public Socket accept() throws IOException {
		return new CDCSocket(serverSocket.accept(), getIPAddress());
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#close()
	 */
	public void close() throws IOException {
		serverSocket.close();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getAddress()
	 */
	public IPAddress getIPAddress() {
		return ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getPort()
	 */
	public int getPort() {
		return serverSocket.getLocalPort();
	}

}
