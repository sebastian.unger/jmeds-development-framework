/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.util.Log;

/**
 * This class implements a connection for the SE Platform.
 */
public class CDCSocket implements Socket {

	protected java.net.Socket	socket;

	protected IPAddress			ipAddress	= null;

	protected int				port		= -1;

	protected InputStream		in			= null;

	protected OutputStream		out			= null;

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param host host name.
	 * @param port port number.
	 * @throws IOException
	 */
	public CDCSocket(IPAddress host, int port) throws IOException {
		socket = new java.net.Socket(host.getAddressWithoutNicId(), port);
		this.port = socket.getLocalPort();
	}

	public CDCSocket(java.net.Socket socket, IPAddress address) {
		this.socket = socket;
		this.ipAddress = address;
		this.port = socket.getLocalPort();
	}

	protected CDCSocket() {}

	/**
	 * Closes the connection.
	 */
	public void close() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not close connection");
		}
		socket.close();
	}

	/**
	 * Opens an <code>InputStream</code> on the socket.
	 * 
	 * @return an InputStream.
	 */
	public InputStream getInputStream() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not open input stream");
		}
		if (in == null) {
			in = socket.getInputStream();
		}
		return in;
	}

	/**
	 * Opens an <code>OutputStream</code> on the socket.
	 * 
	 * @return an OutputStream.
	 */
	public OutputStream getOutputStream() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not open output stream");
		}
		if (out == null) {
			out = new BufferedOutputStream(socket.getOutputStream());
		}
		return out;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemoteAddress()
	 */
	public String getRemoteAddress() {
		if (socket == null) return null;
		InetAddress i = socket.getInetAddress();
		if (i != null) {
			return i.getHostAddress();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemotePort()
	 */
	public int getRemotePort() {
		if (socket == null) return -1;
		return socket.getPort();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalAddress()
	 */
	public IPAddress getLocalAddress() {
		if (ipAddress != null) {
			return ipAddress;
		}

		InetAddress localInetAdr = socket.getLocalAddress();
		String localAdr = localInetAdr.getHostAddress();

		if (Log.isWarn() && localInetAdr.isAnyLocalAddress()) {
			Log.debug("CDCSocket.getLocalAddress(): Local IP address is wildcard (" + localAdr + ", local port: ." + port + ", remote address: " + getRemoteAddress() + ", remote port: " + getRemotePort() + ")");
		}

		ipAddress = IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface(localAdr, true);

		return ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalPort()
	 */
	public int getLocalPort() {
		return port;
	}

	public CredentialInfo getRemoteCredentialInfo() {
		return null;
	}

}
