/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.ip;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.util.Log;

/**
 * IP address detection for CDC.
 */
public class PlatformIPNetworkDetection extends IPNetworkDetectionNotCLDC {

	PlatformIPNetworkDetection() {
		super();
	}

	protected org.ws4d.java.communication.connection.ip.NetworkInterface createNetworkInterface(NetworkInterface niSE) throws IOException {
		return new org.ws4d.java.communication.connection.ip.NetworkInterface(niSE.getName(), niSE.getDisplayName(), true, true, isLoopback(niSE));
	}

	/**
	 * CDC specific method.
	 * 
	 * @param ni
	 * @return
	 */
	private boolean isLoopback(NetworkInterface ni) {
		Enumeration enu = ni.getInetAddresses();
		while (enu.hasMoreElements()) {
			InetAddress a = (InetAddress) enu.nextElement();
			if (a.isLoopbackAddress()) {
				return true;
			}
		}
		return false;
	}

	protected void startRefreshNetworkInterfacesThreadInternal() {
		try {
			updater.running = true;
			if (Log.isDebug()) {
				Log.debug("Start network refreshing unit");
			}
			boolean refresh = JMEDSFramework.getThreadPool().executeOrAbort(updater);
			if (!refresh) {
				throw new RuntimeException("Cannot start the watchdog.");
			}
		} catch (Exception e) {
			Log.error("Could not start network refreshing unit.");
			Log.error(e.getMessage());
		}
	}

	protected void stopRefreshNetworkInterfacesThreadInternal() {
			updater.running = false;
			if (Log.isDebug()) {
				Log.debug("Stop network refreshing unit");
			}
			updater.notifyAll();
	}

}