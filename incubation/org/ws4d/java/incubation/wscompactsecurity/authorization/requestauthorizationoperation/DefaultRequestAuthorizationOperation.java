package org.ws4d.java.incubation.wscompactsecurity.authorization.requestauthorizationoperation;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityToken;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityTokenResponseCollection;
import org.ws4d.java.incubation.wscompactsecurity.authorization.authorizationclient.DefaultAuthorizationClient;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AsynchronousDecisionCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.IDGenerator;
import org.ws4d.java.util.Log;

public class DefaultRequestAuthorizationOperation extends Operation {

	AsynchronousDecisionCallbacks adcb = null;

	public DefaultRequestAuthorizationOperation() {
		this(null);
	}

	public DefaultRequestAuthorizationOperation(AsynchronousDecisionCallbacks cb) {
		super(WSSecurityForDevicesConstants.STSAuthorizationRequestMethodName, QName.construct(WSSecurityForDevicesConstants.STSAuthorizationBindingPortType));

		/* wsa:Actions */
		this.setInputAction(Constants.WST_SAML_BINDING_RST_ISSUE_ACTION);
		this.setOutputAction(Constants.WST_SAML_BINDING_RSTRC_ISSUEFINAL_ACTION);

		Element in = new Element(Constants.RequestedSecurityToken, new RequestSecurityToken());
		this.setInput(in);

		Element out = new Element(Constants.RequestSecurityTokenResponseCollection, new RequestSecurityTokenResponseCollection());
		this.setOutput(out);

		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);

		this.adcb = cb;

	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {


		AuthorizationEngine aze = AuthorizationEngine.getInstance();

		if (aze == null) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error! Authorization Engine not ready!");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		ParameterValue result = this.createOutputValue();

		if (aze.isAuthorizer()) {
			String tokenType = ParameterValueManagement.getString(parameterValue,
					Constants.TokenType.getLocalPart());

			String requestType = ParameterValueManagement.getString(parameterValue,
					Constants.RequestType.getLocalPart());

			String appliesTo = ParameterValueManagement.getString(parameterValue,
					Constants.AppliesTo.getLocalPart());

			String claimDisplayName = ParameterValueManagement.getString(parameterValue,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.DisplayName.getLocalPart());
			String claimDescription = ParameterValueManagement.getString(parameterValue,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.Description.getLocalPart());
			String claimValue = ParameterValueManagement.getString(parameterValue,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.Value.getLocalPart());

			Log.info(tokenType + ", " + requestType + ", " + appliesTo + ", " + claimDisplayName + ", " + claimDescription + ", " + claimValue);

			//TODO get requested Ressource from somewhere
			Operation ressource = null;

			AuthorizationDecision decision = aze.checkAuthorization(claimValue, appliesTo, ressource);

			Log.info("Authorization decision: " + decision.toString());

			if (decision == AuthorizationDecision.INDETERMINATE) {
				Log.info("Trying to get an asynchronous decision");
				DefaultAuthorizationClient dac = new DefaultAuthorizationClient();
				decision = dac.uiAuthorize(parameterValue);
				Log.info("Result: " + decision.toString());
			}

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.TokenType.getLocalPart(),
							tokenType);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestType.getLocalPart(),
							requestType);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.AppliesTo.getLocalPart(),
							claimValue);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.version.getLocalPart(),
							AuthorizationConstants.SAMLVersion2String);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.id.getLocalPart(),
							IDGenerator.getUUID());


			/* Date date = new Date(System.currentTimeMillis());
			DateFormat df = DateFormat.getInstance();  What the... F*CK THIS SH*T!*/
			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.issueInstant.getLocalPart(),
							String.format("%d",  System.currentTimeMillis())); /* sry, my bad! */

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.issuerElement.getLocalPart(),
							AuthenticationEngine.getDefaultOwnerID());

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.subject.getLocalPart() + "/" +
							AuthorizationConstants.nameIDElement.getLocalPart(),
							appliesTo);

			ParameterValueManagement.setAttributeValue(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart(),
							AuthorizationConstants.ResourceAttribute,
							ressource == null ? "ALL" : ressource.getName());

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" +
							AuthorizationConstants.action.getLocalPart(),
					"777");

			ParameterValueManagement.setAttributeValue(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart(),
							AuthorizationConstants.DecisionAttribute,
							decision.toString());

			Log.info(result.toString());

			/* Now, inject into subject */
			DefaultAuthorizationClient dac = new DefaultAuthorizationClient();
			dac.validate(appliesTo, result);


		} else if (aze.isUIAuthorizer()) {

			String tokenType = ParameterValueManagement.getString(parameterValue,
					Constants.TokenType.getLocalPart());

			String requestType = ParameterValueManagement.getString(parameterValue,
					Constants.RequestType.getLocalPart());

			String appliesTo = ParameterValueManagement.getString(parameterValue,
					Constants.AppliesTo.getLocalPart());

			String claimDisplayName = ParameterValueManagement.getString(parameterValue,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.DisplayName.getLocalPart());
			String claimDescription = ParameterValueManagement.getString(parameterValue,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.Description.getLocalPart());
			String claimValue = ParameterValueManagement.getString(parameterValue,
					Constants.Claims.getLocalPart() + "/" +
							Constants.Claim.getLocalPart() + "/" +
							Constants.Value.getLocalPart());

			Log.info(tokenType + ", " + requestType + ", " + appliesTo + ", " + claimDisplayName + ", " + claimDescription + ", " + claimValue);

			//TODO get requested Ressource from somewhere
			Operation ressource = null;

			AuthorizationDecision decision;

			if (adcb == null)
				decision = AuthorizationDecision.INDETERMINATE;
			else
				decision = adcb.getAsynchronousDecision(parameterValue);

			Log.info("Authorization decision: " + decision.toString());

			StringBuilder hashMe = new StringBuilder();

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.TokenType.getLocalPart(),
							tokenType);
			hashMe.append(tokenType);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestType.getLocalPart(),
							requestType);
			hashMe.append(requestType);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.AppliesTo.getLocalPart(),
							claimValue);
			hashMe.append(claimValue);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.version.getLocalPart(),
							AuthorizationConstants.SAMLVersion2String);
			hashMe.append(AuthorizationConstants.SAMLVersion2String);

			String uuid = IDGenerator.getUUID();
			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.id.getLocalPart(),
							uuid);
			hashMe.append(uuid);

			/* Date date = new Date(System.currentTimeMillis());
			DateFormat df = DateFormat.getInstance();  What the... F*CK THIS SH*T!*/
			String date = String.format("%d",  System.currentTimeMillis());
			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.issueInstant.getLocalPart(),
							date); /* sry, my bad! */
			hashMe.append(date);

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.issuerElement.getLocalPart(),
							AuthenticationEngine.getDefaultOwnerID());
			hashMe.append(AuthenticationEngine.getDefaultOwnerID());

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.subject.getLocalPart() + "/" +
							AuthorizationConstants.nameIDElement.getLocalPart(),
							appliesTo);
			hashMe.append(appliesTo);

			ParameterValueManagement.setAttributeValue(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart(),
							AuthorizationConstants.ResourceAttribute,
							ressource == null ? "ALL" : ressource.getName());
			hashMe.append(ressource == null ? "ALL" : ressource.getName());

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" +
							AuthorizationConstants.action.getLocalPart(),
					"777");
			hashMe.append("777");

			ParameterValueManagement.setAttributeValue(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart(),
							AuthorizationConstants.DecisionAttribute,
							decision.toString());
			hashMe.append(decision.toString());

			/* FIXME: This is a place (though not a good one) to calculate the signature
			 * from the user credential. To provide the proof of concept, this quick-fix must
			 * do it for now. I apologize if it should ever be up to you todo this correctly.
			 * Come see me, I'll have you a beer. Or coffee. Or vegan carrot juice.
			 */

			String hms = hashMe.toString();
			//			CMac cmac = new CMac(new AESFastEngine(), 64);
			//			cmac.init(new KeyParameter(AuthorizationEngine.getInstance().getUserCredential("password").getBinaryData()));
			//			cmac.update(hms.getBytes(), 0, hms.getBytes().length);
			//			byte[] out = new byte[8];
			//			cmac.doFinal(out, 0);

			/* sorry!! */
			/* pseudo hash */
			byte[] out = badPseudoHash(hms, AuthorizationEngine.getInstance().getUserCredential("password").getBinaryData());

			ParameterValueManagement.setString(result,
					Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
							Constants.RequestedSecurityToken.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" +
							AuthorizationConstants.evidence.getLocalPart(),
							Base64Util.encodeBytes(out));

			Log.info(result.toString());

		} else {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "I am no Authorizer. And I am no UI-Authorizer. But you keep talking to me like I was one of them. What am I? And what Do you want? I'm confused. And scared. Mommy?");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		return result;
	}

	static public byte[] badPseudoHash(String s, byte[] pass) {
		byte[] in = s.getBytes();
		byte[] out = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

		/* start with credential */
		for (int i = 0 ; i < pass.length; i++) {
			out[i % 8] = (byte) ((int)out[i%8] ^ (int)in[i]);
		}

		for (int i = 0; i < ((int)((in.length-1) / out.length)) + 1 ; i++) {
			for (int j=0; j < out.length; j++) {
				if (i*8+j >= in.length)
					break;
				out[j] = (byte) ((int)out[j] ^ (int)in[i*8+j]);
			}
		}
		return out;
	}

}
