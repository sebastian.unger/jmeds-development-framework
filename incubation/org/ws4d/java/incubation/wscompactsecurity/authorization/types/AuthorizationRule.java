package org.ws4d.java.incubation.wscompactsecurity.authorization.types;

import java.io.Serializable;

public class AuthorizationRule implements Serializable {
	private static final long serialVersionUID = -2302446756048105123L;
	/* for now veeeeeery simple */
	String clientUri = null;
	String subjectUri = null;
	String ressource = null;
	AuthorizationDecision decision = AuthorizationDecision.INDETERMINATE;

	public String getClientUri() {
		return clientUri;
	}
	public void setClientUri(String clientUri) {
		this.clientUri = clientUri;
	}
	public String getSubjectUri() {
		return subjectUri;
	}
	public void setSubjectUri(String subjectUri) {
		this.subjectUri = subjectUri;
	}
	public String getRessource() {
		return ressource;
	}
	public void setRessource(String ressource) {
		this.ressource = ressource;
	}
	public AuthorizationDecision getDecision() {
		return decision;
	}
	public void setDecision(AuthorizationDecision decision) {
		this.decision = decision;
	}
	@Override
	public String toString() {
		return "May " + clientUri + " access " + subjectUri + "'s ressource " + ((ressource == null) ? "[NONE]" : ressource) + "? " + decision.toString();
	}
}
