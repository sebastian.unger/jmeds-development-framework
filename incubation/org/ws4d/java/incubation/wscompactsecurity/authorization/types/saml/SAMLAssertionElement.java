package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Element;

public class SAMLAssertionElement extends Element {
	public SAMLAssertionElement() {
		super(AuthorizationConstants.samlAssertionElement, new SAMLAssertionType());
	}
}
