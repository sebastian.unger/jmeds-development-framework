package org.ws4d.java.incubation.wscompactsecurity.authorization.types;

import org.ws4d.java.service.parameter.ParameterValue;

public interface AsynchronousDecisionCallbacks {
	AuthorizationDecision getAsynchronousDecision(ParameterValue request);
}
