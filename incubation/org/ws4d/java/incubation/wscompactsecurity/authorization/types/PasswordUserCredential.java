package org.ws4d.java.incubation.wscompactsecurity.authorization.types;

public class PasswordUserCredential implements IUserCredential {

	private final String pass;
	public final int LENGTH = 16;
	byte[] result;

	public PasswordUserCredential(String password) {
		pass = password;
		result = new byte[LENGTH];
		for (int loop = 0; loop < 6; loop++) {
			for (int i = 0; i < LENGTH; i++) {
				result[((i+loop) % LENGTH)] = (byte) (((loop == 0) ? 0x00 : (result[(i+loop)%LENGTH] + pass.charAt( ((loop*LENGTH)+i) % pass.length() ))) & 0xFF);
			}
		}
	}

	@Override
	public byte[] getBinaryData() {
		return pass.getBytes();
	}

	public void arrPrint(char[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.printf("%02X ", (byte) arr[i]);
		}
		System.out.printf("\n");
	}

	public void arrPrint(byte[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.printf("%02X ", arr[i]);
		}
		System.out.printf("\n");
	}
}
