package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Element;

public class NameIDElement extends Element {
	public NameIDElement() {
		super(AuthorizationConstants.nameIDElement, new NameIDType());
	}

}
