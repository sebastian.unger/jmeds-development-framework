package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ExtendedSimpleContent;
import org.ws4d.java.schema.SchemaUtil;

public class NameIDType extends ExtendedSimpleContent {
	public NameIDType() {
		super(AuthorizationConstants.nameIDType);
		this.setBase(SchemaUtil.TYPE_STRING);
		this.addAttributeElement(new Attribute(AuthorizationConstants.nameIDTypeNameQualifierAttribute, SchemaUtil.TYPE_STRING));
		this.addAttributeElement(new Attribute(AuthorizationConstants.nameIDTypeSPNameQualifierAttribute, SchemaUtil.TYPE_STRING));
		this.addAttributeElement(new Attribute(AuthorizationConstants.nameIDTypeFormatAttribute, SchemaUtil.TYPE_STRING));
		this.addAttributeElement(new Attribute(AuthorizationConstants.nameIDTypeSPProvidedIDAttribute, SchemaUtil.TYPE_STRING));
	}
}
