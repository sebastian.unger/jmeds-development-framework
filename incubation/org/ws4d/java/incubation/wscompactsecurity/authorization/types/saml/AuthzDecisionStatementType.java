package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class AuthzDecisionStatementType extends ComplexType {
	public AuthzDecisionStatementType() {
		super(AuthorizationConstants.authzDecisionStatementType, ComplexType.CONTAINER_ALL);

		Element action = new Element(AuthorizationConstants.action, SchemaUtil.TYPE_STRING);
		Element evidence = new Element(AuthorizationConstants.evidence, SchemaUtil.TYPE_STRING); /* Stub */

		action.setMinOccurs(1); /* see possible actions in Constants file */
		action.setMaxOccurs(-1);

		this.addElement(action);
		this.addElement(evidence);

		this.addAttributeElement(new Attribute(AuthorizationConstants.ResourceAttribute));
		this.addAttributeElement(new Attribute(AuthorizationConstants.DecisionAttribute));
	}
}
