package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;

public class SubjectConfirmationDataType extends ComplexType {
	public SubjectConfirmationDataType() {
		super(AuthorizationConstants.subjectConfirmationDataType, ComplexType.CONTAINER_SEQUENCE);
		this.addElement(new Element(new QName("Any"), SchemaUtil.TYPE_ANYTYPE));
		this.addAttributeElement(new Attribute(new QName("NotBefore")));
		this.addAttributeElement(new Attribute(new QName("NotOnOrAfter")));
		this.addAttributeElement(new Attribute(new QName("Recipient")));
		this.addAttributeElement(new Attribute(new QName("InResponseTo")));
		this.addAttributeElement(new Attribute(new QName("Address")));
	}
}
