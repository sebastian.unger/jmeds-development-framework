package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Element;

public class IssuerElement extends Element {
	public IssuerElement() {
		super(AuthorizationConstants.issuerElement, new NameIDType());
	}

}
