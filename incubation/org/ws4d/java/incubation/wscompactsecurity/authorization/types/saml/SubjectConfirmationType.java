package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class SubjectConfirmationType extends ComplexType {
	public SubjectConfirmationType() {
		super(AuthorizationConstants.subjectConfirmationType, ComplexType.CONTAINER_SEQUENCE);
		this.addElement(new NameIDElement());
		this.addElement(new Element(AuthorizationConstants.subjectConfirmationData, new SubjectConfirmationDataType()));
		this.addAttributeElement(new Attribute(AuthorizationConstants.MethodAttribute, SchemaUtil.TYPE_ANYURI));
	}
}
