package org.ws4d.java.incubation.wscompactsecurity.authorization.types;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;

public enum AuthorizationDecision {
	PERMIT, DENY, INDETERMINATE;
	@Override
	public String toString() {
		switch (this) {
		case PERMIT:
			return AuthorizationConstants.DecisionPermit;
		case DENY:
			return AuthorizationConstants.DecisionDeny;
		case INDETERMINATE:
			return AuthorizationConstants.DecisionIndeterminate;
		default:
			return "WAT?!";
		}
	}

	public static AuthorizationDecision construct(String s) {
		if (PERMIT.toString().toUpperCase().equals(s.toUpperCase())) {
			return PERMIT;
		} else if (DENY.toString().toUpperCase().equals(s.toUpperCase())) {
			return DENY;
		}
		return INDETERMINATE;
	}
}
