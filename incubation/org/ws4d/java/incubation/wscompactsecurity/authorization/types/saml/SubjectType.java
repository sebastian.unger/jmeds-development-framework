package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

/**
 * Strongly simplified. A subject may contain zero or one identifier as well as
 * zero or more SubjectConfirmations. An identifier may be a BaseID, a NameID
 * or an EnctyptedID. However, in this usecase only NameIDs are relevant.
 *
 * However
 *
 * @author su009
 *
 */
public class SubjectType extends ComplexType {
	public SubjectType() {
		super(AuthorizationConstants.subjectType, ComplexType.CONTAINER_SEQUENCE);
		this.addElement(new NameIDElement());
		this.addElement(new Element(AuthorizationConstants.subjectConfirmation, new SubjectConfirmationType()));
	}
}
