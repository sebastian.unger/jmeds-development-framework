package org.ws4d.java.incubation.wscompactsecurity.authorization.types;

public interface IUserCredential {
	public byte[] getBinaryData();
}
