package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class SAMLAssertionType extends ComplexType {
	public SAMLAssertionType() {
		super(AuthorizationConstants.samlAssertionType, ComplexType.CONTAINER_ALL);

		Element version = new Element(AuthorizationConstants.version, SchemaUtil.TYPE_STRING);
		Element id = new Element(AuthorizationConstants.id, SchemaUtil.TYPE_STRING);
		Element issueInstant = new Element(AuthorizationConstants.issueInstant, SchemaUtil.TYPE_DATE_TIME);
		IssuerElement issuer = new IssuerElement();

		// <ds:Signature> - ommitted

		Element subject = new Element(AuthorizationConstants.subject, new SubjectType());

		Element conditions = new Element(AuthorizationConstants.conditions, new ConditionsType());

		Element advice = new Element(AuthorizationConstants.advice, SchemaUtil.TYPE_STRING); /* dummy stub */

		// 0..MAX * <saml:Statement> - ommitted
		// 0..MAX * <saml:AuthnStatement> - ommitted

		Element authzDecisionStatement = new Element(AuthorizationConstants.authzDecisionStatement, new AuthzDecisionStatementType());
		authzDecisionStatement.setMaxOccurs(-1);

		// 0..MAX * <saml:AttributeStatement> - ommitted

		this.addElement(version);
		this.addElement(id);
		this.addElement(issueInstant);
		this.addElement(issuer);

		this.addElement(subject);
		this.addElement(conditions);
		this.addElement(advice);

		this.addElement(authzDecisionStatement);

	}
}
