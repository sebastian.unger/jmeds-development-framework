package org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml;

import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class ConditionsType extends ComplexType {
	public ConditionsType() {
		super(AuthorizationConstants.conditionsType, ComplexType.CONTAINER_ALL);

		/* actually, Conditions are waaaaayyyy more complex. However, since I doubt
		 * that I am using them in such a detail, I will only implement a stub here
		 */

		this.addElement(new Element(AuthorizationConstants.condition, SchemaUtil.TYPE_STRING));
		this.addElement(new Element(AuthorizationConstants.audienceRestriction, SchemaUtil.TYPE_STRING));
		this.addElement(new Element(AuthorizationConstants.oneTimeUse, SchemaUtil.TYPE_STRING));
		this.addElement(new Element(AuthorizationConstants.proxyRestriction, SchemaUtil.TYPE_STRING));

		this.addAttributeElement(new Attribute(AuthorizationConstants.notBeforeAttribute, SchemaUtil.TYPE_DATE_TIME));
		this.addAttributeElement(new Attribute(AuthorizationConstants.notOnOrAfterAttribute, SchemaUtil.TYPE_DATE_TIME));
	}
}
