package org.ws4d.java.incubation.wscompactsecurity.authorization.authorizationclient;

import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.requestauthorizationoperation.DefaultRequestAuthorizationOperation;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthorizerPolicy;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

public class DefaultAuthorizationClient extends DefaultClient {

	public DefaultAuthorizationClient() {
		super();
	}

	public AuthorizationDecision authorize(String subjectURI) throws CommunicationException, AuthorizationException, InvocationException {
		return authorize(subjectURI, null);
	}
	public AuthorizationDecision authorize(String subjectURI, Operation resource) throws CommunicationException, AuthorizationException, InvocationException {
		return authorize(new EndpointReference(new AttributedURI(subjectURI)), resource);
	}

	public AuthorizationDecision authorize(EndpointReference target) throws CommunicationException, AuthorizationException, InvocationException {
		return authorize(target, null);
	}
	public AuthorizationDecision authorize(EndpointReference target, Operation resource) throws CommunicationException, AuthorizationException, InvocationException {
		DeviceReference devRef = getDeviceReference(target, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return authorize(devRef, null, resource);
	}

	public AuthorizationDecision authorize(DeviceReference target) throws CommunicationException, AuthorizationException, InvocationException {
		return authorize(target, null, null);
	}
	public AuthorizationDecision authorize(DeviceReference target, DeviceReference authorizer) throws CommunicationException, AuthorizationException, InvocationException {
		return authorize(target, authorizer, null);
	}
	public AuthorizationDecision authorize(DeviceReference target, DeviceReference authorizer, Operation resource) throws CommunicationException, AuthorizationException, InvocationException {

		String pAuthorizerURI;
		if (authorizer == null) {
			pAuthorizerURI = getPrimaryAuthorizer(target);
		} else {
			pAuthorizerURI = authorizer.getEndpointReference().getAddress().toString();
		}

		if (pAuthorizerURI == null) {
			return AuthorizationDecision.INDETERMINATE;
		}
		Log.info("Target's primary Authorizer is " + pAuthorizerURI);

		Operation authOp = getAuthorizationOperation(pAuthorizerURI);
		if (authOp == null) {
			Log.warn("No matching Operation found!");
			return AuthorizationDecision.INDETERMINATE;
		}
		Log.info("Found Authorization Operation: " + authOp.getService().getServiceId().toString() + "/" + authOp.getInputName() );

		ParameterValue input = authOp.createInputValue();

		ParameterValueManagement.setString(input,
				Constants.TokenType.getLocalPart(),
				Constants.AuthorizationTokenType);

		ParameterValueManagement.setString(input,
				Constants.RequestType.getLocalPart(),
				Constants.WST_ISSUE_BINDING);

		ParameterValueManagement.setString(input,
				Constants.AppliesTo.getLocalPart(),
				target.getEndpointReference().getAddress().toString());

		ParameterValueManagement.setAttributeValue(input,
				Constants.Claims.getLocalPart(),
				new QName(Constants.ClaimsDialectAttribute),
				Constants.ClaimTypeDialectURI);

		ParameterValueManagement.setString(input,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.DisplayName.getLocalPart(),
				"Identity");
		ParameterValueManagement.setString(input,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.Description.getLocalPart(),
				"Shows Requestors Identity");
		ParameterValueManagement.setString(input,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.Value.getLocalPart(),
						AuthenticationEngine.getDefaultOwnerID());

		ParameterValue result = authOp.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);

		String decision =
				ParameterValueManagement.getAttributeValue(result,
						Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
								Constants.RequestedSecurityToken.getLocalPart() + "/" +
								AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
								AuthorizationConstants.authzDecisionStatement.getLocalPart(),
								AuthorizationConstants.DecisionAttribute);



		Log.info(decision);

		return AuthorizationDecision.construct(decision);
	}

	public String getPrimaryAuthorizer(String subject) throws CommunicationException {
		EndpointReference epr = new EndpointReference(new AttributedURI(subject));
		return getPrimaryAuthorizer(epr);
	}

	public String getPrimaryAuthorizer(EndpointReference epr) throws CommunicationException {
		DeviceReference devRef = getDeviceReference(epr, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return getPrimaryAuthorizer(devRef);
	}

	public String getPrimaryAuthorizer(DeviceReference devRef) throws CommunicationException {
		Device targetDevice = devRef.getDevice();
		return getPrimaryAuthorizer(targetDevice);
	}

	public String getPrimaryAuthorizer(Device targetDevice) throws CommunicationException {

		/* find out target's primary Authorizer */
		ServiceReference targetServRef = (ServiceReference) targetDevice.getServiceReferences(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType)), SecurityKey.EMPTY_KEY).next();
		Service targetSTS = targetServRef.getService();

		Iterator allPolicies = targetSTS.getPolicies().keySet().iterator();

		while (allPolicies.hasNext()) {
			QName polName = (QName) allPolicies.next();
			Log.info(polName.toString());
		}

		Policy p = targetSTS.getPolicy(new QName(WSSecurityForDevicesConstants.AuthorizersPolicyName));

		if (p == null) {
			Log.warn("Could not retrieve target's Authorizer Policy!");
			return null;
		}

		String pAuthorizerURI = AuthorizerPolicy.getPrimaryAuthorizer(p);

		return pAuthorizerURI;
	}

	public Operation getAuthorizationOperation(String authorizer) throws CommunicationException {
		EndpointReference authEPR = new EndpointReference(new AttributedURI(authorizer));
		return getAuthorizationOperation(authEPR);
	}

	public Operation getAuthorizationOperation(EndpointReference authEPR) throws CommunicationException {
		DeviceReference authDevRef = getDeviceReference(authEPR, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return getAuthorizationOperation(authDevRef);
	}

	public Operation getAuthorizationOperation(DeviceReference authDevRef) throws CommunicationException {
		Device authDev = authDevRef.getDevice();
		return getAuthorizationOperation(authDev);
	}

	public Operation getAuthorizationOperation(Device authDev) throws CommunicationException {
		ServiceReference authServRef = (ServiceReference) authDev.getServiceReferences(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthorizationBindingPortType)), SecurityKey.EMPTY_KEY).next();
		if (authServRef == null) {
			Log.warn("No matching Service found!");
			return null;
		}
		Service authServ = authServRef.getService();
		Operation authOp = authServ.getOperation(QName.construct(WSSecurityForDevicesConstants.STSAuthorizationBindingPortType), null, Constants.WST_SAML_BINDING_RST_ISSUE_ACTION, Constants.WST_SAML_BINDING_RSTRC_ISSUEFINAL_ACTION);
		return authOp;
	}

	public boolean validate(String target, ParameterValue token) throws CommunicationException, AuthorizationException, InvocationException {
		EndpointReference targetEPR = new EndpointReference(new AttributedURI(target));
		return validate(targetEPR, token);
	}

	public boolean validate(EndpointReference targetEPR, ParameterValue token) throws CommunicationException, AuthorizationException, InvocationException {
		DeviceReference targetDevRef = getDeviceReference(targetEPR, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return validate(targetDevRef, token);
	}

	public boolean validate(DeviceReference targetDevRef, ParameterValue token) throws CommunicationException, AuthorizationException, InvocationException {
		return validate(targetDevRef.getDevice(), token);
	}

	public boolean validate(Device device, ParameterValue token) throws CommunicationException, AuthorizationException, InvocationException {

		ServiceReference authServRef = (ServiceReference) device.getServiceReferences(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthorizationValidateBindingPortType)), SecurityKey.EMPTY_KEY).next();

		Service authServ = authServRef.getService();

		Operation op = authServ.getOperation(QName.construct(WSSecurityForDevicesConstants.STSAuthorizationValidateBindingPortType), null, Constants.WST_VALIDATE_BINDING_RST_VALIDATE_ACTION, Constants.WST_VALIDATE_BINDING_RSTR_VALIDATEFINAL_ACTION);

		ParameterValue input = op.createInputValue();

		ParameterValueManagement.setString(input, Constants.TokenType.getLocalPart(), Constants.WST_VALIDATE_AUTHORIZATION_TOKEN_TYPE);
		ParameterValueManagement.setString(input, Constants.RequestType.getLocalPart(), Constants.WST_VALIDATE_BINDING);
		ParameterValueManagement.setString(input, Constants.OnBehalfOf.getLocalPart(), AuthenticationEngine.getDefaultOwnerID());

		ParameterValueManagement.setString(input,
				Constants.AppliesTo.getLocalPart(),
				ParameterValueManagement.getString(token,
						Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
								Constants.AppliesTo.getLocalPart()
						)
				);

		ParameterValueManagement.setString(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.version.getLocalPart(),
						ParameterValueManagement.getString(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.version.getLocalPart()
								)
				);

		ParameterValueManagement.setString(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.id.getLocalPart(),
						ParameterValueManagement.getString(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.id.getLocalPart()
								)
				);



		ParameterValueManagement.setString(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.issueInstant.getLocalPart(),
						ParameterValueManagement.getString(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.issueInstant.getLocalPart()
								)
				);


		ParameterValueManagement.setString(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.issuerElement.getLocalPart(),
						ParameterValueManagement.getString(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.issuerElement.getLocalPart()
								)
				);



		ParameterValueManagement.setString(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.subject.getLocalPart() + "/" +
						AuthorizationConstants.nameIDElement.getLocalPart(),
						ParameterValueManagement.getString(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.subject.getLocalPart() + "/" +
										AuthorizationConstants.nameIDElement.getLocalPart()
								)
				);

		ParameterValueManagement.setString(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" +
						AuthorizationConstants.action.getLocalPart(),
						ParameterValueManagement.getString(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" +
										AuthorizationConstants.action.getLocalPart()
								)
				);

		ParameterValueManagement.setAttributeValue(input,
				Constants.ValidateTarget.getLocalPart() + "/" +
						AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
						AuthorizationConstants.authzDecisionStatement.getLocalPart(),
						AuthorizationConstants.ResourceAttribute,
						ParameterValueManagement.getAttributeValue(token,
								Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
										Constants.RequestedSecurityToken.getLocalPart() + "/" +
										AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
										AuthorizationConstants.authzDecisionStatement.getLocalPart(),
										AuthorizationConstants.ResourceAttribute
								)
				);

		ParameterValueManagement.setAttributeValue(input, Constants.ValidateTarget.getLocalPart() + "/" +
				AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
				AuthorizationConstants.authzDecisionStatement.getLocalPart(),
				AuthorizationConstants.DecisionAttribute,
				ParameterValueManagement.getAttributeValue(token,
						Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
								Constants.RequestedSecurityToken.getLocalPart() + "/" +
								AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
								AuthorizationConstants.authzDecisionStatement.getLocalPart(),
								AuthorizationConstants.DecisionAttribute
						)
				);

		Log.info("Invoking validate: " + input.toString());

		ParameterValue result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);

		Log.info("Returned: " + result.toString());

		String resultCode = ParameterValueManagement.getString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart());

		if (Constants.StatusCodeValid.equals(resultCode))
			return true;
		else
			return false;
	}

	public AuthorizationDecision uiAuthorize(ParameterValue parameterValue) {

		DeviceReference uiAuthorizer = findFirstUIAuthorizer();

		if (uiAuthorizer == null) {
			return AuthorizationDecision.INDETERMINATE;
		}

		Operation op = null;
		try {
			op = getAuthorizationOperation(uiAuthorizer);
		} catch (CommunicationException e) {
		}
		if (op == null)
			return AuthorizationDecision.INDETERMINATE;

		String tokenType = ParameterValueManagement.getString(parameterValue,
				Constants.TokenType.getLocalPart());

		String requestType = ParameterValueManagement.getString(parameterValue,
				Constants.RequestType.getLocalPart());

		String appliesTo = ParameterValueManagement.getString(parameterValue,
				Constants.AppliesTo.getLocalPart());

		String claimDisplayName = ParameterValueManagement.getString(parameterValue,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.DisplayName.getLocalPart());
		String claimDescription = ParameterValueManagement.getString(parameterValue,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.Description.getLocalPart());
		String claimValue = ParameterValueManagement.getString(parameterValue,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.Value.getLocalPart());

		ParameterValue request = op.createInputValue();

		ParameterValueManagement.setString(request,
				Constants.TokenType.getLocalPart(),
				tokenType);

		ParameterValueManagement.setString(request,
				Constants.RequestType.getLocalPart(),
				requestType);

		ParameterValueManagement.setString(request,
				Constants.AppliesTo.getLocalPart(),
				appliesTo);

		ParameterValueManagement.setString(request,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.DisplayName.getLocalPart(),
						claimDisplayName);

		ParameterValueManagement.setString(request,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.Description.getLocalPart(),
						claimDescription);

		ParameterValueManagement.setString(request,
				Constants.Claims.getLocalPart() + "/" +
						Constants.Claim.getLocalPart() + "/" +
						Constants.Value.getLocalPart(),
						claimValue);

		ParameterValue result = null;
		try {
			result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result == null) {
			return AuthorizationDecision.INDETERMINATE;
		}

		String decision =
				ParameterValueManagement.getAttributeValue(result,
						Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
								Constants.RequestedSecurityToken.getLocalPart() + "/" +
								AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
								AuthorizationConstants.authzDecisionStatement.getLocalPart(),
								AuthorizationConstants.DecisionAttribute);

		Log.info(decision);

		StringBuilder hashMe = new StringBuilder();

		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.TokenType.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestType.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.AppliesTo.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.version.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.id.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.issueInstant.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.issuerElement.getLocalPart()));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.subject.getLocalPart() + "/" + AuthorizationConstants.nameIDElement.getLocalPart()));
		hashMe.append(ParameterValueManagement.getAttributeValue(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.authzDecisionStatement.getLocalPart(), AuthorizationConstants.ResourceAttribute));
		hashMe.append(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" + AuthorizationConstants.action.getLocalPart()));
		hashMe.append(ParameterValueManagement.getAttributeValue(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.authzDecisionStatement.getLocalPart(), AuthorizationConstants.DecisionAttribute));

		String rHash = ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" + AuthorizationConstants.authzDecisionStatement.getLocalPart() + "/" + AuthorizationConstants.evidence.getLocalPart());

		String lHash = Base64Util.encodeBytes( DefaultRequestAuthorizationOperation.badPseudoHash(hashMe.toString(), AuthorizationEngine.getInstance().getUserCredential("password").getBinaryData()) );

		Log.info("Received hash: " + rHash);
		Log.info("Local Hash   : " + lHash);
		if (rHash.equals(lHash)) {
			Log.info("hash okay");
		} else {
			Log.warn("Hashes DO NOT MATCH!!");
			return AuthorizationDecision.INDETERMINATE;
		}

		if (AuthorizationDecision.construct(decision) == AuthorizationDecision.PERMIT) {
			Log.info("Will save permission");
			AuthorizationEngine.getInstance().addAuthorizationRuleByURI(claimValue, appliesTo, null);
		}

		return AuthorizationDecision.construct(decision);

	}

	DeviceReference firstFound = null;
	boolean searchFinished = false;

	DeviceReference findFirstUIAuthorizer() {

		SearchParameter s = new SearchParameter();
		s.setDeviceTypes(new QNameSet(new QName(WSSecurityForDevicesConstants.UIAuthorizerType, WSSecurityForDevicesConstants.NAMESPACE)), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		firstFound = null;
		searchFinished = false;
		searchDevice(s);

		while (firstFound == null && searchFinished == false) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return firstFound;

	}

	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search,
			String comManId) {
		super.deviceFound(devRef, search, comManId);
		firstFound = devRef;
	}
	@Override
	public void finishedSearching(int searchIdentifier, boolean entityFound,
			SearchParameter search) {
		super.finishedSearching(searchIdentifier, entityFound, search);
		searchFinished = true;
	}

}
