package org.ws4d.java.incubation.wscompactsecurity.authorization.engine;

import java.io.IOException;

import org.ws4d.java.incubation.CredentialManagement.AuthorizationDBFactory;
import org.ws4d.java.incubation.CredentialManagement.PersistenceFactory;
import org.ws4d.java.incubation.CredentialManagement.SimpleAuthorizationDB;
import org.ws4d.java.incubation.util.MyUtil;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationRule;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.IUserCredential;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.service.Operation;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.util.Log;

public class AuthorizationEngine {
	private boolean isAuthorizer = false;
	private boolean isUIAuthorizer = false;
	private String identifier = null;

	String primaryAuthorizer = null;
	ArrayList secondaryAuthorizers = new ArrayList();

	public boolean hasPrimaryAuthorizer() {
		if (primaryAuthorizer == null)
			return false;
		if (primaryAuthorizer.isEmpty())
			return false;
		return true;
	}

	public boolean isPrimaryauthorizer(String uri) {
		return MyUtil.uriCompare(uri, primaryAuthorizer);
	}

	public boolean hasSecondaryAuthorizers() {
		return secondaryAuthorizers.size() != 0;
	}

	private AuthorizationEngine(String key) {
		identifier = key;
	}

	static HashMap engines = new HashMap();

	/**
	 * Return default instance
	 * @return defaultInstance
	 */
	public static AuthorizationEngine getInstance() {
		return getInstance(null);
	}

	/**
	 * In case you need more than one.
	 * @param key
	 * @return Instance to key. if key does not exist, an instance is created.
	 */
	public static AuthorizationEngine getInstance(String key) {
		String k;
		if (key == null) {
			k = AuthenticationEngine.getDefaultOwnerID();
		} else {
			k = key;
		}

		AuthorizationEngine engine = (AuthorizationEngine) engines.get(k);
		if (engine == null) {
			engine = new AuthorizationEngine(k);
			engines.put(k, engine);
		}

		return engine;
	}

	public boolean isAuthorizer() {
		return isAuthorizer;
	}

	public void setIsAuthorizer(boolean isAuthorizer) {
		this.isAuthorizer = isAuthorizer;
	}

	public boolean isUIAuthorizer() {
		return isUIAuthorizer;
	}

	public void setIsUIAuthorizer(boolean isUIAuthorizer) {
		this.isUIAuthorizer = isUIAuthorizer;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getPrimaryAuthorizer() {
		return primaryAuthorizer;
	}

	public boolean setPrimaryAuthorizer(String authz) {
		if (primaryAuthorizer == null || AuthenticationEngine.getInstance().getContextDatabase().getContextByReference(primaryAuthorizer) == null) {
			/* it could happen that the primary authorizer was set during an authentication that did not conclude successfully */
			primaryAuthorizer = authz;
			Log.info("Successfully determined " + authz + " as primary authorizer");
			return true;
		} else {
			return false;
		}
	}

	public void resetPrimaryAuthorizer() {
		primaryAuthorizer = null;
	}

	public boolean addSecondaryAuthorizer(String authz) {
		secondaryAuthorizers.add(authz);
		return true;
	}

	public void resetSecondaryAuthorizers() {
		secondaryAuthorizers.clear();
	}

	public void reset() {
		reset(true);
	}

	public void reset(boolean restart) {
		resetPrimaryAuthorizer();
		resetSecondaryAuthorizers();
		if (restart && DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance() != null) {
			synchronized (DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance()) {
				try {
					DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().stop();
					DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().start();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	HashMap mySheep = new HashMap();
	/* contains the subjects I am an authorizer for */

	public void addSheep(String target, String type) {
		mySheep.put(target, type);
	}

	public HashMap getSheep() {
		return mySheep;
	}

	private boolean freshAccept = false;
	public void freshPrimaryAccept(boolean b) {
		freshAccept = b;
	}
	public boolean wasAFreshAccept() {
		return freshAccept;
	}


	private static final String dbNameDefault = "/home/pi/authz";
	private String dbName = null;

	public String getDBName() {
		if (dbName == null || dbName.isEmpty())
			return dbNameDefault;
		else
			return dbName;
	}
	public void setDBName(String n) {
		dbName = n;
	}

	public SimpleAuthorizationDB getAuthorizationDatabase() {
		return AuthorizationDBFactory.getAuthorizationDB(AuthenticationEngine.getDefaultOwnerID());
	}
	public void addAuthorizationRuleByURI(String clienturi, String subjecturi, String ressource) {
		AuthorizationRule r = new AuthorizationRule();
		r.setClientUri(clienturi);
		r.setSubjectUri((subjecturi != null) ? subjecturi : AuthenticationEngine.getDefaultOwnerID());
		r.setRessource(ressource);
		r.setDecision(AuthorizationDecision.PERMIT);
		getAuthorizationDatabase().add(r);
		saveDatabase();
	}

	public void clearDatabase() {
		getAuthorizationDatabase().clearContexts();
		PersistenceFactory.getAuthorizationPersistence().clearDatabase(getAuthorizationDatabase(), getDBName());
	}

	public void saveDatabase() {
		PersistenceFactory.getAuthorizationPersistence().saveToFile(getAuthorizationDatabase(), getDBName());
	}

	public void loadDatabase() {
		PersistenceFactory.getAuthorizationPersistence().loadFromFile(getAuthorizationDatabase(), getDBName());
	}

	public boolean checkAuthorization(String rId, Operation ressource) {
		return checkAuthorization(rId, null, ressource) == AuthorizationDecision.PERMIT;
	}

	public AuthorizationDecision checkAuthorization(String rId, String theSubject, Operation resource) {
		return getAuthorizationDatabase().checkAuthorization(rId, theSubject, resource);
	}

	private HashMap credentials = new HashMap();

	public void addUserCredential(String id, IUserCredential userCredential) {
		credentials.put(id, userCredential);
	}

	public IUserCredential getUserCredential(String id) {
		return (IUserCredential) credentials.get(id);
	}


}
