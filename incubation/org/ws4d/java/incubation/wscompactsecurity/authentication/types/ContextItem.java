package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;

public class ContextItem extends ComplexType {

	public ContextItem() {
		super(Constants.ContextItem, ComplexType.CONTAINER_ALL);

		Attribute nameAttr = new Attribute(new QName(Constants.ContextItemNameAttribute), SchemaUtil.TYPE_STRING);
		nameAttr.setUse(USE_REQUIRED);
		Attribute scopeAttr = new Attribute(new QName(Constants.ContextItemScopeAttribute), SchemaUtil.TYPE_STRING);
		scopeAttr.setUse(USE_OPTIONAL);

		this.addAttributeElement(nameAttr);
		this.addAttributeElement(scopeAttr);

		this.addElement(new Element(Constants.Value, SchemaUtil.TYPE_STRING));

	}
}
