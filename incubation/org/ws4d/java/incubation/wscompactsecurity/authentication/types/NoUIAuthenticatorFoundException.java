package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.communication.CommunicationException;

public class NoUIAuthenticatorFoundException extends CommunicationException {

	private static final long serialVersionUID = 1321351351L;

	public NoUIAuthenticatorFoundException() {
		this("Could not find a UI Authenticator");
	}
	
	public NoUIAuthenticatorFoundException(String message) {
		super(message);
	}

}
