package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class EncryptedValue extends ComplexType {

	public EncryptedValue() {
		super(Constants.EncryptedValue, ComplexType.CONTAINER_ALL);
		this.addElement(new Element(Constants.EncryptedData, SchemaUtil.TYPE_STRING)); /* for now */
	}

}
