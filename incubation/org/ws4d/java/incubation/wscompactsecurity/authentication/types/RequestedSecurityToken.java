package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml.SAMLAssertionElement;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

public class RequestedSecurityToken extends ComplexType {

	public RequestedSecurityToken() {
		super(Constants.RequestedSecurityToken, ComplexType.CONTAINER_CHOICE);
		Element securityContextToken = new Element(Constants.SecurityContextToken, new SecurityContextToken());
		Element authorizationToken = new SAMLAssertionElement();

		this.addElement(securityContextToken);
		this.addElement(authorizationToken);
	}

}
