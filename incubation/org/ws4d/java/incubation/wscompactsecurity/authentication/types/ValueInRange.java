package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

public class ValueInRange extends ComplexType {
	public ValueInRange() {
		super(Constants.ValueInRange, ComplexType.CONTAINER_ALL);
		this.addElement(new Element(Constants.ValueUpperBound, new AbstractCompareType(Constants.ValueUpperBound)));
		this.addElement(new Element(Constants.ValueLowerBound, new AbstractCompareType(Constants.ValueLowerBound)));
	}
}
