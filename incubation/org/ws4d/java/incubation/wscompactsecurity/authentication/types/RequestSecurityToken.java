package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.types.AuthECCDHParameters;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.ExtendedSimpleContent;
import org.ws4d.java.schema.SchemaUtil;

public class RequestSecurityToken extends ComplexType {

	public RequestSecurityToken() {
		super(Constants.RequestSecurityToken, ComplexType.CONTAINER_ALL);

		Element tokenType = new Element(Constants.TokenType, SchemaUtil.TYPE_STRING);
		Element requestType = new Element(Constants.RequestType, SchemaUtil.TYPE_STRING);
		Element appliesTo = new Element(Constants.AppliesTo, SchemaUtil.TYPE_STRING);
		Element onBehalfOf = new Element(Constants.OnBehalfOf, SchemaUtil.TYPE_STRING);
		Element authenticationType = new Element(Constants.AuthenticationType, SchemaUtil.TYPE_STRING);
		Element authParams = new Element(Constants.AuthECCDHParameters, new AuthECCDHParameters());
		Element additionalContext = new Element(Constants.AddtionalContext, new AdditionalContext());
		Element claims = new Element(Constants.Claims, new Claims());

		ExtendedSimpleContent brokerType = new ExtendedSimpleContent(Constants.isBroker);
		brokerType.setBase(SchemaUtil.TYPE_STRING);
		brokerType.addAttributeElement(new Attribute(Constants.authorizerAttribute, SchemaUtil.TYPE_STRING));
		Element broker = new Element(Constants.isBroker, brokerType);


		/* these are desired parameters - this is actually defined in WST14, p.61 */
		Element requestedSignatureAlgorithms = new Element(Constants.SignWith, SchemaUtil.TYPE_STRING);
		Element requestedEncryptionAlgorithms = new Element(Constants.EncryptWith, SchemaUtil.TYPE_STRING);

		this.addElement(tokenType);
		this.addElement(requestType);
		this.addElement(appliesTo);
		this.addElement(onBehalfOf);
		this.addElement(authenticationType);
		this.addElement(additionalContext);
		this.addElement(claims);
		this.addElement(broker);
		this.addElement(requestedSignatureAlgorithms);
		this.addElement(requestedEncryptionAlgorithms);
		this.addElement(authParams);
	}

}
