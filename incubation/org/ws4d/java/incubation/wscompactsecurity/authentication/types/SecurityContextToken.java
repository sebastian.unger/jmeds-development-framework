package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class SecurityContextToken extends ComplexType {

	public SecurityContextToken() {
		super(Constants.SecurityContextToken, ComplexType.CONTAINER_ALL);
		Element id = new Element(Constants.Identifier, SchemaUtil.TYPE_STRING);
		Element sig = new Element(Constants.SignWith, SchemaUtil.TYPE_STRING);
		Element enc = new Element(Constants.EncryptWith, SchemaUtil.TYPE_STRING);

		/* for now... to come: KDF and POP-Token */

		this.addElement(id);
		this.addElement(sig);
		this.addElement(enc);
	}
}
