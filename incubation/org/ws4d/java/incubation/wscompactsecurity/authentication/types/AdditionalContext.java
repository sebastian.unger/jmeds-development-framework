package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

public class AdditionalContext extends ComplexType {

	public AdditionalContext() {
		super(Constants.AddtionalContext, ComplexType.CONTAINER_ALL);

		Element contextItem = new Element(Constants.ContextItem, new ContextItem());

		contextItem.setMaxOccurs(-1); /* Unbounded */

		this.addElement(contextItem);



	}
}
