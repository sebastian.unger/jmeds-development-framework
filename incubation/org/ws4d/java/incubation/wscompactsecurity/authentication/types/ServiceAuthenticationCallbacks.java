package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;

public interface ServiceAuthenticationCallbacks {
	void populatePin(int pin, QName mechanism);
	void authenticationResult(EndpointReference ep, boolean success);
}
