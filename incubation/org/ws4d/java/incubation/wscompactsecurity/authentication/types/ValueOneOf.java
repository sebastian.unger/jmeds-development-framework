package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class ValueOneOf extends ComplexType {
	public ValueOneOf() {
		super(Constants.ValueOneOf, ComplexType.CONTAINER_ALL);
		Element v = new Element(Constants.Value, SchemaUtil.TYPE_STRING);
		v.setMaxOccurs(-1); /* Unbounded */
		Element sv = new Element(Constants.Value, SchemaUtil.TYPE_ANYTYPE);
		sv.setMaxOccurs(-1); /* Unbounded */
		this.addElement(v);
		this.addElement(sv);
	}
}
