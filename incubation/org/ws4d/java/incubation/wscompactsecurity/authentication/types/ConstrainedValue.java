package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

public class ConstrainedValue extends ComplexType {

	public ConstrainedValue() {
		super(Constants.ConstrainedValue, ComplexType.CONTAINER_CHOICE);

		this.addElement(new Element(Constants.ValueLessThan, new AbstractCompareType(Constants.ValueLessThan)));
		this.addElement(new Element(Constants.ValueLessThanOrEqual, new AbstractCompareType(Constants.ValueLessThanOrEqual)));
		this.addElement(new Element(Constants.ValueGreaterThan, new AbstractCompareType(Constants.ValueGreaterThan)));
		this.addElement(new Element(Constants.ValueGreaterThanOrEqual, new AbstractCompareType(Constants.ValueGreaterThanOrEqual)));

		this.addElement(new Element(Constants.ValueInRange, new ValueInRange()));
		this.addElement(new Element(Constants.ValueOneOf, new ValueOneOf()));

	}
}
