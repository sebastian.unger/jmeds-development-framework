package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

public class AuthenticationServiceMalformedException extends Exception {

	private static final long serialVersionUID = 3807351014441055928L;
	
	public AuthenticationServiceMalformedException() {
		super("Discovered AuthenticationService was malformed");
	}

	public AuthenticationServiceMalformedException(String message) {
		super("Discovered AuthenticationService was malformed. Detail: " + message);
	}

}
