package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;

public class Claims extends ComplexType {

	public Claims() {
		super(Constants.Claims, ComplexType.CONTAINER_ALL);

		Attribute dialect = new Attribute(new QName(Constants.ClaimTypeDialectURI), SchemaUtil.TYPE_STRING);
		dialect.setUse(USE_REQUIRED);
		dialect.setDefault(Constants.ClaimTypeDialectURI);
		this.addAttributeElement(dialect);

		Element claimType = new Element(Constants.Claim, new ClaimType());
		claimType.setMaxOccurs(-1); /* unbounded */

		this.addElement(claimType);

	}

}
