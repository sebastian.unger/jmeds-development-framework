package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types.RequestedProofToken;
import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.types.AuthECCDHParameters;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.ExtendedSimpleContent;
import org.ws4d.java.schema.SchemaUtil;

public class RequestSecurityTokenResponse extends ComplexType {

	public RequestSecurityTokenResponse() {
		super(Constants.RequestSecurityTokenResponse, ComplexType.CONTAINER_ALL);

		Element tokenType = new Element(Constants.TokenType, SchemaUtil.TYPE_STRING);
		Element requestType = new Element(Constants.RequestType, SchemaUtil.TYPE_STRING);
		Element appliesTo = new Element(Constants.AppliesTo, SchemaUtil.TYPE_STRING);
		Element onBehalfOf = new Element(Constants.OnBehalfOf, SchemaUtil.TYPE_STRING);
		Element authenticationType = new Element(Constants.AuthenticationType, SchemaUtil.TYPE_STRING);
		Element authParams = new Element(Constants.AuthECCDHParameters, new AuthECCDHParameters());

		ExtendedSimpleContent brokerType = new ExtendedSimpleContent(Constants.isBroker);
		brokerType.setBase(SchemaUtil.TYPE_STRING);
		brokerType.addAttributeElement(new Attribute(Constants.authorizerAttribute, SchemaUtil.TYPE_STRING));
		Element broker = new Element(Constants.isBroker, brokerType);
		Element reqSecToken = new Element(Constants.RequestedSecurityToken, new RequestedSecurityToken());

		Element reqProofToken = new Element(Constants.RequestedProofToken, new RequestedProofToken());

		this.addElement(tokenType);
		this.addElement(requestType);
		this.addElement(appliesTo);
		this.addElement(onBehalfOf);
		this.addElement(authenticationType);
		this.addElement(authParams);
		this.addElement(broker);
		this.addElement(reqSecToken);
		this.addElement(reqProofToken);
	}

}
