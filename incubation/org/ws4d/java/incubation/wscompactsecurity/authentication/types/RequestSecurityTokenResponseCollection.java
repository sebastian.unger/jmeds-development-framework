package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

public class RequestSecurityTokenResponseCollection extends ComplexType {

	public RequestSecurityTokenResponseCollection() {
		super(Constants.RequestSecurityTokenResponseCollection, ComplexType.CONTAINER_ALL);

		Element e = new Element(Constants.RequestSecurityTokenResponse, new RequestSecurityTokenResponse());

		this.addElement(e);

	}

}
