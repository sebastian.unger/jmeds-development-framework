package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.communication.CommunicationException;

public class UiAuthenticationFailedException extends CommunicationException {

	public UiAuthenticationFailedException() {
		this("UI Authentication failed!");
	}

	public UiAuthenticationFailedException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -7718929087682659127L;

}
