package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;

public class AbstractCompareType extends ComplexType {

	public AbstractCompareType(QName name) {
		super(name, ComplexType.CONTAINER_CHOICE);
		this.addElement(new Element(Constants.Value, SchemaUtil.TYPE_STRING));
		this.addElement(new Element(Constants.StructuredValue, SchemaUtil.TYPE_ANYTYPE));
	}
}
