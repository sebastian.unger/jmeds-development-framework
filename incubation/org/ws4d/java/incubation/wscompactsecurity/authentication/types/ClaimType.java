package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;

public class ClaimType extends ComplexType {

	ClaimType() {
		super(Constants.ClaimType, ComplexType.CONTAINER_ALL);

		Attribute uriAttribute = new Attribute(new QName("Uri"), SchemaUtil.TYPE_STRING);
		uriAttribute.setUse(USE_REQUIRED);
		this.addAttributeElement(uriAttribute);

		Element displayName = new Element(Constants.DisplayName, SchemaUtil.TYPE_STRING);
		Element description = new Element(Constants.Description, SchemaUtil.TYPE_STRING);
		Element displayValue = new Element(Constants.DisplayValue, SchemaUtil.TYPE_STRING);
		Element value = new Element(Constants.Value, SchemaUtil.TYPE_STRING);
		Element encryptedValue = new Element(Constants.EncryptedValue, new EncryptedValue());
		Element structuredValue = new Element(Constants.StructuredValue, SchemaUtil.TYPE_ANYTYPE);
		Element constrainedValue = new Element(Constants.ConstrainedValue, new ConstrainedValue());

		this.addElement(displayName);
		this.addElement(description);
		this.addElement(displayValue);
		this.addElement(value);
		this.addElement(encryptedValue);
		this.addElement(structuredValue);
		this.addElement(constrainedValue);

	}

}
