package org.ws4d.java.incubation.wscompactsecurity.authentication.types;

import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.QName;

public interface ClientAuthenticationCallbacks {
	/**
	 * Callback to start OOB Pin exchange user interface and to deliver
	 * back the exchanged pin
	 * @param mechanism UI mechanism to be used
	 * @return the exchanged OOB PINs
	 */
	int getOOBpinAsInt(QName mechanism);
	/**
	 * Once authentication was successful, the established trust context
	 * is delivered by this callback
	 * @param ct
	 */
	void deliverAuthenticationData(SecurityContext ct);
	/**
	 * To provide a seamless user experience it is sometimes important to
	 * provide the detected OOB mechanism as early as possible. Do this here.
	 * @param mechanism
	 */
	void prereportMechanism(QName mechanism);
	/**
	 * Sometimes, Authentication clients need additional information that can not
	 * be provided through the API interface. Use this callback by putting
	 * key-value-whatevers into a Hashmap to be delivered.
	 * @return
	 */
	HashMap receiveAdditionalAuthenticationData();
	void provideAdditionalAuthenticationData(Object key, Object data);

}
