package org.ws4d.java.incubation.wscompactsecurity.authentication;

import org.ws4d.java.types.QName;

public class Constants {

	/* FIXME: There is a sick issue regarding namespaces. Something absolutely
	 * didn't work with the correct namespaces, so for the sake of getting sh*t
	 * done, I used the ws4d Namespace for everything.
	 *
	 * ********************************************************************** */
	public static final String Namespace = "http://www.ws4d.org";
	public static final String Namespace_WS_Security = Namespace;//"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	public static final String Namespace_WS_Trust = Namespace;//"http://docs.oasis-open.org/ws-sx/ws-trust/200512";
	public static final String Namespace_WS_SecureConversation = Namespace;//"http://docs.oasis-open.org/ws-sx/ws-secureconversation/200512";
	public static final String Namespace_WS_Policy = Namespace;//"http://schemas.xmlsoap.org/ws/2004/09/policy";
	public static final String Namespace_WS_Security_Utility = Namespace;//"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	public static final String Namespace_WS_Federation_fed = Namespace; //"http://docs.oasis-open.org/wsfed/federation/200706";
	public static final String Namespace_WS_Federation_auth = Namespace; //"http://docs.oasis-open.org/wsfed/authorization/200706";
	public static final String Namespace_XML_Encryption = Namespace; //"BLA BLA BLA XENC";
	public static final String Namespace_WS_SAML = Namespace; // Naaahh... not really. But... you know...
	public static final String AuthenticationServiceTestPortType = "SimpleTestOperations";

	/* WST and WSC Bindings and WSA Actions */
	/* WSC SCT WSA Actions */
	public static final String WST_SCT_BINDING_RST_SCT_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/SCT";
	public static final String WST_SCT_BINDING_RSTR_SCT_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/SCT";

	/* WST ISSUE Binding and Actions */
	public static final String WST_ISSUE_BINDING = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue";
	public static final String WST_ISSUE_BINDING_RST_ISSUE_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue";
	public static final String WST_ISSUE_BINDING_RSTR_ISSUE_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/Issue";
	public static final String WST_ISSUE_BINDING_RSTRC_ISSUEFINAL_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTRC/IssueFinal";

	/* WST VALIDATE Binding and Actions */
	public static final String WST_VALIDATE_BINDING = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/Validate";
	public static final String WST_VALIDATE_BINDING_RST_VALIDATE_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Validate";
	public static final String WST_VALIDATE_BINDING_RSTR_VALIDATE_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/Validate";
	public static final String WST_VALIDATE_BINDING_RSTR_VALIDATEFINAL_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/ValidateFinal";
	public static final String WST_VALIDATE_TOKEN_TYPE = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/Status";
	public static final String WST_VALIDATE_BROKERED_TOKEN_TYPE = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/SCT";

	/* WSC SCT WSA Actions */
	public static final String WST_SAML_BINDING_RST_ISSUE_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/SAML";
	public static final String WST_SAML_BINDING_RSTRC_ISSUEFINAL_ACTION = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTRC/SAML";

	/* Public Key */
	public static final QName auth_ecc_dh_public_key = new QName("auth-ecc-dh-public-key", Constants.Namespace);
	public static final QName auth_ecc_dh_public_key_X = new QName("auth-ecc-dh-public-key-X", Constants.Namespace);
	public static final QName auth_ecc_dh_public_key_Y = new QName("auth-ecc-dh-public-key-Y", Constants.Namespace);

	/* Authenticated Elliptic Curve Diffie Hellman Parameters */
	public static final QName AuthECCDHParameters = new QName("AuthECCDHParameters", Constants.Namespace);
	public static final QName auth_ecc_dh_curvename = new QName("auth-ecc-dh-curvename", Constants.Namespace);
	public static final QName attr_encoding_type = new QName("EncodingType", Constants.Namespace_WS_Security);
	public static final QName auth_ecc_dh_nonce = new QName("auth-ecc-dh-nonce", Constants.Namespace);
	/* public key: see above */
	public static final QName auth_ecc_dh_cmac = new QName("auth-ecc-dh-cmac", Constants.Namespace);
	public static final QName auth_ecc_dh_numerical_pin = new QName("auth-ecc-dh-numerical-pin", Constants.Namespace);

	/* Requested Security Token */
	public static final QName RequestedSecurityToken = new QName("RequestedSecurityToken", Constants.Namespace_WS_Trust);

	/* Security Context Token */
	public static final QName SecurityContextToken = new QName("SecurityContextToken", Constants.Namespace_WS_SecureConversation);
	public static final QName Identifier = new QName("Identifier", Constants.Namespace_WS_Trust);
	public static final QName SignWith = new QName("SignWith", Constants.Namespace_WS_Trust);
	public static final QName EncryptWith = new QName("EncryptWith", Constants.Namespace_WS_Trust);

	/* Requested Proof Token */
	public static final QName RequestedProofToken = new QName("RequestedProofToken", Constants.Namespace_WS_Trust);
	public static final QName BinarySecret = new QName("BinarySecret", Constants.Namespace_WS_Trust);

	/* Request Security Token */
	public static final QName RequestSecurityToken = new QName("RequestSecurityToken", Constants.Namespace_WS_Trust);
	public static final QName TokenType = new QName("TokenType", Constants.Namespace_WS_Trust);
	public static final QName RequestType = new QName("RequestType", Constants.Namespace_WS_Trust);
	public static final QName AppliesTo = new QName("AppliesTo", Constants.Namespace_WS_Policy);
	public static final QName OnBehalfOf = new QName("OnBehalfOf", Constants.Namespace_WS_Trust);
	public static final QName AuthenticationType = new QName("AuthenticationType", Constants.Namespace_WS_Trust);
	// AuthParams : see above
	public static final QName RequestedSignatureAlgorithms = new QName("SignWith", Constants.Namespace_WS_Trust);
	public static final QName RequestedEncryptionAlgorithms = new QName("EncryptWith", Constants.Namespace_WS_Trust);

	/* RST - Authorization Parameters */
	public static final String AuthorizationTokenType = "urn:oasis:names:tc:SAML:1.0:assertion";
	public static final String WST_VALIDATE_AUTHORIZATION_TOKEN_TYPE = AuthorizationTokenType;
	public static final QName AddtionalContext = new QName("AdditionalContext", Constants.Namespace_WS_Federation_auth);
	public static final QName ContextItem = new QName("ContextItem", Namespace_WS_Federation_auth);
	public static final String ContextItemNameAttribute = "Name";
	public static final String ContextItemScopeAttribute = "Scope";
	public static final String ContextItemRequestorScope = "http://docs.oasis-open.org/wsfed/authorization/200706/ctx/requestor";
	public static final String ContextItemTargetScope = "http://docs.oasis-open.org/wsfed/authorization/200706/ctx/target";
	public static final String ContextItemActionScope = "http://docs.oasis-open.org/wsfed/authorization/200706/ctx/action";
	public static final QName Claims = new QName("Claims", Constants.Namespace_WS_Trust);
	public static final QName Claim = new QName("Claim", Constants.Namespace_WS_Federation_auth);
	public static final QName ClaimType = new QName("ClaimType", Namespace_WS_Federation_auth);
	public static final String ClaimsDialectAttribute = "Dialect";
	public static final String ClaimTypeDialectURI = "http://docs.oasis-open.org/wsfed/authorization/200706/authclaims";
	public static final String ClaimTypeOptionalAttribute = "Optional";
	public static final QName DisplayName = new QName("DisplayName", Namespace_WS_Federation_auth);
	public static final QName Description = new QName("Description", Namespace_WS_Federation_auth);
	public static final QName DisplayValue = new QName("DisplayValue", Namespace_WS_Federation_auth);
	public static final QName Value = new QName("Value", Namespace_WS_Federation_auth);
	public static final QName EncryptedValue = new QName("EncryptedValue", Namespace_WS_Federation_auth);
	public static final QName EncryptedData = new QName("EncryptedData", Namespace_XML_Encryption);
	public static final QName StructuredValue = new QName("StructuredValue", Namespace_WS_Federation_auth);
	public static final QName ConstrainedValue = new QName("ConstrainedValue", Namespace_WS_Federation_auth);
	public static final QName ValueLessThan = new QName("ValueLessThan", Namespace_WS_Federation_auth);
	public static final QName ValueLessThanOrEqual = new QName("ValueLessThanOrEqual", Namespace_WS_Federation_auth);
	public static final QName ValueGreaterThan = new QName("ValueGreaterThan", Namespace_WS_Federation_auth);
	public static final QName ValueGreaterThanOrEqual = new QName("ValueGreaterThanOrEqual", Namespace_WS_Federation_auth);
	public static final QName ValueInRange = new QName("ValueInRange", Namespace_WS_Federation_auth);
	public static final QName ValueUpperBound = new QName("ValueUpperBound", Namespace_WS_Federation_auth);
	public static final QName ValueLowerBound = new QName("ValueLowerBound", Namespace_WS_Federation_auth);
	public static final QName ValueOneOf = new QName("ValueOneOf", Namespace_WS_Federation_auth);

	/* Request Security Token Response (Collection)*/
	public static final QName RequestSecurityTokenResponse = new QName("RequestSecurityTokenResponse", Constants.Namespace_WS_Trust);
	/* rest: see above */
	public static final QName RequestSecurityTokenResponseCollection = new QName("RequestSecurityTokenResponseCollection", Constants.Namespace_WS_Trust);
	public static final QName isBroker = new QName("isBroker", Constants.Namespace);
	public static final QName authorizerAttribute = new QName("Authorizer", Namespace);

	/* Validate RST(R) */
	public static final QName ValidateTarget = new QName("ValidateTarget", Namespace_WS_Trust);
	public static final QName Status = new QName("Status", Namespace_WS_Trust);
	public static final QName Code = new QName("Code", Namespace_WS_Trust);
	public static final String StatusCodeValid = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/status/valid";
	public static final String StatusCodeInvalid = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/status/invalid";
	public static final QName Reason = new QName("Reason", Namespace_WS_Trust);
	/* FIXME just a dirty QuickFix: */
	public static final QName VRequestSecurityToken = new QName("VRequestSecurityToken", Constants.Namespace_WS_Trust);
	public static final QName VRequestSecurityTokenResponse = new QName("VRequestSecurityTokenResponse", Constants.Namespace_WS_Trust);

}
