package org.ws4d.java.incubation.wscompactsecurity.authentication.engine;

import java.io.IOException;
import java.util.UUID;

import org.ws4d.java.incubation.CredentialManagement.ContextDBFactory;
import org.ws4d.java.incubation.CredentialManagement.PersistenceFactory;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SimpleContextDB;
import org.ws4d.java.incubation.CredentialManagement.SimpleContextDBPersistenceInterface;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.arne.compactsecurity.SecurityControl;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper_Factory;
import org.ws4d.wscompactsecurity.authentication.crypto.types.ECC_DH_HelperType;

//TODO
// Right now, only a single authentication process can be covered at a time. Not ideal...

public class AuthenticationEngine {

	private static AuthenticationEngine instance = null;
	public static AuthenticationEngine getInstance() {
		if (instance == null) {
			instance = new AuthenticationEngine();
			instance.setState(STATE_NONE_ACTIVE);
		}
		return instance;
	}

	private boolean isABroker = false;
	public boolean isABroker() {
		return isABroker;
	}
	public void setIsABroker(boolean isABroker) {
		this.isABroker = isABroker;
	}
	public ArrayList getBrokers() {
		ArrayList brokers = new ArrayList();
		Iterator it = getContextDatabase().getAllContexts().values().iterator();
		synchronized(this) {
			while (it.hasNext()) {
				SecurityContext sc = (SecurityContext) it.next();
				if (sc.isBroker()) {
					brokers.add(sc.getContextReference());
				}
			}
		}
		if (brokers.isEmpty())
			return null;
		else {
			return brokers;
		}
	}

	private ArrayList brokerables = null; /* in case I am an Authentictator, this is the place where I save the Endpoints for whom I am a broker */

	public void addBrokerable(String uri) {
		if (brokerables == null) {
			brokerables = new ArrayList();
		}
		brokerables.add(uri);
	}

	public ArrayList getBrokerables() {
		return brokerables;
	}

	public boolean isABrokerable(String uri) {
		return brokerables.contains(uri);
	}

	/* might possibly be used to block pending authentications */
	public static final int STATE_NONE_ACTIVE = 0;
	public static final int STATE_AUTHENTICATION_STARTED = 1;
	public static final int STATE_POPULATING_PIN = 2;
	public static final int STATE_POPULATED_PIN = 3;
	public static final int STATE_AUTHENTICATION_RESUMED = 4;

	private int state;
	public int getState() {
		return state;
	}
	public void setState(int s) {
		this.state = s;
	}

	private static final String dbNameDefault = "/home/pi/keys";
	private String dbName = null;
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	SimpleContextDBPersistenceInterface persistence = null;
	public SimpleContextDBPersistenceInterface getPersistence() {
		if (persistence == null) {
			persistence = PersistenceFactory.getPersistence();
		}
		return persistence;
	}
	public void setPersistence(SimpleContextDBPersistenceInterface persistence) {
		this.persistence = persistence;
	}

	private QNameSet supportedOOBauthenticationMechanisms = null;
	public QNameSet getSupportedOOBauthenticationMechanisms() {
		return supportedOOBauthenticationMechanisms;
	}
	public void setSupportedOOBauthenticationMechanisms(
			QNameSet supportedOOBauthenticationMechanisms) {
		this.supportedOOBauthenticationMechanisms = supportedOOBauthenticationMechanisms;
	}
	public void addSupportedOOBauthenticationMechanism(QName mechanism) {
		if (supportedOOBauthenticationMechanisms == null) {
			supportedOOBauthenticationMechanisms = new QNameSet();
		}
		supportedOOBauthenticationMechanisms.add(mechanism);
	}
	public void addSupportedOOBauthenticationMechanisms(QNameSet mechanisms) {
		if (supportedOOBauthenticationMechanisms == null) {
			supportedOOBauthenticationMechanisms = new QNameSet();
		}
		supportedOOBauthenticationMechanisms.addAll(mechanisms);
	}


	private ArrayList supportedAlgorithms = null;
	public ArrayList getSupportedAlgorithms() {
		return supportedAlgorithms;
	}
	public void setSupportedAlgorithms(ArrayList supportedAlgorithms) {
		this.supportedAlgorithms = supportedAlgorithms;
	}
	public void addSupportedAlgorithmsSet(SecurityAlgorithmSet set) {
		if (this.supportedAlgorithms == null) {
			supportedAlgorithms = new ArrayList();
		}
		supportedAlgorithms.add(set);
	}
	private String algListString(int type, char delimiter) {
		// 0 - Signature
		// 1 - Encryption
		// 2 - Derivation
		ArrayList algorithms = new ArrayList();
		for (int i = 0 ; i < supportedAlgorithms.size(); i++) {
			SecurityAlgorithmSet set = (SecurityAlgorithmSet) supportedAlgorithms.get(i);
			QName algorithm = null;
			switch (type) {
			case 0:
				algorithm = set.getSignatureAlgorithm();
				break;
			case 1:
				algorithm = set.getEncryptionAlgorithm();
				break;
			case 2:
				algorithm = set.getDerivationAlgorithm();
				break;
			default:
				return null;
			}
			if (algorithm != null) {
				if (!algorithms.contains(algorithm)) {
					algorithms.add(algorithm);
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("");
		for (int i = 0; i < algorithms.size(); i++) {
			if (i != 0)
				sb.append(delimiter);
			sb.append(((QName)algorithms.get(i)).toString());
		}
		return sb.toString();
	}
	public String getSupportedSignatureAlgorithmsAsString() {
		return getSupportedSignatureAlgorithmsAsString(',');
	}
	public String getSupportedSignatureAlgorithmsAsString(char delimiter) {
		return algListString(0, delimiter);
	}
	public String getSupportedEncryptionAlgorithmsAsString() {
		return getSupportedEncryptionAlgorithmsAsString(',');
	}
	public String getSupportedEncryptionAlgorithmsAsString(char delimiter) {
		return algListString(1, delimiter);
	}
	public String getSupportedDerivationAlgorithmsAsString() {
		return getSupportedDerivationAlgorithmsAsString(',');
	}
	public String getSupportedDerivationAlgorithmsAsString(char delimiter) {
		return algListString(2, delimiter);
	}


	/* Device (esp. client) ID */
	private static String ownerID = null;
	public static void setDefaultOwnerID(String id) {
		ownerID = id;
	}
	public static String getDefaultOwnerID() {
		return ownerID;
	}
	public static String getSafeDefaultOwnerID() {
		if (ownerID == null)
			ownerID = "urn:uuid:" + UUID.randomUUID().toString();
		return getDefaultOwnerID();
	}


	private ECC_DH_HelperType mCryptoHelper = null;
	public ECC_DH_HelperType getCryptoHelper() {
		return mCryptoHelper;
	}

	public void setCryptoHelper(ECC_DH_HelperType cryptoHelper) {
		this.mCryptoHelper = cryptoHelper;
	}

	public void reset() {
		this.mCryptoHelper = null;
		setCryptoHelper(ECC_DH_Helper_Factory.getHelper());
		this.state = STATE_NONE_ACTIVE;
		/* Just a hack. This is by far not sufficient */
		SecurityControl.useBodyencryption = false;
		SecurityControl.useSignature = false;
	}


	public SimpleContextDB getContextDatabase() {
		return ContextDBFactory.getContextDatabase(ownerID);
	}

	private AuthenticationEngine() {
		setCryptoHelper(ECC_DH_Helper_Factory.getHelper());
		setDbName(dbNameDefault);

	}

	public boolean removeSecurityAssociation(String id) {
		return getContextDatabase().removeContext(id);
	}

	public void removeAllSecurityAssociations() {
		removeAllSecurityAssociations(true);
	}

	public void removeAllSecurityAssociations(boolean restart) {
		getPersistence().clearDatabase(getContextDatabase(), dbName);
		if (restart && DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance() != null) {
			synchronized (DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance()) {
				try {
					DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().stop();
					DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().start();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		this.getContextDatabase().clearContexts();
		/* Just a hack. This is by far not sufficient */
		SecurityControl.useBodyencryption = false;
		SecurityControl.useSignature = false;
	}

	public void saveSecurityAssociations() {
		getPersistence().saveToFile(getContextDatabase(), dbName);
	}

	public boolean loadSecurityAssociations() {
		SimpleContextDB db = getContextDatabase();
		if (db.size() > 0) {
			/* basically this means that they already have been read
			 * e.g. because we are under Android and the are read
			 * everytime the db is accessed */
			return true;
		}
		/* I feel, there is no really good place for reading the authorizer from the contexts
		 * However, I also feel, that there are a couple of places that are WAY worse */
		if (getPersistence().loadFromFile(db, dbName)) {
			Iterator it = db.getAllContexts().values().iterator();
			while (it.hasNext()) {
				SecurityContext ct = (SecurityContext) it.next();
				if (ct.getAuthorizerHint() == 1) {
					AuthorizationEngine.getInstance().setPrimaryAuthorizer(ct.getContextReference());
				} else if (ct.getAuthorizerHint() == 2) {
					AuthorizationEngine.getInstance().addSecondaryAuthorizer(ct.getContextReference());
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
