package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SecurityToken;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.AuthorizationConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types.VRequestSecurityToken;
import org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types.VRequestSecurityTokenResponse;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class DefaultValidateOperation extends Operation {

	public DefaultValidateOperation() {
		super (WSSecurityForDevicesConstants.STSAuthenticationValidateMethodName, QName.construct(WSSecurityForDevicesConstants.STSAuthenticationBrokeredValidateBindingPortType));

		this.setInputAction(Constants.WST_VALIDATE_BINDING_RST_VALIDATE_ACTION);
		this.setOutputAction(Constants.WST_VALIDATE_BINDING_RSTR_VALIDATEFINAL_ACTION);

		Element in  = new Element(Constants.VRequestSecurityToken, new VRequestSecurityToken());
		Element out = new Element(Constants.VRequestSecurityTokenResponse, new VRequestSecurityTokenResponse());
		this.setInput(in);
		this.setOutput(out);


		/* Errors */
		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {

		String brokerAddress = ParameterValueManagement.getString(parameterValue, Constants.OnBehalfOf.getLocalPart());
		ParameterValue result = createOutputValue();
		ParameterValueManagement.setString(result, Constants.TokenType.getLocalPart(), Constants.WST_VALIDATE_TOKEN_TYPE);

		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		SecurityContext sctx = ae.getContextDatabase().getContextByReference(brokerAddress);

		/* authentication or authorization? */
		String tokenType = ParameterValueManagement.getString(parameterValue, Constants.TokenType.getLocalPart());

		if (Constants.WST_VALIDATE_BROKERED_TOKEN_TYPE.equals(tokenType)) { /* authentication */

			/* is the broker accepted? */
			if (!sctx.isBroker()) {
				ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart(), Constants.StatusCodeInvalid);
				ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Reason.getLocalPart(), "Broker " + brokerAddress + " not accepted!");
				return result;
			}

			/* hmm... all good I guess... */
			ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart(), Constants.StatusCodeValid);

			String targetAddress = ParameterValueManagement.getString(parameterValue, Constants.AppliesTo.getLocalPart());
			String identifier = ParameterValueManagement.getString(parameterValue,
					Constants.ValidateTarget.getLocalPart() + "/" +
							Constants.SecurityContextToken.getLocalPart() + "/" +
							Constants.Identifier.getLocalPart());
			String sig = ParameterValueManagement.getString(parameterValue,
					Constants.ValidateTarget.getLocalPart() + "/" +
							Constants.SecurityContextToken.getLocalPart() + "/" +
							Constants.SignWith.getLocalPart());
			String enc = ParameterValueManagement.getString(parameterValue,
					Constants.ValidateTarget.getLocalPart() + "/" +
							Constants.SecurityContextToken.getLocalPart() + "/" +
							Constants.EncryptWith.getLocalPart());
			String b64Key = ParameterValueManagement.getString(parameterValue,
					Constants.ValidateTarget.getLocalPart() + "/" +
							Constants.RequestedProofToken.getLocalPart() + "/" +
							Constants.BinarySecret.getLocalPart());
			byte[] key = Base64Util.decode(b64Key);

			/* TODO:
			 * Check if Algorithms are supported...
			 */

			ae.getContextDatabase().addContext(new SecurityContext(
					targetAddress,
					new SecurityToken(
							identifier,
							key,
							AlgorithmConstants.idByName(QName.construct(enc)),
							AlgorithmConstants.idByName(QName.construct(sig)))));

			ae.saveSecurityAssociations();
		} else if (Constants.WST_VALIDATE_AUTHORIZATION_TOKEN_TYPE.equals(tokenType)) { /* authorization */

			AuthorizationEngine aze = AuthorizationEngine.getInstance();

			/* is the broker accepted? as Authorizer */
			if (!aze.isPrimaryauthorizer(brokerAddress)) {
				ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart(), Constants.StatusCodeInvalid);
				ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Reason.getLocalPart(), "Authorizer " + brokerAddress + " not accepted!");
				return result;
			}

			String decision = ParameterValueManagement.getAttributeValue(parameterValue,
					Constants.ValidateTarget.getLocalPart() + "/" +
							AuthorizationConstants.samlAssertionElement.getLocalPart() + "/" +
							AuthorizationConstants.authzDecisionStatement.getLocalPart(),
							AuthorizationConstants.DecisionAttribute);
			if (!AuthorizationDecision.PERMIT.toString().equals(decision)) {
				Log.warn("Uhm... All good. Except for the decision. Sorry, mate!");
				ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart(), Constants.StatusCodeInvalid);
				ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Reason.getLocalPart(), "Authorization decision was " + decision);
				return result;
			}

			/* all good, I guess */
			aze.addAuthorizationRuleByURI(ParameterValueManagement.getString(parameterValue, Constants.AppliesTo.getLocalPart()), null, null);
			ParameterValueManagement.setString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart(), Constants.StatusCodeValid);


		} else {
			/* WAT?! */
			Log.error("Incompatible Token Type! " + tokenType);
			return null;
		}

		return result;
	}

}
