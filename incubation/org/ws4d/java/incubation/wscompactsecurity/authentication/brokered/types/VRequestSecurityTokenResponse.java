package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class VRequestSecurityTokenResponse extends ComplexType {

	public VRequestSecurityTokenResponse() {
		super(Constants.VRequestSecurityTokenResponse, ComplexType.CONTAINER_SEQUENCE);

		Element tokenType = new Element(Constants.TokenType, SchemaUtil.TYPE_STRING);
		Element status = new Element(Constants.Status, new Status());

		this.addElement(tokenType);
		this.addElement(status);
	}

}
