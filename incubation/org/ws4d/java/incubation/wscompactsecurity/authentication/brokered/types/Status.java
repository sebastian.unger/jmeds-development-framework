package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class Status extends ComplexType {

	public Status() {
		super(Constants.Status, ComplexType.CONTAINER_SEQUENCE);
		this.addElement(new Element(Constants.Code, SchemaUtil.TYPE_STRING));
		this.addElement(new Element(Constants.Reason, SchemaUtil.TYPE_STRING));
	}

}
