package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

/**
 * the V is for Validate... didn't want to risk confusion by only distinguishing by package
 * @author su009
 *
 */
public class VRequestSecurityToken extends ComplexType {

	public VRequestSecurityToken() {
		super(Constants.VRequestSecurityToken, ComplexType.CONTAINER_SEQUENCE);
		Element tokenType = new Element(Constants.TokenType, SchemaUtil.TYPE_STRING);
		Element requestType = new Element(Constants.RequestType, SchemaUtil.TYPE_STRING);
		Element validateTarget = new Element(Constants.ValidateTarget, new ValidateTarget());
		/* the following two appear not to completely conform with the spec */
		Element appliesTo = new Element(Constants.AppliesTo, SchemaUtil.TYPE_STRING); /* second half of trust relationship (e.g. Client) */
		Element onBehalfOf = new Element(Constants.OnBehalfOf, SchemaUtil.TYPE_STRING); /* The Broker's UUID */

		this.addElement(tokenType);
		this.addElement(requestType);
		this.addElement(validateTarget);
		this.addElement(appliesTo);
		this.addElement(onBehalfOf);

	}

}
