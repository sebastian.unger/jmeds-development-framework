package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.SecurityContextToken;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.saml.SAMLAssertionElement;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;

public class ValidateTarget extends ComplexType {

	public ValidateTarget() {
		super(Constants.ValidateTarget, ComplexType.CONTAINER_SEQUENCE);

		this.addElement(new Element(Constants.SecurityContextToken, new SecurityContextToken()));
		this.addElement(new Element(Constants.RequestedProofToken, new RequestedProofToken()));

		this.addElement(new SAMLAssertionElement());

	}

}
