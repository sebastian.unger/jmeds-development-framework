package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class RequestedProofToken extends ComplexType {

	public RequestedProofToken() {
		super(Constants.RequestedProofToken, ComplexType.CONTAINER_ALL);

		Element binarySecret = new Element(Constants.BinarySecret, SchemaUtil.TYPE_STRING);

		this.addElement(binarySecret);

	}

}
