package org.ws4d.java.incubation.wscompactsecurity.authentication.brokered;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityToken;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityTokenResponseCollection;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class DefaultBrokerOperation extends Operation {

	DefaultAuthenticationClient authenticationClient = null;

	public DefaultBrokerOperation() {
		this(null);
	}

	public DefaultBrokerOperation(DefaultAuthenticationClient client) {
		super(WSSecurityForDevicesConstants.STSAuthenticationBrokerMethodName, QName.construct(WSSecurityForDevicesConstants.STSAuthenticationBrokeredBindingPortType));

		authenticationClient = client;

		/* wsa:Actions */
		this.setInputAction(Constants.WST_SCT_BINDING_RST_SCT_ACTION);
		this.setOutputAction(Constants.WST_SCT_BINDING_RSTR_SCT_ACTION);

		/* in- and output */
		Element in = new Element(Constants.RequestSecurityToken, new RequestSecurityToken());
		Element out = new Element(Constants.RequestSecurityTokenResponseCollection, new RequestSecurityTokenResponseCollection());
		this.setInput(in);
		this.setOutput(out);

		/* Errors */
		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {

		String msg = "";
		if (authenticationClient == null) {
			msg = "DefaultBrokerOperation.invoke(): Don't have an authentication client! Will fail.";
		} else if (authenticationClient.getAuthenticationCallbacks() == null) {
			msg = "DefaultBrokerOperation.invoke(): Have an authentication client but it has no callbacks implemented! Will fail.";
		}
		if (authenticationClient == null || authenticationClient.getAuthenticationCallbacks() == null) {
			Log.error(msg);
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error! Client Components not set!");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		final String sTarget = ParameterValueManagement.getString(parameterValue, Constants.AppliesTo.getLocalPart());
		final String sOrigin = ParameterValueManagement.getString(parameterValue, Constants.OnBehalfOf.getLocalPart());
		final String sigAlgs = ParameterValueManagement.getString(parameterValue, Constants.SignWith.getLocalPart());
		final String encAlgs = ParameterValueManagement.getString(parameterValue, Constants.EncryptWith.getLocalPart());

		authenticationClient.getAuthenticationCallbacks().provideAdditionalAuthenticationData("sig", sigAlgs);
		authenticationClient.getAuthenticationCallbacks().provideAdditionalAuthenticationData("enc", encAlgs);

		ParameterValue result = createOutputValue();

		Log.info("DefaultBrokerOperation.invokeImpl(): target = " + sTarget + ", origin = " + sOrigin);

		int authenticationResult = authenticationClient.authenticate(
				new EndpointReference(new AttributedURI(sTarget)),
				null,
				new EndpointReference(new AttributedURI(sOrigin)),
				true);

		if (authenticationResult != DefaultAuthenticationClient.ERR_NO_ERROR) {
			Log.error("Brokered Authentication returned Error Code " + authenticationResult);
			throw new InvocationException(new Fault("SimpleError"), new QName(Integer.toString(authenticationResult)));
		}

		StringBuilder sb = new StringBuilder();
		sb.append("b");
		sb.append(sOrigin);
		String reference = sb.toString();
		Log.info("Trying to access authResult by reference " + reference);
		SecurityContext authResult = AuthenticationEngine.getInstance().getContextDatabase().getContextByReference(reference);

		if (authResult == null) {
			// I feel sooo dirty. It's Friday afternoon though. Please, have mercy when you judge me...
			// FIXME!!!
			StringBuilder sc = new StringBuilder();
			sc.append("b");
			sc.append(sTarget);
			reference = sc.toString();
			Log.info("No success. Trying to access authResult by reference " + reference + " instead ... :'(");
			authResult = AuthenticationEngine.getInstance().getContextDatabase().getContextByReference(reference);
			if (authResult != null) {
				Log.info("success");
			} else {
				Log.info("no success either");
			}
		}

		Log.info("DefaultBrokerOperation.invokeImpl(): authResult = " + authResult.toString());

		/* There is no need to save it - it is only written in the database to be delivered to this point */
		AuthenticationEngine.getInstance().getContextDatabase().removeContext(reference);
		AuthenticationEngine.getInstance().saveSecurityAssociations();

		ParameterValueManagement.setString(result,
				Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
						Constants.RequestedSecurityToken.getLocalPart() + "/" +
						Constants.SecurityContextToken.getLocalPart() + "/" +
						Constants.Identifier.getLocalPart(), authResult.getSecurityToken().getIdentification());
		ParameterValueManagement.setString(result,
				Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
						Constants.RequestedSecurityToken.getLocalPart() + "/" +
						Constants.SecurityContextToken.getLocalPart() + "/" +
						Constants.SignWith.getLocalPart(), ((QName)AlgorithmConstants.algorithmString.get(authResult.getSecurityToken().getsAlgorithmType())).toString());
		ParameterValueManagement.setString(result,
				Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
						Constants.RequestedSecurityToken.getLocalPart() + "/" +
						Constants.SecurityContextToken.getLocalPart() + "/" +
						Constants.EncryptWith.getLocalPart(), ((QName)AlgorithmConstants.algorithmString.get(authResult.getSecurityToken().geteAlgorithmType())).toString());
		ParameterValueManagement.setString(result,
				Constants.RequestSecurityTokenResponse.getLocalPart() + "/" +
						Constants.RequestedProofToken.getLocalPart() + "/" +
						Constants.BinarySecret.getLocalPart(), authResult.getSecurityToken().getKeymaterialB64());

		authResult = null;

		return result;
	}


}
