package org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.types;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.ExtendedSimpleContent;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;

public class AuthECCDHParameters extends ComplexType {

	public AuthECCDHParameters() {
		super(Constants.AuthECCDHParameters, ComplexType.CONTAINER_ALL);

		Element curveName = new Element(Constants.auth_ecc_dh_curvename, SchemaUtil.TYPE_STRING);

		Attribute encAttr = new Attribute(Constants.attr_encoding_type, SchemaUtil.TYPE_STRING);
		encAttr.setUse(SchemaConstants.USE_OPTIONAL);

		ExtendedSimpleContent sc = new ExtendedSimpleContent(new QName ("sc", Constants.Namespace));
		sc.setBase(SchemaUtil.TYPE_STRING);
		sc.addAttributeElement(encAttr);

		Element nonce = new Element(Constants.auth_ecc_dh_nonce, sc);

		Element publicKey = new Element(Constants.auth_ecc_dh_public_key, sc);
		Element cmac = new Element(Constants.auth_ecc_dh_cmac, sc);

		Element pin = new Element(Constants.auth_ecc_dh_numerical_pin, SchemaUtil.TYPE_STRING);

		this.addElement(curveName);
		this.addElement(nonce);
		this.addElement(publicKey);
		this.addElement(cmac);
		this.addElement(pin);

	}

}
