package org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityTokenResponse;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityTokenResponseCollection;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.Operation;
import org.ws4d.java.types.QName;

public abstract class DefaultECC_DH2 extends Operation {

	public DefaultECC_DH2() {
		super(WSSecurityForDevicesConstants.STSAuthenticationECDH2MethodName, QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType));

		Element in = new Element(Constants.RequestSecurityTokenResponse, new RequestSecurityTokenResponse());
		Element out = new Element(Constants.RequestSecurityTokenResponseCollection, new RequestSecurityTokenResponseCollection());

		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);

		this.setInputAction(Constants.WST_ISSUE_BINDING_RSTR_ISSUE_ACTION);
		this.setOutputAction(Constants.WST_ISSUE_BINDING_RSTRC_ISSUEFINAL_ACTION);

		this.setInput(in);
		this.setOutput(out);
	}
}
