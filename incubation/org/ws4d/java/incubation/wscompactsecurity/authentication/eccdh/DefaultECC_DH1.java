package org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityToken;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.RequestSecurityTokenResponse;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.Operation;
import org.ws4d.java.types.QName;

public abstract class DefaultECC_DH1 extends Operation {

	public DefaultECC_DH1() {
		super(WSSecurityForDevicesConstants.STSAuthenticationECDH1MethodName, QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType));

		Element in = new Element(Constants.RequestSecurityToken, new RequestSecurityToken());
		Element out = new Element(Constants.RequestSecurityTokenResponse, new RequestSecurityTokenResponse());

		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);

		this.setInputAction(Constants.WST_ISSUE_BINDING_RST_ISSUE_ACTION);
		this.setOutputAction(Constants.WST_ISSUE_BINDING_RSTR_ISSUE_ACTION);

		this.setInput(in);
		this.setOutput(out);
	}
}
