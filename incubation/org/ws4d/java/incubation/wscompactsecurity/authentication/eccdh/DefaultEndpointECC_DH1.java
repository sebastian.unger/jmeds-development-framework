package org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SecurityToken;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;


public class DefaultEndpointECC_DH1 extends DefaultECC_DH1 {

	ServiceAuthenticationCallbacks sab = null;

	public DefaultEndpointECC_DH1() {
		this(null);
	}

	public DefaultEndpointECC_DH1(ServiceAuthenticationCallbacks serviceAuthenticationCallback) {
		super();
		this.sab = serviceAuthenticationCallback;
	}

	public void setServiceAuthenticationCallback(ServiceAuthenticationCallbacks serviceAuthenticationCallback) {
		this.sab = serviceAuthenticationCallback;
	}

	@SuppressWarnings("unused")
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		Log.info(" ---------------- Invoked ECC_DH1 ---------------- ");

		ParameterValue result = createOutputValue();
		String authType = null;

		AuthenticationEngine engine = AuthenticationEngine.getInstance();
		if (engine.getState() != AuthenticationEngine.STATE_NONE_ACTIVE) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error! I appear to have an active Authentication!");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}
		engine.setState(AuthenticationEngine.STATE_AUTHENTICATION_STARTED);
		try {
			engine.getCryptoHelper().init();
			engine.getCryptoHelper().getParams().setOrigin(ParameterValueManagement.getString(parameterValue, Constants.AppliesTo.getLocalPart()));
			engine.getCryptoHelper().getParams().setTarget(ParameterValueManagement.getString(parameterValue, Constants.OnBehalfOf.getLocalPart()));

			authType = ParameterValueManagement.getString(parameterValue, Constants.AuthenticationType.getLocalPart());

			//FIXME: Artefact! Use engine!
			Policy authpol = this.getService().getPolicy(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName);
			QNameSet mechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(authpol);

			Log.info("Mechanisms: " + mechanisms.toString());

			if (!mechanisms.contains(QName.construct(authType))) {
				/*
				 * Do whatever you like to indicate a fatal error - authentication is not possible
				 * 
				 * However, once, the is set active, consider indirect authentication via STS
				 */
				Log.error("FATAL ERROR!! AuthenticationType " + authType +" not supported!");
			}

			engine.getCryptoHelper().getParams().addOtherA(authType);
			engine.getCryptoHelper().getParams().addOtherB(authType);

			/* Check requested Algorithms
			 * The deal here is that a device may feel free
			 * to reject a certain combination of algorithms
			 * but this cannot be reflected when requesting
			 * algorithms (error in design). So device should
			 * check if it's fine w/ the picked combination.
			 * **************************************************** */
			boolean algorithmsOK = false;
			ArrayList algorithms = engine.getSupportedAlgorithms();
			QName reqSigAlg = QName.construct(ParameterValueManagement.getString(parameterValue, Constants.SignWith.getLocalPart()));
			QName reqEncAlg = QName.construct(ParameterValueManagement.getString(parameterValue, Constants.EncryptWith.getLocalPart()));
			QName reqKDFalg = null;//QName.construct(ParameterValueManagement.getString(parameterValue, "????????"));
			for (int i = 0 ; i < algorithms.size(); i++) {
				SecurityAlgorithmSet saset = (SecurityAlgorithmSet) algorithms.get(i);
				if ((reqSigAlg == null && saset.getSignatureAlgorithm() == null) || reqSigAlg.equals(saset.getSignatureAlgorithm())) {
					if ((reqEncAlg == null && saset.getEncryptionAlgorithm() == null) || reqEncAlg.equals(saset.getEncryptionAlgorithm())) {
						if ((reqKDFalg == null && saset.getDerivationAlgorithm() == null) || reqKDFalg.equals(saset.getDerivationAlgorithm())) {
							algorithmsOK = true;
						}
					}
				}
			}
			if (!algorithmsOK) {
				Log.error("FATAL ERROR!! Requested Algorithms Combination not possible: "
						+ ((reqSigAlg == null) ? "null" : reqSigAlg.toString()) + ", "
						+ ((reqEncAlg == null) ? "null" : reqEncAlg.toString()) + ", "
						+ ((reqKDFalg == null) ? "null" : reqKDFalg.toString()));
				/* Do whatever you feel is necessary */
			}
			if (reqSigAlg != null)
				engine.getCryptoHelper().getParams().addOtherB(reqSigAlg.toString());
			if (reqEncAlg != null)
				engine.getCryptoHelper().getParams().addOtherB(reqEncAlg.toString());
			if (reqKDFalg != null)
				engine.getCryptoHelper().getParams().addOtherB(reqKDFalg.toString());

			/* Device may set the Security Token's ID. For now, it picks a combination of Target and Origin ID */
			StringBuilder idb = new StringBuilder();
			idb.append(engine.getCryptoHelper().getParams().getTarget());
			idb.append("/");
			idb.append(engine.getCryptoHelper().getParams().getOrigin());

			boolean isOriginBroker = false;
			boolean isOriginAuthorizer = false;
			/* also, check if origin offers itself as broker and authorizer */
			if (ParameterValueManagement.getString(parameterValue, Constants.isBroker.getLocalPart()) != null) {
				isOriginBroker = true;
				if ("true".equals(ParameterValueManagement.getAttributeValue(parameterValue, Constants.isBroker.getLocalPart(), Constants.authorizerAttribute))) {
					isOriginAuthorizer = true;
				}
			}

			/* save preliminary Token and Context */
			SecurityToken ak = new SecurityToken(idb.toString(), (byte[])null, AlgorithmConstants.idByName(reqEncAlg), AlgorithmConstants.idByName(reqSigAlg));
			SecurityContext ct = new SecurityContext(engine.getCryptoHelper().getParams().getTarget(), ak);
			ct.setIsBroker(isOriginBroker);
			engine.getContextDatabase().addContext(ct);
			if (isOriginAuthorizer) {
				if (AuthorizationEngine.getInstance().setPrimaryAuthorizer(engine.getCryptoHelper().getParams().getTarget())) {
					AuthorizationEngine.getInstance().freshPrimaryAccept(true);
					ct.setAuthorizerHint(1);
					Log.info("Saved " + AuthorizationEngine.getInstance().getPrimaryAuthorizer() + " as primary authorizer.");
				} else {
					Log.info("Did not save because " + AuthorizationEngine.getInstance().getPrimaryAuthorizer() + " is my primary authorizer.");
				}
			}

			Random r = new Random(System.currentTimeMillis()); /* some seed - maybe use something more secure... */
			boolean[] oobSharedSecret = new boolean[20];
			for (int i=0; i<20; i++) {
				oobSharedSecret[i] = r.nextBoolean();
			}

			engine.getCryptoHelper().getParams().setOOBSharedSecret(oobSharedSecret);
			engine.getCryptoHelper().OOBkey2Integer();

			engine.getCryptoHelper().createNonce();
			engine.getCryptoHelper().encryptPublicKey();

			ParameterValueManagement.setString(result, Constants.TokenType.getLocalPart(), "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
			ParameterValueManagement.setString(result, Constants.RequestType.getLocalPart(), "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH1Response");
			ParameterValueManagement.setString(result, Constants.AppliesTo.getLocalPart(), engine.getCryptoHelper().getParams().getTarget());
			ParameterValueManagement.setString(result, Constants.OnBehalfOf.getLocalPart(), engine.getCryptoHelper().getParams().getOrigin());
			ParameterValueManagement.setString(result, Constants.AuthenticationType.getLocalPart(), authType);

			ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_curvename.getLocalPart(), engine.getCryptoHelper().getParams().getCurveName());
			ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart(), Base64Util.encodeBytes(engine.getCryptoHelper().getParams().getNonceA()));
			//			ParameterValueManagement.setAttributeValue(result, "AuthECCDHParameters/auth-ecc-dh-nonce", "encoding", "base64");
			ParameterValueManagement.setString(result, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart(), Base64Util.encodeBytes(engine.getCryptoHelper().getParams().getDistortedPublicKey().getEncoded()));
			//			ParameterValueManagement.setAttributeValue(result, "AuthECCDHParameters/auth-ecc-dh-public-key", "encoding", "base64");
			//FIXME: Setting Attributes results in error...


		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}

		engine.setState(AuthenticationEngine.STATE_POPULATING_PIN);
		if (this.sab != null)
			sab.populatePin(engine.getCryptoHelper().getParams().getOOBSharedSecretAsInt(), QName.construct(authType));
		engine.setState(AuthenticationEngine.STATE_POPULATED_PIN);
		return result;
	}



}
