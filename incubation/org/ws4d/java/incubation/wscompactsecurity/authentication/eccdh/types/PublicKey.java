package org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.types;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;

public class PublicKey extends ComplexType {

	public PublicKey() {
		super(Constants.auth_ecc_dh_public_key, ComplexType.CONTAINER_SEQUENCE);
		Element xElement = new Element(Constants.auth_ecc_dh_public_key_X, SchemaUtil.TYPE_STRING);
		Element yElement = new Element(Constants.auth_ecc_dh_public_key_Y, SchemaUtil.TYPE_STRING);
		this.addElement(xElement);
		this.addElement(yElement);
	}

}
