package org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SecurityToken;
import org.ws4d.java.incubation.CredentialManagement.SimpleContextDB;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;


public class DefaultEndpointECC_DH2 extends DefaultECC_DH2 {

	ServiceAuthenticationCallbacks sab = null;

	public DefaultEndpointECC_DH2() {
		this(null);
	}

	public DefaultEndpointECC_DH2(ServiceAuthenticationCallbacks serviceAuthenticationCallbacks) {
		super();
		this.sab = serviceAuthenticationCallbacks;
	}

	public void setClientAuthenticationCallback(ServiceAuthenticationCallbacks serviceAuthenticationCallbacks) {
		this.sab = serviceAuthenticationCallbacks;
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		Log.info(" ---------------- Invoked ECC_DH2 ---------------- ");

		AuthenticationEngine engine = AuthenticationEngine.getInstance();

		if (engine == null) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error! No Authentication Engine Instance!");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		if (engine.getState() != AuthenticationEngine.STATE_POPULATED_PIN) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error! No Authentication Instantiated yet!");
			engine.reset();
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		engine.setState(AuthenticationEngine.STATE_AUTHENTICATION_RESUMED);

		try {
			byte[] encodedPublicKey = Base64Util.decode(ParameterValueManagement.getString(parameterValue, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart()));
			engine.getCryptoHelper().getParams().setForeignPublicKey(engine.getCryptoHelper().decodePublicKey(encodedPublicKey));
			engine.getCryptoHelper().getParams().setForeignCMAC(Base64Util.decode(ParameterValueManagement.getString(parameterValue, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart())));
			engine.getCryptoHelper().getParams().setNonceB(Base64Util.decode(ParameterValueManagement.getString(parameterValue, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart())));
		} catch (NullPointerException e) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Parameters malformed!");
			engine.reset();
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		try {
			engine.getCryptoHelper().calculateSharedSecret();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}

		if (engine.getCryptoHelper().checkCMAC()) {
			Log.info("Alice's CMAC passed the test! All fine");
		} else {
			Log.error("Alice's CMAC did NOT pass the test! No Good!!");

			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Authentication Error! CMAC did not match!");
			engine.getContextDatabase().removeContext(engine.getCryptoHelper().getParams().getTarget());
			if (AuthorizationEngine.getInstance().wasAFreshAccept()) {
				AuthorizationEngine.getInstance().resetPrimaryAuthorizer();
			}
			engine.reset();
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}

		engine.getCryptoHelper().calculateMasterKey();

		byte[] k = engine.getCryptoHelper().getParams().getMasterKey();
		StringBuilder sb = new StringBuilder();
		sb.append("Calculated Master Key:");
		for (int i = 0; i < k.length; i++) {
			sb.append(String.format(" %02x", k[i]));
		}
		sb.append("\n");
		Log.info(sb.toString());

		ParameterValue result = createOutputValue();

		/* add token content */
		SecurityToken st = null;
		try {
			SecurityContext ct;
			synchronized(engine) {
				SimpleContextDB ctdb = engine.getContextDatabase();
				String id = engine.getCryptoHelper().getParams().getTarget();
				ct = ctdb.getContextByReference(id);
				st = ct.getSecurityToken();
				ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.Identifier.getLocalPart(), st.getIdentification());
				engine.getCryptoHelper().getParams().addOtherA(st.getIdentification());
				ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.SignWith.getLocalPart(), ((QName)AlgorithmConstants.algorithmString.get(st.getsAlgorithmType())).toString());
				engine.getCryptoHelper().getParams().addOtherA(((QName)AlgorithmConstants.algorithmString.get(st.getsAlgorithmType())).toString());
				ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.EncryptWith.getLocalPart(), ((QName)AlgorithmConstants.algorithmString.get(st.geteAlgorithmType())).toString());
				engine.getCryptoHelper().getParams().addOtherA(((QName)AlgorithmConstants.algorithmString.get(st.geteAlgorithmType())).toString());
				st.setKeymaterial(engine.getCryptoHelper().getParams().getMasterKey());
			}
			if (ct.isBroker()) {
				synchronized (DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance()) {
					try {
						Log.info("New Broker - Saving request to restart service");
						DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().getPolicies().remove(new QName(WSSecurityForDevicesConstants.BrokersPolicyName));
						DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().getPolicies().remove(new QName(WSSecurityForDevicesConstants.AuthorizersPolicyName));
						DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().stop();
						DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().start();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (NullPointerException e) {
			Log.error("FATAL! Could not retreive Context / Associated key for " + engine.getCryptoHelper().getParams().getTarget());
			e.printStackTrace();
		}

		engine.getCryptoHelper().calculateCMAC();

		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.TokenType.getLocalPart(), "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestType.getLocalPart(), "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH2Response");
		ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart(), Base64Util.encodeBytes(engine.getCryptoHelper().getParams().getCMAC()));

		if (engine.isABroker()) {
			/* I can offer myself as a broker then */
			ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), "true");
			engine.addBrokerable(engine.getCryptoHelper().getParams().getTarget());
		} else if (AuthorizationEngine.getInstance().wasAFreshAccept()){
			/* however, maybe, I want to accept it as primary Authorizer */
			Log.info("Telling Requestor that I accept it as Authorizer!");
			ParameterValueManagement.setString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), "false");
			ParameterValueManagement.setAttributeValue(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), Constants.authorizerAttribute, "true");
		}

		engine.saveSecurityAssociations();

		if (sab != null) {
			sab.authenticationResult(new EndpointReference(new AttributedURI(engine.getCryptoHelper().getParams().getTarget())), true);
		}

		engine.reset();

		engine.setState(AuthenticationEngine.STATE_NONE_ACTIVE);

		return result;
	}

}
