package org.ws4d.java.incubation.wscompactsecurity.authentication.authenticator;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultDevice;

public class DefaultGiantClassDevice extends DefaultDevice {
	public DefaultGiantClassDevice() {
		this(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
	}
	public DefaultGiantClassDevice(String communicationManagerId) {
		super(communicationManagerId);
	}
}
