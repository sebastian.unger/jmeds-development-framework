package org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SecurityToken;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.AuthenticationServiceMalformedException;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ClientAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.NoUIAuthenticatorFoundException;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.UiAuthenticationFailedException;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.BrokerPolicy;
import org.ws4d.java.incubation.wspolicy.wspolicyclient.WSSecurityPolicyClient;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;

public class DefaultAuthenticationClient extends DefaultClient {

	public static final int ERR_NO_ERROR = 0;
	public static final int ERR_NO_UIAUTHENTICATOR = 1;
	public static final int ERR_COMMUNICATION = 2;
	public static final int ERR_WEIRD = 3;
	public static final int ERR_AUTHORIZATION = 4;
	public static final int ERR_INVOCATION = 5;
	public static final int ERR_CRYPTO = 6;
	public static final int ERR_UNKNOWN_ERROR = 7;
	public static final int ERR_UIAUTHENTICATION_FAILED = 8;
	ClientAuthenticationCallbacks authenticationCB = null;

	public static long AUTHENTICATOR_DEVICE_DISCOVERY_TIMEOUT_MILLIS = 3000;

	/* in case you don't want to let an authenticator determine your favorite
	 * crypto algorithms, you may specify them here to only request a certain
	 * set   */
	private SecurityAlgorithmSet saset = null;

	public void setSecurityAlgorithmSet(SecurityAlgorithmSet set) {
		saset = set;
	}

	public void setSecurityAlgorithmSet(String sig, String enc, String kdf) {
		setSecurityAlgorithmSet(new QName(sig), new QName(enc), new QName(kdf));
	}

	public void setSecurityAlgorithmSet(QName sig, QName enc, QName kdf) {
		saset = new SecurityAlgorithmSet(sig, enc, kdf);
	}

	public ClientAuthenticationCallbacks getAuthenticationCallbacks() {
		return authenticationCB;
	}

	public void setAuthenticationCallbacks(ClientAuthenticationCallbacks authenticationcb) {
		this.authenticationCB = authenticationcb;
	}

	public DefaultAuthenticationClient() {
	}

	public int authenticate(EndpointReference target, EndpointReference via) {
		return authenticate(target, via, false);
	}
	public int authenticate(EndpointReference target, EndpointReference via, boolean tryDirect) {
		return authenticate(target, via, null, tryDirect);
	}
	/**
	 * Automagically authenticate the best way possible
	 * @param target the Device to authenticate with
	 * @param via the UI-Authenticator to use (may or even should be null)
	 * @param origin the initiator of the authentication request (only used for brokered authentication)
	 * @param tryDirect if checked, tries authentication on its own before relying on others. Should be the case for (UI)-Authenticators
	 * @return statusCode.
	 */
	public int authenticate(EndpointReference target, EndpointReference via, EndpointReference origin, boolean tryDirect) {
		DeviceReference targetRef = getDeviceReference(target, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		DeviceReference viaRef = null;
		DeviceReference originRef = null;
		if (via != null) {
			viaRef = getDeviceReference(via, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		}
		if (origin != null) {
			originRef = getDeviceReference(origin, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		}
		return authenticate(targetRef, viaRef, originRef, tryDirect, true);
	}

	public int authenticate(DeviceReference target, DeviceReference via) {
		return authenticate(target, via, false);
	}
	public int authenticate(DeviceReference target, DeviceReference via, boolean tryDirect) {
		return authenticate(target, via, null, tryDirect, true);
	}
	/**
	 * Automagically authenticate the best way possible
	 * @param target the Device to authenticate with
	 * @param via the UI-Authenticator to use (may or even should be null)
	 * @param origin the initiator of the authentication request (only used for brokered authentication)
	 * @param tryDirect if checked, tries authentication on its own before relying on others. Should be the case for (UI)-Authenticators
	 * @return statusCode.
	 */
	public int authenticate(DeviceReference target, DeviceReference via, DeviceReference origin, boolean tryDirect, boolean authenticateBrokers) {
		/* find first authenticator and / or dispatch according to types...*/

		boolean currently_brokering = (origin != null && !origin.getEndpointReference().getAddress().toString().equals(AuthenticationEngine.getDefaultOwnerID()));

		boolean success = false;
		if (tryDirect && !currently_brokering) {
			try {
				success = authenticateViaUI(target, via, tryDirect);
			} catch (NoUIAuthenticatorFoundException e) {
				e.printStackTrace();
				return ERR_NO_UIAUTHENTICATOR;
			} catch (CommunicationException e) {
				e.printStackTrace();
				return ERR_COMMUNICATION;
			} catch (AuthenticationServiceMalformedException e) {
				e.printStackTrace();
				return ERR_WEIRD;
			} catch (AuthorizationException e) {
				e.printStackTrace();
				return ERR_AUTHORIZATION;
			} catch (InvocationException e) {
				e.printStackTrace();
				return ERR_INVOCATION;
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (NoSuchProviderException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (InvalidAlgorithmParameterException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (InvalidKeyException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (Exception e) {
				e.printStackTrace();
				return ERR_UNKNOWN_ERROR;
			}
			if (success)
				return ERR_NO_ERROR;
		}

		try {
			success = authenticateBrokered(target, via, origin, authenticateBrokers);
		} catch (AuthorizationException e) {
			e.printStackTrace();
			return ERR_AUTHORIZATION;
		} catch (UiAuthenticationFailedException e){
			Log.error(e.getMessage());
			e.printStackTrace();
			return ERR_UIAUTHENTICATION_FAILED;
		} catch (CommunicationException e) {
			e.printStackTrace();
			return ERR_COMMUNICATION;
		} catch (AuthenticationServiceMalformedException e) {
			e.printStackTrace();
			return ERR_WEIRD;
		} catch (InvocationException e) {
			e.printStackTrace();
			return ERR_INVOCATION;
		}

		if (success) {
			return ERR_NO_ERROR;
		}

		if (!tryDirect) {

			try {
				success = authenticateViaUI(target, via, tryDirect);
			} catch (NoUIAuthenticatorFoundException e) {
				e.printStackTrace();
				return ERR_NO_UIAUTHENTICATOR;
			} catch (CommunicationException e) {
				e.printStackTrace();
				return ERR_COMMUNICATION;
			} catch (AuthenticationServiceMalformedException e) {
				e.printStackTrace();
				return ERR_WEIRD;
			} catch (AuthorizationException e) {
				e.printStackTrace();
				return ERR_AUTHORIZATION;
			} catch (InvocationException e) {
				e.printStackTrace();
				return ERR_INVOCATION;
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (NoSuchProviderException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (InvalidAlgorithmParameterException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (InvalidKeyException e) {
				e.printStackTrace();
				return ERR_CRYPTO;
			} catch (Exception e) {
				e.printStackTrace();
				return ERR_UNKNOWN_ERROR;
			}
			if (success)
				return ERR_NO_ERROR;
		}

		return ERR_WEIRD;

	}

	/**
	 * Same as authenticateBrokered(target, null, false); (this is actually what is called)
	 * 
	 * Use brokered authentication to authenticate with target. This is probably
	 * not what you want. You might want to check out authenticate() and let it
	 * do some work for you.
	 *
	 * @param target Device to authenticate with
	 * @return true if brokered authentication was possible, false if not
	 * @throws CommunicationException
	 * @throws AuthenticationServiceMalformedException
	 * @throws InvocationException
	 * @throws AuthorizationException
	 */
	public boolean authenticateBrokered(DeviceReference target) throws CommunicationException, AuthenticationServiceMalformedException, AuthorizationException, InvocationException {
		return authenticateBrokered(target, null, null, false);
	}

	/**
	 * Use brokered authentication to authenticate with target. This is probably
	 * not what you want. You might want to check out authenticate() and let it
	 * do some work for you.
	 *
	 * @param target Device to authenticate with
	 * @param via local Broker to pick. May be null, it is automatically detected then. This is actually the normal case.
	 * @param authenticateBroker if no trusted broker is available, determine if it should set up trust relationship with target's broker.
	 * @return true if brokered authentication was possible, false if not
	 * @throws CommunicationException
	 * @throws AuthenticationServiceMalformedException
	 * @throws InvocationException
	 * @throws AuthorizationException
	 */
	DeviceReference myBroker = null;
	boolean finishedSearching = false;
	public boolean authenticateBrokered(DeviceReference target, DeviceReference via, DeviceReference origin, boolean authenticateBroker) throws CommunicationException, AuthenticationServiceMalformedException, AuthorizationException, InvocationException {

		/* Excessive amount of internal documentation: We got these cases:
		 * 
		 *             |      origin       |       origin
		 *             |        == null    |          != null
		 *   ----------+-------------------+-----------------------
		 *    direct   | client that auths.|     BROKER
		 *      ==true | directly if nec.  |
		 *   ----------+-------------------+-----------------------
		 *    direct   | very passive      | client if origin
		 *      !=true |   client          |   == ownID
		 */


		myBroker = null;
		Device myBrokerDevice = null;

		if (origin != null && !origin.getEndpointReference().getAddress().toString().equals(AuthenticationEngine.getDefaultOwnerID())) {
			/* ************************** */
			/* this means I act as broker */
			/* ************************** */

			Log.debug("authenticateBrokered(): Entered Broker-Logic");

			/* Following scenarios are possible:
			 * 
			 * 1. target has no brokers yet
			 *    --> initiate authentication with target, then goto 2
			 *
			 * 2. target already has a broker and this is me.
			 *    --> simply create token and issue to origin and target
			 *
			 * 3. target has a broker B2 that isn't me but that I trust
			 *    --> make B2 validate a token for my origin at target
			 * 
			 * 4. target has a broker that isn't me and that I don't trust.
			 *    --> establish trust relationship with B2 and then do 1.
			 * 
			 */

			/* First check if I am the target's broker */
			HashMap secPolicies = WSSecurityPolicyClient.getAllSecurityRelatedPolicies(target);
			Policy brokerPolicy = (Policy) secPolicies.get(new QName(WSSecurityForDevicesConstants.BrokersPolicyName));
			ArrayList brokers = BrokerPolicy.getBrokers(brokerPolicy);

			boolean amItargetsBroker = false;

			if (brokerPolicy == null || brokers == null || brokers.isEmpty()) {
				/* No brokers yet. Case 1 */
				Log.info("Attention! Target hasn't published any brokers. I will try to become target's broker now.");

				boolean result = false;

				try {
					result = authenticateViaUI(target, null, true);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (result == false) {
					Log.warn("Apparently, it was not possible for me to become a broker for " + target.getEndpointReference().getAddress().toString());
					Log.warn("Will fault as I don't think it is reasonable to proceed.");
					throw new UiAuthenticationFailedException();
				} else {
					Log.info("(In)direct authentication with target successful. Will proceed as its broker");
					amItargetsBroker = true;
				}

			} else {
				/* Has brokers. Check if case 2 */
				for (int i = 0; i < brokers.size(); i++) {
					Log.debug("Testing brokers and myself. Me: " + AuthenticationEngine.getDefaultOwnerID() + ", broker to test: " + ((AttributedURI)brokers.get(i)).toString());
					if (((AttributedURI)brokers.get(i)).toString().equals(AuthenticationEngine.getDefaultOwnerID())) {
						amItargetsBroker = true;
						break;
					}
				}

			}
			if (amItargetsBroker) {
				/* I am the broker. This IS case 2 */
				Log.info("I appear to be one of " + target.getEndpointReference().getAddress().toString() + "'s brokers.");

				/* get necessary data of initial request */
				HashMap data = null;
				if (this.authenticationCB != null) {
					data = this.authenticationCB.receiveAdditionalAuthenticationData();
				}
				if (data == null) {
					Log.error("Something wrong. Did not get required data for authentication. Brokered authentication failed at unexpected position.");
					return false;
				}
				String sigAlgs = (String) data.get("sig");
				String encAlgs = (String) data.get("enc");

				Policy algPol = (Policy) secPolicies.get(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName));
				SecurityAlgorithmSet saset = WSSecurityPolicyClient.pickSecurityAlgorithms(algPol, sigAlgs, encAlgs, null);

				StringBuilder sb = new StringBuilder();
				sb.append(origin.getEndpointReference().getAddress().toString());
				sb.append("/");
				sb.append(target.getEndpointReference().getAddress().toString());
				String id = sb.toString();

				byte[] key = new byte[16];
				AuthenticationEngine.getInstance().getCryptoHelper().generateSymmetricKey(key);

				SecurityToken sct = new SecurityToken(id, key, AlgorithmConstants.idByName(saset.getEncryptionAlgorithm()), AlgorithmConstants.idByName(saset.getSignatureAlgorithm()));
				/* the preceeding b marks it as brokered - there is no need to store it */
				StringBuilder reference = new StringBuilder();
				reference.append("b");
				reference.append(origin.getEndpointReference().getAddress().toString());
				SecurityContext sctx = new SecurityContext(reference.toString(), sct);

				if (authenticationCB != null)
					authenticationCB.deliverAuthenticationData(sctx);

				Device targetDev = target.getDevice();

				ServiceReference servRef = (ServiceReference) targetDev.getServiceReferences(new QNameSet(QName.construct(Constants.WST_VALIDATE_BINDING)), SecurityKey.EMPTY_KEY).next();

				Operation op = servRef.getService().getOperation(QName.construct(Constants.WST_VALIDATE_BINDING), null, Constants.WST_VALIDATE_BINDING_RST_VALIDATE_ACTION, Constants.WST_VALIDATE_BINDING_RSTR_VALIDATEFINAL_ACTION);

				ParameterValue p = op.createInputValue();

				ParameterValueManagement.setString(p, Constants.TokenType.getLocalPart(), Constants.WST_VALIDATE_BROKERED_TOKEN_TYPE);
				ParameterValueManagement.setString(p, Constants.RequestType.getLocalPart(), Constants.WST_VALIDATE_BINDING);

				ParameterValueManagement.setString(p, Constants.ValidateTarget.getLocalPart() + "/" +
						Constants.SecurityContextToken.getLocalPart() + "/" +
						Constants.Identifier.getLocalPart(),
						sctx.getSecurityToken().getIdentification());

				ParameterValueManagement.setString(p, Constants.ValidateTarget.getLocalPart() + "/" +
						Constants.SecurityContextToken.getLocalPart() + "/" +
						Constants.SignWith.getLocalPart(),
						saset.getSignatureAlgorithm().toString());

				ParameterValueManagement.setString(p, Constants.ValidateTarget.getLocalPart() + "/" +
						Constants.SecurityContextToken.getLocalPart() + "/" +
						Constants.EncryptWith.getLocalPart(),
						saset.getEncryptionAlgorithm().toString());

				ParameterValueManagement.setString(p, Constants.ValidateTarget.getLocalPart() + "/" +
						Constants.RequestedProofToken.getLocalPart() + "/" +
						Constants.BinarySecret.getLocalPart(),
						sctx.getSecurityToken().getKeymaterialB64());

				ParameterValueManagement.setString(p, Constants.AppliesTo.getLocalPart(), origin.getEndpointReference().getAddress().toString());
				ParameterValueManagement.setString(p, Constants.OnBehalfOf.getLocalPart(), AuthenticationEngine.getDefaultOwnerID());

				ParameterValue result = op.invoke(p, CredentialInfo.EMPTY_CREDENTIAL_INFO);

				String valid = ParameterValueManagement.getString(result, Constants.Status.getLocalPart() + "/" + Constants.Code.getLocalPart());

				if (Constants.StatusCodeValid.equals(valid)) {
					Log.info("Brokered Authentication sucessful");
				} else {
					Log.error("Brokered Authentication not successful. Reason: ");
					String reason = ParameterValueManagement.getString(result, Constants.Status.getLocalPart() + "/" + Constants.Reason.getLocalPart());
					Log.error(reason);
					return false;
				}


			} else {
				/* case 3 or 4 (see beginning of function)
				 * Has broker(s) but it's not me
				 */
				Log.info(target.getEndpointReference().getAddress().toString() + "already has brokers. But I' not one of them.");

				DeviceReference knownBroker = null;

				for (int i = 0 ; i < brokers.size(); i++) {
					knownBroker = this.getDeviceReference(new EndpointReference( (AttributedURI) brokers.get(i)), SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
					if (AuthenticationEngine.getInstance().getContextDatabase().getContextByReference(knownBroker.getEndpointReference().getAddress().toString()) != null)
						break; /* gotcha! */
					knownBroker = null;
				}

				if (knownBroker == null) {
					/* **************************** *
					 * Case 4
					 * authenticate with a broker,
					 * then proceed
					 * **************************** */

					/* Don't know. Don't care. Try to authenticate with one of them brokers */

					for (int i = 0 ; i < brokers.size(); i++) {
						knownBroker = this.getDeviceReference(new EndpointReference( (AttributedURI) brokers.get(i)), SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
						int result = -1;
						try {
							result = authenticate(knownBroker, null, null, true, true);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							knownBroker = null;
						}
						if (result != DefaultAuthenticationClient.ERR_NO_ERROR) {
							knownBroker = null;
						} else {
							break;
						}
					}

				}

				if (knownBroker != null) {
					/* **************************** *
					 * Case 3
					 * There is a broker I already
					 * trust
					 * **************************** */

					Device brokerDevice = knownBroker.getDevice();

					SecurityContext sctx = invokeBrokerOperation(
							brokerDevice,
							origin.getEndpointReference().getAddress().toString(),
							target.getEndpointReference().getAddress().toString());

					if (sctx == null) {
						Log.error("invokeBrokerOperation() delivered empty Security Context!");
						return false;
					} else {
						Log.debug("invokeBrokerOperation() delivered " + sctx.toString());
						Log.debug("Marking it as brokered at this point.");
						final String ref = sctx.getContextReference();
						final String nref = "b" + ref;
						sctx.setContextReference(nref);
						Log.debug("New Security Context after it has been marked: " + sctx.toString());
					}

					authenticationCB.deliverAuthenticationData(sctx);

				} else {
					Log.error("For some reason it was completely impossible to authenticate with target. Sorry. I am a bad computer.");
					return false;
				}

			}

		} else {
			/* ************************** */
			/* this means I act as client */
			/* ************************** */

			Log.debug("authenticateBrokered(): Entered Client-Logic. I " + (AuthenticationEngine.getInstance().isABroker() ? "AM a" : "am NOT a") + " broker");

			/* *******************************************
			 * a tiny client and a giant client (broker)
			 * act differently here.
			 *
			 * This is all and foremost because giants
			 * are not resource-constrained and may be
			 * burdened with way more load
			 *
			 * A tiny client tries to authenticate in the
			 * following order:
			 *
			 * 1. check if you have a broker. If you do, use it
			 * 2. if you wish (authenticateBroker == true),
			 *    search a broker and authenticate with it
			 *   2.1 if target has a broker, then use this one
			 *   2.2 if not check for any existing broker
			 * 3. try to use a UI-Authenticator (so: fail here)
			 * 4. try direct authentication
			 *
			 *
			 * A giant client works differently
			 *
			 * 1. check if target has a broker and if you trust it
			 * 2. check if target has a broker that you don't trust
			 *    and authenticate with it.
			 * 3. try direct authentication with target
			 * 4. try indirect authentication with target
			 *
			 */

			/* Check if I have a broker */
			if (myBroker == null && !AuthenticationEngine.getInstance().isABroker()) {
				ArrayList knownBrokers = AuthenticationEngine.getInstance().getBrokers();
				if (knownBrokers == null || knownBrokers.size() == 0) {
					Log.info("No known brokers");
					/* That's okay, need need to cry. We gonna go for UI-Authentication then */
				} else {
					for (int i = 0 ; i < knownBrokers.size(); i++) {
						DeviceReference devReference = DeviceServiceRegistry.getDeviceReference(
								new EndpointReference(new AttributedURI((String) knownBrokers.get(i))),
								SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
						try {
							myBrokerDevice = devReference.getDevice();
						} catch (CommunicationException e) {
							/* basically it's possible that this device is simply not available atm. Let's try the next one */
							Log.warn("FYI: Could not resolve " + devReference.getEndpointReference().getAddress().toString());
							myBrokerDevice = null;
						}
						if (myBrokerDevice != null) {
							/* found an an active broker */
							myBroker = devReference;
							break;
						}
					}
				}
			}

			if (myBroker == null) {
				Log.info("No active Broker found");
				if (!authenticateBroker && !AuthenticationEngine.getInstance().isABroker()) {
					Log.info("I am not supposed to change anything about it (authenticateBroker = false) so I will fault");
					return false;
				}

				/* *********************************************************** */
				/*   So, I am a Client, I don't have a broker yet, however,    */
				/*   I am allowed to change that very fact...                  */
				/* *********************************************************** */
				Log.info("Will try to acquire target's broker");

				/* try to authenticate with target's broker */
				Policy brokersPolicy = (Policy) WSSecurityPolicyClient.getAllSecurityRelatedPolicies(target).get(new QName(WSSecurityForDevicesConstants.BrokersPolicyName));
				ArrayList brokers = BrokerPolicy.getBrokers(brokersPolicy);
				if (brokers != null && brokers.size() != 0) {

					/* if I am a broker, I need to check whether there is a broker I already trust. Then there is no need
					 * for authentication and I may skip the following part */
					if (AuthenticationEngine.getInstance().isABroker()) {
						for (int i = 0; i < brokers.size(); i ++) {
							if (AuthenticationEngine.getInstance().getContextDatabase().getContextByReference( ((AttributedURI) brokers.get(i)).toString()) != null) {
								myBroker = getDeviceReference(new EndpointReference((AttributedURI) brokers.get(i)), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
								break;
							}
						}
					}

					if (myBroker == null) {

						boolean success = false;
						for (int i = 0; i < brokers.size(); i ++) {
							AttributedURI brokerAddr = (AttributedURI) brokers.get(i);
							if (brokerAddr.toString().equals(AuthenticationEngine.getDefaultOwnerID())) {
								Log.info("Current tested broker is my own address. Will skip");
								continue;
							}
							myBroker = DeviceServiceRegistry.getDeviceReference(new EndpointReference(brokerAddr), SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
							Log.info("Trying (in)direct authentication with " + myBroker.getEndpointReference().getAddress().toString());
							try {
								success = authenticateViaUI(myBroker, null, false);
							} catch (Exception e) {
								e.printStackTrace();
								myBroker = null;
							}
							if (success) {
								Log.warn("(In)direct authentication was successfull. ");
								myBrokerDevice = myBroker.getDevice();
								break;
							} else {
								myBroker = null;
								Log.warn("(In)direct authentication was not successfull. I'll check if there are more...");
							}
						}

					} else {
						myBrokerDevice = myBroker.getDevice();
					}

				} else if (AuthenticationEngine.getInstance().isABroker()) {
					/* cases 1 & 2 failed for a Broker-Client
					 * authenticate via UI will cover case 3 & 4
					 * ***************************************** */
					return false;
				}

			}

			if (myBroker == null) {

				/* still no broker? Get the first you find. */
				DiscoveryClient dc = new DiscoveryClient(new QName(WSSecurityForDevicesConstants.AuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
				dc.setCallbacks(new DiscoveryClientCallbacks() {
					@Override
					public void searchTimeOut() {
						finishedSearching = true;
					}
					@Override
					public void deviceFound(DeviceReference devRef) {
						synchronized (AuthenticationEngine.getInstance()) {
							if (myBroker == null) {
								/* if one found, stop reporting */
								/* FIXME: maintain a list - if the first fails, you can use others */
								myBroker = devRef;
								finishedSearching = true;
							}
						}
					}
				});
				/* Waiting for 10 Seconds for Authenticators is a bit cumbersome,
				 * especially because
				 *   -  this is a somewhat special case that rarely happens (but
				 *      needs to be checked anyways)
				 *   -  Authenticators ought to be fast-communicating high-power
				 *      devices which should respond very quick
				 */
				// save original timeout
				long mwt = DPWSProperties.getInstance().getMatchWaitTime();
				// set new timeout
				DPWSProperties.getInstance().setMatchWaitTime(AUTHENTICATOR_DEVICE_DISCOVERY_TIMEOUT_MILLIS);
				dc.addToBlacklist(target.getEndpointReference().getAddress().toString());
				finishedSearching = false;
				dc.start(false, false, false);
				int i = 0;
				while (!finishedSearching) {
					if (i++ % 5 == 0)
						Log.info("waiting for Authenticator to be discovered");
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				finishedSearching = false;
				// restore timeout
				DPWSProperties.getInstance().setMatchWaitTime(mwt);
				Log.info("Finished Searching");

				if (myBroker == null) {
					Log.warn("Did not find any brokers. Must fault");
					return false;
				} else {
					Log.info("Found Broker: " + myBroker.getEndpointReference().getAddress().toString());

					boolean success = false;

					Log.info("Trying to authenticate");

					try {
						success = authenticateViaUI(myBroker, null, false);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (success) {
						Log.info("Success");
						myBrokerDevice = myBroker.getDevice();
					} else {
						Log.info("No way. Sorry. Must fault.");
						return false;
					}
				}

			}

			Log.debug("Firing invokeBrokerOperation now");

			/* FIRE !!! */
			SecurityContext sctx = invokeBrokerOperation(myBrokerDevice, getClientId(), target.getEndpointReference().getAddress().toString());

			Log.debug("invokeBrokerOperation() deliver sctx " + (sctx == null ? "null" : sctx.toString()));

			if (sctx == null) {
				return false;
			}

			if (authenticationCB != null) {
				Log.info("Delivering Authentication Data!");
				authenticationCB.deliverAuthenticationData(sctx);
			} else {
				Log.info("NOT delivering authentication Data");
			}
		}
		Log.debug("authenticateBrokered() returns true now");
		return true;
	}

	private SecurityContext invokeBrokerOperation(Device myBrokerDevice, String origin, String target) throws AuthenticationServiceMalformedException, CommunicationException, AuthorizationException, InvocationException {


		Log.debug("invokeBrokerOperation()\n\tmyBrokerDevice: "+myBrokerDevice.getEndpointReference().toString()+"\n\torigin " + origin + "\n\ttarget:" + target);


		ServiceReference servRef = (ServiceReference) myBrokerDevice.getServiceReferences(new QNameSet( QName.construct( Constants.WST_ISSUE_BINDING ) ), SecurityKey.EMPTY_KEY).next();

		if (servRef == null) {
			throw new AuthenticationServiceMalformedException("No Service with given criteria found!");
		}

		Operation op = null;

		try {
			op = servRef.getService().getOperation(
					QName.construct(WSSecurityForDevicesConstants.STSAuthenticationBrokeredBindingPortType),
					null,
					Constants.WST_SCT_BINDING_RST_SCT_ACTION,
					Constants.WST_SCT_BINDING_RSTR_SCT_ACTION);
		} catch (CommunicationException e) {
			throw new CommunicationException("Did not find given Service.");
		}

		if (op == null) {
			throw new AuthenticationServiceMalformedException("Service is here - however right operation is not. Dafuq?");
		}

		ParameterValue p = op.createInputValue();

		ParameterValueManagement.setString(p, Constants.TokenType.getLocalPart(), WSSecurityForDevicesConstants.SymmetricTokenType);
		ParameterValueManagement.setString(p, Constants.RequestType.getLocalPart(), WSSecurityForDevicesConstants.BrokeredRequestType);
		ParameterValueManagement.setString(p, Constants.AppliesTo.getLocalPart(), target);
		ParameterValueManagement.setString(p, Constants.OnBehalfOf.getLocalPart(), origin);
		ParameterValueManagement.setString(p, Constants.AuthenticationType.getLocalPart(), WSSecurityForDevicesConstants.BrokeredAuthenticationAsQNameString);
		ParameterValueManagement.setString(p, Constants.SignWith.getLocalPart(), AuthenticationEngine.getInstance().getSupportedSignatureAlgorithmsAsString());
		ParameterValueManagement.setString(p, Constants.EncryptWith.getLocalPart(), AuthenticationEngine.getInstance().getSupportedEncryptionAlgorithmsAsString());

		ParameterValue result = op.invoke(p,CredentialInfo.EMPTY_CREDENTIAL_INFO);

		String id = ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.Identifier.getLocalPart());
		int sType = AlgorithmConstants.idByName(QName.construct(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.SignWith.getLocalPart())));
		int eType = AlgorithmConstants.idByName(QName.construct(ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.EncryptWith.getLocalPart())));

		String b64key = ParameterValueManagement.getString(result, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedProofToken.getLocalPart() + "/" + Constants.BinarySecret.getLocalPart());

		byte[] key = Base64Util.decode(b64key);

		if (key == null) {
			Log.error("Received Binary Secret not valid b64 key: " + b64key);
			return null;
		}

		SecurityToken st = new SecurityToken(id, key, eType, sType);
		SecurityContext sctx = new SecurityContext(target, st);

		Log.debug("invokeBrokerOperation(): returning " + sctx.toString());

		return sctx;

	}

	/**
	 * Use direct / indirect authentication to authenticate with target. This is
	 * probably not what you want. You might want to check out authenticate() and
	 * let it do some work for you.
	 *
	 * @param target Device to authenticate with
	 * @throws Exception
	 */
	public boolean authenticateViaUI(DeviceReference target, DeviceReference via, boolean tryDirect) throws Exception {

		DeviceReference uiAuthenticatorRef = via;
		QName clientMechanism = null;

		AuthenticationEngine ae = AuthenticationEngine.getInstance();

		QNameSet mechanisms = ae.getSupportedOOBauthenticationMechanisms();

		/* if required, check if direct authentication is possible */
		if (tryDirect || AuthenticationEngine.getInstance().isABroker()) {

			HashMap policies = WSSecurityPolicyClient.getAllSecurityRelatedPolicies(target);

			/* check for common OOB mechanism */
			Policy p = (Policy) policies.get(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName));
			QNameSet remote = AuthenticationMechanismPolicy.getAuthenticationMechanisms(p);
			clientMechanism = WSSecurityPolicyClient.pickAuthenticationMechanism(mechanisms, remote);
			if (clientMechanism != null) {
				authenticationCB.prereportMechanism(clientMechanism);
				uiAuthenticatorRef = target;

				/* if direct authentication possible, check for security algorithms to use */
				Policy q = (Policy) policies.get(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName));
				if (uiAuthenticatorRef != null) {
					saset = WSSecurityPolicyClient.pickSecurityAlgorithms(q,
							ae.getSupportedSignatureAlgorithmsAsString(),
							ae.getSupportedEncryptionAlgorithmsAsString(),
							ae.getSupportedDerivationAlgorithmsAsString());

					if (saset == null) {
						ae.reset();
						throw new Exception("Direct Authentication Possible but no chance to match algorithms");
					} else {
						Log.info("Success! Going for direct authentication with " + clientMechanism.toString() + " and " + saset.toString());
					}

				}
			}
		}

		if (uiAuthenticatorRef == null) {

			Iterator it = mechanisms.iterator();

			/* find suitable UI Authenticator */
			while (it.hasNext() && uiAuthenticatorRef == null) {
				clientMechanism = (QName) it.next();
				if (clientMechanism.equals(new QName(WSSecurityForDevicesConstants.BrokeredAuthentication, WSSecurityForDevicesConstants.NAMESPACE))) {
					continue;
				}
				uiAuthenticatorRef = WSSecurityPolicyClient.getFirstUIAuthenticatorByMechanism(clientMechanism);
			}

			if (uiAuthenticatorRef == null) {
				ae.reset();
				throw new NoUIAuthenticatorFoundException();
			}

			Log.info("Found Authenticator @ " + uiAuthenticatorRef.getEndpointReference().getAddress() + " for Mechanism " + clientMechanism);

		}

		/* Get Device  */
		Device uiAuthenticatorDev = uiAuthenticatorRef.getDevice();

		/* Get Authentication Service - this actually should not be possible to fail... */

		Iterator servicesIt = uiAuthenticatorDev.getServiceReferences(new QNameSet( QName.construct( Constants.WST_ISSUE_BINDING ) ), SecurityKey.EMPTY_KEY);
		ServiceReference servRef = null;

		while (servicesIt.hasNext()) {
			servRef = (ServiceReference) servicesIt.next();
			/* I'm pretty much convinced that there should be at most one */
			if (!servRef.getServiceId().toString().equals(WSSecurityForDevicesConstants.SecurityTokenServiceID)) {
				Log.warn("Found Service with correct binding but incorrect ID. Will try to proceed and see what happens.");
			} else {
				break; /* found one - pointless though 'cause there should only be a single one... */
			}
		}

		if (servRef == null) {
			ae.reset();
			throw  new AuthenticationServiceMalformedException("No Service with correct binding at target. ");
			//TODO: Consider Blacklisting and Searching again
		}

		/* if clientMechanism == null at this point, it means UI-Authenticator came by parameter */
		if (clientMechanism == null) {
			QNameSet remoteMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(servRef.getService().getPolicy(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName)));
			Iterator it = mechanisms.iterator();
			while (it.hasNext()) {
				QName cur = (QName) it.next();
				if (remoteMechanisms.contains(cur)) {
					clientMechanism = cur;
					break;
				}
			}
			if (clientMechanism == null) {
				Log.warn("Could not find suitable authentication mechanism. Remote: " + remoteMechanisms.toString() + "; Local: " + mechanisms.toString());
				ae.reset();
				throw new Exception("Could not find suitable authentication mechanism. Remote: " + remoteMechanisms.toString() + "; Local: " + mechanisms.toString());
			}
		}

		if (!servRef.containsAllPortTypes(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType)))) {
			ae.reset();
			throw new AuthenticationServiceMalformedException("Port Types not correct");
		}

		Service authenticationService = servRef.getService();

		Operation op1 = authenticationService.getOperation(
				QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType),
				WSSecurityForDevicesConstants.STSAuthenticationECDH1MethodName,
				Constants.WST_ISSUE_BINDING_RST_ISSUE_ACTION,
				Constants.WST_ISSUE_BINDING_RSTR_ISSUE_ACTION);

		ParameterValue p1 = op1.createInputValue();

		ParameterValueManagement.setString(p1, Constants.TokenType.getLocalPart(), WSSecurityForDevicesConstants.SymmetricTokenType);
		ParameterValueManagement.setString(p1, Constants.RequestType.getLocalPart(), WSSecurityForDevicesConstants.ECCDH1RequestType);
		ParameterValueManagement.setString(p1, Constants.AppliesTo.getLocalPart(), target.getEndpointReference().getAddress().toString());
		ParameterValueManagement.setString(p1, Constants.OnBehalfOf.getLocalPart(), getClientId());
		ParameterValueManagement.setString(p1, Constants.AuthenticationType.getLocalPart(), clientMechanism.toString());

		if (ae.isABroker()) {
			ParameterValueManagement.setString(p1, Constants.isBroker.getLocalPart(), "true");
			if (AuthorizationEngine.getInstance().isAuthorizer()) {
				ParameterValueManagement.setAttributeValue(p1, Constants.isBroker.getLocalPart(), Constants.authorizerAttribute, "true");
			}
		}

		if (saset == null) {
			ParameterValueManagement.setString(p1, Constants.SignWith.getLocalPart(), ae.getSupportedSignatureAlgorithmsAsString());
			ParameterValueManagement.setString(p1, Constants.EncryptWith.getLocalPart(), ae.getSupportedEncryptionAlgorithmsAsString());
		} else {
			ParameterValueManagement.setString(p1, Constants.SignWith.getLocalPart(), saset.getSignatureAlgorithm().toString());
			ParameterValueManagement.setString(p1, Constants.EncryptWith.getLocalPart(), saset.getEncryptionAlgorithm().toString());
		}

		Log.info("\n" + p1.toString() + "\n");

		ParameterValue r1 = null;
		r1 = op1.invoke(p1, CredentialInfo.EMPTY_CREDENTIAL_INFO);

		Log.info("\n" + r1.toString() + "\n");

		String cName = ParameterValueManagement.getString(r1, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_curvename.getLocalPart());

		ae.getCryptoHelper().init(cName);
		ae.getCryptoHelper().getParams().setTarget(target.getEndpointReference().getAddress().toString());
		ae.getCryptoHelper().getParams().setOrigin(getClientId());
		ae.getCryptoHelper().getParams().addOtherA(clientMechanism.toString());
		if (saset==null) {
			ae.getCryptoHelper().getParams().addOtherA(ae.getSupportedSignatureAlgorithmsAsString());
			ae.getCryptoHelper().getParams().addOtherA(ae.getSupportedEncryptionAlgorithmsAsString());
			ae.getCryptoHelper().getParams().addOtherA(ae.getSupportedDerivationAlgorithmsAsString());
		} else {
			ae.getCryptoHelper().getParams().addOtherA(saset.getSignatureAlgorithm().toString());
			ae.getCryptoHelper().getParams().addOtherA(saset.getEncryptionAlgorithm().toString());
			//			ae.getCryptoHelper().getParams().addOtherA(saset.getDerivationAlgorithm().toString());
		}

		String sNonce = ParameterValueManagement.getString(r1, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart());

		ae.getCryptoHelper().getParams().setNonceB(Base64Util.decode(sNonce));
		ae.getCryptoHelper().getParams().addOtherB(ParameterValueManagement.getString(r1, Constants.AuthenticationType.getLocalPart()));

		byte[] encodedPublicKey = Base64Util.decode(ParameterValueManagement.getString(r1, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart()));

		ae.getCryptoHelper().getParams().setDistortedPublicKey(ae.getCryptoHelper().decodePublicKey(encodedPublicKey));

		ae.getCryptoHelper().createNonce();

		/* End of first request */


		/*
		 * Here be magic: OOB Shared Secret is exchanged
		 */
		if (authenticationCB == null) {
			ae.reset();
			throw new NullPointerException("OOB Pin Reader not set!!");
		}

		int intOOBss = authenticationCB.getOOBpinAsInt(clientMechanism);

		ae.getCryptoHelper().getParams().setOOBSharedSecretAsInt(intOOBss);
		ae.getCryptoHelper().OOBkeyFromInteger();
		ae.getCryptoHelper().decryptPublicKey();

		/* Do the second request */

		Operation op2 = authenticationService.getOperation(
				QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType),
				WSSecurityForDevicesConstants.STSAuthenticationECDH2MethodName,
				Constants.WST_ISSUE_BINDING_RSTR_ISSUE_ACTION,
				Constants.WST_ISSUE_BINDING_RSTRC_ISSUEFINAL_ACTION);

		ParameterValue p2 = op2.createInputValue();

		ae.getCryptoHelper().calculateSharedSecret();

		ae.getCryptoHelper().calculateCMAC();

		ParameterValueManagement.setString(p2, Constants.TokenType.getLocalPart(), WSSecurityForDevicesConstants.SymmetricTokenType);
		ParameterValueManagement.setString(p2, Constants.RequestType.getLocalPart(), WSSecurityForDevicesConstants.ECCDH2RequestType);
		ParameterValueManagement.setString(p2, Constants.AppliesTo.getLocalPart(), ae.getCryptoHelper().getParams().getTarget());
		ParameterValueManagement.setString(p2, Constants.OnBehalfOf.getLocalPart(), ae.getCryptoHelper().getParams().getOrigin());

		ParameterValueManagement.setString(p2, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_nonce.getLocalPart(), Base64Util.encodeBytes(ae.getCryptoHelper().getParams().getNonceA()));
		ParameterValueManagement.setString(p2, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_public_key.getLocalPart(), Base64Util.encodeBytes(ae.getCryptoHelper().getParams().getKeyPair().getPublic().getEncoded()));
		ParameterValueManagement.setString(p2, Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart(), Base64Util.encodeBytes(ae.getCryptoHelper().getParams().getCMAC()));

		Log.info("\n" + p2.toString() + "\n");

		ParameterValue r2;
		r2 = op2.invoke(p2, CredentialInfo.EMPTY_CREDENTIAL_INFO);

		Log.info("\n" + r2.toString() + "\n");

		/* only thing left is to check Bob's CMAC and calculate shared secret */
		ae.getCryptoHelper().getParams().setForeignCMAC(Base64Util.decode(ParameterValueManagement.getString(r2, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.AuthECCDHParameters.getLocalPart() + "/" + Constants.auth_ecc_dh_cmac.getLocalPart())));

		SecurityToken ak = new SecurityToken(
				ParameterValueManagement.getString(r2, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.Identifier.getLocalPart()),
				(byte[]) null,
				AlgorithmConstants.idByName(QName.construct(ParameterValueManagement.getString(r2, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.EncryptWith.getLocalPart()))),
				AlgorithmConstants.idByName(QName.construct(ParameterValueManagement.getString(r2, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.RequestedSecurityToken.getLocalPart() + "/" + Constants.SecurityContextToken.getLocalPart() + "/" + Constants.SignWith.getLocalPart()))));

		ae.getCryptoHelper().getParams().addOtherB(ak.getIdentification());
		ae.getCryptoHelper().getParams().addOtherB(((QName)AlgorithmConstants.algorithmString.get(ak.getsAlgorithmType())).toString());
		ae.getCryptoHelper().getParams().addOtherB(((QName)AlgorithmConstants.algorithmString.get(ak.geteAlgorithmType())).toString());

		boolean success = false;

		boolean isOriginBroker = false;
		boolean wantAuthorizer = false;
		String sBroker = ParameterValueManagement.getString(r2, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart());
		if (sBroker != null && sBroker.equals("true")) {
			isOriginBroker = true;
		} else {
			String authzAttrString = ParameterValueManagement.getAttributeValue(r2, Constants.RequestSecurityTokenResponse.getLocalPart() + "/" + Constants.isBroker.getLocalPart(), Constants.authorizerAttribute);
			if ("true".equals(authzAttrString)) {
				wantAuthorizer = true;
			}
		}

		if (ae.getCryptoHelper().checkCMAC()) {
			Log.info("Congratulations! Bob's CMAC passed!");
			ae.getCryptoHelper().calculateMasterKey();
			success = true;
			ak.setKeymaterial(ae.getCryptoHelper().getParams().getMasterKey());
			SecurityContext ct = new SecurityContext (ae.getCryptoHelper().getParams().getTarget(), ak);
			ct.setIsBroker(isOriginBroker); /* I is client that requests authentication with Authenticator / Broker */
			if (ae.isABroker()) { /* I is Authenticator that requests authentication with an Endpoint */
				ae.addBrokerable(ae.getCryptoHelper().getParams().getTarget());
			}
			AuthorizationEngine auze = AuthorizationEngine.getInstance();
			if (auze.isAuthorizer() && wantAuthorizer) {
				Log.info("Target wants me as primary Authorizer. Will accept.");
				auze.addSheep(ae.getCryptoHelper().getParams().getTarget(), "primary");
			}
			authenticationCB.deliverAuthenticationData(ct);
		} else {
			Log.warn("ERROR!! Bob's CMAC did not pass the test!!");
		}

		ae.reset();
		return success;
	}

	private String getClientId() {
		return AuthenticationEngine.getSafeDefaultOwnerID();
	}

}
