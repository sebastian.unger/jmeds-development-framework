package org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.constants.DPWS2009.DPWSConstants2009;
import org.ws4d.java.constants.general.DPWSConstants;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;


public class DiscoveryClient extends DefaultClient {

	ArrayList blacklist = new ArrayList();
	public void addToBlacklist(String uuid) {
		blacklist.add(uuid);
	}

	public static QNameSet filterDefaultDeviceTypes(QName[] completeSet) {
		QNameSet result = new QNameSet();
		for (QName type : completeSet) {
			if (!type.equals(new QName(DPWSConstants.DPWS_TYPE_DEVICE, DPWSConstants2009.DPWS_NAMESPACE_NAME))
					&& !type.equals(new QName(WSSecurityForDevicesConstants.AuthenticationEndpointType, WSSecurityForDevicesConstants.NAMESPACE))
					&& !type.equals(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE))
					&& !type.equals(new QName(WSSecurityForDevicesConstants.AuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE))) {
				/* best guess for a reasonable Device Type */
				result.add(type);
			}
		}
		return result;
	}

	public static QName[] getDeviceTypes(String target) throws CommunicationException {
		return getDeviceTypes(new EndpointReference(new AttributedURI(target)));
	}

	public static QName[] getDeviceTypes(EndpointReference target) throws CommunicationException {
		DeviceReference targetDevRef = DeviceServiceRegistry.getDeviceReference(target, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return getDeviceTypes(targetDevRef);
	}

	public static QName[] getDeviceTypes(DeviceReference target) throws CommunicationException {
		return target.getDevicePortTypesAsArray(true);
	}



	QNameSet mTypes = null;

	public DiscoveryClient() {
		this((QNameSet)null);
	}

	public DiscoveryClient(QName type) {
		this(new QNameSet(type));
	}

	public DiscoveryClient(QNameSet types) {
		super();
		this.mTypes = types;
	}

	DiscoveryClientCallbacks mcb = null;
	public void setCallbacks(DiscoveryClientCallbacks cb) {
		mcb = cb;
	}
	public DiscoveryClientCallbacks getCallbacks() {
		return mcb;
	}

	private boolean mKeepHelloListenerAlive = false;
	private boolean mConsiderSelf = false;

	public void start(boolean keepHelloListenerAlive) {
		start(false, keepHelloListenerAlive, false);
	}

	public void start(boolean startHelloListener, boolean keepHelloListenerAlive, boolean considerSelf) {

		if (!JMEDSFramework.isRunning())
			JMEDSFramework.start(null);

		mKeepHelloListenerAlive = keepHelloListenerAlive;
		this.mConsiderSelf = considerSelf;

		SearchParameter s = new SearchParameter();

		if (mTypes == null) {
			s = null;
		} else {
			s.setDeviceTypes(mTypes, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		}
		if (startHelloListener) {
			registerHelloListening(s);
		}
		searchDevice(s);
	}

	private void reportDevice(DeviceReference devRef) {
		if (mcb != null)
			mcb.deviceFound(devRef);
	}

	@Override
	public void helloReceived(HelloData helloData) {
		super.helloReceived(helloData);
		reportDevice(this.getDeviceReference(helloData.getDiscoveryData().getEndpointReference(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));
	}

	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search,	String comManId) {
		super.deviceFound(devRef, search, comManId);
		if (mConsiderSelf == false && devRef.getEndpointReference().getAddress().toString().equals(AuthenticationEngine.getDefaultOwnerID())) {
			Log.info("Found a device. However, this appears to be and I am not supposed to consider my self. Will not report.");
		} else if(blacklist.contains(devRef.getEndpointReference().getAddress().toString())) {
			Log.info("Found a device. But it is blacklistedm so I will not report (" + devRef.getEndpointReference().getAddress().toString() + ")");
		} else {
			reportDevice(devRef);
		}
	}

	@Override
	public void finishedSearching(int searchIdentifier, boolean entityFound, SearchParameter search) {
		super.finishedSearching(searchIdentifier, entityFound, search);
		if (mcb != null)
			mcb.searchTimeOut();
		if (!mKeepHelloListenerAlive)
			unregisterHelloListening();
	}

}
