package org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient;

import org.ws4d.java.service.reference.DeviceReference;

public interface DiscoveryClientCallbacks {
	public void deviceFound(DeviceReference devRef);
	public void searchTimeOut();
}
