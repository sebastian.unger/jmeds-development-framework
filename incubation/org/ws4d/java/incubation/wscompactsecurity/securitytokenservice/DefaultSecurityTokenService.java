package org.ws4d.java.incubation.wscompactsecurity.securitytokenservice;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.incubation.Types.SecurityTokenServiceType;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.policymanagement.PolicyManager;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.SecurityAlgorithmPolicy;
import org.ws4d.java.incubation.wspolicy.wspolicyclient.WSSecurityPolicyClient;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

public class DefaultSecurityTokenService extends DefaultService {

	private int serviceType = SecurityTokenServiceType.NONE;
	QNameSet devicePortTypes = null;
	public static DefaultSecurityTokenService defaultServiceInstance = null;
	private boolean encryptedPinExchange = false;

	public static DefaultSecurityTokenService getDefaultSecurityTokenServiceInstance() {
		return defaultServiceInstance;
	}

	public DefaultSecurityTokenService(int type, String comManId) {
		this(type, -1, comManId);
	}

	public boolean isEndpoint() {
		return (serviceType & SecurityTokenServiceType.ENDPOINT) > 0;
	}

	public boolean isUIAuthenticator() {
		return (serviceType & SecurityTokenServiceType.UIAUTHENTICATOR) > 0;
	}

	public boolean isAuthenticator() {
		return (serviceType & SecurityTokenServiceType.AUTHENTICATOR) > 0;
	}

	public boolean isSelfhostedAuthorizer() {
		return (serviceType & SecurityTokenServiceType.SELFHOSTEDAUTHORIZER) > 0;
	}

	public boolean isAsynchronousUIAuthorizer() {
		return (serviceType & SecurityTokenServiceType.ASYNCHRONOUSUIAUTHORIZER) > 0;
	}

	public boolean isSynchronousAuthorizer() {
		return (serviceType & SecurityTokenServiceType.SYNCHRONOUSAUTHORIZER) > 0;
	}

	public void activateEncryptedPinExchange() {
		activateEncryptedPinExchange(true);
	}

	public void activateEncryptedPinExchange(boolean b) {
		encryptedPinExchange = b;
	}

	private void getDevicePortTypes() {
		if (devicePortTypes == null)
			devicePortTypes = new QNameSet();
		try {
			Iterator it = this.getParentDeviceReference(SecurityKey.EMPTY_KEY).getDevicePortTypes(true);
			while (it.hasNext()) {
				QName t = (QName) it.next();
				devicePortTypes.add(t);
			}
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Picks a suitable common authentication mechanism for specified target
	 * <p>
	 * Will be the first mechanism mentioned by target which is also supported by (this)
	 * </p>
	 * 
	 * @param target to pick authentication method from
	 * @return picked authentication method or null if no match possible
	 * @throws CommunicationException
	 */
	public QName pickTargetAuthenticationMechanism(String target) throws CommunicationException {
		Policy authPol = WSSecurityPolicyClient.getAuthenticationMechanismsPolicy(target);
		return pickAuthenticationMechanism(authPol);
	}

	public QName pickAuthenticationMechanism(Policy authPol) {
		QNameSet targetMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(authPol);

		Policy myPol = PolicyManager.getSecurityPolicyManagerInstance().getLocalAuthenticationPolicy().getPolicy();

		QNameSet ownMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(myPol);

		return WSSecurityPolicyClient.pickAuthenticationMechanism(ownMechanisms, targetMechanisms);
	}

	private void activateDirectAuthentication() {
		/* ... for service */
		this.addPortType(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType));
	}

	/* ws4d:Authenticator-part */
	private void activateBrokeredAuthentication() {
		this.addPortType(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType));
		AuthenticationEngine.getInstance().setIsABroker(true);
	}

	/* ws4d:Endpoint-part */
	private void activateAcceptBrokers() {
		this.addPortType(QName.construct(Constants.WST_VALIDATE_BINDING));
	}

	/**
	 * Default service with given configuration identifier.
	 * <p>
	 * Creates an default service and tries to load the configuration properties for the service.
	 * </p>
	 * 
	 * @param configurationId configuration identifier.
	 */
	public DefaultSecurityTokenService(int type, int configurationId, String comManId) {
		super(configurationId, comManId);

		serviceType = type;

		this.setServiceId(new URI(WSSecurityForDevicesConstants.SecurityTokenServiceID));

		if (isEndpoint()) {
			activateDirectAuthentication();
			activateAcceptBrokers();
		}

		if (isUIAuthenticator()) {
			/* start peripherals */
			PolicyManager.initSecurityPolicyManager();
			/*
			 * :'( :'( :'( :'( :'( :'( :'( :'( :'( :'(
			 *
			 * Right now, I'm not even convinced if I need this... dafuq?!
			 *
			 * Maybe, it makes sense if (UI)Authenticators have a trust relationship. Maybe.
			 *
			 * WSPolicyClient.startPolicyClient();
			 */

			activateDirectAuthentication();
			encryptedPinExchange=true;
		}

		if (isAuthenticator()) {
			/* start peripherals */
			PolicyManager.initSecurityPolicyManager();
			//			WSPolicyClient.startPolicyClient();

			activateDirectAuthentication();
			activateBrokeredAuthentication();
		}

		defaultServiceInstance = this;

	}

	@Override
	public synchronized void start() throws IOException {

		if (encryptedPinExchange) {
			QName encXmech = new QName(WSSecurityForDevicesConstants.EncryptedPinExchange,  WSSecurityForDevicesConstants.NAMESPACE);
			AuthenticationEngine.getInstance().addSupportedOOBauthenticationMechanism(encXmech);
		}

		/* Announce Authentication Mechanisms */
		if (AuthenticationEngine.getInstance().getSupportedOOBauthenticationMechanisms() == null) {
			Log.warn("Warning! AuthenticationEngine does not contain OOB mechanisms! They need to be set BEFORE the AuthenticationService is started");
		} else {
			this.addPolicy(AuthenticationMechanismPolicy.getAuthenticationMechanismPolicies(AuthenticationEngine.getInstance().getSupportedOOBauthenticationMechanisms()));
		}

		/* Announce Security Algorithms */
		if (AuthenticationEngine.getInstance().getSupportedAlgorithms() == null) {
			Log.warn("Warning! AuthenticationEngine does not contain security algorithms! They need to be set BEFORE the AuthenticationService is started");
		} else {
			this.addPolicy(SecurityAlgorithmPolicy.getSecurityAlgorithmsPolicy(AuthenticationEngine.getInstance().getSupportedAlgorithms()));
		}

		if (devicePortTypes == null) { /* build them only once */
			getDevicePortTypes();
		}

		QNameSet types = new QNameSet();

		if (isEndpoint()) {
			types.add(new QName(WSSecurityForDevicesConstants.AuthenticationEndpointType, WSSecurityForDevicesConstants.NAMESPACE));
		}
		if (isUIAuthenticator()) {
			types.add(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
		}
		if (isAuthenticator()) {
			types.add(new QName(WSSecurityForDevicesConstants.AuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
			QName brokerMech = new QName(WSSecurityForDevicesConstants.BrokeredAuthentication,  WSSecurityForDevicesConstants.NAMESPACE);
			AuthenticationEngine.getInstance().addSupportedOOBauthenticationMechanism(brokerMech);
		}
		if (isSelfhostedAuthorizer()) {
			// dunno yet
		}
		if (isSynchronousAuthorizer()) {
			types.add(new QName(WSSecurityForDevicesConstants.AuthorizerType, WSSecurityForDevicesConstants.NAMESPACE));
			AuthorizationEngine.getInstance().setIsAuthorizer(true);
		}
		if (isAsynchronousUIAuthorizer()) {
			types.add(new QName(WSSecurityForDevicesConstants.UIAuthorizerType, WSSecurityForDevicesConstants.NAMESPACE));
			AuthorizationEngine.getInstance().setIsUIAuthorizer(true);
		}

		QNameSet mechs = AuthenticationEngine.getInstance().getSupportedOOBauthenticationMechanisms();
		PolicyManager.initSecurityPolicyManager();
		PolicyManager.getSecurityPolicyManagerInstance().updateLocalAuthenticationMechansims(mechs);

		devicePortTypes.addAll(types);
		this.getParentDevice().setPortTypes(devicePortTypes);

		super.start();

	}
}
