package org.ws4d.java.incubation.wscompactsecurity.securitytokenservice;

import org.ws4d.java.description.wsdl.WSDLOperation;
import org.ws4d.java.incubation.Types.AuthenticationFault;
import org.ws4d.java.incubation.Types.AuthorizationFault;
import org.ws4d.java.incubation.Types.ConfidentialityFault;
import org.ws4d.java.incubation.arne.compactsecurity.SecurityControl;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;

public abstract class SecureOperation extends Operation {

	/* for backwards compatibility, security completely disabled *sigh* */
	private boolean authenticatedOnly = false;
	private boolean encryptedOnly = false;
	private boolean authorizedOnly = false;

	public SecureOperation(String name, QName portType) {
		super(name, portType);

		this.addFault(new AuthenticationFault());
		this.addFault(new AuthorizationFault());
		this.addFault(new ConfidentialityFault());

	}
	public SecureOperation() {
		super();
	}
	public SecureOperation(String name) {
		super(name);
	}
	public SecureOperation(String name, String serviceName) {
		super(name, new QName(serviceName, null));
	}
	protected SecureOperation(WSDLOperation operation) {
		super(operation);
	}


	public boolean isAuthenticatedOnly() {
		return authenticatedOnly;
	}
	public void setAuthenticatedOnly(boolean authenticatedOnly) {
		this.authenticatedOnly = authenticatedOnly;
	}
	public boolean isEncryptedOnly() {
		return encryptedOnly;
	}
	public void setEncryptedOnly(boolean encryptedOnly) {
		this.encryptedOnly = encryptedOnly;
	}
	public boolean isAuthorizedOnly() {
		return authorizedOnly;
	}
	public void setAuthorizedOnly(boolean authorizedOnly) {
		this.authorizedOnly = authorizedOnly;
	}

	public void enforceSecurity(ParameterValue parameterValue) throws InvocationException {
		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		AuthorizationEngine autze = AuthorizationEngine.getInstance();

		if (ae == null) {
			throw new InvocationException(getFault(AuthenticationFault.FAULTNAME), AuthenticationFault.INTERNALERROR);
		}

		/* FIXME! Client Identification has to come from somewhere else!! */
		String rId = "urn:uuid:822a81bb-e6bf-4ce2-a50a-09c949d315d2";

		/* Check if requestor is authenticated */
		if (authenticatedOnly && (rId == null || !ae.getContextDatabase().checkAuthentication(rId))) { /* no id specified or no key for id... */
			throw new InvocationException(getFault(AuthenticationFault.FAULTNAME), AuthenticationFault.CLIENTNOTAUTHENTICATED);
		}

		/* Check if message is authenticated */
		if (authenticatedOnly && !SecurityControl.useSignature) {
			throw new InvocationException(getFault(AuthenticationFault.FAULTNAME), AuthenticationFault.SIGNATUREMISSING);
		}

		if (encryptedOnly && !SecurityControl.useBodyencryption) {
			throw new InvocationException(getFault(ConfidentialityFault.FAULTNAME), ConfidentialityFault.MESSAGENOTENCRYPTED);
		}

		/* check if requestor is authorized */
		if (authorizedOnly && !autze.checkAuthorization(rId, this)) {
			throw new InvocationException(getFault(AuthorizationFault.FAULTNAME), AuthorizationFault.CLIENTNOTAUTORIZED);
		}
	}

}
