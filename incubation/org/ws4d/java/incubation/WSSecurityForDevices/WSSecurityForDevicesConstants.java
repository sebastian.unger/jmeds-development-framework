package org.ws4d.java.incubation.WSSecurityForDevices;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.types.QName;

//TODO: consolidate with package org.ws4d.java.incubation.wscompactsecurity.authentication.Constants

public class WSSecurityForDevicesConstants {
	public static final String NAMESPACE = "http://www.ws4d.org";
	public static final String AuthenticatorType = "Authenticator";
	public static final String UIAuthenticatorType = "UI-Authenticator";
	public static final String AuthenticationEndpointType = "AuthenticationEndpoint";
	public static final String SecurityTokenServiceID = "http://www.ws4d.org/SecurityTokenService";
	public static final String STSAuthenticationECCDHBindingPortType = Constants.WST_ISSUE_BINDING;
	public static final String STSAuthenticationBrokeredBindingPortType = Constants.WST_ISSUE_BINDING;
	public static final String STSAuthorizationBindingPortType = Constants.WST_ISSUE_BINDING;
	public static final String STSAuthenticationBrokeredValidateBindingPortType = Constants.WST_VALIDATE_BINDING;
	public static final String STSAuthenticationECDH1MethodName = "ECC_DH1";
	public static final String STSAuthenticationECDH2MethodName = "ECC_DH2";
	public static final String STSAuthenticationBrokerMethodName = "Broker";
	public static final String STSAuthenticationValidateMethodName = "Validate";
	public static final String STSAuthorizationRequestMethodName = "RequestAuthorization";
	public static final String SymmetricTokenType = "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token"; /* maybe rather Security Context Token Type..? */
	public static final String ECCDH1RequestType = "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH1";
	public static final String ECCDH2RequestType = "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH2"; /* not quite sure about those though yet... */
	public static final String BrokeredRequestType = "http://www.ws4d.org/BrokeredAuthentication/Broker";

	public static final String AuthenticationMechanismPolicyName = "AuthenticationMechanism";
	public static final String FlickerAuthentication = "FlickerAuthentication";
	public static final String TappingAuthentication = "TappingAuthentication";
	public static final String PinAuthentication = "PinAuthentication";
	public static final String BrokeredAuthentication = "BrokeredAuthentication";
	public static final String EncryptedPinExchange = "EncryptedPinExchange";
	public static final String FlickerAuthenticationAsQNameString = new QName(FlickerAuthentication, NAMESPACE).toString();
	public static final String TappingAuthenticationAsQNameString = new QName(TappingAuthentication, NAMESPACE).toString();
	public static final String PinAuthenticationAsQNameString = new QName(PinAuthentication, NAMESPACE).toString();
	public static final String BrokeredAuthenticationAsQNameString = new QName(BrokeredAuthentication, NAMESPACE).toString();
	public static final String EncryptedPinExchangeAsQNameString = new QName(EncryptedPinExchange, NAMESPACE).toString();

	public static final String SupportedAlgorithmsPolicyName = "SupportedAlgorithms";
	public static final String SupportedAlgorithmsPolicyAttributeName = "AlgorithmType";
	public static final String SupportedAlgorithmsPolicyAttributeSignature = "signature";
	public static final String SupportedAlgorithmsPolicyAttributeEncryption = "encryption";
	public static final String SupportedAlgorithmsPolicyAttributeDerivation = "derivation";

	public static final String BrokersPolicyName = "Brokers";
	public static final String BrokerPolicyName = "Broker";
	public static final String BrokerPolicyAttributeName = "uri";

	public static final String AuthorizerType = "SynchronousAuthorizer";
	public static final String UIAuthorizerType = "AsynchronousUIAuthorizer";
	public static final String AuthorizersPolicyName = "Authorizers";
	public static final QName AuthorizerPolicyName = new QName("Authorizer", NAMESPACE);
	public static final QName PrimaryAuthorizerPolicyName = new QName("PrimaryAuthorizer", NAMESPACE);
	public static final String AuthorizerPolicyAttributeName = "uri";
	public static final String STSAuthorizationValidateBindingPortType = Constants.WST_VALIDATE_BINDING;

	public static final String LOCALINDICATOR = "http://local";
	public static final String NAMESPACEprefix = "ws4d";
}

