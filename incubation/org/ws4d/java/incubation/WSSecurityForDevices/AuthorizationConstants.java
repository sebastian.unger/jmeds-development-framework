package org.ws4d.java.incubation.WSSecurityForDevices;

import org.ws4d.java.incubation.wscompactsecurity.authentication.Constants;
import org.ws4d.java.types.QName;

public class AuthorizationConstants {
	/* See FIXME comment in Constants regarding namespaces */
	public static final String SAMLAssertionNamespace = Constants.Namespace;//"urn:oasis:names:tc:SAML:2.0:assertion";
	public static final String SAMLAssertionNamespacePrefix = "saml";
	public static final String SAMLProtocolNamespace = "urn:oasis:names:tc:SAML:2.0:protocol";
	public static final String SAMLProtocolNamespacePrefix = "samlp";
	public static final String SAMLVersion2String = "2.0"; /* yeah... It's really THAT simple! */

	public static final String SAMLActionNamespaceRWEDC = "urn:oasis:names:tc:SAML:1.0:action:rwedc";
	public static final String SAMLActionNamespaceRWEDCwNegation = "urn:oasis:names:tc:SAML:1.0:action:rwedc-negation";
	public static final String SAMLActionNamespaceGHPP = "urn:oasis:names:tc:SAML:1.0:action:ghpp";
	public static final String SAMLActionNamespaceUnix = "urn:oasis:names:tc:SAML:1.0:action:unix";

	public static final String ActionRead = "Read";
	public static final String ActionWrite = "Write";
	public static final String ActionExecute = "Execute";
	public static final String ActionDelete = "Delete";
	public static final String ActionControl = "Control";

	public static final String ActionNotRead = "~Read";
	public static final String ActionNotWrite = "~Write";
	public static final String ActionNotExecute = "~Execute";
	public static final String ActionNotDelete = "~Delete";
	public static final String ActionNotControl = "~Control";

	public static final String ActionGET = "GET";
	public static final String ActionHEAD = "HEAD";
	public static final String ActionPUT = "PUT";
	public static final String ActionPOST = "POST";

	public static final QName nameIDType = new QName ("NameIDType", SAMLAssertionNamespace);
	public static final QName nameIDTypeNameQualifierAttribute = new QName ("NameQualifier", SAMLAssertionNamespace);
	public static final QName nameIDTypeSPNameQualifierAttribute = new QName ("SPNameQualifier", SAMLAssertionNamespace);
	public static final QName nameIDTypeFormatAttribute = new QName ("Format", SAMLAssertionNamespace);
	public static final QName nameIDTypeSPProvidedIDAttribute = new QName ("NameQualifier", SAMLAssertionNamespace);
	public static final QName nameIDElement = new QName ("NameID", SAMLAssertionNamespace);
	public static final QName issuerElement = new QName ("Issuer", SAMLAssertionNamespace);

	public static final QName samlAssertionType = new QName("AssertionType", SAMLAssertionNamespace);
	public static final QName samlAssertionElement = new QName("Assertion", SAMLAssertionNamespace);

	public static final QName version = new QName("Version", SAMLAssertionNamespace);
	public static final QName id = new QName("ID", SAMLAssertionNamespace);
	public static final QName issueInstant = new QName("IssueInstant", SAMLAssertionNamespace);

	public static final QName subjectType = new QName("SubjectType", SAMLAssertionNamespace);
	public static final QName subject= new QName("Subject", SAMLAssertionNamespace);

	public static final QName subjectConfirmationType = new QName("SubjectConfirmationType", SAMLAssertionNamespace);
	public static final QName subjectConfirmation = new QName("SubjectConfirmation", SAMLAssertionNamespace);
	public static final QName MethodAttribute = new QName("Method", SAMLAssertionNamespace);
	public static final QName subjectConfirmationDataType = new QName("SubjectConfirmationDataType", SAMLAssertionNamespace);
	public static final QName subjectConfirmationData = new QName("SubjectConfirmationData", SAMLAssertionNamespace);

	public static final QName conditionsType = new QName("ConditionsType", SAMLAssertionNamespace);
	public static final QName conditions = new QName("Conditions", SAMLAssertionNamespace);
	public static final QName notBeforeAttribute = new QName("NotBefore", SAMLAssertionNamespace);
	public static final QName notOnOrAfterAttribute = new QName("NotOnOrAfter", SAMLAssertionNamespace);
	public static final QName condition = new QName("Condition", SAMLAssertionNamespace);
	public static final QName audienceRestriction = new QName("AudienceRestriction", SAMLAssertionNamespace);
	public static final QName oneTimeUse = new QName("OneTimeUse", SAMLAssertionNamespace);
	public static final QName proxyRestriction = new QName("ProxyRestriction", SAMLAssertionNamespace);

	public static final QName advice = new QName("Advice", SAMLAssertionNamespace);

	public static final QName authzDecisionStatement = new QName("AuthzDecisionStatement", SAMLAssertionNamespace);
	public static final QName authzDecisionStatementType = new QName("AuthzDecisionStatementType", SAMLAssertionNamespace);
	public static final QName action = new QName("Action", SAMLAssertionNamespace);

	public static final QName evidence = new QName("Evidence", SAMLAssertionNamespace);
	public static final QName ResourceAttribute = new QName("Resource", SAMLAssertionNamespace);
	public static final QName DecisionAttribute = new QName("Decision", SAMLAssertionNamespace);
	public static final String DecisionPermit = "Permit";
	public static final String DecisionDeny = "Deny";
	public static final String DecisionIndeterminate = "Indeterminate";

}
