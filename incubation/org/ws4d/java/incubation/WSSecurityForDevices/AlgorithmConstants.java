package org.ws4d.java.incubation.WSSecurityForDevices;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.QName;

public class AlgorithmConstants {
	public final static int ALG_UNKNOWN 					= -1;

	/* encryption algorithms */
	public final static int ALG_ENC_AES_CBC128 				= 0x00000001;
	public final static int ALG_ENC_RC4 					= 0x00000002;
	/*                                                           ^        */
	/* signature algorithms                                      v        */
	public final static int ALG_SIG_SHA1_AES_CBC128 		= 0x08000001;
	public final static int ALG_SIG_SHA1_RC4	 			= 0x08000002;

	/* strings (as defined by WS Compact Security ) */
	public final static HashMap algorithmString = new HashMap();
	static {
		algorithmString.put(ALG_ENC_RC4, new QName("rc4", WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix));
		algorithmString.put(ALG_ENC_AES_CBC128, new QName("aes-cbc-128", WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix));
		algorithmString.put(ALG_SIG_SHA1_RC4, new QName("rc4-sha1-mac", WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix));
		algorithmString.put(ALG_SIG_SHA1_AES_CBC128, new QName("aes-cbc-128-sha1-mac", WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix));
	}

	public static int idByName(QName name) {
		Set keys = algorithmString.keySet();
		Iterator it = keys.iterator();
		while (it.hasNext()) {
			int key = ((Integer) it.next()).intValue();
			if (name.equals((QName) algorithmString.get(key))) {
				return key;
			}
		}
		return ALG_UNKNOWN;
	}

	/* combined, maybe? */
	/* ... */
}
