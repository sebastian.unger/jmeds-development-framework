package org.ws4d.java.incubation.wspolicy;

import java.util.HashMap;

public class ServicePolicies {
	private HashMap<String, Policy> mServicePolicies = null;

	public HashMap<String, Policy> getService() {
		return mServicePolicies;
	}

	public void setService(HashMap<String, Policy> servicePolicies) {
		this.mServicePolicies = servicePolicies;
	}
	
	@Deprecated
	public void addPolicy(Policy policy) {
		addPolicy(policy.getName().toString(), policy);
	}
	
	public void addPolicyByName(Policy policy) {
		addPolicy(policy.getName().toString(), policy);
	}
	
	public void addPolicyById(Policy policy) {
		addPolicy(policy.getId().toString(), policy);
	}
	
	public void addPolicy (String identifier, Policy policy) {
		if (mServicePolicies == null) {
			mServicePolicies = new HashMap<String, Policy>();
		}
		mServicePolicies.put(identifier, policy);
	}
}
