package org.ws4d.java.incubation.wspolicy;

import org.ws4d.java.constants.DPWS2009.DPWSConstants2009;
import org.ws4d.java.constants.general.WSPConstants;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class Policy {
	private QName mName;
	private QName mId;
	private boolean mIsCompactFormat;
	
	public static Policy getDefaultPolicy() {
		return getDefaultPolicy(true);
	}
	
	public static Policy getDefaultPolicy(boolean compact) {
		Policy defaultPolicy = null;
		
		if (compact) {
			defaultPolicy = new Policy();
		} else {
			defaultPolicy = new Policy(new QName(WSPConstants.WSP_ELEM_PROFILE_POLICY, DPWSConstants2009.DPWS_NAMESPACE_NAME, "dpws"));
		}
		
		PolicyAssertion pas = new PolicyAssertion(new QName(WSPConstants.WSP_ELEM_PROFILE_POLICY, DPWSConstants2009.DPWS_NAMESPACE_NAME, "dpws"));
		
		if (compact) {
			defaultPolicy.setPolicyAssertion(pas);
		} else {
			PolicyAlternative pa = new PolicyAlternative();
			pa.addPolicyAssertion(pas);
			
			ExactlyOne eo = new ExactlyOne();
			eo.addPolicyAlternative(pa);
			
			defaultPolicy.addExactlyOne(eo);
		}

		return defaultPolicy;
		
	}
	
	private ArrayList mExactlyOnes = null;
	private PolicyAssertion mPolicyAssertion = null;
	
	public Policy() {
		
	}
	
	public Policy(QName name) {
		mName = name;
	}
	
	public void removeAssertions(QName assertion) {
		for (int i = 0; i < mExactlyOnes.size(); i++) {
			ExactlyOne eo = (ExactlyOne) mExactlyOnes.get(i);
			eo.removeAssertions(assertion);
			if (eo.isEmpty())
				mExactlyOnes.remove(i);
		}
	}
	
	public boolean removeAssertionsByRemote(AttributedURI address) {
		boolean res = false;
		for (int i = 0; i < mExactlyOnes.size(); i++) {
			ExactlyOne eo = (ExactlyOne) mExactlyOnes.get(i);
			res = res | eo.removeAssertionsByRemote(address);
			if (eo.isEmpty())
				mExactlyOnes.remove(i);
		}
		return res;
	}
	
	public QName getName() {
		return mName;
	}
	public void setName(QName name) {
		this.mName = name;
	}
	public QName getId() {
		return mId;
	}
	public void setId(QName id) {
		this.mId = id;
	}
	public ArrayList getExactlyOnes() {
		return mExactlyOnes;
	}
	public void setExactlyOnes(ArrayList exactlyOnes) {
		this.mExactlyOnes = exactlyOnes;
		this.setIsCompactFormat(false);
	}
	
	public boolean addExactlyOne(ExactlyOne exactlyOne) {
		if (mExactlyOnes == null) {
			this.mExactlyOnes = new ArrayList();
		}
		return this.mExactlyOnes.add(exactlyOne);
	}
	
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
		
		sb.append("Policy name=\"").append(mName).append("\", id=\"").append(mId).append("\" (");
		
		if (mIsCompactFormat) {
			sb.append(mPolicyAssertion.toString());
		} else {
			for (Iterator it = mExactlyOnes.iterator(); it.hasNext();) {
				sb.append(((ExactlyOne)it.next())).append(", ");
			}
		}
		
		sb.append(")");
		
		return sb.toString();
	}

	public boolean isCompactFormat() {
		return mIsCompactFormat;
	}

	public void setIsCompactFormat(boolean isCompactFormat) {
		this.mIsCompactFormat = isCompactFormat;
	}

	public PolicyAssertion getPolicyAssertion() {
		return mPolicyAssertion;
	}

	public void setPolicyAssertion(PolicyAssertion mPolicyAssertion) {
		this.mPolicyAssertion = mPolicyAssertion;
		this.mIsCompactFormat = true;
	}
	
	public void setPolicyAssertion(QName assertion) {
		setPolicyAssertion(new PolicyAssertion(assertion));
	}

	public void addExactlyOnes(ArrayList exactlyOnes) {
		if (exactlyOnes != null && !exactlyOnes.isEmpty())
			this.mExactlyOnes = new ArrayList();
		this.mExactlyOnes.addAll(exactlyOnes);
	}
	
}
