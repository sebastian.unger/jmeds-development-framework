package org.ws4d.java.incubation.wspolicy.wspolicyclient;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.incubation.wspolicy.securitypolicy.SecurityAlgorithmPolicy;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;

public class WSSecurityPolicyClient extends DefaultClient {

	public static int timeout = 20000;

	QNameSet mMechanisms = null;
	DeviceReference mDevice = null;
	int mTimeout = 0;

	private QName mMechanism = null;

	public static HashMap getAllSecurityRelatedPolicies(String target) throws CommunicationException {
		return getAllSecurityRelatedPolicies(new EndpointReference(new AttributedURI(target)));
	}

	public static HashMap getAllSecurityRelatedPolicies(EndpointReference target) throws CommunicationException {
		return getAllSecurityRelatedPolicies(DeviceServiceRegistry.getDeviceReference(target, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));
	}

	public static HashMap getAllSecurityRelatedPolicies(DeviceReference target) throws CommunicationException {
		return getAllSecurityRelatedPolicies( (ServiceReference) target.getDevice().getServiceReferences(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType)), SecurityKey.EMPTY_KEY).next());

	}

	public static HashMap getAllSecurityRelatedPolicies(ServiceReference targetService) throws CommunicationException {
		Service serv = targetService.getService();
		HashMap allPolicies = serv.getPolicies();
		HashMap securityPolicies = new HashMap();
		securityPolicies.put(
				new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName),
				allPolicies.get(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName)));
		securityPolicies.put(
				new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName),
				allPolicies.get(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName)));
		securityPolicies.put(
				new QName(WSSecurityForDevicesConstants.BrokersPolicyName),
				allPolicies.get(new QName(WSSecurityForDevicesConstants.BrokersPolicyName)));
		return securityPolicies;
	}

	public static Policy getAuthenticationMechanismsPolicy(String target) throws CommunicationException {
		return getAuthenticationMechanismsPolicy(new EndpointReference(new AttributedURI(target)));
	}

	public static Policy getAuthenticationMechanismsPolicy(EndpointReference target) throws CommunicationException {
		DeviceReference targetDevRef = DeviceServiceRegistry.getDeviceReference(target, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return getAuthenticationMechanismsPolicy(targetDevRef);
	}

	public static Policy getAuthenticationMechanismsPolicy(DeviceReference target) throws CommunicationException {
		Device targetDev = target.getDevice();
		ServiceReference servRef = targetDev.getServiceReference(new AttributedURI(WSSecurityForDevicesConstants.SecurityTokenServiceID), SecurityKey.EMPTY_KEY);
		return getAuthenticationMechanismsPolicy(servRef);
	}

	public static Policy getAuthenticationMechanismsPolicy(ServiceReference target) throws CommunicationException {
		Service serv = target.getService();
		Policy authPol = serv.getPolicy(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName);
		return authPol;
	}

	public static QName pickAuthenticationMechanism(QNameSet ownMechanisms, QNameSet targetMechanisms) {

		if (ownMechanisms == null || targetMechanisms == null)
			return null;

		QName targetAuthenticationMechanism = null;

		Iterator mechIt = targetMechanisms.iterator();

		while (mechIt.hasNext()) {
			QName tMech = (QName) mechIt.next();
			if (tMech.equals(new QName(WSSecurityForDevicesConstants.EncryptedPinExchange, WSSecurityForDevicesConstants.NAMESPACE))) {
				/* not acceptable - EncryptedPinExchange is a special case that is covered separately before */
				continue;
			}
			if (tMech.equals(new QName(WSSecurityForDevicesConstants.BrokeredAuthentication, WSSecurityForDevicesConstants.NAMESPACE))) {
				/* not acceptable either. Needs to be covered elsewhere */
				continue;
			}
			if (ownMechanisms.contains(tMech)) {
				targetAuthenticationMechanism = tMech;
				break;
			}
		}
		return targetAuthenticationMechanism;
	}

	public static Policy getSupportedAlgorithmsPolicy(String target) throws CommunicationException {
		return getSupportedAlgorithmsPolicy(new EndpointReference(new AttributedURI(target)));
	}

	public static Policy getSupportedAlgorithmsPolicy(EndpointReference target) throws CommunicationException {
		DeviceReference targetDevRef = DeviceServiceRegistry.getDeviceReference(target, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		return getSupportedAlgorithmsPolicy(targetDevRef);
	}

	public static Policy getSupportedAlgorithmsPolicy(DeviceReference target) throws CommunicationException {
		Device targetDev = target.getDevice();
		ServiceReference servRef = targetDev.getServiceReference(new AttributedURI(WSSecurityForDevicesConstants.SecurityTokenServiceID), SecurityKey.EMPTY_KEY);
		return getSupportedAlgorithmsPolicy(servRef);
	}

	public static Policy getSupportedAlgorithmsPolicy(ServiceReference target) throws CommunicationException {
		Service serv = target.getService();
		Policy authPol = serv.getPolicy(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName);
		return authPol;
	}

	public static SecurityAlgorithmSet pickSecurityAlgorithms(String target, String reqSigAlgs, String reqEncAlgs, String reqKDFs) throws CommunicationException {
		Policy algPolicies = WSSecurityPolicyClient.getSupportedAlgorithmsPolicy(target);
		return pickSecurityAlgorithms(algPolicies, reqSigAlgs, reqEncAlgs, reqKDFs);
	}

	public static SecurityAlgorithmSet pickSecurityAlgorithms(Policy targetPolicy, String reqSigAlgs, String reqEncAlgs, String reqKDFs) throws CommunicationException {
		/* Receive supported Algorithms from target and parse */

		ArrayList algorithms = SecurityAlgorithmPolicy.getSecurityAlgorithms(targetPolicy);

		ArrayList requestedSignatureAlgorithms = new ArrayList();
		String[] sigStrings = reqSigAlgs.split(",");
		for (int i = 0; i < sigStrings.length; i++) {
			QName q = QName.construct(sigStrings[i]);
			if (q != null)
				requestedSignatureAlgorithms.add(q);
		}

		ArrayList requestedEncryptionAlgorithms = new ArrayList();
		String[] encStrings = reqEncAlgs.split(",");
		for (int i = 0; i < encStrings.length; i++) {
			QName q = QName.construct(encStrings[i]);
			if (q != null)
				requestedEncryptionAlgorithms.add(q);
		}

		ArrayList requestedKDFAlgorithms = new ArrayList();
		// Parsing to be done

		SecurityAlgorithmSet saset = new SecurityAlgorithmSet();

		/* match requested and available at target */
		/* the matching mechanisms can be considered as picking the first algorithm after a stable sort for most matches */
		int max = 0;
		for (int i = 0; i < algorithms.size(); i++) {
			int count = 0;
			SecurityAlgorithmSet setUnderTest = (SecurityAlgorithmSet) algorithms.get(i);
			if (setUnderTest.getSignatureAlgorithm() != null && requestedSignatureAlgorithms.contains(setUnderTest.getSignatureAlgorithm())) {
				count++;
			}
			if (setUnderTest.getEncryptionAlgorithm() != null && requestedEncryptionAlgorithms.contains(setUnderTest.getEncryptionAlgorithm())) {
				count++;
			}
			if (setUnderTest.getDerivationAlgorithm() != null && requestedKDFAlgorithms.contains(setUnderTest.getDerivationAlgorithm())) {
				count++;
			}
			if (count > max) {
				max = count;
				saset = setUnderTest;
			}
		}

		return saset;

	}

	public static DeviceReference getFirstUIAuthenticatorByMechanism(QName mechanism) {
		return getFirstUIAuthenticatorByMechanisms(new QNameSet(mechanism));
	}

	public static DeviceReference getFirstUIAuthenticatorByMechanisms(QNameSet mechanisms) {

		if (!JMEDSFramework.isRunning())
			JMEDSFramework.start(null);

		WSSecurityPolicyClient client = new WSSecurityPolicyClient();
		client.mMechanisms = mechanisms;


		SearchParameter sp1 = new SearchParameter();
		sp1.setDeviceTypes(new QNameSet(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE)), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		client.searchDevice(sp1);

		int sleepy = 500;
		while (client.mTimeout < timeout && client.mDevice == null) {
			try {
				Thread.sleep(sleepy);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			client.mTimeout += sleepy;
		}

		return client.mDevice;
	}

	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search,	String comManId) {
		super.deviceFound(devRef, search, comManId);

		String f = devRef.getEndpointReference().getAddress().toString();
		String o = AuthenticationEngine.getDefaultOwnerID();

		/* you found yourself - skip it, otherwise you'd end up in cycles */
		if (f.equals(o))
			return;

		try {
			Device d = devRef.getDevice();

			ServiceReference sr = d.getServiceReference(new AttributedURI(WSSecurityForDevicesConstants.SecurityTokenServiceID), SecurityKey.EMPTY_KEY);
			Service s = sr.getService();

			Policy p = s.getPolicy(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName);

			QNameSet targetMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(p);

			this.mMechanism = pickAuthenticationMechanism(mMechanisms, targetMechanisms);

			if (this.mMechanism != null)
				this.mDevice = devRef;

		} catch (CommunicationException e) {
			e.printStackTrace();
		}

	}

}
