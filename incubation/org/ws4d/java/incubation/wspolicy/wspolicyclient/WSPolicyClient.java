package org.ws4d.java.incubation.wspolicy.wspolicyclient;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.policymanagement.PolicyManager;
import org.ws4d.java.incubation.wspolicy.policymanagement.RemotePolicy;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

/* This client code periodically looks for new Authentication Devices. It also
 * listens to incoming Hello Messages
 */

public class WSPolicyClient extends DefaultClient {

	private static WSPolicyClient policyClient = null;

	static private final QNameSet deviceTypes;
	static private final QNameSet serviceTypes;

	static boolean running = false;

	static {
		deviceTypes = new QNameSet();
		deviceTypes.add(new QName(WSSecurityForDevicesConstants.AuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
		deviceTypes.add(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
		serviceTypes = new QNameSet();
		serviceTypes.add(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType));
	}

	int timeout = 60000; //ms
	int initTimeout = 5000; //ms

	private boolean somethingNew = false;

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	private void restartAuthenticationService() {
		Log.debug("++++++++++++++++++++++++++++++++WSPolicyClient Found Something new. Will restart Service!");
		try {
			synchronized (DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance()) {
				DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().stop();
				DefaultSecurityTokenService.getDefaultSecurityTokenServiceInstance().start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.debug("++++++++++++++++++++++++++++++++DONE");
	}

	private class PolicyCollector implements Runnable {

		@Override
		public void run() {
			policyClient.collectAuthenticationPolicies();
			/* I feel kinda bad for this... */
			// Initial Thread sleep to be able to catch the first round
			try {
				Thread.sleep(initTimeout);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while(true) {
				if (somethingNew) {
					somethingNew = false;
					restartAuthenticationService();
				}
				try {
					Thread.sleep(timeout);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				collectAuthenticationPolicies();
			}
		}

	}

	private void keepCollecting() {
		PolicyCollector pc = new PolicyCollector();
		Thread pct = new Thread(pc);
		pct.start();
	}

	private WSPolicyClient() {
	}

	/**
	 * Starts PolicyClient if not already running
	 * 
	 * @param authenticationService Service to restart when new Authentication Methods are found
	 */

	public static void startPolicyClient() {

		if (running)
			return;
		running = true;

		if (!JMEDSFramework.isRunning()) {
			JMEDSFramework.start(null);
		}

		if (policyClient == null) {
			policyClient = new WSPolicyClient();
		}

		PolicyManager.initSecurityPolicyManager();

		Iterator it = deviceTypes.iterator();

		while (it.hasNext()) {
			SearchParameter sp = new SearchParameter();
			sp.setDeviceTypes(new QNameSet((QName) it.next()), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			policyClient.registerHelloListening(sp);
		}

		policyClient.keepCollecting();

	}

	public void collectPolicies(PolicyManager policyManager) {
		collectPolicies(null, policyManager);
	}

	public void collectPolicies(SearchParameter searchParameter, PolicyManager policyManager) {
		collectPolicies(searchParameter, null, policyManager);
	}

	/**
	 * Collects Policies
	 * @param searchParameter Device Types to discover (null for all)
	 * @param policyNames Policy Names to collect (null for all)
	 * @param policyManager PolicyManager to use (null for none - won't crash but won't save anything either)
	 */

	public void collectPolicies(SearchParameter searchParameter, QNameSet policyNames, PolicyManager policyManager) {
		//TODO: Create Mapping between searchParameters, PolicyManagers and
		//      policyNames to accordingly look them up when a device is found

		this.searchDevice(searchParameter);
	}

	public void collectAuthenticationPolicies() {
		/* Adjusted so that BYE-Events a are received. Filtering in deviceFound() */
		this.searchDevice(null);
	}

	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search, String comManId) {
		/* gosh... okay... let's focus on the policies for authentication...
		 * no multipurpose stuff... */

		boolean correctDevice = false;

		try {
			Iterator it = devRef.getDevicePortTypes(false);
			while (it.hasNext()) {
				QName pt = (QName) it.next();
				if (deviceTypes.contains(pt)) {
					correctDevice = true;
					break; /* no need to continue */
				}
			}
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (correctDevice) {
			Log.debug("++++++++++++++++++++++++++++++++WSPolicyClient found matching Device: " + devRef.getDiscoveryData().getEndpointReference().getAddress());
			fetchSecurityPolicies(devRef, false);
		}

	}

	private void fetchSecurityPolicies(DeviceReference devRef, boolean fromHello) {
		try {
			Device d = devRef.getDevice();

			ReadOnlyIterator sRefs = (ReadOnlyIterator) d.getServiceReferences(serviceTypes, SecurityKey.EMPTY_KEY);

			while (sRefs.hasNext()) {
				ServiceReference sRef = (ServiceReference) sRefs.next();

				Service s = sRef.getService();

				if (s.hasPolicies()) {
					for (Iterator it = s.getPolicies().values().iterator(); it.hasNext();) {
						Policy p = (Policy) (it.next());
						if (p.getName() != null && p.getName().equals(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName))) {
							PolicyManager pm = PolicyManager.getSecurityPolicyManagerInstance();
							RemotePolicy rp = new RemotePolicy(p, devRef);
							synchronized (this) { /* this part is incredibly likely to be called several times at once */
								boolean t = pm.addRemotePolicy(rp);
								this.somethingNew = (t && !fromHello) ? t : this.somethingNew;
								if (this.somethingNew) {
									Log.debug("++++++++++++++++++++++++++++++++Added new Policy: " + rp);
								} else {
									Log.debug("++++++++++++++++++++++++++++++++Ignored Policy: " + rp);
								}
								if (t && fromHello)
									restartAuthenticationService();
							}
						}
					}
				}

			}

		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void helloReceived(HelloData helloData) {

		DeviceReference devRef = getDeviceReference(helloData.getEndpointReference(), SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		/* This is a fix for a JMEDS bug resulting in Bye messages not
		 * processed for devices solely learned of by Hello messages */
		this.searchDevice(null);

		Log.debug("++++++++++++++++++++++++++++++++WSPolicyClient received Hello from matching Device: " + devRef.getDiscoveryData().getEndpointReference().getAddress());

		fetchSecurityPolicies(devRef, true);

	}

	//TODO: Maybe implement Device Listener, so Devices that are gracefully leaving the network are
	//      marked as unavaillable in the PolicyManager

	@Override
	public void deviceBye(DeviceReference deviceRef) {
		super.deviceBye(deviceRef);
		Log.debug("++++++++++++++++++++++++++++++++WSPolicyClient received Bye from: " + deviceRef.getDiscoveryData().getEndpointReference().getAddress());
		boolean removed = PolicyManager.getSecurityPolicyManagerInstance().removePoliciesForDevice(deviceRef);
		if (removed) {
			restartAuthenticationService();
			Log.debug("++++++++++++++++++++++++++++++++Removed its policies");
		} else {
			Log.debug("++++++++++++++++++++++++++++++++Nothing to be removed");
		}
	}

}
