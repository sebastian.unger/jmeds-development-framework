package org.ws4d.java.incubation.wspolicy.policymanagement;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wspolicy.ExactlyOne;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.PolicyAlternative;
import org.ws4d.java.incubation.wspolicy.PolicyAssertion;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;

public class PolicyManager {

	static PolicyManager sSecurityPolicyManager = null;



	private PolicyManager() {
		remotePolicies = new HashMap();
	}

	private HashMap remotePolicies = null;

	public boolean addRemotePolicy(RemotePolicy p) {
		/* Dragons ahead!
		 * 
		 * The point is to not add any remote mechanisms that are supported
		 * locally. For one, it would be silly to offer (and therefore store)
		 * them. Second, this prevents the live lock
		 * (A's fetching B's fetching A's fetching B's fetching...)
		 * 
		 * The following however assumes, that alternatives do have exactly
		 * one (1) assertion. No more. (And not less, obviously).
		 * 
		 * If you happen to need a better matching (e.g. by combining
		 * assertions in one alternative) you need to come up with something
		 * better.
		 * 
		 * By the way: storing identical remote mechanisms makes perfectly
		 * sense (redenundancy, different metrics, ...) it's just not useful
		 * to *display* every entry. Thus, double entries will be temporarily
		 * eleminated in getPolicies().
		 * 
		 * ***************************************************************** */

		if (p.getPolicy() == null)
			return false;

		if (p.getDeviceReference() != null && getLocalAuthenticationPolicy() != null) {
			Policy local = getLocalAuthenticationPolicy().getPolicy();

			Iterator it = ((ExactlyOne)local.getExactlyOnes().get(0)).getPolicyAlternatives().iterator();

			while (it.hasNext()) {
				PolicyAlternative pa = (PolicyAlternative) it.next();
				p.getPolicy().removeAssertions(((PolicyAssertion)pa.getPolicyAssertions().values().toArray()[0]).getName());
			}
		}
		AttributedURI key = (p.getDeviceReference() == null) ? new AttributedURI(WSSecurityForDevicesConstants.LOCALINDICATOR) : p.devRef.getEndpointReference().getAddress();
		/* finally, set remote Attribute if not already present and if not local*/
		if (p.getDeviceReference() != null) { /* not local */
			Iterator paIt = ((ExactlyOne)p.getPolicy().getExactlyOnes().get(0)).getPolicyAlternatives().iterator();
			while (paIt.hasNext()) {
				PolicyAlternative pa = (PolicyAlternative) paIt.next();
				Iterator pasIt = pa.getPolicyAssertions().values().iterator();
				while (pasIt.hasNext()) {
					PolicyAssertion assertion = (PolicyAssertion) pasIt.next();
					if (assertion.getAttributeName() == null) { /* not present */
						assertion.setAttribute(new QName("remote", WSSecurityForDevicesConstants.NAMESPACE), new QName(key.toString()));
					}
				}
			}
		}
		if (!p.equals((RemotePolicy)remotePolicies.get(key))) {
			remotePolicies.put(key, p);
			return true;
		}
		return false;
	}

	public HashMap getRemotePolicies() {
		return remotePolicies;
	}

	public RemotePolicy getLocalAuthenticationPolicy() {
		return (RemotePolicy) remotePolicies.get(new AttributedURI(WSSecurityForDevicesConstants.LOCALINDICATOR));
	}

	public HashMap getPolicies() {

		if (remotePolicies == null || remotePolicies.size() == 0)
			return null;

		HashMap hm = new HashMap(remotePolicies.size());

		Iterator policIterator = remotePolicies.values().iterator();

		Policy daPolicy = new Policy(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName));

		ExactlyOne eo = new ExactlyOne();

		while (policIterator.hasNext()) {
			RemotePolicy rp = (RemotePolicy) policIterator.next();
			Policy p = rp.getPolicy();

			Iterator eoIterator = p.getExactlyOnes().iterator();

			while (eoIterator.hasNext()) {
				ExactlyOne e = (ExactlyOne) eoIterator.next();
				Iterator altIterator = e.getPolicyAlternatives().iterator();
				while (altIterator.hasNext()) {
					PolicyAlternative pa = (PolicyAlternative) altIterator.next();
					Log.info("======================" + pa);
					/* yeah... implementing sth. simple with contains() and equals()
					 * doesn't work. Too tired.
					 */
					ArrayList palts = eo.getPolicyAlternatives();
					boolean exists = false;
					if (palts != null) {
						QName a = ((PolicyAssertion)pa.getPolicyAssertions().values().toArray()[0]).getName();
						for (int i = 0; i < palts.size(); i++) {
							QName b = ((PolicyAssertion)((PolicyAlternative)palts.get(i)).getPolicyAssertions().values().toArray()[0]).getName();
							Log.info("========= Comparing " + a + " and " + b);
							if (a.equals(b)) {
								exists = true;
								break;
							}
						}
					}
					if (!exists)
						eo.addPolicyAlternative(pa);
				}
			}

		}

		daPolicy.addExactlyOne(eo);

		hm.put(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName, daPolicy);

		return hm;
	}

	/**
	 * Creates instance of SecurityPolicyManager if none exists
	 * @return the (maybe created) Security Policy Manager instance
	 */
	public static PolicyManager initSecurityPolicyManager() {
		if (sSecurityPolicyManager == null) {
			sSecurityPolicyManager = new PolicyManager();
		}
		return sSecurityPolicyManager;
	}

	/**
	 * Delivers instance of SecurityPolicyManager
	 * 
	 * @return Security Policy Manager instance. null if none was initialized.
	 * @author Sebastian Unger
	 */
	public static PolicyManager getSecurityPolicyManagerInstance() {
		/* Do not initialize Security Policy Manager by default. It can be
		 * null on purpose  */
		return sSecurityPolicyManager;
	}

	public void clear() {
		remotePolicies.clear();
	}

	public void clearRemotes() {
		/* save local Policy */
		RemotePolicy rp = getLocalAuthenticationPolicy();
		remotePolicies.clear();
		addRemotePolicy(rp);
	}

	public boolean contains(RemotePolicy p) {
		return remotePolicies.containsValue(p);
	}

	public boolean removePoliciesForDevice(DeviceReference deviceRef) {

		boolean removedDirect = false;
		boolean removedIndirect = false;

		/* remove all Policies received from the device that left */
		removedDirect = remotePolicies.remove(deviceRef.getEndpointReference().getAddress()) != null;

		/* also, remove policies from other devices that had the device that left as its remote */
		PolicyManager pm = getSecurityPolicyManagerInstance();

		Iterator it = pm.getRemotePolicies().values().iterator();

		while (it.hasNext()) {
			Policy p = ((RemotePolicy)it.next()).getPolicy();
			removedIndirect = removedIndirect | p.removeAssertionsByRemote(deviceRef.getEndpointReference().getAddress());
		}

		return removedDirect || removedIndirect;
	}

	public void setLocalAuthenticationMechanism(QName mechanism) {
		setLocalAuthenticationMechanisms(new QNameSet(mechanism));
	}

	public void setLocalAuthenticationMechanisms(QNameSet mechanisms) {
		addRemotePolicy(new RemotePolicy(AuthenticationMechanismPolicy.getAuthenticationMechanismPolicies(mechanisms)));
	}

	/**
	 * This one is so frickin' important!!
	 * 
	 * If you ever happen to change local authentication mechanisms after they were initially set, you MUST use this
	 * method. You MUST NOT use anything else.
	 * 
	 * @param mechanisms
	 */

	public void updateLocalAuthenticationMechansims(QNameSet mechanisms) {
		remotePolicies.put(new AttributedURI(WSSecurityForDevicesConstants.LOCALINDICATOR), new RemotePolicy(AuthenticationMechanismPolicy.getAuthenticationMechanismPolicies(mechanisms)));
	}

}
