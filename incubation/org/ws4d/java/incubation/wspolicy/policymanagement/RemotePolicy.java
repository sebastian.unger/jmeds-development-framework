package org.ws4d.java.incubation.wspolicy.policymanagement;

import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class RemotePolicy {
	DeviceReference devRef = null; /* if null, it means local! */
	Policy policy = null;

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public DeviceReference getDeviceReference() {
		return devRef;
	}

	public void setDeviceReference(DeviceReference devRef) {
		this.devRef = devRef;
	}
	
	public RemotePolicy(Policy p) {
		this(p, null);
	}
	
	public RemotePolicy(Policy p, DeviceReference e) {
		policy = p;
		devRef = e;
	}
	
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
		
		sb.append(devRef.toString());
		
		sb.append(": ");
		
		sb.append(policy);
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		RemotePolicy p1 = (RemotePolicy) obj;
		if (devRef == null && p1.devRef == null) { 
			// okay... catch first...
		} else if ((devRef == null && p1.devRef != null) || (devRef != null && p1.devRef == null)) {
			return false;
		} else if (!devRef.getEndpointReference().getAddress().equals(p1.getDeviceReference().getEndpointReference().getAddress()))
			return false;
		return getPolicy().getName().equals(p1.getPolicy().getName());
	}
}
