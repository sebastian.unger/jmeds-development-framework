package org.ws4d.java.incubation.wspolicy;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class ExactlyOne {
	private ArrayList mPolicyAlternatives = null;

	public ArrayList getPolicyAlternatives() {
		return mPolicyAlternatives;
	}

	public void setPolicyAlternatives(ArrayList policyAlternatives) {
		this.mPolicyAlternatives = policyAlternatives;
	}
	
	public boolean addPolicyAlternative(PolicyAlternative policyAlternative) {
		if (mPolicyAlternatives == null) {
			mPolicyAlternatives = new ArrayList();
		}
		return mPolicyAlternatives.add(policyAlternative);
	}
	
	public String toString()  {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
		
		sb.append("ExactlyOne (");
		
		for (Iterator it = mPolicyAlternatives.iterator(); it.hasNext();) {
			sb.append((PolicyAlternative)it.next()).append(", ");
		}
		
		sb.append(")");
		
		return sb.toString();
		
	}

	public void removeAssertions(QName assertion) {
		for (int i = 0 ; i < mPolicyAlternatives.size(); i++) {
			PolicyAlternative pa = (PolicyAlternative) mPolicyAlternatives.get(i);
			pa.removeAssertions(assertion);
			if (pa.isEmpty())
				mPolicyAlternatives.remove(i);
		}
	}

	public boolean isEmpty() {
		if (mPolicyAlternatives == null)
			return true;
		return (mPolicyAlternatives.size() == 0);
	}
	
	public boolean contains(PolicyAlternative pa) {
		if (mPolicyAlternatives == null || pa == null)
			return false;
		return mPolicyAlternatives.contains(pa);
	}

	public boolean removeAssertionsByRemote(AttributedURI address) {
		boolean res = false;
		for (int i = 0 ; i < mPolicyAlternatives.size(); i++) {
			PolicyAlternative pa = (PolicyAlternative) mPolicyAlternatives.get(i);
			res = res | pa.removeAssertionsByRemote(address);
			if (pa.isEmpty())
				mPolicyAlternatives.remove(i);
		}
		return res;
	}
	
}
