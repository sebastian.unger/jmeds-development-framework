package org.ws4d.java.incubation.wspolicy.securitypolicy;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wspolicy.ExactlyOne;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.PolicyAlternative;
import org.ws4d.java.incubation.wspolicy.PolicyAssertion;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;

/*
 * AuthenticationMechanisms always do have the following form:
 * - A policy with name attribute "AuthenticationMechanism"
 * --  Exactly one ExactlyOne-Child
 * ---   Each mechanism is depicted by exactly one assertion in an alternative
 * 
 * Example:
 *   <wsp:Policy Name="AuthenticationMechanism" xmlns:wsp="http://www.w3.org/ns/ws-policy">
 *   	<wsp:ExactlyOne>
 *   		<wsp:All>
 *   			<ns:mechanismname1 ns="..." />
 *   		</wsp:All>
 *   		<wsp:All>
 *   			<ns:mechanismname2 ns="..." />
 *   		</wsp:All>
 *              .
 *              .
 *              .
 *   		<wsp:All>
 *   			<ns:mechanismnamen ns="..." />
 *   		</wsp:All>
 *   	</wsp:ExactlyOne>
 *   </wsp:Policy>
 */

public class AuthenticationMechanismPolicy extends Policy {

	public AuthenticationMechanismPolicy() {
		setName(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName));
		this.addExactlyOne(new ExactlyOne());
	}

	public void addMechanism(String mechanism) {
		addMechanism(new QName(mechanism));
	}

	public void addMechanism(QName mechanism) {
		PolicyAlternative palt = new PolicyAlternative();
		palt.addPolicyAssertion(new PolicyAssertion(mechanism));
		((ExactlyOne)this.getExactlyOnes().get(0)).addPolicyAlternative(palt);
	}
	public void addMechanisms(QNameSet mechanisms) {
		if (mechanisms == null)
			return;
		Iterator mechIt = mechanisms.iterator();
		while (mechIt.hasNext()) {
			QName mech = (QName) mechIt.next();
			addMechanism(mech);
		}
	}

	public static Policy getAuthenticationMechanismPolicies(QName mechanism) {
		return getAuthenticationMechanismPolicies(new QNameSet(mechanism));
	}

	public static Policy getAuthenticationMechanismPolicies(QNameSet mechanisms) {

		if (mechanisms.isEmpty())
			return null;

		AuthenticationMechanismPolicy p = new AuthenticationMechanismPolicy();
		p.addMechanisms(mechanisms);

		return p;
	}

	public static QNameSet getAuthenticationMechanisms(Policy policy) {

		if (policy == null)
			return null;

		QNameSet mechanisms = new QNameSet();

		if (policy.getName() == null || !policy.getName().equals(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName))) {
			Log.warn("Policy name is not right: " + policy.getName());
			return mechanisms;
		}

		if (policy.getExactlyOnes() == null) {
			return mechanisms;
		}

		Iterator eoit = policy.getExactlyOnes().iterator();

		while (eoit.hasNext()) {
			ExactlyOne eo = (ExactlyOne) eoit.next();
			if (eo.getPolicyAlternatives() == null) {
				continue;
			}
			Iterator altit = eo.getPolicyAlternatives().iterator();
			while (altit.hasNext()) {
				PolicyAlternative palt = (PolicyAlternative) altit.next();
				if (palt.getPolicyAssertions() == null) {
					continue;
				}
				Iterator assit = palt.getPolicyAssertions().values().iterator();
				while (assit.hasNext()) {
					PolicyAssertion pa = (PolicyAssertion) assit.next();
					mechanisms.add(pa.getName());
				}
			}
		}

		return mechanisms;
	}

}


