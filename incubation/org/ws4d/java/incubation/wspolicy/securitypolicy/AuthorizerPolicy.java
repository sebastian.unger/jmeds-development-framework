package org.ws4d.java.incubation.wspolicy.securitypolicy;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wspolicy.ExactlyOne;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.PolicyAlternative;
import org.ws4d.java.incubation.wspolicy.PolicyAssertion;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;

/*
 * Authorizer Policies always do have the following form:
 * - A policy with name attribute "Authorizers"
 * --  Exactly one ExactlyOne-Child
 * --- Exactly one alternative
 * ---- The Primary Authorizer is depicted by the PrimaryAuthorizer Assertion
 * ---- Each remaining Authorizer is depicted by exactly one Authorizer Assertion in the alternative
 * 
 *
 * Example:
 *   <wsp:Policy Name="Authorizers" xmlns:wsp="http://www.w3.org/ns/ws-policy">
 *   	<wsp:ExactlyOne>
 *   		<wsp:All>
 *   			<ns:PrimaryAuthorizer address="[Address1]" />
 *   			<ns:Authorizer address="[Address2]" />
 *              .
 *              .
 *              .
 *   			<ns:Authorizer address="[Addressn]" />
 *   		</wsp:All>
 *   	</wsp:ExactlyOne>
 *   </wsp:Policy>
 */

// FIXME CRITICAL!!!
//
// PolicyAssertions are organized as Hashmap which ain't no good for this!

public class AuthorizerPolicy extends Policy {
	public AuthorizerPolicy() {
		super(new QName(WSSecurityForDevicesConstants.AuthorizersPolicyName));
		ExactlyOne eo = new ExactlyOne();
		eo.addPolicyAlternative(new PolicyAlternative());
		this.addExactlyOne(eo);
	}

	public void addPrimaryAuthorizer(String urn) {
		addAuthorizer(urn, true);
	}

	public void addAuthorizer(String urn, boolean primary) {
		PolicyAssertion pass = null;
		if (primary) {
			pass = new PolicyAssertion(WSSecurityForDevicesConstants.PrimaryAuthorizerPolicyName);
		} else {
			pass = new PolicyAssertion(WSSecurityForDevicesConstants.AuthorizerPolicyName);
		}
		pass.setAttributeName(new QName(WSSecurityForDevicesConstants.AuthorizerPolicyAttributeName));
		pass.setAttributeValue(new QName(urn));
		((PolicyAlternative)((ExactlyOne)this.getExactlyOnes().get(0)).getPolicyAlternatives().get(0)).addPolicyAssertion(pass);
	}

	public static String getPrimaryAuthorizer(Policy policy) {
		if (policy == null || policy.getExactlyOnes() == null || policy.getExactlyOnes().isEmpty()){
			return null;
		}
		ExactlyOne eo = (ExactlyOne) policy.getExactlyOnes().get(0);

		if (eo.getPolicyAlternatives() == null || eo.getPolicyAlternatives().isEmpty()){
			return null;
		}

		PolicyAlternative palt = (PolicyAlternative) eo.getPolicyAlternatives().get(0);

		HashMap assertions = palt.getPolicyAssertions();

		if (assertions == null || assertions.size() == 0){
			return null;
		}

		Iterator assit = assertions.values().iterator();
		while (assit.hasNext()) {
			PolicyAssertion pass = (PolicyAssertion) assit.next();
			if (pass.getName().equals(WSSecurityForDevicesConstants.PrimaryAuthorizerPolicyName)) {
				return "urn:" + pass.getAttributeValue().getLocalPart(); /* GOSH!!*/
			}
		}
		return null;
	}

}
