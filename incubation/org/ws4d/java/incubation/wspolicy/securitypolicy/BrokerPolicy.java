package org.ws4d.java.incubation.wspolicy.securitypolicy;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wspolicy.ExactlyOne;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.PolicyAlternative;
import org.ws4d.java.incubation.wspolicy.PolicyAssertion;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.QName;

public class BrokerPolicy extends Policy {

	/*
	 * Broker Policies always do have the following form:
	 * - A policy with name attribute "Brokers"
	 * --  Exactly one ExactlyOne-Child
	 * ---   Each mechanism is depicted by exactly one assertion in an alternative
	 *
	 * Example:
	 *   <wsp:Policy Name="Brokers" xmlns:wsp="http://www.w3.org/ns/ws-policy">
	 *   	<wsp:ExactlyOne>
	 *   		<wsp:All>
	 *   			<ns:broker address="[Broker1 Address]" />
	 *   		</wsp:All>
	 *   		<wsp:All>
	 *   			<ns:broker address="[Broker2 Address]" />
	 *   		</wsp:All>
	 *              .
	 *              .
	 *              .
	 *   		<wsp:All>
	 *   			<ns:broker address="[Brokern Address]" />
	 *   		</wsp:All>
	 *   	</wsp:ExactlyOne>
	 *   </wsp:Policy>
	 */

	public BrokerPolicy() {
		setName(new QName(WSSecurityForDevicesConstants.BrokersPolicyName));
		this.addExactlyOne(new ExactlyOne());
	}

	public void addBroker(AttributedURI uri) {
		addBroker(uri.toString());
	}

	public void addBroker(String uri) {
		PolicyAlternative palt = new PolicyAlternative();
		palt.addPolicyAssertion(new PolicyAssertion(new QName(WSSecurityForDevicesConstants.BrokerPolicyName, WSSecurityForDevicesConstants.NAMESPACE), new QName(WSSecurityForDevicesConstants.BrokerPolicyAttributeName), new QName(uri)));
		((ExactlyOne)this.getExactlyOnes().get(0)).addPolicyAlternative(palt);
	}

	/**
	 * Converts a BrokersPolicy to an ArrayList of AttributedURIs of Brokers
	 * @param brokersPolicy Policy to convert
	 * @return ArrayList of AttributedURIs of Brokers
	 */
	public static ArrayList getBrokers(Policy brokersPolicy) {

		if (brokersPolicy == null)
			return null;

		ArrayList result = new ArrayList();

		if (!brokersPolicy.getName().equals(new QName(WSSecurityForDevicesConstants.BrokersPolicyName))) {
			return null;
		}

		if (brokersPolicy.getExactlyOnes() == null) {
			return result;
		}

		Iterator eoit = brokersPolicy.getExactlyOnes().iterator();

		while (eoit.hasNext()) {
			ExactlyOne eo = (ExactlyOne) eoit.next();
			if (eo.getPolicyAlternatives() == null) {
				continue;
			}
			Iterator altit = eo.getPolicyAlternatives().iterator();
			while (altit.hasNext()) {
				PolicyAlternative palt = (PolicyAlternative) altit.next();
				if (palt.getPolicyAssertions() == null) {
					continue;
				}
				Iterator assit = palt.getPolicyAssertions().values().iterator();
				while (assit.hasNext()) {
					PolicyAssertion pa = (PolicyAssertion) assit.next();
					String uri = pa.getAttributeValue().toString();

					/* I DO know this is an ugly fix and I certainly AM ashamed of it...
					 * But for heaven's sake I'm so sick of it...
					 */

					if (!uri.substring(0, 4).equals("urn:")) {
						StringBuilder res = new StringBuilder();
						res.append("urn:");
						res.append(uri);
						result.add(new AttributedURI( res.toString()));
					} else {
						result.add(new AttributedURI( uri ));
					}
				}
			}
		}
		return result;
	}

}
