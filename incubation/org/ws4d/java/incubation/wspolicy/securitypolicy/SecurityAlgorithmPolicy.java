package org.ws4d.java.incubation.wspolicy.securitypolicy;

import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wspolicy.ExactlyOne;
import org.ws4d.java.incubation.wspolicy.Policy;
import org.ws4d.java.incubation.wspolicy.PolicyAlternative;
import org.ws4d.java.incubation.wspolicy.PolicyAssertion;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class SecurityAlgorithmPolicy extends Policy {

	/* 
	 * SecurityAlgorithmPolicies always do have the following form:
	 * - A policy with name attribute "SupportedAlgorithms"
	 * --  Exactly one ExactlyOne-Child
	 * ---   Each mechanism combination(!) is depicted by a Policy Alternative (an <All>-Element)
	 * 
	 * Example: 
	 *   <wsp:Policy Name="SuppertedAlgorithms" xmlns:wsp="http://www.w3.org/ns/ws-policy">
	 *   	<wsp:ExactlyOne>
	 *   		<wsp:All>
	 *   			<ns:signaturealgorithmname1 ns="..." />
	 *   			<ns:encryptionalgorithmname1 ns="..." />
	 *   			<ns:keyderivationalgorithmname1 ns="..." />
	 *   		</wsp:All>
	 *   		<wsp:All>
	 *   			<ns:signaturealgorithmname2 ns="..." />
	 *   			<ns:encryptionalgorithmname2 ns="..." />
	 *   			<ns:keyderivationalgorithmname2 ns="..." />
	 *   		</wsp:All>
	 *              .
	 *              .
	 *              .
	 *   		<wsp:All>
	 *   			<ns:signaturealgorithmnamen ns="..." />
	 *   			<ns:encryptionalgorithmnamen ns="..." />
	 *   			<ns:keyderivationalgorithmnamen ns="..." />
	 *   		</wsp:All>
	 *   	</wsp:ExactlyOne>
	 *   </wsp:Policy>
	 */
	
	public SecurityAlgorithmPolicy() {
		super();
		this.addExactlyOne(new ExactlyOne());
		this.setName(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName));
	}
	
	public void addAlgorithms(ArrayList algorithmsSets) {
		for (int i = 0 ; i < algorithmsSets.size(); i++) {
			SecurityAlgorithmSet set = (SecurityAlgorithmSet) algorithmsSets.get(i);
			addAlgorithms(set.getSignatureAlgorithm(), set.getEncryptionAlgorithm(), set.getDerivationAlgorithm());
		}
	}
	
	public void addAlgorithms(SecurityAlgorithmSet set) {
		addAlgorithms(set.getSignatureAlgorithm(), set.getEncryptionAlgorithm(), set.getDerivationAlgorithm());
	}
	
	public void addAlgorithms(QName sigAlg, QName encAlg, QName dervAlg) {
		PolicyAlternative palt = new PolicyAlternative();
		if (sigAlg  != null) palt.addPolicyAssertion(new PolicyAssertion(sigAlg, new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeName), new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeSignature)));
		if (encAlg  != null) palt.addPolicyAssertion(new PolicyAssertion(encAlg, new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeName), new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeEncryption)));
		if (dervAlg != null) palt.addPolicyAssertion(new PolicyAssertion(dervAlg, new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeName), new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeDerivation)));
		((ExactlyOne)this.getExactlyOnes().get(0)).addPolicyAlternative(palt);
	}
	
	public static Policy getSecurityAlgorithmsPolicy(SecurityAlgorithmSet set) {
		ArrayList al = new ArrayList(1);
		al.add(set);
		return getSecurityAlgorithmsPolicy(al);
	}
	
	public static Policy getSecurityAlgorithmsPolicy(ArrayList algorithmSets) {
		SecurityAlgorithmPolicy sap = new SecurityAlgorithmPolicy();
		sap.addAlgorithms(algorithmSets);
		return sap;
	}
	
	public static ArrayList getSecurityAlgorithms(Policy policy) {
		ArrayList al = new ArrayList();
		
		if (policy.getName() == null || !policy.getName().getLocalPart().equals(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyName)) {
			Log.warn("Parsing Security Algorithms: Wrong Policy Name: " + policy.getName());
			return al;
		}
		
		if (policy.getExactlyOnes() == null || policy.getExactlyOnes().size() == 0) {
			return al;
		}
		
		ArrayList alts = ((ExactlyOne) policy.getExactlyOnes().get(0)).getPolicyAlternatives();
		
		for (int i = 0; i < alts.size(); i++) {
			SecurityAlgorithmSet sas = new SecurityAlgorithmSet();
			PolicyAlternative palt = (PolicyAlternative) alts.get(i);
			
			Iterator assIt = palt.getPolicyAssertions().values().iterator();
			
			while (assIt.hasNext()) {
				PolicyAssertion pass = (PolicyAssertion) assIt.next();
				if (pass.getAttributeName().equals(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeName))) {
					if (pass.getAttributeValue().equals(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeSignature))) {
						sas.setSignatureAlgorithm(pass.getName());
					} else if (pass.getAttributeValue().equals(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeEncryption))) {
						sas.setEncryptionAlgorithm(pass.getName());
					} else if (pass.getAttributeValue().equals(new QName(WSSecurityForDevicesConstants.SupportedAlgorithmsPolicyAttributeDerivation))) {
						sas.setDerivationAlgorithm(pass.getName());
					}
				}
			}
			
			al.add(sas);			
		}
		
		return al;
	}
	
}
