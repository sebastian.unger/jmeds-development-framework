package org.ws4d.java.incubation.wspolicy;

import org.ws4d.java.types.QName;

public class PolicyAssertion {
	private QName mName;
	private QName mAttributeName;
	private QName mAttributeValue;
	public PolicyAssertion() {
		this(null, null, null);
	}
	public PolicyAssertion(QName name) {
		this(name, null, null);
	}
	public PolicyAssertion(QName name, QName attributeName, QName attributeValue) {
		mName = name;
		mAttributeName = attributeName;
		mAttributeValue = attributeValue;
	}
	public QName getName() {
		return mName;
	}
	public void setName(QName name) {
		this.mName = name;
	}
	public void setAttribute(QName name, QName value) {
		this.mAttributeName = name;
		this.mAttributeValue = value;
	}
	public QName getAttributeValue() {
		return mAttributeValue;
	}
	public void setAttributeValue(QName value) {
		this.mAttributeValue = value;
	}
	public QName getAttributeName() {
		return mAttributeName;
	}
	public void setAttributeName(QName name) {
		this.mAttributeName = name;
	}
	public String toString() {
		return mName.toString() + " " 
				+ ((mAttributeName == null) ? "" : mAttributeName.toString()) + "=\""
				+ ((mAttributeValue == null) ? "" : mAttributeValue.toString()) + "\"";
	}
	public boolean nameEquals(PolicyAssertion pa) {
		return this.mName.equals(pa.mName);
	}
	public boolean equals(PolicyAssertion pa) {
		return this.mName.equals(pa.mName) 
				&& ((mAttributeValue == null) ? true : (mAttributeValue.equals(pa.mAttributeValue)))
				&& ((mAttributeName == null) ? true : (mAttributeName.equals(pa.mAttributeName)));
	}
}
