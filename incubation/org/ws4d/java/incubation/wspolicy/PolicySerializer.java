package org.ws4d.java.incubation.wspolicy;

import org.ws4d.java.structures.Iterator;

public class PolicySerializer {
	
	public static String serialize(Policy policy) {
	String string = "";	
	string += "<Policy";
	if (policy.getName() != null) {
		string += " Name=\"" + policy.getName().toString() + "\"";
	}
	if (policy.getId() != null) {
		string += " Id=\"" + policy.getId().toString() + "\"";
	}
	string += ">";
	
	
	
	for (Iterator it = policy.getExactlyOnes().iterator(); it.hasNext(); ) {
		
		ExactlyOne e = (ExactlyOne) it.next();
		
		string += "<ExactlyOne>";
		
		for (Iterator jt = e.getPolicyAlternatives().iterator();jt.hasNext();) {
			PolicyAlternative p = (PolicyAlternative) jt.next();
			string += "<All>";
			
			for (Iterator kt = p.getPolicyAssertions().values().iterator(); kt.hasNext();) {
			PolicyAssertion a = (PolicyAssertion) kt.next();
				if (a.getAttributeValue() == null)
					string += "<" + a.getName().toString() + " />";
				else
					string += "<" + a.getName().toString() + ">"
					+ a.getAttributeValue().toString()
					+ "</" + a.getName().toString() + ">";
			}
			
			string += "</All>";
		}
		
		string += "</ExactlyOne>";
	}
	
	
	string += "</Policy>";
	
	
	return string;
	}
}
