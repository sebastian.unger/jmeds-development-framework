package org.ws4d.java.incubation.wspolicy;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class PolicyAlternative {
	private HashMap mPolicyAssertions = null;

	public HashMap getPolicyAssertions() {
		return mPolicyAssertions;
	}

	public void setPolicyAssertions(HashMap policyAssertions) {
		this.mPolicyAssertions = policyAssertions;
	}
	
	public void addPolicyAssertion(PolicyAssertion policyAssertion) {
		if (mPolicyAssertions == null) {
			mPolicyAssertions = new HashMap();
		}
		mPolicyAssertions.put(policyAssertion.getName().toString(), policyAssertion);
	}
	
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
		
		sb.append("All (");
		
		for (Iterator it = mPolicyAssertions.values().iterator(); it.hasNext();) {
			sb.append(((PolicyAssertion)it.next())).append(",");
		}
		
		sb.append(")");
		
		return sb.toString();		
	}

	public void removeAssertions(QName assertion) {
		mPolicyAssertions.remove(assertion.toString());
	}

	public boolean isEmpty() {
		if (mPolicyAssertions == null)
			return true;
		return mPolicyAssertions.size() == 0;
	}
	
	public boolean equals(PolicyAlternative pa) {
		if (pa == null)
			return false;
		return this.mPolicyAssertions.equals(pa.getPolicyAssertions());
	}

	public boolean removeAssertionsByRemote(AttributedURI address) {
		boolean res = false;
		Iterator it = mPolicyAssertions.values().iterator();
		while (it.hasNext()) {
			PolicyAssertion pass = (PolicyAssertion) it.next();
			if (pass.getAttributeValue() != null && pass.getAttributeValue().getLocalPart().equals(address.toString())) {
				mPolicyAssertions.remove(pass.getName());
				res = true;
				it  = mPolicyAssertions.values().iterator();
			}
		}
		return res;
	}
	
}
