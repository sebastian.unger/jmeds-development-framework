package org.ws4d.java.incubation.CredentialManagement;

import org.ws4d.java.structures.HashMap;

public class AuthorizationDBFactory {

	static HashMap dbs = new HashMap();

	public static SimpleAuthorizationDB getAuthorizationDB(String id) {
		SimpleAuthorizationDB result = (SimpleAuthorizationDB) dbs.get(id);
		if (result == null) {
			result = new SimpleAuthorizationDB();
			dbs.put(id, result);
		}
		return result;
	}
}
