package org.ws4d.java.incubation.CredentialManagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

public class DefaultSimpleContextDBPersistence implements SimpleContextDBPersistenceInterface {

	@Override
	public boolean loadFromFile(SimpleContextDB db, String filename) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(filename));

			SecurityContext ct = (SecurityContext) ois.readObject();
			while(ct != null) {
				db.addContext(ct.getContextReference(), ct);
				ct = (SecurityContext) ois.readObject();
			}
		} catch (FileNotFoundException e) {
			Log.warn("No Database file found: " + filename);
			return false;
		} catch (IOException e) {
			/* this most likely means that there is no (more) data to read. */
			//TODO: use an EOF-Marker to prevent Exceptions
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			Log.info("Read " + db.size() + " security associations from file " + filename);
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public boolean saveToFile(SimpleContextDB db, String filename) {
		ObjectOutputStream oos = null;
		try {
			Iterator it = db.getAllContexts().values().iterator();

			oos = new ObjectOutputStream(new FileOutputStream(filename));
			while (it.hasNext()) {
				SecurityContext ct = (SecurityContext)it.next();
				oos.writeObject(ct);
				Log.info(ct.toString());
			}
			Log.info("Wrote " + db.size() + " Security associations to file " + filename);
		} catch (IOException e) {
			Log.warn(e.getMessage());
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public boolean clearDatabase(SimpleContextDB db, String filename) {
		synchronized (AuthenticationEngine.getInstance()) {
			db.clearContexts();
		}
		File f = new File(filename);
		if (f.delete()) {
			Log.info("Key file deleted");
			return true;
		} else {
			Log.warn("could not delete key file");
			return false;
		}
	}

}
