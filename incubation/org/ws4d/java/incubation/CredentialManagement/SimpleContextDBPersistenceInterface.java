package org.ws4d.java.incubation.CredentialManagement;

public interface SimpleContextDBPersistenceInterface {
	boolean loadFromFile(SimpleContextDB db, String filename);
	boolean saveToFile(SimpleContextDB db, String filename);
	boolean clearDatabase(SimpleContextDB db, String filename);
}
