package org.ws4d.java.incubation.CredentialManagement;

import java.io.Serializable;

import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;

public class SecurityContext implements Serializable {
	private static final long serialVersionUID = -7288883212605796643L;
	private String contextReference = null;
	private SecurityToken securityToken = null;
	private boolean broker = false;
	private int authorizerHint = 0; /* 0 - none, 1 - primary, 2 - secondary */

	public SecurityContext(String reference, SecurityToken token) {
		contextReference = reference;
		securityToken = token;
	}

	public String getContextReference() {
		return contextReference;
	}

	public void setContextReference(String contextReference) {
		this.contextReference = contextReference;
	}

	public SecurityToken getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(SecurityToken token) {
		this.securityToken = token;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Reference: ");
		sb.append(contextReference);
		sb.append("\n (Broker: " + (this.isBroker() ? "yes" : "no") + ")SecurityToken: (");
		if (this.securityToken == null) {
			sb.append("null");
		} else {
			sb.append("   Identification: " + securityToken.getIdentification() + "\n");
			sb.append("   Signature Algorithm: " + AlgorithmConstants.algorithmString.get(securityToken.getsAlgorithmType()) + "(" + securityToken.getsAlgorithmType() + ")\n");
			sb.append("   Encryption Algorithm: " + AlgorithmConstants.algorithmString.get(securityToken.geteAlgorithmType()) + "(" + securityToken.geteAlgorithmType() + ")\n");
			sb.append("   Key Material:");
			for (int i = 0; i < securityToken.getKeymaterial().length; i++) {
				sb.append(String.format(" %02X", securityToken.getKeymaterial()[i]));
			}
			sb.append("\n");
		}
		sb.append(")");
		return sb.toString();
	}

	public void setIsBroker(boolean isOriginBroker) {
		this.broker = isOriginBroker;
	}

	public boolean isBroker() {
		return broker;
	}

	public int getAuthorizerHint() {
		return authorizerHint;
	}

	public void setAuthorizerHint(int authorizerHint) {
		this.authorizerHint = authorizerHint;
	}

}
