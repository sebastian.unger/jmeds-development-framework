package org.ws4d.java.incubation.CredentialManagement;

import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationRule;
import org.ws4d.java.service.Operation;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

public class SimpleAuthorizationDB {
	ArrayList daRules;

	public SimpleAuthorizationDB() {
		daRules = new ArrayList();
	}

	public void add(AuthorizationRule rule) {
		daRules.add(rule);
	}

	public AuthorizationDecision checkAuthorization(String rId, String theSubject, Operation resource) {
		String subject = (theSubject == null) ? AuthenticationEngine.getDefaultOwnerID() : theSubject;

		Iterator rules = daRules.iterator();

		AuthorizationDecision result = AuthorizationDecision.INDETERMINATE;

		while (rules.hasNext()) {
			AuthorizationRule rule = (AuthorizationRule) rules.next();
			Log.debug("checkAuthorization(): Checking " + rule.toString());
			if (theSubject == null && resource == null) {
				Log.warn("WHACKO!!!");
				continue;
			}
			if (!rule.getSubjectUri().equals(subject)) {
				Log.debug("Subject no good");
				continue;
			}
			if (!rule.getClientUri().equals(rId)) {
				Log.debug("Client no good");
				continue;
			}
			if (resource != null) {
				if (rule.getRessource() != null && !resource.getNameQuallified().equals(rule.getRessource())) {
					Log.debug("Resource no good!");
					continue;
				}
			}
			if (rule.getDecision() == AuthorizationDecision.DENY) {
				Log.debug("Decision no good!");
				result = AuthorizationDecision.DENY;
				continue;
			}
			Log.info("checkAuthorization(): Rule " + rule.toString() + "FIRES!!");
			return AuthorizationDecision.PERMIT;
		}

		return result;
	}

	public ArrayList getAllRules() {
		return daRules;
	}

	public int size() {
		return daRules.size();
	}

	public void clearContexts() {
		daRules.clear();
	}

}
