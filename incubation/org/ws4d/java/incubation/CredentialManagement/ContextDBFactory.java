package org.ws4d.java.incubation.CredentialManagement;

import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.structures.HashMap;

public class ContextDBFactory {
	private static HashMap dbs = new HashMap();
	
	public static SimpleContextDB getContextDatabase(String id) {
		String lid = (id == null) ? AuthenticationEngine.getSafeDefaultOwnerID() : id;
		SimpleContextDB result = (SimpleContextDB) dbs.get(lid);
		if (result == null) {
			result = new SimpleContextDB(id);
			dbs.put(lid, result);
		}
		return result;
	}
	
	public static SimpleContextDB getContextDatabase() {
		return getContextDatabase(null);
	}
	
}
