package org.ws4d.java.incubation.CredentialManagement;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;

/**
 * You should not instantiate this directly but rather use the ContextDBFactory
 * to access it.
 * 
 * @author cht
 *
 */
public class SimpleContextDB {
	private String ownerID = null;
	HashMap savedContexts = null;

	public HashMap getAllContexts() {
		return savedContexts;
	}

	/**
	 * You would not want to use this! Rather use ContextDBFactory!
	 * @param myID
	 */
	public SimpleContextDB(String myID) {
		ownerID = myID;
		savedContexts = new HashMap();
	}

	public String getOwnerID() {
		return ownerID;
	}

	public void addContext(SecurityContext ct) {
		addContext(ct.getContextReference(), ct);
	}
	public void addContext(String reference, SecurityContext ct) {
		savedContexts.put(reference, ct);
	}

	public void addContext(SecurityToken ak) {
		SecurityContext ct = new SecurityContext(ak.getIdentification(), ak);
		addContext(ct.getContextReference(), ct);
	}

	public boolean removeContext(String reference) {
		return (savedContexts.remove(reference) != null);
	}

	public void clearContexts() {
		savedContexts.clear();
	}

	/**
	 * Reference usually is the UUID of the device the context is associated with
	 * @param reference
	 * @return
	 */
	public SecurityContext getContextByReference(String reference) {
		return (SecurityContext) savedContexts.get(reference);
	}

	/**
	 * Basically same as by reference. However, if it fails, the
	 * Security Tokens' id's are considered as well.
	 * @param id
	 * @return
	 */
	public SecurityContext getContextById(String id) {
		/* first, try getting by key */
		SecurityContext result = (SecurityContext) savedContexts.get(id);
		if (result != null) {
			return result;
		}

		/* If not successful, try getting by token's reference (which SHOULD be key */
		Iterator it = savedContexts.values().iterator();
		while (it.hasNext()) {
			SecurityContext ct = (SecurityContext) it.next();
			System.err.println("Comparing saved " + ct.getContextReference() + " and required " + id);
			if (id.equals(ct.getContextReference()))
				return ct;
		}

		/* If all fails, try to get by token's(aka associated key's) Identifier */
		it = savedContexts.values().iterator();
		while (it.hasNext()) {
			result = (SecurityContext) it.next();
			if (result.getSecurityToken().getIdentification().equals(id)) {
				return result;
			}
		}

		return null;
	}

	public int size() {
		return savedContexts.size();
	}

	//FIXME: That's not yet all, folks!!
	public boolean checkAuthentication(String rId) {
		SecurityContext ct = this.getContextById(rId);
		if (ct == null)
			return false;
		SecurityToken st = ct.getSecurityToken();
		if (st == null)
			return false;
		if (st.getKeymaterial() == null)
			return false;
		else
			return true;
	}

}
