package org.ws4d.java.incubation.CredentialManagement;

import java.io.Serializable;

import org.ws4d.java.communication.protocol.http.Base64Util;

/**
 * Very basic credentialmanagement. Contains the partner's UUID, the 
 * associated key and algorithms for encryption and signatures.
 * Algorithms are saved as types that are defined in
 * WSSecurityForDevices.AlgorithmConstants
 * 
 * @author cht
 *
 */

public class SecurityToken implements Serializable {
	private static final long serialVersionUID = -3850648761447631906L;
	private String identification = null;
	private byte[] keymaterial = null;
	private int eAlgorithmType;
	private int sAlgorithmType;
	
	/**
	 * Create an Associated Key
	 * @param The communication partner's ID
	 * @param the keying material
	 * @param encryption Algorithm Type (see WSSecurityForDevices.AlgorithmConstants)
	 * @param signature Algorithm Type (see WSSecurityForDevices.AlgorithmConstants)
	 * @param optional reference, may be null or empty string
	 */
	public SecurityToken(String id, byte[] key, int eType, int sType) {
		identification = id;
		keymaterial = key;
		eAlgorithmType = eType;
		sAlgorithmType = sType;
	}
	
	/**
	 * Create an Associated Key
	 * @param The communication partner's ID
	 * @param the keying material (base64-coded)
	 * @param encryption Algorithm Type (see WSSecurityForDevices.AlgorithmConstants)
	 * @param signature Algorithm Type (see WSSecurityForDevices.AlgorithmConstants)
	 * @param optional reference, may be null or empty string
	 */
	public SecurityToken(String id, String key, int eType, int sType) {
		identification = id;
		keymaterial = Base64Util.decode(key);
		eAlgorithmType = eType;
		sAlgorithmType = sType;
	}
	
	public String getIdentification() {
		return identification;
	}


	public void setIdentification(String identification) {
		this.identification = identification;
	}


	public byte[] getKeymaterial() {
		return keymaterial;
	}


	public String getKeymaterialB64() {
		return Base64Util.encodeBytes(keymaterial);
	}


	public void setKeymaterial(byte[] keymaterial) {
		this.keymaterial = keymaterial;
	}


	public void setKeymaterial(String b64Keymaterial) {
		this.keymaterial = Base64Util.decode(b64Keymaterial);
	}


	public int geteAlgorithmType() {
		return eAlgorithmType;
	}


	public void seteAlgorithmType(int eAlgorithmType) {
		this.eAlgorithmType = eAlgorithmType;
	}


	public int getsAlgorithmType() {
		return sAlgorithmType;
	}


	public void setsAlgorithmType(int sAlgorithmType) {
		this.sAlgorithmType = sAlgorithmType;
	}


}
