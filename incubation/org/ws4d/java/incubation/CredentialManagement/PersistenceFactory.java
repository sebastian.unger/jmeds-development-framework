package org.ws4d.java.incubation.CredentialManagement;

import java.util.Locale;

public class PersistenceFactory {
	public static SimpleContextDBPersistenceInterface getPersistence() {
		SimpleContextDBPersistenceInterface result = null;
		String runtime = System.getProperty("java.runtime.name", "WAT VM");
		if (runtime.toLowerCase(Locale.ENGLISH).contains("android")) {
			try {
				result = ((SimpleContextDBPersistenceInterface) Class.forName("org.ws4d.wscompactsecurity.authentication.engine.AndroidSimpleContextDBPersistence").newInstance());
				return result;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			return new DefaultSimpleContextDBPersistence();
		}

		return null;
	}

	public static SimpleAuthorizationDBInterface getAuthorizationPersistence() {
		SimpleAuthorizationDBInterface result = null;
		String runtime = System.getProperty("java.runtime.name", "WAT VM");
		if (runtime.toLowerCase(Locale.ENGLISH).contains("android")) {
			result = null;
			return result;
		} else {
			return new DefaultSimpleAuthorizationDBPersistence();
		}
	}

}
