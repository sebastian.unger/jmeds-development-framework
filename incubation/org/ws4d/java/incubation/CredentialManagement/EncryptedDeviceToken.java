package org.ws4d.java.incubation.CredentialManagement;

import java.io.Serializable;

// TODO: THIS IS NOT YET FIXED. JUST A ROUGH DRAFT!

/** 
 * contains the enctypted device token a client receives when using brokered
 * authentication.
 * 
 * @author cht
 *
 */
public class EncryptedDeviceToken implements Serializable {
	private static final long serialVersionUID = -579801739999591024L;
	private String issuerID;
	private String b64eStringncryptedToken;
	private String signature;
	private int sAlgType;
	private int eAlgType;

	public String getIssuerID() {
		return issuerID;
	}
	public void setIssuerID(String issuerID) {
		this.issuerID = issuerID;
	}
	public String getB64eStringncryptedToken() {
		return b64eStringncryptedToken;
	}
	public void setB64eStringncryptedToken(String b64eStringncryptedToken) {
		this.b64eStringncryptedToken = b64eStringncryptedToken;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public int getsAlgType() {
		return sAlgType;
	}
	public void setsAlgType(int sAlgType) {
		this.sAlgType = sAlgType;
	}
	public int geteAlgType() {
		return eAlgType;
	}
	public void seteAlgType(int eAlgType) {
		this.eAlgType = eAlgType;
	}
}
