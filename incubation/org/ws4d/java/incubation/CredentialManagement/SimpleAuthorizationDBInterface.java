package org.ws4d.java.incubation.CredentialManagement;

public interface SimpleAuthorizationDBInterface {
	boolean loadFromFile(SimpleAuthorizationDB db, String filename);
	boolean saveToFile(SimpleAuthorizationDB db, String filename);
	boolean clearDatabase(SimpleAuthorizationDB db, String filename);
}
