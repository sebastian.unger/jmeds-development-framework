package org.ws4d.java.incubation.CredentialManagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationRule;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

public class DefaultSimpleAuthorizationDBPersistence implements
SimpleAuthorizationDBInterface {

	@Override
	public boolean loadFromFile(SimpleAuthorizationDB db, String filename) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(filename));

			AuthorizationRule ar = (AuthorizationRule) ois.readObject();
			while(ar != null) {
				db.add(ar);
				ar = (AuthorizationRule) ois.readObject();
			}
		} catch (FileNotFoundException e) {
			Log.warn("No Authorization Database file found: " + filename);
			return false;
		} catch (IOException e) {
			/* this most likely means that there is no (more) data to read. */
			//TODO: use an EOF-Marker to prevent Exceptions
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			Log.info("Read " + db.size() + " authorization rules from file " + filename);
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public boolean saveToFile(SimpleAuthorizationDB db, String filename) {
		ObjectOutputStream oos = null;
		try {
			Iterator it = db.getAllRules().iterator();

			oos = new ObjectOutputStream(new FileOutputStream(filename));
			while (it.hasNext()) {
				AuthorizationRule ar = (AuthorizationRule)it.next();
				oos.writeObject(ar);
				Log.info(ar.toString());
			}
			Log.info("Wrote " + db.size() + " Authorization Rules to file " + filename);
		} catch (IOException e) {
			Log.warn(e.getMessage());
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public boolean clearDatabase(SimpleAuthorizationDB db, String filename) {
		db.clearContexts();
		File f = new File(filename);
		if (f.delete()) {
			Log.info("Authorization Rules file deleted");
			return true;
		} else {
			Log.warn("could not delete Authorization Rule file");
			return false;
		}
	}

}
