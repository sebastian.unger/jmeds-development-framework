package org.ws4d.java.incubation.Types;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.Fault;
import org.ws4d.java.types.QName;

public class AuthenticationFault extends Fault {
	public final static String FAULTNAME = "AuthenticationFault";
	public final static QName INTERNALERROR = new QName("InternalError", WSSecurityForDevicesConstants.NAMESPACE);
	public final static QName CLIENTNOTAUTHENTICATED = new QName("ClientNotAuthenticated", WSSecurityForDevicesConstants.NAMESPACE);
	public final static QName SIGNATUREMISSING = new QName("NoSignatureInMessage", WSSecurityForDevicesConstants.NAMESPACE);

	public AuthenticationFault() {
		super(FAULTNAME);
		this.setElement(new Element(new QName("message", WSSecurityForDevicesConstants.NAMESPACE), SchemaUtil.TYPE_STRING));
	}
}
