package org.ws4d.java.incubation.Types;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.Fault;
import org.ws4d.java.types.QName;

public class ConfidentialityFault  extends Fault  {

	public final static String FAULTNAME = "ConfidentialityFault";
	public final static QName MESSAGENOTENCRYPTED = new QName("MessageNotEncrypted", WSSecurityForDevicesConstants.NAMESPACE);

	public ConfidentialityFault() {
		super(FAULTNAME);
		this.setElement(new Element(new QName("message", WSSecurityForDevicesConstants.NAMESPACE), SchemaUtil.TYPE_STRING));
	}
}
