package org.ws4d.java.incubation.Types;

import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.Fault;
import org.ws4d.java.types.QName;

public class AuthorizationFault extends Fault {
	public final static String FAULTNAME = "AuthorizationFault";
	public final static QName INTERNALERROR = new QName("InternalError", WSSecurityForDevicesConstants.NAMESPACE);
	public final static QName CLIENTNOTAUTORIZED = new QName("ClientNotAuthorized", WSSecurityForDevicesConstants.NAMESPACE);

	public AuthorizationFault() {
		super(FAULTNAME);
		this.setElement(new Element(new QName("message", WSSecurityForDevicesConstants.NAMESPACE), SchemaUtil.TYPE_STRING));
	}
}
