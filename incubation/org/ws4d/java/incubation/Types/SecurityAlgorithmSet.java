package org.ws4d.java.incubation.Types;

import java.io.Serializable;

import org.ws4d.java.types.QName;

public class SecurityAlgorithmSet implements Serializable {
	/* simple container class for Security Algorithms */
	
	private static final long serialVersionUID = 3513615386024798247L;
	private String signatureAlgorithm = null;
	private String encryptionAlgorithm = null;
	private String derivationAlgorithm = null;
	
	public SecurityAlgorithmSet() {
		this(null, null, null);
	}
	
	public SecurityAlgorithmSet(QName sigAlg, QName encAlg) {
		this(sigAlg, encAlg, null);
	}
	
	public SecurityAlgorithmSet(QName sigAlg, QName encAlg, QName dervAlg) {
		signatureAlgorithm =  (sigAlg == null)  ? null : sigAlg.toString();
		encryptionAlgorithm = (encAlg == null)  ? null : encAlg.toString();
		derivationAlgorithm = (dervAlg == null) ? null : dervAlg.toString();
	}

	public SecurityAlgorithmSet(SecurityAlgorithmSet orig) {
		this.signatureAlgorithm = orig.signatureAlgorithm;
		this.encryptionAlgorithm = orig.encryptionAlgorithm;
		this.derivationAlgorithm = orig.derivationAlgorithm;
	}

	public QName getSignatureAlgorithm() {
		return QName.construct(signatureAlgorithm);
	}

	public void setSignatureAlgorithm(QName signatureAlgorithm) {
		this.signatureAlgorithm = signatureAlgorithm.toString();
	}

	public QName getEncryptionAlgorithm() {
		return QName.construct(encryptionAlgorithm);
	}

	public void setEncryptionAlgorithm(QName encryptionAlgorithm) {
		this.encryptionAlgorithm = encryptionAlgorithm.toString();
	}

	public QName getDerivationAlgorithm() {
		return QName.construct(derivationAlgorithm);
	}

	public void setDerivationAlgorithm(QName derivationAlgorithm) {
		this.derivationAlgorithm = derivationAlgorithm.toString();
	}

	public String toString() {
		return "Signature Algorithm: " + (this.signatureAlgorithm == null ? "null" : this.signatureAlgorithm.toString())
				+ "; Encryption Algorithm: " + (this.encryptionAlgorithm == null ? "null" : this.encryptionAlgorithm.toString())
				+ "; KDF: " + (this.derivationAlgorithm == null ? "null" : this.derivationAlgorithm.toString()) + ";";
	}

	public void copy(SecurityAlgorithmSet orig) {
		this.signatureAlgorithm = orig.signatureAlgorithm;
		this.encryptionAlgorithm = orig.encryptionAlgorithm;
		this.derivationAlgorithm = orig.derivationAlgorithm;
		
	}
}
