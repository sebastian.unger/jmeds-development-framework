package org.ws4d.java.incubation.Types;

public class SecurityTokenServiceType {
	public static final int NONE = 0x00;

	public static final int ENDPOINT = 0x01;
	public static final int UIAUTHENTICATOR = 0x02;
	public static final int AUTHENTICATOR = 0x04;
	public static final int AUTHENTICATORANDUIAUTHENTICATOR = AUTHENTICATOR | UIAUTHENTICATOR;
	public static final int AUTHENTICATORANDUIAUTHENTICATORANDENDPOINT = AUTHENTICATOR | UIAUTHENTICATOR | ENDPOINT;

	public static final int SELFHOSTEDAUTHORIZER = 0x10;
	public static final int SYNCHRONOUSAUTHORIZER = 0x20;
	public static final int ASYNCHRONOUSUIAUTHORIZER = 0x40;

}
