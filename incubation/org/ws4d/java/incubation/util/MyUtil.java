package org.ws4d.java.incubation.util;

public class MyUtil {

	/**
	 * Compares uri and regards possible missing urn:uuid:prefixes
	 * @param uri1
	 * @param uri2
	 * @return
	 */

	public static boolean uriCompare(String uri1, String uri2) {
		String s1 = null;
		if ("urn:uuid:".equals(uri1.substring(0, 9))) {
			s1 = uri1;
		} else if("uuid:".equals(uri1.substring(0, 5))) {
			StringBuilder sb = new StringBuilder();
			sb.append("urn:");
			sb.append(uri1);
			s1 = sb.toString();
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("urn:uuid:");
			sb.append(uri1);
			s1 = sb.toString();
		}
		String s2;
		if ("urn:uuid:".equals(uri2.substring(0, 9))) {
			s2 = uri2;
		} else if("uuid:".equals(uri2.substring(0, 5))) {
			StringBuilder sb = new StringBuilder();
			sb.append("urn:");
			sb.append(uri2);
			s2 = sb.toString();

		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("urn:uuid:");
			sb.append(uri2);
			s2 = sb.toString();
		}
		return s2.equals(s1);
	}

}
