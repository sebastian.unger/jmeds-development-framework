\section{Subscribe to events}
\label{sec:events}
The goals of this tutorial are to:

\begin{itemize}
\item learn how to specify an event source
\item learn how to subscribe to an event
\item learn how to fire an event 
\end{itemize}
  
\subsection*{Events}

The JMEDS framework supports two types of WS-Eventing compliant events:
notifications and solicit-response operations. While the first type represents
one-way messages sent from the event source to its subscribers, the later additionally includes response messages sent back from the subscribers to the source.

\subsubsection*{Specify a simple event source}

First, we will create an event source by using the \class{DefaultEventSource} class. It is like an Operation in JMEDS, except that we don't have to implement the invoke method.

The best way to create an event source and keep the code simple is creating a class that extends \class{DefaultEventSource}:

\begin{lstlisting}[language=Java]
public class DocuExampleSimpleEvent extends DefaultEventSource {

private final static String	namespace	= "http://www.mydemo.com/tutorial";

public DocuExampleSimpleEvent() {
		super("DocuExampleSimpleEvent", new QName("BasicServices", namespace));

		Element name = new Element(new QName("name", namespace), SchemaUtil.TYPE_STRING);

		setOutput(name);
	}
 
 
 public void fireHelloWorldEvent() {
	ParameterValue paramValue = createOutputValue();
		if (eventCounter % 2 == 0) {
			ParameterValueManagement.setString(paramValue, "name", "me (" + eventCounter + ")");
		} else
			ParameterValueManagement.setString(paramValue, "name", "me again (" + eventCounter + ")");
		fire(paramValue, eventCounter++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
 }
}
\end{lstlisting}\vspace*{1em}
You can specify the output of the event by calling \texttt{setOutput()} (just like an operation). The event will send this parameter to the subscribers when it is fired. To define what should happen when the event is fired, we add the method \texttt{fireHelloWorldEvent()}. For some variation we will send two different values.

The method \texttt{fire(...)} which is already implemented in
\class{DefaultEventSource} will send notifications to each subscriber of this
event source. The values of any parameters for the notification are taken from
the argument ``paramValue''. As we need to provide a \class{CredentialInfo} as a
parameter but don't want to use security features right now, we use \texttt{CredentialInfo.EMPTY\_CREDENTIAL\_INFO} instead. See tutorial \ref{sec:security} for more information about security.


\subsubsection*{Providing events}

As eventing was created to notify listeners of a service about value changes or
something else that is important enough to be spread across the world, we need a
class to simulate this behaviour. We will call it
\class{DocuExampleEventProvider}. It does nothing beside calling the
\texttt{fireHelloWorldEvent} method we created in our event class regularly every five seconds.

\begin{lstlisting}[language=Java]
public class DocuExampleEventProvider extends Thread {
  
  private static int			eventCounter	= 0;
  
  private DocuExampleSimpleEvent	event;
  
  public DocuExampleEventProvider(DocuExampleSimpleEvent event) {
    this.event = event;
  }
  
  @Override
  public void run() {
    while (true) {
      try {
        Thread.sleep(5000);
        event.fireHelloWorldEvent(eventCounter++);
        System.out.println("fire Event");
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      }
    }
 }

\end{lstlisting}

\subsubsection*{Specify a complex event source}

Now we will create a solicit-response event. We put the code in the DocuExampleComplexEvent class.

Let's have a look at the constructor first:

\begin{lstlisting}[language=Java]
public DocuExampleComplexEvent() {
 super("DocuExampleComplexEvent", new QName("BasicServices", namespace));
  
 Element count = new Element(new QName("counter", namespace), SchemaUtil.TYPE_INT);
 setOutput(count);
 
 Element add = new Element(new QName("addToCounter", namespace), SchemaUtil.TYPE_INT);
 setInput(add);
}
\end{lstlisting}\vspace*{1em}
As we already mentioned in a solicit-response event we have input and output. In this case we choose Integer as a type for our element. In addition we have to specify what we are going to do, when receiving a solicit response to our event. For this reason we have to overwrite the solicitResponseReceived method of \class{DefaultEventSource}:

\begin{lstlisting}[language=Java]
public void solicitResponseReceived(ParameterValue paramValue, int eventNumber, ServiceSubscription subscription) {
		// Get the solicit response
		String solicitResponse = ParameterValueManagement.getString(paramValue, "addToCounter");
		System.out.println("Solicit Response received: " + solicitResponse);

		if (solicitResponse != null) {
			exampleCounter = exampleCounter + Integer.parseInt(solicitResponse);
		} else {
			exampleCounter++;
		}
	}
\end{lstlisting}\vspace*{1em}
In our case we just update a counter which we will use as eventNumber in the next event, we will fire. Therefore we add another method, that defines our Output (like we did with the simple event before):

\begin{lstlisting}[language=Java]
	public void fireComplexEvent() {
		ParameterValue paramValue = createOutputValue();
		ParameterValueManagement.setString(paramValue, "counter", String.valueOf(exampleCounter));

		fire(paramValue, eventCounter++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
	}
\end{lstlisting}\vspace*{1em}
Nothing surprising. Can you figure out what is missing? Correct. Something that calls the fireComplexEvent method.

\subsubsection*{Again an event provider}

Again we are going to write our own, small event provider and simulate that there is actually something we need to send to the world. We just provide the full code here without further explanations because it should be quite intuitive.
\begin{lstlisting}[language=Java]
public class DocuExampleComplexEventProvider extends Thread {

	private DocuExampleComplexEvent	event;

	public DocuExampleComplexEventProvider(DocuExampleComplexEvent event) {
		this.event = event;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(6000);
				event.fireComplexEvent();
				System.out.println("fire ComplexEvent");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}

\end{lstlisting}\vspace*{1em} 
\subsubsection*{Adding event source to a service}

After creating the two different event sources, they must be registered with a service. You can just add them to one of the existing services we created before or, like we will do here for clarity reasons, create a new Service.

Our class \class{DocuExampleEventService} which extends \class{DefaultService} (of course):

\begin{lstlisting}[language=Java]
/**
* Standard Constructor
*/
public DocuExampleEventService() {
   super();
   
   this.addBinding(new HTTPBinding(IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1", false), 5678, "docuEventService", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));
   
   // add Events to the service
   DocuExampleSimpleEvent notification = new DocuExampleSimpleEvent();
   addEventSource(notification);
   
   DocuExampleComplexEvent solicit = new DocuExampleComplexEvent();
   addEventSource(solicit);
   
}
\end{lstlisting}\vspace*{1em}
As we do not add an operation but an event source, we use
\texttt{addEventSource(...)} to add them. Everything else is not very surprising.

\subsubsection*{Starting our eventing service}

What we have is a service that hosts two event sources. What we need to do is to add this service to a device, start our event providers and start the device. We will do this in a class called DocuExampleEventServiceProvider [And yes, this is the point we should start thinking about the names of classes...].

The class just provides a main method, which does the following:
\begin{itemize}
\item start the framework
\item create a DefaultDevice
\item create an instance of our service
\item get the two event sources we created from the service
\item use the sources to start our event providers
\item add the service to the device
\item start the device 
\end{itemize}
         

The corresponding Java code looks like that:

\begin{lstlisting}[language=Java]
 public class DocuExampleEventServiceProvider {
 
 private final static String	namespace	= "http://www.mydemo.com/tutorial";
 
 /**
 * @param args
 */
 public static void main(String[] args) {
 
   // mandatory: Starting the DPWS Framework
   JMEDSFramework.start(args);
   
   // first we need a device
   DefaultDevice device = new DefaultDevice();
   
   // then we create a service
   final DocuExampleEventService service = new DocuExampleEventService();
   
   // we get the event source from the service to start the event thread
   EventSource event = service.getEventSource(new QName("BasicServices", namespace), "DocuExampleSimpleEvent", null, "DocuExampleSimpleEvent");
   DocuExampleEventProvider eventProvider = new DocuExampleEventProvider((DocuExampleSimpleEvent) event);
   eventProvider.start();
   
   // we get the complex event source from the service to start the event
   // thread
   EventSource solicitEvent = service.getEventSource(new QName("BasicServices", namespace), "DocuExampleComplexEvent", "DocuExampleComplexEventResponse", "DocuExampleComplexEventSolicit");
   DocuExampleComplexEventProvider solicitEventProvider = new DocuExampleComplexEventProvider((DocuExampleComplexEvent) solicitEvent);
   solicitEventProvider.start();
   
   // in the end we add our service to the device
   device.addService(service);
   
   // do not forget to start the device!
   try {
      device.start();
   } catch (IOException e) {
      e.printStackTrace();
   }
  }
 }
 \end{lstlisting}
 
\subsubsection*{Subscribe to the event source}

Now we have a running device, hosting a service with two event sources and
two event providers running and always ready to fire. But sadly nobody is subscribed to our event and is interested yet in what we have to say. So we need to write a client to find our service (see Tutorial \ref{sec:hello}) and to subscribe to the events provided.

As there are some interesting and important lines of code we will provide the whole code of \class{DocuExampleEventClient} here and explain it afterwards.

\begin{lstlisting}[language=Java]
/**
 * Sample client for receiving events.
 */
public class DocuExampleEventClient extends DefaultClient {

	final static String			namespace					= "http://www.mydemo.com/tutorial";

	final static QName			service						= new QName("BasicServices", namespace);

	private ClientSubscription	solicitSub					= null;

	private ClientSubscription	notificationSub				= null;

	private EventSource			eventSourceSolicitResponse	= null;

	public static void main(String[] args) {

		// mandatory starting of DPWS framework
		JMEDSFramework.start(args);

		DPWSProperties.getInstance().removeSupportedDPWSVersion(new DPWSProtocolVersion(DPWSConstants2006.DPWS_VERSION2006));

		// create client
		DocuExampleEventClient client = new DocuExampleEventClient();

		// search for event and subscribe
		SearchParameter search = new SearchParameter();
		search.setServiceTypes(new QNameSet(service));
		SearchManager.searchService(search, client);
	}

	public void serviceFound(ServiceReference servRef, SearchParameter search) {

		try {
			// use this code to subscribe to the simple event
			{
				// get event source
				EventSource eventSource = servRef.getService().getEventSource(service, "DocuExampleSimpleEvent", null, null);

				if (eventSource != null) {
					// add binding
					DataStructure bindings = new org.ws4d.java.structures.ArrayList();
					HTTPBinding binding = new HTTPBinding(IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1", false), 10235, "/EventSink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
					bindings.add(binding);

					// subscribe
					notificationSub = eventSource.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
				}
			}
			// use this code to subscribe to the solicit-response event
			{
				// get event source
				eventSourceSolicitResponse = servRef.getService().getEventSource(service, "DocuExampleComplexEvent", null, null);

				if (eventSourceSolicitResponse != null) {
					// add binding
					DataStructure bindings = new org.ws4d.java.structures.ArrayList();
					HTTPBinding binding = new HTTPBinding(IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1", false), 7896, "/EventSink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
					bindings.add(binding);

					// subscribe
					solicitSub = eventSourceSolicitResponse.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
				}
			}
		} catch (CommunicationException e) {
			e.printStackTrace();
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ParameterValue eventReceived(ClientSubscription subscription, URI actionURI, ParameterValue parameterValue) {

		if (subscription.equals(notificationSub)) {
			String eventText = ParameterValueManagement.getString(parameterValue, "name");
			System.out.println("Notification event received from " + eventText);
			return null;
		} else if (subscription.equals(solicitSub)) {
			// solicit-response
			String eventText = ParameterValueManagement.getString(parameterValue, "counter");
			System.out.println("Solicit-response event received, counter is " + eventText);

			// create response
			ParameterValue pvResponse = eventSourceSolicitResponse.createInputValue();
			ParameterValueManagement.setString(pvResponse, null, "5");
			return pvResponse;
		}
		return null;
	}

	public void subscriptionTimeoutReceived(ClientSubscription subscription) {
		subscriptionEndReceived(subscription, SubscriptionEndMessage.WSE_STATUS_UNKNOWN);
	}

	public void subscriptionEndReceived(ClientSubscription subscription, int reason) {
		System.err.println("Subscription ended.");
	}
}
\end{lstlisting}\vspace*{1em}
Let's have a look at the \texttt{serviceFound(...)} method. We first check if the service has the event source we're searching for. If there is indeed an event source that matches \class{DocuExampleSimpleEvent} or, in the second case, \class{DocuExampleComplexEvent}, we can subscribe to it. We write the return value of type ClientSubscription to a variable, so that we can distinguish between the two events later on.

Let's discuss the arguments of the subscribe method:
\begin{itemize}
\item The first argument is the \class{EventListener}, here we take the \class{DefaultClient}.
\item The second argument is the number of milliseconds until subscription expires. 0 means that the subscription will never expire.
\item The third argument is a list of CommunicationBindings, those are used to
bind a local event sink to it and allow it to listen for event notifications
from a remote service.
\end{itemize}
   
The second method that we are interested in is the following one, which is inherited from DefaultClient:
\begin{lstlisting}[language=Java]
public ParameterValue eventReceived(ClientSubscription subscription, URI actionURI, ParameterValue parameterValue)
\end{lstlisting}\vspace*{1em}
In this method we have to specify what we want to do, when our client receives an event. First we use the given ClientSubscription to decide which event was fired. In the first case we are just waiting for notifications. Therefore we read the value from given ParameterValue and just print it. We return null as the eventing service does not expect a response at all. 

In the second case we print the given value as well but afterwards we define a response. The response must be an Integer value as specified. As we think it is a good idea to increase the event counter by 5 each time, we create a new input value, set the value of the "addToCounter" element to 5 and return it. In this case the method 
\texttt{solicitResponseReceived(...)} which we implemented in our \class{DocuExampleComplex\-Event} will be called automatically.

\subsubsection*{What to remember}

This is what you should not forget after trying this tutorial:
\begin{itemize}
\item You subscribe to an event source by calling \texttt{subscribe()} on it.
\item An \class{EventListener} is bound to an event sink and listens to
notifications.
\item When an event source is fired, all subscribers are notified through their
\class{EventListener}.
\end{itemize}