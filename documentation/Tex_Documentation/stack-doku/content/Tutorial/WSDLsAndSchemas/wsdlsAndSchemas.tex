\section{WSDLs and Schema}
\label{sec:wsdl}

The goals of the tutorial are to:

\begin{itemize}
\item learn how to load and store WSDL and Schema files into the WSDL
repository
\item learn how to load WSDL and Schema files from any jar file 
\end{itemize}

\subsection*{WSDL Repository}

To use the WSDL repository first you need an instance of the repository.
Therefore we have to call the \texttt{getInstance()} method on the
\class{WSDLRepository}.
\begin{lstlisting}[language=Java]
WSDLRepository wsdlRepo = WSDLRepository.getInstance();
\end{lstlisting}\vspace*{1em}
With an instance of the \class{WSDLRepository} we can use a lot of methods to
load, save and get WSDL or Schema files. 

But first there are three additional methods available to manage the repository.
With the \texttt{getRepoPath()} method the relative path (from project dir) will
returned.

\begin{lstlisting}[language=Java]
	// Returns the repo path, where the loaded and stored wsdls are saved.
	// By default path is "pathToProject\wsdl_repo".
	System.out.println(wsdlRepo.getRepoPath());
\end{lstlisting}\vspace*{1em}
With the \texttt{getRepoPathPrefix()} method the prefix of the repo path (can be
set to change the default relative path) will be returned.

\begin{lstlisting}[language=Java] 
	// Returns the prefix of the repo path. By default prefix is empty ().
	System.out.println(wsdlRepo.getRepoPathPrefix());
\end{lstlisting}\vspace*{1em}
Finally the \texttt{setRepoPathPrefix()} method allows to change the default
path of the repository. So you can change the path of the wsdl repository for example
from \begin{verbatim} projectDir\wsdl_repo \end{verbatim} to
\begin{verbatim} D:\tmp\...\wsdl_repo\end{verbatim}

\begin{lstlisting}[language=Java]
	// Sets the prefix of the repo path. That allows to change
	// the path of directory if an absolute path is in the prefix.
	wsdlRepo.setRepoPathPrefix(newRepoPathPrefix);
\end{lstlisting}\vspace*{1em}

\subsection*{Load and Store}
In the following we will list up the methods to load and store files.
There are methods available to 
\begin{itemize}
  \item load a file,
  \item to store a file,
  \item to get a file,
  \item or a combination of the methods above.
\end{itemize}

The first method we will have a look at is called \texttt{loadAndStore(wsdlUri,
credential\-Info)} and loads a wsdl from the given uri (can be an filesystem or
remote), stores the wsdl into the wsdl repository and returns the wsdl. The
second method just loads the wsdl from the given URI and returns it. (The
\class{CredentialInfo} is just needed if the WSDL file defines a remote
device and this one needs the credential for accessing the file.
For more Informations see \ref{sec:security}.) 

\begin{lstlisting}[language=Java] 
	try {
		// Load a wsdl from any uri, stores the wsdl in the repository on 
		// the file system and returns the wsdl.
		wsdl = wsdlRepo.loadAndStore(wsdlUri, CredentialInfo.EMPTY_CREDENTIAL_INFO);

		// Load the wsdl and just returns the wsdl.
		wsdl1 = WSDLRepository.loadWsdl(wsdlUri1, CredentialInfo.EMPTY_CREDENTIAL_INFO);
	} catch (IOException e) { }
\end{lstlisting}\vspace*{1em}
The \texttt{store(wsdl, newWSDLFileName)} method stores a given WSDL (must be
object of class \class(WSDL)) with the ``newWSDLFileName'' into the WSDL
repository.

\begin{lstlisting}[language=Java] 
	// Stores a given wsdl in the repository on the filesystem with given
	// filename.
	wsdlRepo.store(wsdl, newWSDLFileName);
\end{lstlisting}\vspace*{1em}
With \texttt{store(wsdlUri, credentialInfo, newWSDLFileName)} a WSDL file will
be loaded from the given \emph{wsdlUri} and stored into the WSDL repository. 

\begin{lstlisting}[language=Java]
	try {
		// load the wsdl form wsdl uri and stores the wsdl with given new
		// filename
		wsdlRepo.store(wsdlUri1, CredentialInfo.EMPTY_CREDENTIAL_INFO, newWSDLFileName1);
	} catch (IOException e) {}
\end{lstlisting}\vspace*{1em}
The \texttt{getWSDL(wsdlFileName)} returns a WSDL file which already must have
been saved in the WSDL repository (the method searches in the index of the WSDL
repository for the matching file and will return \emph{null}, if no match is
found).
\begin{lstlisting}[language=Java]
	// Returns the wsdl with given filename if exists in the repository.
	wsdl1 = wsdlRepo.getWSDL(newWSDLFileName1);
\end{lstlisting}\vspace*{1em}
Some of the WSDL methods are also available for Schema files. This applies for
the \texttt{ storeSchema(schemaUri, credentialInfo, newSchema FileName)} method
which loads the schema from the given ``schemaUri'' and stores it with the
new filename into the WSDL repository. 

\begin{lstlisting}[language=Java]
	try {
		// load the schema from schema uri and stores the schema with given
		// new filename
		wsdlRepo.storeSchema(schemaUri, CredentialInfo.EMPTY_CREDENTIAL_INFO, newSchemaFileName);
	} catch (IOException e) {}
\end{lstlisting}\vspace*{1em}
The \texttt{getSchema(schemaLocation, credentialInfo, schemaNamespace)}
method returns the schema for a given ``schemaLocation'' and ``schemaNamespace'' from
the WSDL repository (schema files will be saved with a combination of schema
name/location and targetNamespace of the schema file). The schema file must
have been stored in the wsdl repo before, to get a return value. 
\begin{lstlisting}[language=Java]
	// Returns the schema for given schema location and schema namespace
	schema = wsdlRepo.getSchema(schemaLocation, CredentialInfo.EMPTY_CREDENTIAL_INFO, schemaNamespace);
\end{lstlisting}

\subsection*{Load WSDL and Schema files from any jar file}
If you want to refer some WSDL or schema files out of a jar file you need
the path of the file. To get the path there are three possibilities.

The first is to load the path of the resource from the system. Therefore you
need the \texttt{object.getClass().getResource(path)} method. This will return an URL
to the resource. The method \texttt{Schema.parse(schemaUri, credentialInfo,
loadReferenced\-Files)} needs an \class{URI}, therefore a new \class{URI} must
be created with the path to resource file.

\begin{lstlisting}[language=Java]
	// Load file with URL from System
	URL schemaFile = lffj.getClass().getResource("path/file.xsd");
	Schema schema1 = Schema.parse(new URI(schemaFile.toString()), CredentialInfo.EMPTY_CREDENTIAL_INFO, true);
\end{lstlisting}\vspace*{1em}

If the path to the resource is known, it can be entered by hand. Important is
that the prefix \emph{jar:} is added to the path and at the end of the path to
jar file an exclamation mark is needed to mark the end of filepath and the
beginning of classpath. After the exclamation mark the classpath of the file
must be defined.
 
 \begin{lstlisting}[language=Java]
	// Load file with own path
	URI wsdlFile = new URI("jar:/C:/pathToJar/.../file.jar!/pathInJar/.../file.wsdl");
	WSDL wsdl = WSDL.parse(wsdlFile, CredentialInfo.EMPTY_CREDENTIAL_INFO, true); wsdl.serialize(System.out);
\end{lstlisting}\vspace*{1em}


A third possibility is to use the relative path, but therefore the jar must be
the jar which is running and it is necassary to
use the prefix \emph{local:}.

\begin{lstlisting}[language=Java]
	// Load file just with path in jar
	URI wsdlFile1 = new URI("local:/pathInJar/.../file.wsdl");
	WSDL wsdl1 = WSDL.parse(wsdlFile1, CredentialInfo.EMPTY_CREDENTIAL_INFO, true);
\end{lstlisting}\vspace*{1em}

\subsection*{What to remember}

 After working on this tutorial you should now:

\begin{itemize}
\item how to access the WSDL repository.
\item how to load and store WSDL files.
\item how to load WSDL and Schema files from jar files.
\end{itemize}
