\section{Security}
\label{sec:security}
The goals of this tutorial are to:
\begin{itemize}
\item Learn how to create clients, devices and services that use secure
connections and XML message signing
\end{itemize}

\subsection*{Security mechanisms}

There are three different security mechanisms. The first two are treated in
this section.
\begin{itemize}
\item HTTPS Connections \\ For all HTTP connections used in the framework it is
also possible to communicate over an encrypted SSL connection.
\item XML Message Signing \\ Since the above is not possible for multicast
messages, we provide
\href{http://www.w3.org/TR/2008/REC-xmldsig-core-20080610/}{XML Message Signing}
using the \href{http://docs.oasis-open.org/ws-dd/discovery/1.1/os/wsdd-discovery-1.1-spec-os.html#_Toc234231850}{Compact Signature Format}. While this mechanism does not provide protection against
reading the messages, it allows validation of their authenticity. 
\item Authorization \\ Authorization mechanism using username and password.
\end{itemize}
    
\subsection*{CredentialInfo}

The class \class{CredentialInfo} is used to store information for security
purposes. It can contain the key- and truststore as well as discovery
information and the private key used for message signing. 

\begin{figure}[htb]
\includegraphics[width=1\linewidth]{content/Tutorial/Security/images/CredentialInfo.png}
\caption{CredentialInfo classes}
\label{fig:credentialInfo_UML}
\end{figure}

\subsubsection*{Creating key- and truststores using Java's keytool}
\begin{itemize}
\item Open a terminal in the bin directory of your Java installation and type
"keytool -genkey -alias [alias of the generated key] -sigalg
SHA1withRSA -keyalg RSA". You may add the -keystore [filepath] argument to use a
(new) keystore. It is created automatically if not existent.
\item Answer the questions on the screen (or don't) and confirm. The keystore is
already ready for use now.
\item Export the generated public key to a [file].cer file: "keytool -export
-alias [alias of the generated key] -file [file].cer".
\item Import the certificate into your truststore: "keytool -import -alias
[alias of the generated key] -file [file].cer -keystore [truststore
filepath]". Will also be created automatically if not existent.
\end{itemize}

\subsubsection*{Creating the CredentialInfo}

Load key- and truststore for the signing of multicast messages.
\begin{lstlisting}[language=java]
	katmf = KeyAndTrustManagerFactory.getInstance();
	KeyStore ks = katmf.loadKeyStore(keyStorePath, keyStorePw);
	KeyStore ts = katmf.loadKeyStore(trustStorePath, trustStorePwd);
\end{lstlisting}

Load key- and trustmanagers. They are used for SSL connections.
\begin{lstlisting}[language=java]
	kms = katmf.getKeyManagers(keyStorePath, keyStorePw);
	tms = katmf.getTrustManagers(trustStorePath, trustStorePw);
\end{lstlisting}

Specify the private key used for message signing.
\begin{lstlisting}[language=Java]
	pk = (PrivateKey)keyStore.getKey(alias,keyStorePw);
\end{lstlisting}

Generate a \class{LocalCertificateCredentialInfo}, which is specific
for security.
\begin{lstlisting}[language=Java]
	lcci = new LocalCertificateCredentialInfo(alias,kms,tms,pk,ks,ts)
\end{lstlisting}

Generate a \class{CredentialInfo} containing the
\class{LocalCertificateCredentialInfo}.
\begin{lstlisting}[language=Java]
	CredentialInfo credentialInfo = new CredentialInfo(lcci);
\end{lstlisting}

To enable and disable the signing of outgoing and validating of incoming
messages, use the following methods. Default value for both is \texttt{true}. 
\begin{lstlisting}[language=Java] 
	credentialInfo.setSecureMessagesIn(boolean secureMessagesIn);
	credentialInfo.setSecureMessagesOut(boolean secureMessagesOut);
\end{lstlisting}
\subsection*{Secure devices and services}

After creating a \class{CredentialInfo}, using secure TCP connections for
devices and services becomes very simple by adding a HTTPSBinding instead of HTTPBinding. 
\begin{lstlisting}[language=Java] 
	binding = new HTTPSBinding(ip,port,path,comManId,credentialInfo);
	device.addBinding(binding);
\end{lstlisting}
This binding will only accept SSL connections via the Key- and TrustManagers
within the \class{CredentialInfo}. HTTPS bindings for services are created in
the exact same way.

To sign outgoing and validate incoming multicast messages you have to add\\
\class{DiscoveryBinding}s and \class{OutgoingDiscoveryInfo}s.

At first, get the network interface you want to discover on.
\begin{lstlisting}[language=Java] 
	NetworkInterface networkInterface =	IPNetworkDetection.getInstance().getNetworkInterface("eth3");
\end{lstlisting}

If you don't know which interfaces are available, use the method\\
\texttt{getNetworkInterfaces()} to get them all. After you picked one, create
a\\ \class{DiscoveryDomain} and a \class{DiscoveryBinding}. The passed
credentialInfo is used to validate incoming message's signatures.
\begin{lstlisting}[language=Java]
	IPDiscoveryDomain domain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(networkInterface, false);		
	//true if using IPv6
	IPDiscoveryBinding discoveryBinding = new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain);
	discoveryBinding.setCredentialInfo(credentialInfo);
	device.addBinding(discoveryBinding);
\end{lstlisting}
Create the \class{DiscoveryInfo}s and add them to the device/service to sign
outgoing messages.
\begin{lstlisting}[language=Java] 
	CommunicationManager comMan = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
	OutgoingDiscoveryInfo discoveryInfo = comMan.getOutgoingDiscoveryInfo(discoveryBinding, true, credentialInfo);
	device.addOutgoingDiscoveryInfo(discoveryInfo);
\end{lstlisting}

\subsection*{Secure clients}

\subsubsection*{SecurityKey}

\class{SecurityKey} is a construct containing a \class{CredentialInfo} used for
TCP connections and a \class{Set} of \class{DiscoveryInfo}s for UDP messages. To
create them follow these steps:

Create the \class{DiscoveryInfo}s and add them to a \class{Set}. Each \class{DiscoveryInfo} can have it's own
\class{CredentialInfo} that is used to sign multicast messages on that network. 
\begin{lstlisting}[language=Java] 
CommunicationManager comMan = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
	OutgoingDiscoveryInfo discoveryInfo = comMan.getOutgoingDiscoveryInfo(discoveryBinding, true, credentialInfo);
	HashSet discoveryInfos = new HashSet();
	discoveryInfos.add(discoveryInfo);
\end{lstlisting}

Now create the \class{SecurityKey}. The passed \class{CredentialInfo} is used
to create secure sockets.
\begin{lstlisting}[language=Java]
	securityKey = new SecurityKey(discoveryInfos, credentialInfo);
\end{lstlisting}

The following example shows how to use the \class{SecurityKey} to get a\\
\class{Device}- or \class{ServiceReference}. By passing
the \class{SecurityKey} argument to the methods the underlying system
automatically uses a secure connection. If no securityKey is passed, the
returned \class{DeviceReference} would try to use a normal connection when using
\texttt{devRef.getDevice()}, same goes for the \class{ServiceReference}.
\begin{lstlisting}[language=Java]
public void helloReceived(HelloData helloData) {
	DeviceReference devRef=SearchManager.getDeviceReference(helloData,securityKey,this);
	try { 
		Device device = devRef.getDevice();
		Log.debug("Device found:" + device.getPresentationUrl());
		Iterator servRefs =	device.getServiceReferences(securityKey);
		while (servRefs.hasNext()) {
			ServiceReference servRef = (ServiceReference)servRefs.next();
			Service service = servRef.getService();
			Log.debug("Service found: " + service.getServiceId());
		}
	} catch (CommunicationException e) {
		e.printStackTrace();
	}
}
\end{lstlisting}

\subsubsection*{Using a search parameter to receive secure Device- and
ServiceReferences}

By passing a \class{SearchParameter} to the
\texttt{client.searchDevice(SearchParameter \-search\-Parameter)} and 
\texttt{client.searchService(SearchParameter searchParameter)} methods, the
references in the triggered methods \texttt{deviceFound()} and
\texttt{serviceFound()} already have the correct \class{SecurityKey} attached
and can be used without additional work. To do that follow these steps:

Create a \class{SearchMapEntry} containing a \class{Set} of
\class{DiscoveryInfo}s as well as the \class{Protocol\-Info} and add it to a
\class{SearchMap}.
\begin{lstlisting}[language=Java]
	DPWSProtocolInfo protocolInfo = new DPWSProtocolInfo();
	ArrayList searchMap = new ArrayList();
	searchMap.add(new SearchParameter.SearchMapEntry(discoveryInfos, protocolInfo));
\end{lstlisting}

Create a \class{SearchParameter} and set the {SearchMap}.
\begin{lstlisting}[language=Java]
	SearchParameter searchParameter = new SearchParameter();
	searchParameter.setSearchMap(searchMap);
\end{lstlisting}

Now you can search for devices and services and the references passed in
\texttt{deviceFound()} and \texttt{serviceFound()} will already include a
\class{SecurityKey}.
\begin{lstlisting}[language=Java]
 	client.searchDevice(search);
 	client.searchService(search);
\end{lstlisting}

\subsection*{What to remember}

This is what you should have learned after completing this tutorial:

\begin{itemize}
\item There are three different security mechanisms.
\item \class{CredentialInfo}s are used to store information for security
purposes.
\item You can create key- and truststores using Java's keytool.
\item Use \class{HTTPSBinding} instead of \class{HTTPBinding} to use secure
connections.
\item You can secure devices, services and clients.
\end{itemize}
