\section{Receive device status changes}
\label{sec:lifecycle}
The goals of this tutorial are to:
\begin{itemize}
\item learn about the lifecycle of a device
\item learn how to subscribe to a device for status changes 
\end{itemize}
   
\subsection*{Device states}

\begin{figure}[htb]
\includegraphics[scale=0.5]{content/Tutorial/DeviceStatusChanges/images/DeviceReferenceStateMachine.png} 
\caption{Device Reference State Machine}
\label{fig:devstate}
\end{figure}


As you can see in figure \ref{fig:devstate} a device can have one of the
following four states:
\begin{itemize}
\item Stopped \\ Device has stopped is the latest known information.
\item Running \\ Device is running is the latest known information.
\item Build Up \\ Device is built up is the latest known information. That means
that the device within the givin \class{DeviceReference} has been created.
IMPORTANT: Built up implies running! 
\item Unknown \\ No information about device state.
\end{itemize}

If you have a local device no difference can be made between running and built
up. A local device has always been built up.


\subsection*{Receives status changes}

If you want to receive status changes of a device you have to write a class
that inherits from \class{DefaultClient} and add this class as a callback to the
device.

Adding the client as a callback will be done implicitly when:
\begin{enumerate}
  \item you search for a device and this device is found.
  \item you call the \texttt{getDeviceReference(EndpointReference epr)} method.
  Mostly this will be done after receiving a hello message.
\end{enumerate}

We will do the second. Therefore we create our client and register hello
listening in the \texttt{main(\ldots)}
method of \class{DocuExampleDevice\-LifecycleClient} class.
\begin{lstlisting}[language=Java]
	public static void main(String[] args) {
		JMEDSFramework.start(args);

		// create client who listens to device status changes
		DocuExampleDeviceLifecycleClient lifecycleClient = new DocuExampleDeviceLifecycleClient();
		//register hello listening
		lifecycleClient.registerHelloListening();
		}
\end{lstlisting}\vspace*{1em}

Afterwards we have to overwrite \texttt{helloReceived(\ldots)} like we did in
tutorial \ref{sec:hello}. 
What we have to add now is important. Each device will send a hello message
after changing as well as we will be informed using the callback methods we
registered for. To avoid responding to the same event twice, we will just
remember the devices we already know in a simple \class{HashSet}. 

\begin{lstlisting}[language = java]
 private static HashSet<EndpointReference>	devicesWeKnow	= new
 HashSet<EndpointReference>();
\end{lstlisting}\vspace*{1em}

When receiving a ``hello'' we can now decide if it is a new device that
wants to say ``hello'' to us. In this case we add it to our set and register as callback.
 The method \texttt{getDeviceReference()} returns the \class{DeviceReference}
 for the given EndpointReference and adds the client as a callback for device
 changes as explained above. 
 
 
 \begin{lstlisting}[language=Java]
 public void helloReceived(HelloData helloData) {

	// maybe we are already listening to this device. Then we can 
	// ignore this hello message...
	if (!devicesWeKnow.contains(helloData.getEndpointReference())) {
	
		//we will get the device reference and by doing this we will 
		// tell the device that we are interested and want to listen 
		// to status changes
		DeviceReference deviceReference = getDeviceReference(helloData,
		SecurityKey.EMPTY_KEY);

		// add it to our set so we can look up if we already know it
		devicesWeKnow.add(deviceReference.getEndpointReference());
		System.out.println("Listening to changes of " + deviceReference + " now.");
		}
	}
\end{lstlisting}\vspace*{1em}
 

Afterwards you can overwrite the following methods and decide what you want to
do when the corresponding event occurs. 
\begin{itemize}
\item deviceRunning \\ Callback method, if device is usable.
\item deviceChanged \\ Callback method, if device was changed and the device
data is no longer accurate.
\item deviceBuiltUp \\ Callback method, if device within the
\class{DeviceReference} was created. (For example after you have called
\texttt{getDevice()} on the \class{DeviceReference})
\item deviceBye \\ Callback method, if device bye was received or the local device was stopped.
\item deviceCommunicationErrorOrReset \\ Callback method, if communication with
the remote device of the \class{DeviceReference} has failed or the reference was
reset.
\end{itemize}

\subsubsection*{Example}

To test your code you need a device that goes online and offline and that
changes status information. You can find an example device in class
\class{DocuExampleLifecycleDeviceStarter}.
        

\subsection*{What to remember}

This is what you should not forget after completing this tutorial:

\begin{itemize}
\item Use \texttt{getDeviceReference} and the client will be used as callback for device changes.
\item Override the callback functions to receive device status changes. 
\end{itemize}
    
   