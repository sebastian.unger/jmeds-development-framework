\section{Pass and receive attachments to an operation}
\label{sec:attachments}
The goals of the tutorial are to:

\begin{itemize}
\item learn how to send and receive attachments 
\end{itemize}
    
\subsection*{Receive attachments in an operation}

In this tutorial we will implement two operations: One that sends an attachment and one that receives an attachment. First we will start with an operation that receives an attachment. You can find the code in class \class{DocuExampleSetAttachmentOperation}. If you want to pass an attachment to your operation you have to create an element of the type base64. The rest is not very surprising as you are already an expert in writing an operation at this point of the tutorial:

\begin{lstlisting}[language=Java]
Element attachment = new Element(new QName(ATTACHMENT, Constants.namespace), SchemaUtil.TYPE_BASE64_BINARY);

setInput(attachment);
\end{lstlisting}\vspace*{1em}
Now we will have a look at the invoke method of our operation. We can get the incoming attachment from the given \class{Parameter\-Value} using \class{ParameterValueManagement.\-getAttachment(...)} It returns an instance of \class{IncomingAttachment}. There are different types of attachments:

\begin{itemize}
\item FileAttachment
\item MemoryAttachment
\item StreamAttachment
\end{itemize}
     
\begin{lstlisting}[language=Java]
	final IncomingAttachment attachment = ParameterValueManagement.getAttachment(parameterValue, ATTACHMENT);
\end{lstlisting}\vspace*{1em}
\class{IncomingAttachment} is an interface and defines methods for this three types. After you got the \class{IncomingAttachment} you can get the type and the \class{InputStream} from the attachment.

\begin{lstlisting}[language=Java]
 InputStream inputStream = attachment.getInputStream();
 int attachmentType = attachment.getType();
\end{lstlisting}\vspace*{1em}
As we do not know what kind of attachment the sender sent us, we switch cases and do whatever we want with the attachment, e.g. write it to a file if it is a \class{FileAttachment}:

\begin{lstlisting}[language=Java]
 switch (attachmentType) {
   case Attachment.FILE_ATTACHMENT:
     System.out.println("This is a file attachment");
     
     // write the inputStream to a FileOutputStream
     OutputStream out = new FileOutputStream(new File("c:\\newfile.png"));
     
     byte[] array = new byte[1024];
     
     int length = inputStream.read(array);
     
     while (length > 0) {
       out.write(array, 0, length);
       length = inputStream.read(array);
     }
     
     inputStream.close();
     out.flush();
     out.close();
     
     System.out.println("New file created!");
     
     break;
   case Attachment.MEMORY_ATTACHMENT:
     System.out.println("This is a memory attachment");
     break;
   case Attachment.STREAM_ATTACHMENT:
     System.out.println("This is a stream attachment");
     break;
   default:
     System.out.println("Error");
}
\end{lstlisting}\vspace*{1em}
In the end you should dispose the attachment to free the memory. But be careful: It is not possible to access meta data and raw data after calling this method!

\begin{lstlisting}[language=Java]
 attachment.dispose();
\end{lstlisting}


\subsection*{Send attachments}

Beside receiving attachments it is, of course, possible to send some. In this section we will have a closer look at an operation (\class{DocuExampleGetAttachmentOperation} ) that defines an output element that has type 'base64-binary'.

We create an complex type with three binary elements and set them as output:

\begin{lstlisting}[language=Java]
Element attachment = new Element(new QName(ATTACHMENT, Constants.NAMESPACE));

ComplexType complexType = new ComplexType(new QName("AttachmentType", Constants.NAMESPACE), ComplexType.CONTAINER_SEQUENCE);

complexType.addElement(new Element(new QName("File", Constants.NAMESPACE), SchemaUtil.TYPE_BASE64_BINARY));
complexType.addElement(new Element(new QName("Memory", Constants.NAMESPACE), SchemaUtil.TYPE_BASE64_BINARY));
complexType.addElement(new Element(new QName("Stream", Constants.NAMESPACE), SchemaUtil.TYPE_BASE64_BINARY));

attachment.setType(complexType);

setOutput(attachment);
\end{lstlisting}\vspace*{1em}
And, again, we take a closer look at the invoke method. What do we have to do?

\begin{itemize}
\item Create an attachment
\item Set this attachment as our output 
\end{itemize}
       
The first is done by using the \class{AttachmentFactory} which provides methods to create the different types of attachments. It expects a file path and a content type as parameters, in our case we send a picture. The second point should not be a problem for you as it is quite intuitive if you already have experiences in writing operations with JMEDS.

\begin{lstlisting}[language=Java]
public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		ParameterValue outputValue = createOutputValue();

		AttachmentFactory attachmentFactory = AttachmentFactory.getInstance();

		if (attachmentFactory != null) {
			// FileAttachment
			ParameterValueManagement.setAttachment(outputValue, "File", attachmentFactory.createFileAttachment(DocuExampleAttachmentService.filePath, MIMEConstants.CONTENT_TYPE_IMAGE_GIF));

			// MemoryAttachment
			ParameterValueManagement.setAttachment(outputValue, "Memory", attachmentFactory.createMemoryAttachment(Util.toByteArray(DocuExampleAttachmentService.filePath), MIMEConstants.CONTENT_TYPE_IMAGE_GIF));

			// StreamAttachment
			ParameterValueManagement.setAttachment(outputValue, "Stream", attachmentFactory.createStreamAttachment(Util.getInputStream(DocuExampleAttachmentService.filePath), MIMEConstants.CONTENT_TYPE_IMAGE_GIF));
		} else {
			System.err.println("Could not get AttachmentFactory instance!");
		}

		return outputValue;
	}
}
\end{lstlisting}\vspace*{1em}
As the corresponding service and service provider are analogous to all the
services you have seen before, we will not post the whole code here again. You
can find it in \class{DocuExampleAttachmentService} and \class{DocuExampleAttachmentServiceProvider}

\subsection*{What to remember}

This is what you should not forget after completing this tutorial:

\begin{itemize}
\item You can send and receive attachments with JMEDS.
\item The type of an attachment is \textit{base64}.
\item There are three types of attachments: stream, file or memory
attachments.
\end{itemize}

\newpage
\subsection{Streams as a special attachment}

Since streams are continuous they must be treated differently than simple 
files like e.g. images. Because the procedure is very similiar to the 
handling of a simple file we only explain the most important differences. 

\subsection*{Streaming with an InputStream}
In our example we send a continous stream from our service to our client, 
which we will implement next. Right after the start of the JMEDS Framework 
we can create our device and service, registering the stream and start the device.

\begin{lstlisting}[language=Java]
// create a simple device ...
DefaultDevice device = new DefaultDevice();

// ... and a service
DefaultService service = new DefaultService();

Operation streamer = new DocuExampleAttachmentOperation(GET_STREAM_OPERATION, PORT_TYPE);

Element stream = new Element(new QName("Stream", PORT_TYPE.getNamespace()), SchemaUtil.TYPE_BASE64_BINARY);
stream.setMinOccurs(1);

streamer.setOutput(stream);

service.addOperation(streamer);

// add service to device in order to support automatic discovery ...
device.addService(service);
		
/*
* ... and start the device; this also starts all services located on
* top of that device
*/
device.start();
\end{lstlisting}\vspace*{1em}

Now we can implement a \class{DocuExampleAttachmentOperation} class 
where we generate our stream and only need to implement the \texttt{invokeImpl} 
Method (You can find the example in \class{DocuExampleAttachmentOperation}).

You already know the \class{AttachmentFactory} 
class and now we use a further function of it: 
\texttt{addStreamingMediaType}.\newline
That is the function call inside the \class{DocuExampleAttachmentOperation} class:

\begin{lstlisting}[language=Java]
public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {
		
	ParameterValue result = createOutputValue();
		
	AttachmentFactory afac = AttachmentFactory.getInstance();

	if (afac != null) {
		// it's important to send the data as a string
		((AttachmentValue) result).setAttachment(afac.createStreamAttachment(new myInputStream(), MIMEConstants.CONTENT_TYPE_APPLICATION_OCTET_STREAM));
	} else {
		Log.error("No attachment support available.");
	}
	
	return result;
}
\end{lstlisting}

\texttt{MyInputStream()} is a class which extends the \class{InputStream} class 
(remember we want to send an InputStream) and implement its read() method 
where we only create a random Character. The \texttt{addStreamingMediaType} 
function further sets the MIME type of our attachments content. 
Because we want to send a stream we set the content to 
\texttt{CONTENT\_TYPE\_APPLICATION\_OCTET\_STREAM}.\newline

Since we now have an InputStream we can implement the Client which receives the stream from our service. 
We will call this class \class{DocuExampleStreamingClient} which extends the 
\class{DefaultClient} class. 
Then we do exactly the same than in our Service:
\begin{lstlisting}[language=Java]
// register MIME types to stream ...
AttachmentFactory afac = AttachmentFactory.getInstance();

// it's important to receive the data as a string, be aware of the mime type
if (afac != null) afac.addStreamingMediaType(MIMEConstants.CONTENT_TYPE_APPLICATION_OCTET_STREAM);
\end{lstlisting}\vspace*{1em}

Just after that we create our client and make it known to the service:

\begin{lstlisting}[language=Java]
DefaultClient client = new DocuExampleStreamingClient();
client.registerHelloListening();
SearchParameter search = new SearchParameter();
search.setServiceTypes(new QNameSet(DocuExampleStreamingService.PORT_TYPE));
SearchManager.searchService(search, client);
\end{lstlisting}\vspace*{1em}

In this class you also need to implement the just called method 
\texttt{helloReceived} which you can find in the full example code. 
This method is needed to receive an hello message from available services.

At the end we must read the stream. This occurs in the 
\class{DocuExampleStreamConsumer} class where we get the attachment and
 read it char by char.
 
\begin{lstlisting}[language=Java]
public void run() {
	InputStream in = null;
	try {
		in = ((AttachmentValue) param).getAttachment().getInputStream();
		InputStreamReader reader = new InputStreamReader(in);
		int count = 0;
		int i;
		
		// read the input char by char
		while ((i = reader.read()) != -1) {
			Log.info("Read character: " + (char)i);
			// just writes a little log entry about the received data volume
			if(count % 10 == 0) Log.info(count++ + " characters received till now");
			else count++;
		}
	} catch (Exception e) {	
		Log.printStackTrace(e);
	} finally {
		// it's important to dispose all attachments on client side
		param.disposeAllAttachments();
	}
}
\end{lstlisting}\vspace*{1em}

It's very important to call the \texttt{disposeAllAttachments()} method 
if we have finished reading the InputStream. It's only necessary on client side where 
we must ensure the clean up by our own. \newline
	
If you are running the example code you will see a lot of Logging 
information. This is especially for demonstration purposes. Just 
open the client and service in seperate terminals. Then you will 
see that the client already receives the attachment during the 
creation on services side.

\subsection*{What to remember}

This is what you should not forget after completing this tutorial:

\begin{itemize}
\item The MIME type of an stream is \texttt{CONTENT\_TYPE\_APPLICATION\_OCTET\_STREAM}.
\item The receiver always must add this information by his own.
\item The client must call the \texttt{disposeAllAttachments()} method directly after work.
\end{itemize}

\subsection*{Streaming with an OutputStream}
With an InputStream we could generate our own Stream (which we made in our 
previously example) or just pass through an incoming InputStream. Again we 
generate a random string and send it to our client but this time we generate
an OutputStream. The advantage of an OutputStream is that we can decide 
the length of sended blocks (technically an InputStream always uses an buffer) 
on our own just by calling the \texttt{flush()} method on our OutputStream if 
we want to send the recently wrote Output.

Generally we do the same as before with a few differences in our 
\class{DocuExampleAttachment\-Operation} class (now called 
\class{DocuExampleAttachmentOperationOS}). All other classes do not change, they 
still works with our new attachment. Now lets have a look at the changed code of our 
\texttt{invokeImpl} method:

\begin{lstlisting}[language=Java]
if (afac != null) {
	// it's important to send the data as a stream
	OutgoingOutputStreamAttachment oosa = afac.createStreamAttachment(MIMEConstants.CONTENT_TYPE_APPLICATION_OCTET_STREAM);
	((AttachmentValue) result).setAttachment((oosa));
	new Thread(new myOutputStreamGenerator(oosa.getOutputStream())).start();
} else {...
\end{lstlisting}\vspace*{1em}

As you can see we changed the attachment type from \class{OutgointAttachment} to 
\class{Outgoing\-OutputStreamAttachment} and then start a new thread with our attachments 
outputstream where we generate our random stream like before:

\begin{lstlisting}[language=Java]
private class myOutputStreamGenerator implements Runnable{

	private OutputStream os;
	private int count = 1;
	Random generator = new Random(42);
		
	public myOutputStreamGenerator(OutputStream os){
		this.os = os;
	}

	/*
	 * Create a run() method which constantly generates random characters
	 * for demonstration purposes
	 */
	public void run() {
		while(true){
			char randChar = ALPHABET[generator.nextInt(ALPHABET.length)];
				
			try{
				os.write(new byte[]{(byte)randChar});
			} catch (IOException e){
				e.printStackTrace();
			}
			
			// print a log info and flush every tenth character
			if(count % 10 == 0){
				Log.info(count++ + " characters " + "send");
				try{
					os.flush();
				} catch(IOException e){	
					e.printStackTrace();
				}
			} else {
				count++;
			}
		}
	}
}
\end{lstlisting}\vspace*{1em}

This is what you should not forget after completing this tutorial:

\begin{itemize}
\item An \class{OutputStream} is very similar to our previously \class{InputStream} 
but you have the ability to decide when you want to write your data.
\end{itemize}