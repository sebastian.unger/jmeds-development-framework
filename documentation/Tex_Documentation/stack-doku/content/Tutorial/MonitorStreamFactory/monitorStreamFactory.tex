\section{MonitorStreamFactory}
\label{sec:monStreamFac}
The goal of this tutorial is to:
\begin{itemize}
\item Learn how to use the MonitorStreamFactory and StreamMonitor classes.
\end{itemize}

The \class{MonitoringStreamFactory} and \class{StreamMonitor} class allows to
be informed about every communication on the stack. That means that all received
and sent messages, faults or requests for resources will be traced and given to
the \class{StreamMonitor} if a \class{MonitorStreamFactory} is set in the
\class{JMEDSFramework}.

To use the mechanism of the \class{MonitorStreamFactory} it is necessary to
extend the factory and overwrite the following two methods.

\begin{lstlisting}[language=java]
	public class DefaultMonitorStreamFactory extends MonitorStreamFactory {

		protected StreamMonitor createInputMonitor() {
		...	
		}
	
		protected StreamMonitor createOutputMonitor() {
		...
		}
	}
\end{lstlisting}\vspace*{1em}
To use the functionalities of the \class{Stream\-Moni\-tor} it is necessary
to implement the \\ \class{StreamMonitor} interface.
 
\begin{lstlisting}[language=java]
	private class DefaultStreamMonitor implements StreamMonitor {

		public OutputStream getOutputStream() {...}

		public void setMonitoringContext(MonitoringContext context) {...}

		public void resetMonitoringContext() {...}

		public MonitoringContext getMonitoringContext() {...}

		public void assign(MonitoringContext context, AttributedURI optionalMessageId) {...}

		public void fault(MonitoringContext context, Exception e) {...}

		public void discard(MonitoringContext context, int discardReason) {...}

		public void resource(MonitoringContext context, Resource resource) {...}

		public void request(MonitoringContext context, URI location) {...}

		public void noContent(MonitoringContext context, String reason) {...}
	}
\end{lstlisting}

For example one possibility of implementation is to build a new
\class{DefaultStreamMonitor} class and return the new instance. That should be
done, because if there is just on instance and many messages in a short time
they will use the instance at the same time and that's not possible. Therefore
it is advisable for both input monitor and output monitor to create new
instances.

\begin{lstlisting}[language=java]
	public StreamMonitor createInputMonitor() {
		return new DefaultStreamMonitor();
	}

	public StreamMonitor createOutputMonitor() {
		return new DefaultStreamMonitor();
	}
\end{lstlisting}\vspace*{1em}
In our example we have an own attribute for the context in our default
implementation. Hence we can implement the set, get and reset method easily for
the monitoring context attribute. The context attribute is used for saving the
message, the resource or the fault and other useful information(like receive
time, send time, connection information, etc.) while the message etc. is
processed.

\begin{lstlisting}[language=java]
	private MonitoringContext	context		= null;
	
	public void setMonitoringContext(MonitoringContext context) {
		this.context = context;
	}

	public void resetMonitoringContext() {
		setMonitoringContext(null);
	}

	public MonitoringContext getMonitoringContext() {
		return context;
	}
\end{lstlisting}\vspace*{1em}
The next six methods represent the actual mechanism of the monitor stream. The
methods will be called if: 
\begin{itemize}
  \item a message was sent or received  $\rightarrow$ assign(\ldots)
  \item a fault was sent or received $\rightarrow$ fault(\ldots)
  \item a received message was discarded $\rightarrow$ discard(\ldots)
  \item a resource was sent or received $\rightarrow$ resource(\ldots)
  \item a request for a resource was sent or received $\rightarrow$ request(\ldots)
  \item a message without body was sent, like a ``HTTP OK'' $\rightarrow$ noContent(\ldots)
\end{itemize}\vspace*{1em}
\begin{lstlisting}[language=java]
	public void assign(MonitoringContext context, AttributedURI optionalMessageId){ 
		System.out.println("<DefaultMonitorStream> - Assign: " + context.getMessage().toString()); 
	}

	public void fault(MonitoringContext context, Exception e) {
		System.out.println("<DefaultMonitorStream> - Fault:  " + context.getMessage().toString());
	}
	
	public void discard(MonitoringContext context, int reason) {
		String info = "Message not available";
		SOAPHeader header = context.getHeader();
		if (header != null) {
			info = MessageConstants.getMessageNameForType(header.getMessageType());
		}
		System.out.println("<DefaultMonitorStream> - Discard: " + info + ", because: " + context.getDiscardReasonString(reason));
	}

	public void resource(MonitoringContext context, Resource resource) {
		System.out.println("<DefaultMonitorStream> - Resource:  " + resource.toString());
	}

	public void request(MonitoringContext context, URI location) {
		System.out.println("<DefaultMonitorStream> - Request: " + location);
	}

	public void noContent(MonitoringContext context, String reason) {
		System.out.println("<DefaultMonitorStream> - NoContent:  " + reason);
	}
\end{lstlisting}\vspace*{1em}
A second mechanism that does not work with the methods above is to implement a
new output stream which writes the message, etc. into the outline like in our
example or into a file. Every time a message was sent or received the
write method for the output stream will be called if available.

\begin{lstlisting}[language=java]
	private OutputStream		FORWARDER	= new OutputStream() {
												public void write(int b) throws IOException {
													System.err.write(b);
												}
											};

	public OutputStream getOutputStream() {
		/*
		 * !!! don't return System.out or System.err directly, as they would
		 * get closed by the monitors!!!
		 */
		return FORWARDER;
	}
\end{lstlisting}\vspace*{1em}
The last step is to register the monitor stream factory at the
\class{JMEDSFramework}. 
\begin{lstlisting}[language=java]
	DefaultMonitoredStreamFactory dmsf = new DefaultMonitoredStreamFactory();
	JMEDSFramework.setMonitorStreamFactory(dmsf);
\end{lstlisting}

\subsection*{What to remember}

This is what this tutorial is about and should be remembered:

\begin{itemize}
\item Use the \class {MonitorStreamFactory} to get informed about
all messages that are sent or received by the JMEDS framework.
\item Implement the \class{StreamMonitor} interface to add functionality.
\item Use \class{DefaultMonitorStream} as default implementation.
\item Register the \class {MonitorStreamFactory} with the JMEDS framework.
\end{itemize}