\section{Discover and use a service}
\label{sec:discovery}
The goals of the tutorial are to:

\begin{itemize}
	\item learn how to discover services on the network with JMEDS
	\item learn how to use services on the network with JMEDS 
\end{itemize}
    
If you want to work with a particular device/service or want to list all available devices/services on your network, you have to start a search. In this Tutorial we will find and call the service implemented in Tutorial \ref{sec:device}.

\subsection*{Discover and use the Hello World service}

Before using an operation in JMEDS, the  corresponding device or service needs to be discovered. So we need a new class for searching the network, look at \class{DocuExampleSearchClient} for the entire code. The class should inherit from \class{DefaultClient}, because it has already implemented a search callback.

You need to create a \class{SearchParameter} object to initiate a search, like this: 

\begin{lstlisting}[language=Java]
SearchParameter search = new SearchParameter();
\end{lstlisting}
After this, you have to specify what you are searching for. You can search for devices or for services. If you want to search for devices you have to add: 

\begin{lstlisting}[language=Java]
search.setDeviceTypes(new QNameSet(new QName("DocuExampleDevice", namespace)));
\end{lstlisting}
If you want to search for services you have to add: 

\begin{lstlisting}[language=Java]
search.setServiceTypes(new QNameSet(new QName("BasicServices", namespace)));
\end{lstlisting}
\emph{Be careful!} Searching for a service is very expensive as JMEDS first searches
for all available devices in the network and then takes a closer look at all
devices to look for the correct service. Therefore you should always prefer
searching for a device.

Finally we need to start the search. For this we use the \class{SearchManager}:

\begin{lstlisting}[language=Java]
SearchManager.searchDevice(search, client, null);	//Search for Devices
SearchManager.searchService(search, client);		//Search for Services
\end{lstlisting}

\begin{itemize}
	\item As you can see, we specify what to search for (by passing a
	\class{SearchParameter} object) and whom to call when a service is found (by
	passing a \class{SearchCallback} object, in this case the \class{DefaultClient} is our callback).
	\item The two methods differ by only one parameter. \class{SearchDevice} needs
	a deviceListener in order to obtain status changes of the device. This is
	further explained in tutorial \ref{sec:lifecycle}, so that we only pass null
	here.
\end{itemize}

\subsection*{Use the Hello World service without discovery}
As an alternative, you can search for the service without using discovery.
The following code resolves a device by it's EndpointReference.
\begin{lstlisting}[language=Java]
EndpointReference epr = new EndpointReference(new AttributedURI("urn:uuid:4e125b40-6632-11e2-80d5-fcbdecddf1fb"));
XAddressInfo xAddress = new XAddressInfo(new URI("http://139.2.61.217:63597/4e293ea0-6632-11e2-80d8-fcbdecddf1fb"));
xAddress.setProtocolInfo(new DPWSProtocolInfo(DPWSProtocolVersion.DPWS_VERSION_2009));
XAddressInfoSet addresses = new XAddressInfoSet(xAddress);

DeviceReference defRef = DeviceServiceRegistry.getDeviceReference(epr, addresses, true);
try {
	Device dev = defRef.getDevice();
	// we know the searched service's id
	DefaultServiceReference sRef = (DefaultServiceReference) dev.getServiceReference(DocuExampleService.DOCU_EXAMPLE_SERVICE_ID, SecurityKey.EMPTY_KEY);
	Service serv = sRef.getService();

	// we are looking for an Operation by its name
	Operation op = serv.getOperation(null, "DocuExampleSimpleOperation", null, null);

	ParameterValue input = op.createInputValue();
	ParameterValueManagement.setString(input, DocuExampleSimpleOperation.NAME, "Number One");

	ParameterValue result;
	try {
		result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		System.out.println("Response from the DocuExampleSimpleOperation: " + result.toString());
	} catch (AuthorizationException e) {
		e.printStackTrace();
	} catch (InvocationException e) {
		e.printStackTrace();
	}
} catch (CommunicationException e) {
	e.printStackTrace();
}
\end{lstlisting}
    
\subsection*{Use the Hello World service}
    
To work with the found devices and services, you have to override the two callback methods deviceFound and serviceFound.

This is the callback function for calling the \class{DocuExampleSimpleOperation}
used in this tutorial: 

\begin{lstlisting}[language=Java]
public void serviceFound(ServiceReference servRef, SearchParameter search) {

  try {
    // get Operation
    Operation op = servRef.getService().getAnyOperation(service, "DocuExampleSimpleOperation");

    // create input value and set string value
    ParameterValue request = op.createInputValue();
    ParameterValueManagement.setString(request, "name", "Bobby");

    System.out.println("Invoking HelloWorldOp...");

    // invoke operation with prepared input
    ParameterValue result = op.invoke(request);

   // get string value from answer
   String reply = ParameterValueManagement.getString(result, "reply");

    System.out.println(reply);
  
  } catch (CommunicationException e) {
    e.printStackTrace();
  } catch (InvocationException e) {
    e.printStackTrace();
  }
}
\end{lstlisting}\vspace*{1em}
Let's explain all the important details of the callback function:

\begin{itemize}
	\item You must override the serviceFound and deviceFound methods to work with the devices/services.
	\item deviceFound is called when a device matching the search criteria is found.
	\item serviceFound is called when a service matching the search criteria is found.
	\item To get an operation that belongs to a discovered service, you can call getAnyOperation.
	\item getAnyOperation needs the port type ("namespace") of the service and the name of the operation you want, in this case \class{DocuExampleSimpleOperation}.
	\item You need the input parameter structure if you want to call a service with correct arguments, even if there are no parameters to pass. To get that structure, you have to call createInputValue on an Operation object.
	\item To use an operation, just call invoke on an Operation object. The
	arguments are passed in the \class{ParameterValue} argument of invoke. This
	parameter is what \texttt{createInputValue()} returns.
\end{itemize}

If you override the \texttt{deviceFound()} method you can get all
\class{ServiceReferences} from the given \class{DeviceReference}. Afterwards you
can iterate over them or just do whatever you like.

Now, it is time to put the service to the test. We suggest that you run the service provider written in Tutorial \ref{sec:device}. Do not be afraid of the log output. Wait a few seconds until the service provider has started and then start the search client written in this tutorial. Between all those lines of output you should find the "Hello World" output of our operation. So, the operation invocation has worked! Good job!

\subsection*{What to remember}

This is what you should remember after trying this tutorial:

\begin{itemize}
	 \item Services/Devices must be discovered before being used.
	 \item Callback functions will be called if something (device, service,
	 operation) matches a search criteria.
	 \item Searching for a special service is expensive in DPWS.
	 \item Every operation is used by calling their invoke method.
\end{itemize}

    
    
     