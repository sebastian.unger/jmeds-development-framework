\section{Expose a service}
\label{sec:device}
The goals of the tutorial are to:

\begin{itemize}
	\item use JMEDS in a Java project.
	\item understand the concept of devices and be able to create them.
	\item understand the concept of services and be able to create them.
	\item understand the concept of operations and add a simple operation.
	\item use a WSDL file to define a service.
\end{itemize}    

We will create a simple service that prints "Hello World!". The classes
corresponding to this part of the tutorial are: \class{DocuExampleServiceProvider}, \class{DocuExampleDevice}, \class{DocuExampleService}.
The last paragraph explains how to define a service from a WSDL file. The
example code is in the \textit{servicefromwsdl} folder.

\subsection*{Preliminary steps}

Before we can start implementing our services, we need to add the JMEDS Framework
to our project. Therefore do the following:

\begin{itemize}
	\item If you don't have JMEDS, you can download it on the
	\href{http://sourceforge.net/projects/ws4d-javame/}{Sourceforge project page}.
	\item Create the project in which you want to use JMEDS.
	\item Include the JAR file of JMEDS (the name of the jar is ws4d-java-se-full-without-security.jar or ws4d-java-se-full-debug-without-security.jar, if you also want to have the full source code and Javadoc). 
\end{itemize}

When you are using Eclipse as IDE this can be done in your project properties. Open the "Properties" window which can be found in the right-click menu of your project. Select "Java Build Path" and choose the tab called "Libraries". When you select "Add external jar..." you can choose the file you want to add. 

\subsection*{First steps}

First of all it is a good idea to create a class called \class{DocuExampleServiceProvider} which contains the main method. We could put this code in one of the other classes as well, but to keep track of all the classes we prefer to do it like this.

The first line of code that you MUST put before calling any JMEDS code is: 

\begin{lstlisting}[language=java]
JMEDSFramework.start(args);
\end{lstlisting}\vspace*{1em}
This is the first line of our main method and you should keep it in mind. We
will add more lines during this tutorial. 

\subsection*{Write a device}

Now we will create a device. The device is the basis of DPWS. A device is an entity that offers services.

With JMEDS it is quite easy to create a device, when you use the \class{DefaultDevice} class. We create a new class called \class{DocuExampleDevice} which extends \class{DefaultDevice}.

We will only add some lines to the constructor at the moment because the \class{DefaultDevice} implementation already does most of the work for us. Therefore: 
\begin{lstlisting}[language=java]
public class DocuExampleDevice extends DefaultDevice {

	public final static String	DOCU_NAMESPACE	= "http://ws4d.org/jmeds";

	/**
	 * Constructor of our device.
	 */
	public DocuExampleDevice() {
		super();

		/*
		 * The following lines add metadata information to the device to
		 * illustrate how it works. As default values are defined for all of the
		 * fields, you CAN set new values here but you do NOT have to.
		 */

		// set PortType
		this.setPortTypes(new QNameSet(new QName("DocuExampleDevice", DOCU_NAMESPACE)));
		// add device name (name is language specific)
		this.addFriendlyName("en-US", "DocuDevice");
		this.addFriendlyName(LocalizedString.LANGUAGE_DE, "DokuGeraet");

		// add device manufacturer (manufacturer is language specific)
		this.addManufacturer(LocalizedString.LANGUAGE_EN, "Test Inc.");
		this.addManufacturer("de-DE", "Test GmbH");

		this.addModelName(LocalizedString.LANGUAGE_EN, "DocuModel");

		// add binding (optional!)
		/*
		 * add discovery binding or change the ip (127.0.0.1) with an ip of a
		 * non loopback interface like eth1 or eth3
		 */
		// NetworkInterface iface =
		// IPNetworkDetection.getInstance().getNetworkInterface("eth3");
		// IPDiscoveryDomain domain =
		// IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(iface,
		// false);
		// this.addBinding(new
		// IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID,
		// domain));

		// this.addBinding(new
		// HTTPBinding(IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1",
		// false), 0, "docuDevice",
		// DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));
	}
}
\end{lstlisting}\vspace*{1em}
The first few lines add metadata information to our device. This is helpful for the ones who use our device and services. That way we can tell them what our device is called and where it is from. JMEDS defines default values for all the fields so that it is not necessary to override them. Most of the information are language specific. So you can add different names for different languages. Default language is always "en-US".

Since we expose the device on a network, we have to bind our device to an
address that can be recognized by anybody.
Therefore we can use some different approaches. 
First we can just leave out to add a binding manually. Then a mechanism called ``Autobinding'' will take care of it and add a
binding for all available network interfaces.
Otherwise we can fill a \class{HTTPBinding} object and pass it to the device like you can see in the example code above. 
This is what the last code line is for. 
The first parameter is the IP address to bind the device to, the
second parameter is the port (0 stands for using any free port), the third
parameter is the path the device will be bind to and the fourth parameter is the
communication manager (here DPWS).
If you use an IP address of a loopback interface like 127.0.0.1 (see \class{DocuExampleDevice}) a client can not find your device. 
You have to change the IP address to an IP address of a non loopback interface like eth1 or eth3 or something else.
Or you can add a discovery binding like this: 

\begin{lstlisting}[language=java]
//MY_NETWORK_INTERFACE: your network interface (e.g. "eth3")
NetworkInterface iface = IPNetworkDetection.getInstance().getNetworkInterface("MY_NETWORK_INTERFACE");
IPDiscoveryDomain domain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(iface, false);
		
this.addBinding(new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain));
\end{lstlisting}\vspace*{1em}

That's it for the moment. You can find the code in the \class{DocuExampleDevice} class. 

\subsection*{Write a service}

Now we need to add some functionality to our device: services. Creating a
service works, not very surprising, similar to creating a device. Although it will be very small we will create an own class for our service as well. This is what the whole class looks like:

\begin{lstlisting}[language=java]
public class DocuExampleService extends DefaultService {

	public final static URI	DOCU_EXAMPLE_SERVICE_ID	= new URI(DocuExampleDevice.DOCU_NAMESPACE + "/DocuExampleService");

	/**
	 * Standard Constructor
	 */
	public DocuExampleService() {
		super();

		this.setServiceId(DOCU_EXAMPLE_SERVICE_ID);

		// (tutorial 2) add Operations from tutorial 2 to the service

		DocuExampleSimpleOperation simpleOp = new DocuExampleSimpleOperation();
		addOperation(simpleOp);

		DocuExampleComplexOperation complexOp = new DocuExampleComplexOperation();
		addOperation(complexOp);

		DocuExampleAttributeOperation attrOp = new DocuExampleAttributeOperation();
		addOperation(attrOp);
	}

}

\end{lstlisting}\vspace*{1em}
Like we have done for the device, we add a \class{HTTPBinding} to enable the
service to be exposed and to be recognized in a network. Instead of creating a
new class for our really simple definition of a service, we could just create an
instance of \class{DefaultService} and add the binding to it manually
or, that's the easiest case, use autobinding which works for services as well.

\subsection*{Create device and add service}

Now we go back to our \class{DocuExampleServiceProvider} class. Remember that we have just added one line to start the framework to the main method so far.

What we will do next is to create an instance of our device and of our service. 

\begin{lstlisting}[language=java]
// First we need a device.
DocuExampleDevice device = new DocuExampleDevice();
\end{lstlisting}

\begin{lstlisting}[language=java]
// Then we create a service.
final DocuExampleService service = new DocuExampleService();
\end{lstlisting}\vspace*{1em}
This wasn't really hard, was it?

The next step is important. As a device hosts services, we have to add the service to our device. The code for that is quite intuitive but must not be forgotten! 

\begin{lstlisting}[language=java]
// In the end we add our service to the device.
device.addService(service);
\end{lstlisting}\vspace*{1em}
After creating device and service we have to start our device. So just add the following line: 

\begin{lstlisting}[language=java]
device.start();
\end{lstlisting}\vspace*{1em}
This function starts the device asynchronously. You can still execute some code after this method and the device will keep running.

What we have now is our device hosting a service. The device is running and could be found in network. What we do not have are any operations that really "do" something. 

\subsection*{Create a very simple operation}

As creating an operation is part of another Tutorial we will just add some lines to our \class{DocuExampleServiceProvider}. Our goal was to print "Hello World!" on the screen. Therefore we add a really simple operation to our service in the main method. 

\begin{lstlisting}[language=java]
service.addOperation(new Operation("DocuHelloWorldOp", new QName("BasicServices", DocuExampleDevice.DOCU_NAMESPACE)) {

			// we have to implement the invoke method
			public ParameterValue invokeImpl(ParameterValue pv, CredentialInfo localCredentialInfo) throws InvocationException {
				// all we want to do is to print Hello World!
				System.out.println("HelloWorld!");
				return pv;
			}
		});
\end{lstlisting}\vspace*{1em}
When creating an operation you have to extend the abstract JMEDS \class{Operation} class and implement the invoke method. This method contains what should happen if the operation is called. In our case we just print "Hello World!". As the method expects a return value, we just return the given \class{ParameterValue}. We could return null as well, as we do not use the value anyway. The given \class{CredentialInfo} is used for security. You will not have to worry about that now as we will use empty credential infos in the first examples. A detailed example how to use JMEDS security features can be found in section \ref{sec:security}.

\subsection*{Define a service from WSDL}

To define a service you can also use a WSDL file. Just add the following line to the constructor of the service. 

\begin{lstlisting}[language=java]
define(new URI("local:Tutorial 1/org/ws4d/java/servicefromwsdl/service.wsdl"), CredentialInfo.EMPTY_CREDENTIAL_INFO); 	
\end{lstlisting}

Now you have to implement the operations specified in the WSDL file. 
Therefor you use the following lines of code. 
To get an \class{OperationStub} object use the action name of the operation. 

\begin{lstlisting}[language=java]
OperationStub helloWorld = (OperationStub) this.getOperation(null, "DocuHelloWorldOp", null, null);

helloWorld.setDelegate(new InvokeDelegate() {

	public ParameterValue invokeImpl(Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) throws InvocationException {
		System.out.println("HelloWorld!");
		return arguments;
	}
});
\end{lstlisting}

\subsection*{What to remember}

Congratulations, you have just created your own device with a service and a simple operation. This is what you should remember after this tutorial:

\begin{itemize}
	\item Do not forget to start the \class{JMEDSFramework} before using any JMEDS code.
	\item Devices host services.
	\item Services host operations.
	\item Operations contain functionality. 
\end{itemize}
    