\section{Input and output parameters of an operation}
\label{sec:operation}
The goals of the tutorial are to:

\begin{itemize}
	\item Learn how to specify the input parameters of an operation.
	\item Learn how to specify the output parameters of an operation.
	\item Add the input parameters to an operation.
	\item Add and use the output parameters of an operation.
\end{itemize}

The following code can be found in the \class{DocuExampleSimpleOperation}, the \class{Docu\-Example\-Complex\-Operation} and the \class{DocuExampleAttributeOperation} classes.

Before we start implementing our operation we will have a look at the
concept of XML Schema which will help us to understand the structure of
operations and their input and output.

\subsection*{XML Schema}

XML Schema describes the structure of the content of an XML instance document.
Each element is dedicated to a specific data type. XML Schema comes with built-in primitive data types like string, boolean, decimal and derived data types like byte, int, token and positiveInteger. It is also possible to define own derived data types.
In this section we will just give a very short introduction using a simple
example. Please use other sources if you want to know more about XML and XML
Schema. In section \ref{sec:wsdl} you can find more information about
WSDLs, which are XML documents to describe devices and services. JMEDS will
automatically create correct WSDL documents for our devices, but knowing a bit
about XML and XML Schema really helps to understand how input and output
parameters with more complex types can be created.

An XML Schema can look like this:

\begin{lstlisting}[language=xml]
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.example.org">
  <xs:element name="userInfo" type="tns:userInfoType"/>
  <xs:complexType name="userInfoType">
    <xs:sequence>
      <xs:element name="name" type="xs:string"/>
      <xs:element maxOccurs="2" name="address" type="xs:string"/>
    </xs:sequence>
  </xs:complexType> 
</xs:schema>
\end{lstlisting}\vspace*{1em}
This XML Schema defines a derived data type called userInfoType which contains
inner elements. The inner elements are simple
ones of the primitive type ``String". They are surrounded by a sequence
container which means that the elements must appear in the given order. Other possibilities are
<xs:choice> which allows only one of the following element or <xs:all> which
does not define an order on the elements at all. The derived data type is used by the element
``userInfo''.
This XML schema allows (amongst others) the creation of the following XML
instance document:

\begin{lstlisting}[language=xml]
<?xml version="1.0"?>
 <userInfo>
    <name>John</name>
    <address>Sesamstrasse</address>
    <address>Entenhausen</address>
 </userInfo>
</code>
\end{lstlisting}\vspace*{1em}

\subsection*{Pass a single input parameter to an operation}
In the following we will define the input and output of our Operation. Therefore
we need to define a XML Schema on our own to describe what the parameters the
operation expects look like. We need to set types, how often they are allowed to
occur and in which order they should appear. We do not need to write XML
directly on our own since we use Java code instead, but we need to follow the
principles described above. 

First we create a new class which is called \class{DocuExampleSimpleOperation} and extends the JMEDS class \class{Operation}. 
In the constructor we call the super-constructor and pass the parameters
required.
The first parameter is the name of our Operation, the second one is the port type. A
port type is an abstract collection of operations. We will call it ``Basic
Services'' here.

\begin{lstlisting}[language=java]
super("DocuExampleSimpleOperation", new QName("BasicServices", namespace));
\end{lstlisting}\vspace*{1em}

We will define an input parameter in the construtor as well. A new created 
element will be set as input parameter. As we have to specify the schema types 
we want to use we take the simple type ``String'', which has already been 
defined in class \class{StringUtil}.

\begin{lstlisting}[language=java]
// create new Element called "name" (just a simple one in this case)
Element nameElem = new Element(new QName("name", namespace), SchemaUtil.TYPE_STRING);

// set the input of the operation
setInput(nameElem);
\end{lstlisting}\vspace*{1em}
The first parameter of the element constructor is the qualified name of the element, the second is the type we have declared above.
That's it for our first really simple operation.

What we have to do to use it, is to add the operation to a service. You can use the \class{DocuExampleService} from Tutorial 1. Just add these lines to the constructor of the service class. 

\begin{lstlisting}[language=java]
 DocuExampleSimpleOperation simpleOperation = new DocuExampleSimpleOperation();
 addOperation(simpleOperation);
\end{lstlisting}

\subsection*{Receive a single and simple output parameter from an operation}

In the \class{DocuExampleSimpleOperation} constructor, where we created the types and the elements for specifying the reception of one single input parameter, we have to do the same pattern for specifying a single output parameter. It briefly consists of:

\begin{itemize}
	\item Determine the type of the output (in this case, a string type, like we did
	above).
	\item Creating the element that is of this type (like we did for the "name" element - in this case, we will call it "reply").
	\item Call setOutput with the element we have just created. 
\end{itemize}    

\begin{lstlisting}[language=java]
// create new element called "reply"
Element reply = new Element(new QName("reply", namespace), xsString);

// set this element as output
setOutput(reply);
\end{lstlisting}\vspace*{1em}
So far, so good. But how can we use the parameters for our operation?

\subsubsection*{Using parameters}

Do you remember that we ignored the given \class{ParameterValue} in the
\texttt{invokeImpl(\ldots)} implementation in \ref{sec:device} and just returned the
same one? Now, after we have defined our input and ouput, we will have a closer
look at the \class{ParameterValue} class and fill the \texttt{invokeImpl(\ldots)}
method with more life: 

\begin{lstlisting}[language=java]
public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {
	
	// get string value from input
	String name = ParameterValueManagement.getString(parameterValue, "name");
	System.out.println("Hello World " + name);
	
	// create output and set value
	ParameterValue result = createOutputValue();
	ParameterValueManagement.setString(result, "reply", "How are you " + name);
	
	return result;
}
\end{lstlisting}\vspace*{1em}
Let's explain the details of the \texttt{invokeImpl} method:

To get the input value we will use the \texttt{getString(\ldots)} method of
the \class{ParameterValue\-Managment} class. It is a helper class to make it
easier to access single elements in the ParameterValue. The second parameter of the method
must match the name of the input element we defined in the constructor.
To return a result, we create an output value and set the value that should be
delivered to the caller; in case this is just a one-way operation, this method
must return null.

Again we will ignore the given \class{CredentialInfo} and point to tutorial
\ref{sec:security} for further information.
   
\subsection*{Pass a single complex input parameter to an operation}

If you want the input parameter to be a complex type, e.g. a userInfo,
described in the XML Schema section, there are little changes to make. See the
\class{DocuExampleComplexOperation} class for the full code.
     
\begin{lstlisting}[language=java]
// create inner elements for complex type
Element nameElem = new Element(new QName("name", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
Element addressElem = new Element(new QName("address", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
addressElem.setMaxOccurs(2);

// create complex type and add inner elements
ComplexType userInfo = new ComplexType(new QName("userInfoType", DocuExampleDevice.DOCU_NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
userInfo.addElement(nameElem);
userInfo.addElement(addressElem);

// create element of type userInfo
Element userInfoElement = new Element(new QName("userInfo", DocuExampleDevice.DOCU_NAMESPACE), userInfo);

// set the input of the operation
setInput(userInfoElement);
\end{lstlisting}\vspace*{1em}
The steps to a complex input parameter are the following:

\begin{enumerate}
	\item Determine schema types for the inner elements.
	\item Create the inner elements based on the primitive data types.
	\item Create a \class{ComplexType} and add the inner elements.
	\item Create an element using the complex type.
	\item Set this element as the input of this operation.
\end{enumerate}

Does this sound familar to you? Yes? It is the same structure we described in
the paragraph about XML Schema. This is why it is important that you have a good
understanding of this topic. The following subsection should therefore not tell
anything new to you as well.

\subsubsection*{Model group}

The second parameter of a \class{ComplexType} is the model group. Different model groups allow the assignment of an order to the content structure. The content order can be all, sequence or choice. 

\begin{itemize}
	\item all: The all model group does not restrict the order of the content. 
	\item sequence: The sequence model group requires the correct order of the content. 
	\item choice: The choice model group allows only one element to be chosen for content. 
\end{itemize}

\subsubsection*{Min- and Max-occur}

You can set a minimal and a maximal value of how often the element is allowed
to occur. Default value for both is one.
Here the ``max-occur'' value is set to two. So it is possible that the element address appears one or two times. 

\subsection*{Receiving complex parameters}

To receive complex parameters the invoke method has to look like that:     

\begin{lstlisting}[language=java]
public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

	// get string value from input
	String name = ParameterValueManagement.getString(parameterValue, "name");
	String address = ParameterValueManagement.getString(parameterValue, "address[0]");
	String address2 = ParameterValueManagement.getString(parameterValue, "address[1]");
	System.out.println("Hello World " + name + " from " + address + " or " + address2);

	// create output and set value
	ParameterValue result = createOutputValue();
	ParameterValueManagement.setString(result, "reply", "How are you " + name + " from " + address + " and other places?");

	return result;
}
\end{lstlisting}

One thing that is worth to be mentioned is the way you can access elements that
can appear more than once. The solution is really simple. They are handled like
elements in an array and can be accessed like that. As you can see in the code
example 
\begin{lstlisting}[language=java]
String address = ParameterValueManagement.getString(parameterValue,
"address[0]");
\end{lstlisting}
returns the value of the first address element. (Remember that the first index
in an array is always 0). The path ``address[1]'' refers to the second element
and so on.

\subsection*{Add attributes to elements}

You can also add attributes to elements. For the full code see the \class{DocuExampleAttribute\-Operation} class.

\begin{lstlisting}[language=java]
// create new Attribute called "gender"
Attribute genderAttr = new Attribute(new QName("gender", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
genderAttr.setUse(SchemaConstants.USE_OPTIONAL);

// create extended simple content type
ExtendedSimpleContent simpleCon = new ExtendedSimpleContent("simpleContent", DocuExampleDevice.DOCU_NAMESPACE);
simpleCon.setBase(SchemaUtil.TYPE_STRING);
simpleCon.addAttributeElement(genderAttr);

// // create new element with the extended simple content type
// Element simpleConNameElem = new Element(new QName("firstName",
// DocuExampleDevice.DOCU_NAMESPACE), simpleCon);
//
// // set the input of the operation
setInput(simpleConNameElem);
\end{lstlisting}\vspace*{1em}
You can get the value of the attribute by using the following method in the invoke method. 

\begin{lstlisting}[language=java]
	String attribute = ParameterValueManagement.getAttributeValue(parameterValue, null, "gender");
\end{lstlisting}\vspace*{1em}
The first parameter is the \class{ParameterValue} object you want to get the attribute from. The second parameter describes the path of the element the attribute is set to (the root element do not have to be set). The third parameter is the attributes name. 

\begin{lstlisting}[language=java]
// create inner elements
Element street = new Element(new QName("street", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
Element zip = new Element(new QName("zip", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_INT);
Element city = new Element(new QName("city", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
Element country = new Element(new QName("country", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);

// create attribute
Attribute name = new Attribute(new QName("name", DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
name.setUse(SchemaUtil.USE_REQUIRED);

// create complex type
ComplexType addressType = new ComplexType(new QName("addressType", DocuExampleDevice.DOCU_NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
// add inner elements
addressType.addElement(street);
addressType.addElement(zip);
addressType.addElement(city);
addressType.addElement(country);
// add attribute
addressType.addAttributeElement(name);

// create element of type address
Element adress = new Element(new QName("address", DocuExampleDevice.DOCU_NAMESPACE), addressType);

// set the input of the operation
setInput(adress);
\end{lstlisting}\vspace*{1em}
There are some parameters you can set for an attribute.

\begin{itemize}
	\item use: 
	\begin{itemize}
		\item required: Attribute must be set. 
		\item optional: Attribute can be set. 
		\item prohibited: Attribute must not be set. 
	\end{itemize}
	\item default: A default value is set for the attribute. 
	\item fix: A default value is set for the attribute and it can not be changed. 
\end{itemize}

\subsection*{What to remember}

\begin{itemize}
	\item With XML Schema you can describe what a XML document should look like.
	\item Operations can have input and output parameters.
	\item Elements can be of a simple or a complex type.
	\item Elements can have attributes.
\end{itemize}