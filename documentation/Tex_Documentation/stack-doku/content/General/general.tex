\chapter{General}
\label{cha:general} 
The \textit{Devices Profile for Web Services} (DPWS) defines a profile to enable
secure Web Service messaging, discovery, description and eventing on mobile and 
embedded devices. DWPS is based on existing Web Service specifications and
defines  additional constraints. JMEDS is a framework that allows to implement
and  run web services based on the DPWS specification.

The following documentation will describe the WS4D  \textit{Java Multi Edition
DPWS Stack} (JMEDS) developed by the TU Dortmund, Dept. of Computer Science,
Chair  4, Workgroup Computer Networks \& Distributed Systems and MATERNA
Information and Communications.

\section{Specifications}
The DPWS stack implements the following WS-* specifications:
\begin{itemize}
	\item \textit{WS-Addressing}: Provides transport-neutral mechanisms to address 
	Web Services and messages.
	\item \textit{WS-Discovery}: Defines a discovery protocol to locate services.
	\item \textit{WS-Eventing}: Describes a protocol that allows Web Services to 
	subscribe to or accept subscriptions for event notifications messages.
	\item \textit{WS-MetadataExchange}: Defines how meta data associated with a Web
	Service endpoint can be represented as WS-Transfer resources.
	\item \textit{WS-Transfer}: Describes a general SOAP-based protocol for
	accessing XML representation of Web Service-based resources.
	\item \textit{WS-Security}: Defines how integrity and confidentiality can be
	ensured for messages.
\end{itemize}

\section{Architecture}
Figure \ref{fig:architecture} shows an overview of the architectures components.

%Fig1 (architecture)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/architecture}
	\caption{Stack architecture}
	\label{fig:architecture}
\end{figure}

Both sides, client and server, are separated into three layers: Application, 
Dispatching and Communication.

The Communication layer hosts the technology-specific communication mangers and
is very loosely connected to the Dispatching layer. This ensures an easy
exchange for another manager if the communication technology should be changed.
The user does not have to make changes in the upper layers. Currently there are 
communication managers available for DPWS as shown in the figure and as a proof 
of concept for Bluetooth and ZigBee, which is a specification for low range 
communication for home automation.

The DPWS communication manager can transport SOAP messages via UDP or HTTP over 
TCP. MIME can be used to exchange attachments via HTTP. Network transfer can be 
achieved over IPv4 or IPv6 connections.

The Dispatching layer consists of three main components. The Search Manager
fulfils the WS-Discovery specification. It is used by clients to discover 
devices and services on the network via probe messages and message listeners. 
The Dispatcher handles the outgoing messages, e.g. the refactoring of messages 
for certain communication managers. In the Device/Service Registry references to
already known devices and services are stored. If a client needs a reference to 
an already known entity, it does not need to invoke a search via the Search
Manager but instead can access it via the Device/Service registry. Instead of a 
Search Manager, devices have a Subscription Manager to manage their event 
subscribers, e.g. the clients to which they have to send event messages.

The Application layer hosts the actual implementation of clients on the one side
and devices and services on the other which has to be done by the end-user.

Clients hold references to devices, services and operations/events a service 
provides. These references are lightweight and hold a proxy as a unique 
communication endpoint. On a change on the devices side these references are
easily  updatable. They represent the WS-Addressing specification. Additionally 
a Client holds an Event Sink for receiving events.

On the devices/services side each device can have many services and each
services  can provide many operations or events through event sources.
Additionally, devices and services need to have a binding to turn them into
unique, distinguishable resources. A common example is the \emph{HTTPBinding} 
to attach an IP address to a device.\vspace{1em}

Figure  \ref{fig:moduledependencies} shows an overview of the modules and
their dependencies.

%Fig (modules)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/module_dependencies.png}
	\caption{Stack module dependencies}
	\label{fig:moduledependencies}
\end{figure}


\section{Components}
In the following the most important components of the stack architecture are
further described. In the accompanying figures only the most important classes 
and methods are shown.

\subsection{Devices and Services}
The declaration of devices, services and operations is located in 
\emph{org.ws4d.java.service}. Device classes are shown in figure 
\ref{fig:device_uml}.

%figure 2 (device_uml)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/device_uml}
	\caption{Device classes}
	\label{fig:device_uml}
\end{figure}

The interface \emph{Device} is the most common interface of all devices. A
device in DPWS is a web service with specific functions, such as hosting of 
\emph{Services}. It can be addressed via a unique EndpointRefernce or can be 
discovered via probing from clients by its XAddress and unique PortType. To get 
information about a specific device there are several GET methods which
correspond with the WS-Transfer specification.

Common functionalities of devices are already implemented in
\emph{DeviceCommons}, most importantly noted the WS-Security specification. For
local devices, additional methods are declared in \emph{LocalDevice}.

Finally the classes \emph{DefaultDevice} (for local use) and \emph{ProxyDevice}
(for remote use) are the standard implementations from which the user can
inherit own devices. To reference a remote device, \emph{ProxyDevice} holds a
\emph{DeviceReference}.

A device bears services, represented through the interface \emph{Service}. The
implementation structure - as seen in figure \ref{fig:service_UML} - is roughly
the same as for devices. However, a \emph{Service} contains methods for
subscribing and renewing events. This and the implementation of events (see
below) represents the WS-Eventing specification.

%figure 3 (service_UML)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/service_UML}
	\caption{Service classes}
	\label{fig:service_UML}
\end{figure}

\subsection{Operations and Events}

Services offer Operations to clients. The implementation can be seen in figure
\ref{fig:operation_UML}.

%figure 4 (operation_UML)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/operation_UML}
	\caption{Operation and event classes}
	\label{fig:operation_UML}
\end{figure}

The interface \emph{OperationDescription} and its realization
\emph{OperationCommons} are the common base for both \emph{Operation} and
\emph{DefaultEventSource} within the DPWS framework. They contain all common
properties for WSDL 1.1 compliant operations. They are further used to declare
and return an operations or events input (\emph{getInput()}), input and output
(\emph{getOuput()}) parameters, as well as any faults (\emph{getFaults()}) that
may occur on invocation.

In \emph{EventSource} the special abilities of events are defined. The DPWS
framework supports two types of WSDL 1.1 and WS-Eventing compliant events:
notifications and solicit-response operations. While the first ones represent
one-way messages sent from the event source to its subscribers, the later
additionally includes response messages sent back from the subscribers to the
source.

Clients willing to receive notifications from this event source may simply
subscribe (\emph{subscribe(...)}) to it. A subscription can be defined to
expire after a certain amount of time or it may last forever, i.e. until either
the event source or the subscriber explicitly cancels it or it terminates due
to shut down or missing network reachability.

\emph{DefaultEventSource} is the source of server-side notifications. Services
exposing events create instances of this class and call its \emph{fire(...)}
method each time a notification is sent to subscribers. Also it handles
received solicit response messages. This class can be extended by the user to
implement their own events.

Likewise end-users can extend the \class{Operation} class to create own
operations. An operation can be one-way or request-response. The
method \emph{invoke()} executes an operation.

\subsection{Clients}
As seen in figure \ref{fig:client_UML}, \emph{DefaultClient} is the basic DPWS
Client implementation. This and the search classes (see below) can be found in
\emph{org.ws4d.java.client}.

%figure 5 (client_UML)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/client_UML}
	\caption{Client classes}
	\label{fig:client_UML}
\end{figure}

This class provides easy access to several points of interaction within the
DPWS framework, such as searching for devices/services, tracking a devices or
services state changes and receiving events from subscribed services. The basic
idea behind this class is: it implements several callback and listener
interfaces and provides empty implementations for all of their methods, so that an
implementing client can easily overwrite those in which it is really interested
in. Specialized clients can inherit from \emph{DefaultClient} such as the
Bluetooth and ZigBee Clients.

The \emph{HelloListener} reacts on received \emph{HelloMessages} from devices.
A \emph{HelloMessage} is sent to announce a device in the network and contains
the devices unique EndpointReference, metadata version, port types, scopes and
transport addresses.

On a clients search the \emph{SearchCallback} provides callback methods in case
a matching service or device is found. The clients known device and service
changes are announced via the \emph{ServiceListener} and \emph{DeviceListener}.
Finally the \emph{EventListener} receives the events and can get the current
subscription status.

Notice the four states a client can see a device - to which it holds a
reference - to be in: \emph{Unknown}, \emph{Stopped}, \emph{Running} and
\emph{Build Up} (see \emph{DeviceListener}). A device is \emph{Build Up} when a
client has established a \emph{DeviceProxy} to communicate. The following
figure \ref{fig:state_machine} shows the transitions between the different
states a \emph{DeviceReference} can be in.

% figure ? (state_machine)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/state_machine}
	\caption{DeviceReference state machine}
	\label{fig:state_machine}
\end{figure}


\subsection{Search}
Clients use the \emph{SearchManager} (see figure \ref{fig:search_UML}) to get
hold of other entities on the network.

% figure X (search_UML)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/search_UML}
	\caption{Search classes}
	\label{fig:search_UML}
\end{figure}

This class provides tools for searching local and remote devices and services
given a set of search criteria and obtaining references to devices/services
with known endpoint addresses. The search criteria can de specified using the
class \emph{SearchParameter} and a reference to the discovered device or
service is provided by implementations of the interface \emph{SearchCallback},
which will receive asynchronous notifications of matching services/devices
found during the search. The \emph{SearchManager} also enables the obtaining of
a reference to a (local or remote) device/service when knowing its endpoint
address (i.e. one of its endpoint references). This process differs somewhat
from the afore mentioned  search as it does not involve probing the network to 
assert the existence of the specified device or service (as its endpoint
reference is already known).

\emph{SearchMap} is a data structure of \emph{SearchPath}. Each search path
determines one or more technologies and physical or virtual interfaces of the
local machine within those technologies, which should be carried out over a
search/discovery process.
  
\emph{SearchPath} is an abstract way to describe a technology (e.g. DPWS,
Bluetooth, ZigBee, etc.) and a physical or virtual interface of the local
machine within this technology (such as an IP address, a network adapter name,
a serial port number, etc.). Such interfaces are referred to as domain
identifier whereas different technologies are represented by specific
technology identifiers.


\subsection{ParameterValue}
Since messages are transported between entities using SOAP, their structure is
XML-based. Within each entity the class called \emph{ParameterValue} is used to
allow the object representation of XML documents. The classes shown in figure
\ref{fig:parameter_UML} are located in \emph{org.ws4d.java.service.parameter}.

% figure X (parameter_UML)
\begin{figure}[ht]
	\centering
		\includegraphics[width=1.00\textwidth]{content/general/images/parameter_UML}
	\caption{Parameter classes}
	\label{fig:parameter_UML}
\end{figure}

XML Schema describes the structure of content for XML instance documents. Those
definitions are used inside WSDL documents to describe a messages content. It
is possible to define XML Schema structures with the classes \emph{Schema},
\emph{Element}, \emph{Attribute}, \emph{SimpleType}, \emph{ComplexType},
\emph{Group} and \emph{AttributeGroup}. This is at least necessary to invoke
SOAP operations (like used in DPWS). A complex type consists of a qualified
name and the description of the content structure.

Each element is dedicated to a specific data type. XML Schema comes with
built-in primitive data types like \emph{string}, \emph{boolean},
\emph{decimal} and derived data types like \emph{byte}, \emph{int},
\emph{token} and \emph{positiveInteger}. It is also possible to define ones own
derived data types.

To create a complex type, it is necessary to create the derived data type too
and use the primitive data type \emph{string}. To access predefined primitive
data types use the \emph{SchemaUtil\#getSchemaType(String)} method.

An \emph{Element} can be used as \emph{input} or \emph{output} parameter of an
operation. This will allow to use this parameter within a service. An element
defined inside a XML Schema will be used to create XML instance documents. The
Framework allows to create those XML instance documents with this class. A
parameter value can be created from an element with the
\emph{ParameterValue\#createElementValue(Element)} method, or will be
pass-through within action invocation.

The \emph{ParameterValue} class allows nested structures like in XML. An object
of this class represents a single entry in an XML instance document.
\emph{ParameterValue} allows to access the value of an element, the value of
any inner-element and essentially the inner-elements itself.
