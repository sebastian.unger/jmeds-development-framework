package org.ws4d.java;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.protocol.http.HTTPBinding;
import org.ws4d.java.service.DefaultService;

/**
 * 
 */
public class DocuExampleEventService extends DefaultService {

	/**
	 * Standard Constructor
	 */
	public DocuExampleEventService() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		this.addBinding(new HTTPBinding(IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1", false), 5678, "docuEventService", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));

		// add Events to the service
		DocuExampleSimpleEvent notification = new DocuExampleSimpleEvent();
		addEventSource(notification);

		DocuExampleComplexEvent solicit = new DocuExampleComplexEvent();
		addEventSource(solicit);

	}

}