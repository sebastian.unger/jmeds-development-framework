package org.ws4d.java;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolInfo;
import org.ws4d.java.communication.connection.ip.IPDiscoveryDomain;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.structures.IPDiscoveryBinding;
import org.ws4d.java.communication.structures.OutgoingDiscoveryInfo;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.KeyAndTrustManagerFactory;
import org.ws4d.java.security.SEKeyManagers;
import org.ws4d.java.security.SEKeyStore;
import org.ws4d.java.security.SEPrivateKey;
import org.ws4d.java.security.SETrustManagers;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.security.credentialInfo.LocalCertificateCredentialInfo;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

public class SecureClient extends DefaultClient {

	// For more information on secure clients take a look at tutorial 11 in the
	// documentation.

	private SecurityKey	securityKey;

	private HashSet		discoveryInfos;

	public static void main(String[] args) throws IOException, GeneralSecurityException {

		Log.setLogLevel(Log.DEBUG_LEVEL_DEBUG);
		JMEDSFramework.start(args);

		SecureClient client = new SecureClient();

		// create a SearchParameter. by doing this, the passed DeviceReferences
		// in methods like deviceFound() will already contain a SecurityKey,
		// which allows very easy handling.
		DPWSProtocolInfo protocolInfo = new DPWSProtocolInfo();
		ArrayList searchSet = new ArrayList();
		searchSet.add(new SearchParameter.SearchSetEntry(client.discoveryInfos, protocolInfo));
		SearchParameter search = new SearchParameter();
		search.setSearchSet(searchSet);

		client.searchDevice(search);

	}

	private SecureClient() throws IOException, GeneralSecurityException {
		String alias = "SecureClient";
		KeyAndTrustManagerFactory katmf = KeyAndTrustManagerFactory.getInstance();

		// load key- and truststore. key- and truststore are used for multicast
		// message signing and validating, the key- and trustmanager arrays are
		// for ssl connections.

		String trustStorePath = "security/org/ws4d/java/ClientTruststore.jks";
		String trustStorePwd = "trustpassword";
		SEKeyStore trustStore = (SEKeyStore) katmf.loadKeyStore(trustStorePath, trustStorePwd);
		SETrustManagers tms = (SETrustManagers) katmf.getTrustManagers(trustStorePath, trustStorePwd);

		String keyStorePath = "security/org/ws4d/java/ClientKeystore.jks";
		String keyStorePwd = "clientpassword";
		SEKeyStore keyStore = (SEKeyStore) katmf.loadKeyStore(keyStorePath, keyStorePwd);
		SEKeyManagers kms = (SEKeyManagers) katmf.getKeyManagers(keyStorePath, keyStorePwd);

		SEPrivateKey pk = (SEPrivateKey) keyStore.getPrivateKey(alias, keyStorePwd);

		// create the CredentialInfo that contains all necessary information for
		// security purposes. you may create multiple different CredentialInfos
		// for different purposed, for example to use different TrustStores for
		// different discovery bindings.

		LocalCertificateCredentialInfo lcci = new LocalCertificateCredentialInfo(alias, kms, tms, pk, keyStore, trustStore);
		CredentialInfo credentialInfo = new CredentialInfo(lcci);

		// activate validating of incoming messages, default is true if a
		// non-empty CredentialInfo is set
		credentialInfo.setSecureMessagesIn(true);
		// activate signing of outgoing messages, default is true if a non-empty
		// CredentialInfo is set
		credentialInfo.setSecureMessagesOut(true);

		CommunicationManager comMan = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		NetworkInterface networkInterface = IPNetworkDetection.getInstance().getNetworkInterface("eth3");

		// create a discoveryBinding that uses the CredentialInfo
		IPDiscoveryDomain discoveryDomain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(networkInterface, false);
		IPDiscoveryBinding discoveryBinding = new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, discoveryDomain);
		discoveryBinding.setCredentialInfo(credentialInfo);
		registerHelloListening(discoveryBinding);

		// create a security key. a security key is a construct containing a
		// CredentialInfo for ssl connections and a Set of
		// OutgoingDiscoveryInfos containing information for multicast messages.
		OutgoingDiscoveryInfo discoveryInfo = comMan.getOutgoingDiscoveryInfo(discoveryBinding, true, credentialInfo);
		discoveryInfos = new HashSet();
		discoveryInfos.add(discoveryInfo);
		securityKey = new SecurityKey(discoveryInfos, credentialInfo);

	}

	public void deviceFound(DeviceReference devRef, SearchParameter search, String comManId) {
		System.out.println("Device found: " + devRef);
		try {
			// we can do that because we used a searchParameter. if we didn't,
			// we would need to get a secure DeviceReference from the
			// SearchManager
			Device device = devRef.getDevice();
			System.out.println("Device received: " + device);
			// do something with the device
		} catch (CommunicationException e) {
			e.printStackTrace();
		}
	}

	public void deviceBuiltUp(DeviceReference devRef, Device device) {
		System.out.println("Device built up: " + device.getPresentationUrl());
		// pass the SecurityKey to get secure ServiceReferences. if you leave it
		// out the system will try to open a normal non-secure connection.
		Iterator serviceRefs = device.getServiceReferences(securityKey);
		while (serviceRefs.hasNext()) {
			ServiceReference servRef = (ServiceReference) serviceRefs.next();
			try {
				Service service = servRef.getService();
				System.out.println("Service found: " + service.getServiceId());
			} catch (CommunicationException e) {
				e.printStackTrace();
			}
		}
	}

	public void helloReceived(HelloData helloData) {
		System.out.println("Hello received.");
		DeviceReference devRef = SearchManager.getDeviceReference(helloData, securityKey, this, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		try {
			Device device = devRef.getDevice();
			System.out.println("Device found: " + device.getPresentationUrl());
			Iterator serviceRefs = device.getServiceReferences(securityKey);
			while (serviceRefs.hasNext()) {
				ServiceReference servRef = (ServiceReference) serviceRefs.next();
				Service service = servRef.getService();
				System.out.println("Service found: " + service.getServiceId());
			}
		} catch (CommunicationException e) {
			e.printStackTrace();
		}
	}
}
