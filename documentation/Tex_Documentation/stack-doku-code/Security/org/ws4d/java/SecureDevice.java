package org.ws4d.java;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.IPDiscoveryDomain;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.protocol.http.HTTPSBinding;
import org.ws4d.java.communication.structures.IPDiscoveryBinding;
import org.ws4d.java.communication.structures.OutgoingDiscoveryInfo;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.KeyAndTrustManagerFactory;
import org.ws4d.java.security.SEKeyManagers;
import org.ws4d.java.security.SEKeyStore;
import org.ws4d.java.security.SEPrivateKey;
import org.ws4d.java.security.SETrustManagers;
import org.ws4d.java.security.credentialInfo.LocalCertificateCredentialInfo;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

public class SecureDevice extends DefaultDevice {

	// TODO: Change this IP Address!!!
	public IPAddress	IP	= IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("139.2.63.3", false);

	public SecureDevice(String comManId) throws Exception {
		super(comManId);

		// For more information about secure devices and services, look at
		// Tutorial 11 in the documentation

		String alias = "urn:uuid:1337"; // alias for the private key in the
										// keystore

		setEndpointReference(new EndpointReference(new URI(alias)));
		addFriendlyName("en-US", "SecureDevice");

		KeyAndTrustManagerFactory katmf = KeyAndTrustManagerFactory.getInstance();

		// load key- and truststore. key- and truststore are used for multicast
		// message signing and validating, the key- and trustmanager arrays are
		// for ssl connections.
		String keyStorePath = "security/org/ws4d/java/DeviceKeystore.jks";
		String keyStorePwd = "password";
		SEKeyStore keyStore = (SEKeyStore) katmf.loadKeyStore(keyStorePath, keyStorePwd);
		SEKeyManagers kms = (SEKeyManagers) katmf.getKeyManagers(keyStorePath, keyStorePwd);

		String trustStorePath = "security/org/ws4d/java/DeviceTruststore.jks";
		String trustStorePwd = "devicepassword";
		SEKeyStore trustStore = (SEKeyStore) katmf.loadKeyStore(trustStorePath, trustStorePwd);
		SETrustManagers tms = (SETrustManagers) katmf.getTrustManagers(trustStorePath, trustStorePwd);

		SEPrivateKey pk = (SEPrivateKey) keyStore.getPrivateKey(alias, keyStorePwd);

		// create the CredentialInfo that contains all necessary information for
		// security purposes. you may create multiple different CredentialInfos
		// for different purposed, for example to use different TrustStores for
		// different discovery bindings.
		LocalCertificateCredentialInfo lcci = new LocalCertificateCredentialInfo(alias, kms, tms, pk, keyStore, trustStore);
		CredentialInfo credentialInfo = new CredentialInfo(lcci);

		// activate validating of incoming messages, default is true if a
		// non-empty CredentialInfo is set
		credentialInfo.setSecureMessagesIn(true);
		// activate signing of outgoing messages, default is true if a non-empty
		// CredentialInfo is set
		credentialInfo.setSecureMessagesOut(true);

		setPortTypes(new QNameSet(new QName("SecureDevice", "https://SecureDevice")));

		// the devices https binding
		HTTPSBinding httpsBinding = new HTTPSBinding(IP, 8888, "SecureDevice", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, credentialInfo);
		addBinding(httpsBinding);

		// discovery binding and outgoing discoveryInfos for signing and
		// validating of multicast
		CommunicationManager comMan = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		NetworkInterface networkInterface = IPNetworkDetection.getInstance().getNetworkInterface("eth3");

		IPDiscoveryDomain domain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(networkInterface, false);
		IPDiscoveryBinding discoveryBinding = new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain);
		discoveryBinding.setCredentialInfo(credentialInfo);
		addBinding(discoveryBinding);

		OutgoingDiscoveryInfo discoveryInfo = comMan.getOutgoingDiscoveryInfo(discoveryBinding, true, credentialInfo);
		addOutgoingDiscoveryInfo(discoveryInfo);

		// the services should be secure as well
		SecureService service = new SecureService();
		HTTPSBinding https2Binding = new HTTPSBinding(IP, 9999, "SecureService", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, credentialInfo);
		service.addBinding(https2Binding);
		addService(service);

		try {
			start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		JMEDSFramework.start(args);

		Log.setLogLevel(Log.DEBUG_LEVEL_DEBUG);
		try {
			new SecureDevice(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

class SecureService extends DefaultService {

	public SecureService() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		this.setServiceId(new URI("https://SecureService", "secure"));
	}

}
