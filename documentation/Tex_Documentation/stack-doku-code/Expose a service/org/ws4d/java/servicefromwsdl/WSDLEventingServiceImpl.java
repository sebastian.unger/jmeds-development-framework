package org.ws4d.java.servicefromwsdl;

import java.io.IOException;

import org.ws4d.java.DocuExampleDevice;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.EventDelegate;
import org.ws4d.java.service.EventSourceStub;
import org.ws4d.java.service.ServiceSubscription;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.URI;

public class WSDLEventingServiceImpl extends DefaultService {

	private int				exampleCounter	= 0;

	private int				eventCounter	= 0;

	private EventSourceStub	solicite		= null;

	private ParameterValue	sParamValue		= null;

	private EventSourceStub	notify			= null;

	private ParameterValue	nParamValue		= null;

	public WSDLEventingServiceImpl() throws IOException {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		setServiceId(new URI(DocuExampleDevice.DOCU_NAMESPACE + "/Event"));

		// Define the service from WSDL.
		define(new URI("local:/org/ws4d/java/servicefromwsdl/eventing.wsdl"), CredentialInfo.EMPTY_CREDENTIAL_INFO, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		/**
		 * Implements the events of the service.
		 */

		solicite = (EventSourceStub) getEventSource(null, "DocuExampleComplexEvent", null, null);

		solicite.setDelegate(new EventDelegate() {

			public void solicitResponseReceived(DefaultEventSource event, ParameterValue paramValue, int eventNumber, ServiceSubscription subscription) {
				String solicitResponse = ParameterValueManagement.getString(paramValue, "addToCounter");
				System.out.println("Solicit Response received: " + solicitResponse);
				if (solicitResponse != null) {
					exampleCounter = exampleCounter + Integer.parseInt(solicitResponse);
				} else {
					exampleCounter++;
				}
			}
		});

		// Delegate is not necessary because we do not have to response.
		notify = (EventSourceStub) getEventSource(null, "DocuExampleSimpleEvent", null, null);

	}

	public void run() {
		Thread r = new Thread(new Runnable() {

			public void run() {
				while (true) {
					try {
						Thread.sleep(3000);
						sParamValue = solicite.createOutputValue();
						ParameterValueManagement.setString(sParamValue, "counter", String.valueOf(exampleCounter));
						solicite.fire(sParamValue, exampleCounter++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
						System.out.println("fire SolicitEvent");

						Thread.sleep(3000);
						nParamValue = notify.createOutputValue();
						ParameterValueManagement.setString(nParamValue, "name", "me again (" + eventCounter + ")");
						notify.fire(nParamValue, eventCounter++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
						System.out.println("fire NotifyEvent");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		r.start();
	}
}
