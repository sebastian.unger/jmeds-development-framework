package org.ws4d.java.servicefromwsdl;

import java.io.IOException;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.InvokeDelegate;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationStub;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

public class WSDLServiceImpl extends DefaultService {

	public WSDLServiceImpl() throws IOException {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		// Define the service from WSDL.
		define(new URI("local:/org/ws4d/java/servicefromwsdl/service.wsdl"), CredentialInfo.EMPTY_CREDENTIAL_INFO, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		setServiceId(new URI("Service"));

		/**
		 * Implements the operations of the service.
		 */

		OperationStub helloWorld = (OperationStub) this.getOperation(null, "DocuHelloWorldOp", null, null);

		helloWorld.setDelegate(new InvokeDelegate() {

			public ParameterValue invokeImpl(Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) throws InvocationException {
				System.out.println("HelloWorld!");
				return arguments;
			}
		});

		OperationStub simple = (OperationStub) this.getOperation(null, "DocuExampleSimpleOperation", null, null);

		simple.setDelegate(new InvokeDelegate() {

			public ParameterValue invokeImpl(Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) throws InvocationException {
				// get string value from input
				String name = ParameterValueManagement.getString(arguments, "name");
				System.out.println("Hello World " + name);

				// create output and set value
				ParameterValue result = operation.createOutputValue();
				ParameterValueManagement.setString(result, "reply", "How are you " + name);

				return result;
			}
		});

		OperationStub complex = (OperationStub) this.getOperation(null, "DocuExampleComplexOperation", null, null);
		complex.setDelegate(new InvokeDelegate() {

			public ParameterValue invokeImpl(Operation operation, ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException {
				// get string value from input
				String name = ParameterValueManagement.getString(parameterValue, "name");
				String address = ParameterValueManagement.getString(parameterValue, "address[0]");
				String address2 = ParameterValueManagement.getString(parameterValue, "address[1]");
				System.out.println("Hello World " + name + " from " + address + " or " + address2);

				// create output and set value
				ParameterValue result = operation.createOutputValue();
				ParameterValueManagement.setString(result, "reply", "How are you " + name + " from " + address + " and other places?");

				return result;
			}
		});

		OperationStub attribute = (OperationStub) this.getOperation(null, "DocuExampleAttributeOperation", null, null);
		attribute.setDelegate(new InvokeDelegate() {

			public ParameterValue invokeImpl(Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) throws InvocationException {
				String attribute = ParameterValueManagement.getAttributeValue(arguments, null, new QName("name"));

				System.out.println("attribute: " + attribute);
				return null;
			}
		});

	}

}
