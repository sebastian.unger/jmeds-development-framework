package org.ws4d.java.servicefromwsdl;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultDevice;

public class WSDLServiceProvider {

	public static void main(String[] args) {
		try {
			// mandatory: Starting the DPWS Framework.
			JMEDSFramework.start(args);
			// Create a default device.
			DefaultDevice device = new DefaultDevice(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

			// Create event and add it to the device.
			WSDLEventingServiceImpl event = new WSDLEventingServiceImpl();
			event.run();
			device.addService(event);
			// Create a service and add it to the device.
			WSDLServiceImpl service = new WSDLServiceImpl();
			device.addService(service);

			// Starting the device.
			device.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
