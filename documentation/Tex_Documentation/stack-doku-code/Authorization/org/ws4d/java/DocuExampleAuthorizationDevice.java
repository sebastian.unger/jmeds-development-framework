package org.ws4d.java;

import org.ws4d.java.authorization.XmlAuthorizationManager;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.connection.ip.IPDiscoveryDomain;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.protocol.http.HTTPBinding;
import org.ws4d.java.communication.structures.IPDiscoveryBinding;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

/**
 * Sample device for authorization.
 */
public class DocuExampleAuthorizationDevice extends DefaultDevice {

	private QNameSet						qnsDeviceTypes;

	public static final String				NAMESPACE	= "http://www.ws4d.org";

	public final static EndpointReference	EPR			= new EndpointReference(new URI("urn:uuid:authorization"));

	public DocuExampleAuthorizationDevice() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		qnsDeviceTypes = new QNameSet(new QName("AuthorizationDevice", NAMESPACE));
		int port = 0;

		NetworkInterface iface = IPNetworkDetection.getInstance().getNetworkInterface("eth3");
		IPDiscoveryDomain domain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(iface, false);
		addBinding(new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain));
		addBinding(new HTTPBinding(IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface("127.0.0.1", false), port, "authorizationdevice", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));

		addFriendlyName("DE-de", "Autorisierungsger�t");
		addFriendlyName("en-GB", "Authorization Device");

		addManufacturer("de-DE", "Materna GmbH");
		addManufacturer("en-GB", "Materna Ltd.");

		addModelName("de-DE", "Model 1.0");
		addModelName("en-GB", "Model 1.0");

		setFirmwareVersion("v0.0.1-beta");
		setManufacturerUrl(NAMESPACE);

		setModelNumber("09-46-x-07-07-2011");
		setModelUrl(NAMESPACE);

		setSerialNumber("987654321BUI");

		setManufacturerUrl("http://www.materna.de");
		setModelUrl("https://www.materna.de");

		setPortTypes(qnsDeviceTypes);

		setEndpointReference(EPR);

		// Set the authorization manager
		setAuthorizationManager(new XmlAuthorizationManager("encryption.xml"));
	}

	public static void main(String[] args) {

		DocuExampleAuthorizationDevice device = null;

		try {

			Log.setShowTimestamp(true);
			Log.setLogLevel(Log.DEBUG_LEVEL_DEBUG);

			JMEDSFramework.start(args);

			device = new DocuExampleAuthorizationDevice();

			DocuExampleAuthorizationService service = new DocuExampleAuthorizationService();
			device.addService(service);

			device.start();
		} catch (Exception e) {

		}
	}
}
