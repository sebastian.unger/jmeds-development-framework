package org.ws4d.java;

import java.io.IOException;

import org.ws4d.java.authorization.XmlAuthorizationManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

public class DocuExampleAuthorizationService extends DefaultService {

	private String				namespace		= "http://www.ws4d.org";

	private URI					serviceId		= new URI(namespace + "/authorizationservice");

	private long				eventDelay		= 2000;

	private AuthorizationEvent	autorizationEvent;

	private Object				eventNotifier	= new Object();

	private volatile Thread		eventThread		= null;

	public DocuExampleAuthorizationService() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		setServiceId(serviceId);

		// Set the authorization manager
		setAuthorizationManager(new XmlAuthorizationManager("encryption.xml"));

		autorizationEvent = new AuthorizationEvent();

		addOperation(new AuthorizationOperation());
		addEventSource(autorizationEvent);
	}

	public synchronized void start() throws IOException {
		super.start();
		if (eventThread == null) {
			// define eventing thread...
			eventThread = new Thread() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Thread#run()
				 */
				public void run() {
					int i = 0;
					while (eventThread != null) {
						ParameterValue outputValue = autorizationEvent.createOutputValue();
						ParameterValueManagement.setString(outputValue, null, "event... " + i);

						autorizationEvent.fire(outputValue, i++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
						try {
							synchronized (eventNotifier) {
								if (eventThread != null) {
									eventNotifier.wait(eventDelay);
								}
							}
						} catch (InterruptedException e) {}
					}
				}
			};
			eventThread.start();
		}
	}

	public synchronized void stop() throws IOException {
		if (eventThread != null) {
			eventThread = null;
			synchronized (eventNotifier) {
				eventNotifier.notifyAll();
			}
		}
		super.stop();
	}

	private class AuthorizationOperation extends Operation {

		public AuthorizationOperation() {
			super("AuthorizationOperation", new QName("AuthorizationService", namespace));

			Type stringType = SchemaUtil.TYPE_STRING;
			setInput(new Element(new QName("Input", namespace), stringType));
			setOutput(new Element(new QName("Output", namespace), stringType));
		}

		public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {
			String input = ParameterValueManagement.getString(parameterValue, null);
			ParameterValue paramVal = createOutputValue();
			ParameterValueManagement.setString(paramVal, null, "Input: " + input);
			return paramVal;
		}

	}

	private class AuthorizationEvent extends DefaultEventSource {

		AuthorizationEvent() {
			super("AuthorizationEvent", new QName("AuthorizationService", namespace));

			Type stringType = SchemaUtil.TYPE_STRING;
			setOutput(new Element(new QName("Authorization", namespace), stringType));
		}
	}

}
