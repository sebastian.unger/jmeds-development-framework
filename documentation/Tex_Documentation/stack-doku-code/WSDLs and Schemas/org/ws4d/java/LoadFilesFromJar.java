package org.ws4d.java;

import java.net.URL;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.description.wsdl.WSDL;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.types.URI;

public class LoadFilesFromJar {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		LoadFilesFromJar lffj = new LoadFilesFromJar();

		// !! fill path to jar and path in jar !!
		String pathToJar = "";
		String pathInJar = "";

		System.out.println("1");
		// Load file with URL from System
		URL schemaFile = lffj.getClass().getResource("wsdl/WDPPrint.xsd");
		Schema schema1 = Schema.parse(new URI(schemaFile.toString()), CredentialInfo.EMPTY_CREDENTIAL_INFO, true, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		schema1.serialize(null, null, null, System.out, CredentialInfo.EMPTY_CREDENTIAL_INFO, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		System.out.println("\n 2");

		// Load file with own path
		URI wsdlFile = new URI("jar:/" + pathToJar + "!/" + pathInJar + "/WSDPrinterService.wsdl");
		WSDL wsdl = WSDL.parse(wsdlFile, CredentialInfo.EMPTY_CREDENTIAL_INFO, true, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		wsdl.serialize(System.out, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		System.out.println("\n 3");

		// Load file just with path in jar
		URI wsdlFile1 = new URI("local:/" + pathInJar + "/WSDPrinterService.wsdl");
		WSDL wsdl1 = WSDL.parse(wsdlFile1, CredentialInfo.EMPTY_CREDENTIAL_INFO, true, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		wsdl1.serialize(System.out, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
	}
}
