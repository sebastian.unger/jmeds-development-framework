package org.ws4d.java;

import java.io.IOException;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.description.DescriptionRepository;
import org.ws4d.java.description.wsdl.WSDL;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.types.URI;

public class WSDLHandling {

	public static void main(String[] args) {

		JMEDSFramework.start(args);

		DescriptionRepository wsdlRepo = DescriptionRepository.getInstance(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		URI wsdlUri = new URI("local:WSDLs and Schemas/org/ws4d/java/wsdl/WSDPrinterService.wsdl");
		URI wsdlUri1 = new URI("local:WSDLs and Schemas/org/ws4d/java/wsdl/vcard_v2_1.wsdl");
		String schemaLocation = "WellKnownValues.xsd";
		URI schemaUri = new URI("local:WSDLs and Schemas/org/ws4d/java/wsdl/WellKnownValues.xsd");
		String schemaNamespace = "http://schemas.microsoft.com/windows/2006/08/wdp/print";
		WSDL wsdl = null;
		WSDL wsdl1 = null;
		Schema schema = null;
		String newWSDLFileName = "newWSDL.wsdl";
		String newSchemaFileName = "newSchema.xsd";
		String newRepoPathPrefix = "C:/tmp_java/path";

		// Returns the repo path, where the loaded and stored wsdls are saved.
		// By default path is "pathToProject\wsdl_repo".
		System.out.println(wsdlRepo.getRepoPath());

		// Returns the prefix of the repo path. By default prefix is empty ("").
		System.out.println(wsdlRepo.getRepoPathPrefix());

		// Sets the prefix ot the repo path. That allows to change the path of
		// directory if a absolute path is in the prefix.
		wsdlRepo.setRepoPathPrefix(newRepoPathPrefix);

		System.out.println(wsdlRepo.getRepoPathPrefix());

		try {
			// Stores a given wsdl in the repository on the filesystem with given
			// filename.
			wsdlRepo.store(wsdlUri, CredentialInfo.EMPTY_CREDENTIAL_INFO, newWSDLFileName);
			wsdl = wsdlRepo.getWSDL(newWSDLFileName);

			// Load the wsdl and just returns the wsdl.
			wsdl1 = DescriptionRepository.loadWsdl(wsdlUri1, CredentialInfo.EMPTY_CREDENTIAL_INFO, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		} catch (IOException e) {}

		try {
			// load the schema form schema uri and stores the schema with given
			// new filename
			wsdlRepo.storeSchema(schemaUri, CredentialInfo.EMPTY_CREDENTIAL_INFO, newSchemaFileName);
		} catch (IOException e) {}

		// Returns the schema for given schema location and schema namespace
		schema = wsdlRepo.getSchema(schemaLocation, CredentialInfo.EMPTY_CREDENTIAL_INFO, schemaNamespace);

		System.out.println("WSDL:" + wsdl.toString());

		System.out.println("WSDL1:" + wsdl1.toString());

		System.out.println("Schema:" + schema.toString());
	}
}
