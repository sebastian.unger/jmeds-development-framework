package org.ws4d.java;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.Operation;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class DocuExampleStreamingService extends DefaultService {

	static final QName	PORT_TYPE				= new QName("StreamingService", "http://www.mydemo.com/tutorial/streaming");

	static final String	GET_STREAM_OPERATION	= "GetStream";

	public static void main(String[] args) throws Exception {
		Log.setShowTimestamp(true);

		// always start the framework first
		JMEDSFramework.start(args);

		// create a simple device ...
		DefaultDevice device = new DefaultDevice(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		// ... and a service
		DefaultService service = new DefaultService(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		Operation streamer = new DocuExampleAttachmentOperation(GET_STREAM_OPERATION, PORT_TYPE);

		Element stream = new Element(new QName("Stream", PORT_TYPE.getNamespace()), SchemaUtil.TYPE_BASE64_BINARY);
		stream.setMinOccurs(1);

		streamer.setOutput(stream);

		service.addOperation(streamer);

		// add service to device in order to support automatic discovery ...
		device.addService(service);

		/*
		 * ... and start the device; this also starts all services located on
		 * top of that device
		 */
		device.start();
	}

	public DocuExampleStreamingService() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
	}
}
