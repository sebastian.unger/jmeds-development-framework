package org.ws4d.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.ws4d.java.io.fs.FileSystem;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class LoadFilesFromFileSystem {

	public static void main(String[] args) {

		FileSystem fs = null;
		InputStream is = null;
		OutputStream os = null;
		// change path to right path
		String filePath = "C:/tmp_java/java/testfile.jpg";
		String filePathNew = "C:/tmp_java/java/testfile_new.jpg";
		String filePathPicture = "C:/tmp_java/java/Desert.jpg";

		String filePathText = "C:/tmp_java/java/testfiletext.txt";

		try {
			fs = FileSystem.getInstance();
		} catch (IOException ioe) {
			Log.error("No Filesystem found!");
		}

		if (fs != null) {
			try {
				// Check if dir is already available
				if (fs.fileExists(filePath)) {
					System.out.println("File already exists");

					// read file
					System.out.println("Read file");
					is = fs.readFile(filePath);

					// copy and write file
					os = fs.writeFile(filePathNew);
					byte[] b = new byte[500];
					while (is.read(b) != -1) {
						os.write(b);
					}
					is.close();
					os.close();

					// delete file
					System.out.println("Delete file");
					System.out.println("Could delete file: " + fs.deleteFile(filePath));

					// is file really deleted
					if (!fs.fileExists(filePath)) {
						System.out.println("File is now deleted.");
					} else {
						System.out.println("File always exist");
					}
				} else {
					System.out.println("No file exists");

					// copy and write file
					os = fs.writeFile(filePath);
					is = new FileInputStream(new File(filePathPicture));
					byte[] b = new byte[500];
					while (is.read(b) != -1) {
						os.write(b);
					}

					os.close();
					is.close();
				}

				if (fs.fileExists(filePathText)) {
					System.out.println("File already exists");

					// read file
					System.out.println("Read file");
					is = fs.readFile(filePathText);
					InputStreamReader isr = new InputStreamReader(is);
					// copy and write file
					char[] b = new char[20];
					char[] clear = new char[b.length];
					SimpleStringBuilder content = Toolkit.getInstance().createSimpleStringBuilder();
					while (isr.read(b) != -1) {
						content.append(b);
						b = clear;
					}
					isr.close();

					System.out.println("Content: " + content.toString());
				}
			} catch (Exception e) {}
		}
	}
}
