package org.ws4d.java;

import org.ws4d.java.communication.AutoBindingFactory;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.structures.CommunicationAutoBinding;
import org.ws4d.java.communication.structures.DiscoveryAutoBinding;
import org.ws4d.java.communication.structures.IPAutoInterfaceCommons;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;

/**
 */
public class DocuExampleAutobindingDevice extends DefaultDevice {

	public final static String	DOCU_NAMESPACE	= "http://ws4d.org/jmeds";

	/**
	 * Constructor of our device.
	 */
	public DocuExampleAutobindingDevice() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		// set PortType
		this.setPortTypes(new QNameSet(new QName("DocuExampleAutobindingDevice", DOCU_NAMESPACE)));

		// add device name (name is language specific)
		this.addFriendlyName("en-US", "DocuAutobindingDevice");

		// add device manufacturer (manufacturer is language specific)
		this.addManufacturer(LocalizedString.LANGUAGE_EN, "Test Inc.");
		this.addModelName(LocalizedString.LANGUAGE_EN, "DocuAutobindingModel");

		/*
		 * Add autobindings to a device.
		 */

		// First you have to get the communication manager, e.g. DPWS.
		CommunicationManager manager = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		// Then get the specific autobinding factory
		AutoBindingFactory abf = manager.getAutoBindingFactory();

		// Now you can create and set the autobinding(s):

		// discovery autobinding (use one of the discovery autobindings)
		// DiscoveryAutoBinding discoveryBinding =
		// abf.createDiscoveryMulticastAutobinding();
		DiscoveryAutoBinding discoveryBinding = abf.createDiscoveryMulticastAutobinding(new String[] { "eth3" }, new String[] { IPAutoInterfaceCommons.IPADDRESS_FAMILY_IPV4 }, false);

		// secure discovery autobinding
		// IMPORTANT: See the security tutorial to add a correct CredentialInfo
		// object to the secure binding.

		// DiscoveryAutoBinding discoveryBinding =
		// abf.createSecureDiscoveryMulticastAutobinding(new CredentialInfo(new
		// Object()));

		// DiscoveryAutoBinding discoveryBinding =
		// abf.createSecureDiscoveryMulticastAutobinding(new String[] { "eth3"
		// }, new String[] { IPAutoInterfaceCommons.IPADDRESS_FAMILY_IPV4 },
		// false, new CredentialInfo(new Object()));

		// communication autobinding (use one of the communication autobindings)

		// CommunicationAutoBinding communicationBinding =
		// abf.createCommunicationAutobinding("", 0);

		CommunicationAutoBinding communicationBinding = abf.createCommunicationAutobinding(new String[] { "eth3" }, new String[] { IPAutoInterfaceCommons.IPADDRESS_FAMILY_IPV4 }, false, false, "my/Autobinding/Device", 0);

		// secure communication autobinding
		// IMPORTANT: See the security tutorial to add a correct CredentialInfo
		// object to the secure binding.

		// CommunicationAutoBinding communicationBinding =
		// abf.createCommunicationSecureAutobinding("", 0, new
		// CredentialInfo(new Object()));

		// CommunicationAutoBinding communicationBinding =
		// abf.createCommunicationSecureAutobinding(new String[] { "eth3" }, new
		// String[] { IPAutoInterfaceCommons.IPADDRESS_FAMILY_IPV4 }, false,
		// false, null, 0, new CredentialInfo(new Object()));

		// Add the binding to the device.
		this.addBinding(discoveryBinding);
		this.addBinding(communicationBinding);
	}
}
