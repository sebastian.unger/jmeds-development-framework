package org.ws4d.java;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultService;

public class DocuExampleAutobindingService extends DefaultService {

	public DocuExampleAutobindingService() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		DocuExampleComplexOperation complexOp = new DocuExampleComplexOperation();
		addOperation(complexOp);
	}
}
