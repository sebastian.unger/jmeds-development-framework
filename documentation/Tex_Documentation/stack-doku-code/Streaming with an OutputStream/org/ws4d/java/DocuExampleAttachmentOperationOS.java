package org.ws4d.java;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import org.ws4d.java.attachment.AttachmentFactory;
import org.ws4d.java.attachment.interfaces.outgoing.OutgoingOutputStreamAttachment;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.constants.MIMEConstants;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.AttachmentValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class DocuExampleAttachmentOperationOS extends Operation {

	public final static char[]	ALPHABET	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabsdefghijklmnopqrstuvwxyz".toCharArray();

	public DocuExampleAttachmentOperationOS(String name, QName portType) {
		super(name, portType);
	}

	public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		ParameterValue result = createOutputValue();

		AttachmentFactory afac = AttachmentFactory.getInstance();

		if (afac != null) {
			// it's important to send the data as a stream
			OutgoingOutputStreamAttachment oosa = afac.createStreamAttachment(MIMEConstants.CONTENT_TYPE_APPLICATION_OCTET_STREAM);
			((AttachmentValue) result).setAttachment((oosa));
			new Thread(new myOutputStreamGenerator(oosa.getOutputStream())).start();
		} else {
			Log.error("No attachment support available.");
		}

		return result;
	}

	private class myOutputStreamGenerator implements Runnable {

		private OutputStream	os;

		private int				count		= 1;

		Random					generator	= new Random(42);

		public myOutputStreamGenerator(OutputStream os) {
			this.os = os;
		}

		/*
		 * Create a run() method which constantly generates random characters
		 * for demonstration purposes
		 */
		public void run() {
			while (true) {
				char randChar = ALPHABET[generator.nextInt(ALPHABET.length)];

				try {
					os.write(new byte[] { (byte) randChar });
				} catch (IOException e) {
					e.printStackTrace();
				}

				// print a log info and flush every tenth character
				if (count % 10 == 0) {
					Log.info(count++ + " characters " + "send");
					try {
						os.flush();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					count++;
				}

				// the sleep only serves live demonstration purposes
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
