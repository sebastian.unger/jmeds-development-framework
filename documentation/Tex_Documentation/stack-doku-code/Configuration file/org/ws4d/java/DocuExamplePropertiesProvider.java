package org.ws4d.java;

import java.io.IOException;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.service.DefaultService;

public class DocuExamplePropertiesProvider extends DefaultDevice {

	public DocuExamplePropertiesProvider(String comManId) {
		super(comManId);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// mandatory: Starting the DPWS Framework
		JMEDSFramework.start(new String[] { System.getProperty("user.dir") + "\\Configuration file\\org\\ws4d\\java\\docuExample.properties" });
		// CoreFramework.start(new String[] {
		// "D:\\Workspace\\core\\org\\ws4d\\java\\configuration\\example.properties"
		// });

		// using the 1st device-configId from the properties file
		DefaultDevice device = new DefaultDevice(1, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		// 1st service-configId defines the DefaultService
		DefaultService service = new DefaultService(1, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		// we have to add the service to our device
		device.addService(service);

		// NetworkInterface iface = IPNetworkDetection.getInstance().getNetworkInterface("eth3");
		// IPDiscoveryDomain domain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(iface, false);
		// device.addBinding(new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain));

		try {
			device.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
