\documentclass[11pt,twoside,a4paper]{article}

\usepackage{pslatex,palatino,avant,graphicx,color, cleveref, import, cite, abbrevs, url, tikz}
\usepackage{epstopdf}
\usepackage[margin=2cm]{geometry}

\begin{document}
\title{Device SOA abstraction provided by JMEDS}
\author{Materna GmbH,\\
		Dortmund, \\
		Germany \\
		\texttt{jannis.muething@materna.de}}
\date{\today}
\maketitle

\renewcommand{\abstractname}{Introduction}

\section{Introduction}
There is a great number of technologies available to have functionality exposed to users on a network. We will talk about the approach to have services offered by specific devices. This is different from the device independent approach to implement a service oriented architecture (SOA) \cite{SOA:Online} using for example Java's remote method invocation (RMI) \cite{RMI:2014:Online} or representational state transfer based (REST) webservices \cite{REST:2002:PAPER}. 

In service oriented architectures services are self-contained functional entities\cite{SOA:2007:PAPER}. Devices on the other hand are containers for services or other devices. In device SOAs devices announce their presence on the network and/or can be actively discovered. Both services and devices are enabled to let third parties know their capabilities and metadata information. For devices this may for example include hosted services and friendly names. A service's metadata can respectively include offered operations.

Some protocols enabling a device SOA approach are Universal Plug and Play (UPnP)\cite{UPNP:2008:Online}, Devices Profile for Web Services (DPWS) \cite{DPWS:2009:Online}, Bluetooth (BT) and the  building automation and control networks protocol (BACnet) \cite{BACNET:2014:Online}. These protocols are specialized to serve the needs of their respective domains but are very similar from a more abstract point of view.

This was the motivation behind the development of JMEDS beyond its single technology orientation to be a dynamic device SOA framework. This document will explain how the framework enables the development of devices and services independently of the underlying protocol.

First the most important concepts from the device/service perspective will be explained in \cref{device}. The same will be done from the client's perspective in the following \cref{client}. Please refer to the diagram in figure \ref{fig:jmeds} when reading these sections. Finally some of JMEDS' cross technology security capabilities will be explained in \cref{security}. 

\begin{figure}
\label{fig:jmeds}
\includegraphics[scale=.34]{jmeds_framework.eps}
\caption{ JMEDS framework structure }
\end{figure}

\section{Service / Device}
\label{device}
It does not come as a surprise to find devices and services represented in the internal structure of JMEDS. A device traditionally holds references to services. An exception to the traditional case is the UPnP protocol where it is possible to have devices host other devices. The first case is generally supported by the framework, the second only when using the UPnP module. The diagram in figure \ref{fig:jmeds} shows both containment variants.

Services contain operations. Operations - like methods in java or functions in C - have predefined input parameters and equally defined output parameters. A special kind of operation is an event (also called an "evented operation"). Events can send messages to subscribers, after those have subscribed themselves.

All those entities of course need to be discoverable on the network. Discoverability in this context means nothing more than announcing the presence of a device on a network or probing for a device on a network making use of multicast technologies. DPWS does this utilizing WS-Discovery (WS-DD), UPnP using the Simple Service Discovery Protocol (SSDP). JMEDS does not expose these protocols directly. Instead it only has to be provided with the necessary information about the interfaces to use during this process. A device in JMEDS can be provided with so called "Discovery Bindings" and "Outgoing Discovery Infos". The former specify an interface and an address (for example something like eth0 and IP/PORT in case it is a UDP binding) that the device is going to listen on to receive messages (i.e. probe messages or hello messages), the latter specify the interface to be used when sending discovery messages.

Both of the previously mentioned constructs come in two flavors. The concept will be explained with a focus on the bindings, but the "Outgoing Discovery Infos" work very similar. First there is the classic/static binding which has to be provided with everything (interface, address, port etc.) upfront. The second flavor has auto in its name and thus can do more on its own. The so called "auto bindings" require only interfaces to be supplied to them. Ports and addresses are chosen automatically. When one of those interfaces goes down or comes up, the "auto binding" takes care of the specifics of removing or (re-) adding the device to the corresponding network. In fact it is even possible to make an interface known to the binding that does not yet exist. It  will be used by the "auto binding" as soon as it becomes available.

As discovery bindings are needed for the discovery of devices, devices and services also need bindings to be reachable for metadata requests, operation invocations etc. The concept behind these bindings in JMEDS is very much analog to that behind the discovery bindings. As discovery bindings the so called "communication bindings" also come as static and automatic bindings. For example, if a devices gains reachability through one if its autobindings (an interface becomes available) JMEDS takes care of the logistics of changing the devices metadata and making this change public (in case of DPWS it, for example, sends a new hello message).

\section{Client}
\label{client}
The most important entity in JMEDS on the client side is the main client (the "DefaultClient"). It offers abilities to actively search for interesting devices and to be notified about new devices that appear on the network (i.e. when receiving a DPWS hello message). The client uses the "Discovery Bindings" and "Outgoing Discovery Info" concepts presented in the previous section to provide these abilities.
When a search in a network is successful or a new device appears on the network a device reference is provided by JMEDS. It is important to remark that a device reference does not contain any device metadata. This metadata is obtained only when an actual device (technically a device proxy) is requested from the device reference. This segregation between discovery and metadata exchange exists in many of the supported technologies such as DPWS and UPnP. Even if the segregation does not exist in the technology. For example, if there is only a limited number of profiles and those are all present on the client side - as it is the case in BT - the API does (of course) not change.

A device on its part can be asked for a service reference. One does not need supernatural abilities to guess that such a reference can be asked for an actual service. Again the further metadata exchange is triggered by the request for the service on the reference. 

The services can be asked to provide operation proxies and event source proxies. These can be invoked or in case of the event source can be subscribed to. To receive events an event sink has to be provided. The address of which is included in the subscription message. An event sink must be reachable for connections from the corresponding event source. It is a special kind of binding.

\section{Security}
\label{security}
To foster easier understanding of the basic structures the security support was omitted in the previous sections. JMEDS supports authentication, authorization and encryption. This section will focus on authentication and authorization to some extend.

It is possible for every device, service, operation and event source to be configured to take the user's credentials and way of communication into account when deciding to answer or to disallow the request (authorization).
In JMEDS the authentication information on both client and device/service side is stored in "credential info" objects. Those are, for example, supplied as parameters when invoking an operation on a service reference. They can contain user name/password combinations or digital certificates. 
Another concept used in JMEDS is the "security key". It encapsulates "credential infos" and "outgoing discovery information" objects. On the device/service side the latter are used to control the network interfaces that are to be used for discovery (e.g. hello messages, resolve messages, etc.). The former is used to optionally sign outgoing discovery messages and more importantly enable secure (SSL/TLS) connections between clients and devices, services (encrypted metadata exchange) and operations (encrypted operation invocations).

\bibliographystyle{acm}
\bibliography{main}

\end{document}
