package org.ws4d.java.dispatch;

import org.ws4d.java.communication.ConnectionInfo;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;

public interface ResponseHandler {

	public void handle(ProbeMatchesMessage probeMatches, ConnectionInfo connectionInfo);

	public void handle(ResolveMatchesMessage resolveMatches, ConnectionInfo connectionInfo);

	public Message getRequestMessage();

}
