/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/

package org.ws4d.java.platform.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.ws4d.java.io.buffered.BufferedInputStream;
import org.ws4d.java.platform.util.SimpleStringBuilderImpl;
import org.ws4d.java.util.SimpleStringBuilder;
import org.ws4d.java.util.Toolkit;

public class LocalToolkit extends Toolkit {

	public void printStackTrace(PrintStream err, Throwable t) {
		t.printStackTrace();
	}

	public String[] getStackTrace(Throwable t) {
		return null;
	}

	public void writeBufferToStream(ByteArrayOutputStream source, OutputStream target) throws IOException {
		target.write(source.toByteArray());
	}

	public InputStream buffer(InputStream stream) {
		return new BufferedInputStream(stream);
	}

	public SimpleStringBuilder createSimpleStringBuilder() {
		return new SimpleStringBuilderImpl();
	}

	public SimpleStringBuilder createSimpleStringBuilder(int capacity) {
		return new SimpleStringBuilderImpl(capacity);
	}

	public SimpleStringBuilder createSimpleStringBuilder(String str) {
		return new SimpleStringBuilderImpl(str);
	}
}
