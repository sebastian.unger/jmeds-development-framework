/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.platform.io.fs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.constants.FrameworkConstants;
import org.ws4d.java.io.fs.FileSystem;

public class LocalFileSystem extends FileSystem {

	public String escapeFileName(String rawFileName) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public InputStream readFile(String filePath) throws IOException {
		return getClass().getResourceAsStream(filePath);
	}

	public OutputStream writeFile(String filePath) throws IOException {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public boolean deleteFile(String filePath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public boolean deleteDirectory(String directoryPath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public boolean renameFile(String filePath, String newFilePath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public String[] listFiles(String dirPath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public long fileSize(String filePath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public String fileSeparator() {
		return System.getProperty("file.separator");
	}

	public boolean fileExists(String filePath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public long lastModified(String filePath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public String getBaseDir() {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public String getFileSystemType() {
		return FrameworkConstants.JAVA_VERSION_CLDC;
	}

	protected InputStream readFileInternal(String filePath) throws IOException {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	protected InputStream readJarInternal(String jarName, String pathInJar) throws IOException {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	protected OutputStream writeFileInternal(String filePath) throws IOException {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

	public String getAbsoluteFilePath(String filePath) {
		throw new RuntimeException("The CLDC platform does not support this filesystem operation.");
	}

}
